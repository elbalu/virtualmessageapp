# Virtual Messaging on  UI
Loc UI is the desktop web client for the SDK on cloud product. Loc UI is a portal for users to detect and locate clients. It is designed using React with Redux architecture and LESS styling and Gulp with Webpack for build tasks. The goal of this project is to expose all of the major SDK on cloud use cases, to provide a clean and intuitive user experience, and to act as a test bed for front end innovation within the CMX team and the larger Cisco environment.

## Authors

**[Balu Epalapalli Loganathan](mailto:baepalap@cisco.com)**

## Documentation

**[UI Design Doc](https://wiki.cmxcisco.com/display/MSECMX/User+Interface+Design)**

## Environments
LOC UI can be built or started with different configurations, which we refer to as environments (to mirror the language used in backend development). The main value added by supporting multiple environments is the ability to specify different backend urls for different environments. When running any of the below commands, ```<env>``` should be one of: **mock, development, sandbox, test, demo,unity**. To support a new environment, simply add a configuration JSON file to ```src/js/config/env``` and then run the command with the name of the new JSON file as the environment name. The UI will load in the JSON file at build-time when the source code is bundled.

## Installation
LOC UI is hosted on CMX Gitlab and can be cloned by anyone who has access to the project.

To clone:
```bash
git clone git@gitlab.cmxcisco.com:cmx-location-cloud/ui.git
```

## Build
LOC UI must be built before running because it is developed using React JSX and ES6 syntax. Most browsers currently only support ES5, so LOC UI uses babel to transpilesource code down to ES5. It also uses webpack to bundle all source code into a single file and optionally minify/uglify.

To build:
```bash
npm run build [-- [--env <env>] [--dest <dest>]]
```
- `<env>` should be one of: *mock, development, sandbox, test, production*. Defaults to *development*.
- `<dest` is the destination directory where build files are places. Defaults to */dist*

The build process produces six artifacts, which are listed below. All six artifacts should be served from the same directory with index.html as the point of entry.
1. `index.html` is the main html file that provides HTML DOM and loads other javascript and css files
2. `build.am.js` is JavaScript code that has been transpiled. minified, and bundled
3. `build.am.css` is LESS code that has been compiled into CSS and bundled
4. `vendor.am.js` is third party code that has been transpiled, minified, and bundled
5. `favicon.ico` is the website favicon
6. `fonts/` is a directory where font and icon resources are kept.

**NOTE** There is a directory called *./dist-cloud* that symlinks to *./dist* for backwards compatibility. *./dist-cloud* should not be given as the destination for any build command.

## Development Server
LOC UI is served by an AWS S3 server as part of the full SDK on cloud product, but the UI can also be run locally using a node server. This server watches for file changes and reloads the site which makes development very quick and convenient.

To start:
```bash
npm start -- --env <env>
```
- ```<env>``` should be one of: *mock, development, sandbox, test, production*. Defaults to *development*.

# Project structure

```text
├── README.md
├── dist-cloud          // example build destination (see Build above)
│   ├── build.am.css    
│   ├── build.am.js
│   ├── favicon.ico
│   ├── fonts/
│   ├── index.html
│   └── vendor.am.js
├── gulpfile.babel.js   // gulpfile where build tasks are defined
├── logs/               // log files
|-- node_modules/       // node modules
├── package.json        // main package.json that describes project dependendencies and configuration
├── plugins/            // third party plugins that are not node modules
├── public/             // HTML and .ico files. These are copied into dest directories at build time
├── scripts/            // infrastructure and build scripts
├── src                 // source code
│   ├── css/            // third party css files that are not available as part of a node module
│   ├── js              // javascript source code, written using React JSX and ES6
│   │   ├── components  // React components
│   │   ├── config      // configuration files
│   │   ├── containers  // Redux containers
│   │   ├── index.js    // JavaScript entry point
│   │   ├── misc        // miscellaneous helpers and utility functions/classes
│   │   └── redux       // Redux action creators, constants, reducers, and store
│   ├── less/           // LESS style files
│   └── resources/      // static data used for configuration
├── test/               // functional and unit tests
└── webpack.config.js   // webpack configuration (used by gulp during build process)

```
