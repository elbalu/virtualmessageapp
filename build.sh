#!/usr/bin/env bash
#
# Script called by Jenkins to build a UI tar ball for AWS

# check the command line
if [ $# != 2 ]; then
  echo usage: $0 ENV BUILD
  exit 1
fi

# unload the command line
env=$1
build=$2

# make sure we are in the sdk-ui home directory
cd `dirname $0`

# build the tar ball
rm -rf sdk-ui_$build.tgz dist
npm run build:$env
echo -e "{\"build\":$build}" > dist/version.json
tar cvzf sdk-ui_$build.tgz dist

# copy tar ball to AWS S3 repo
aws s3 cp sdk-ui_$build.tgz s3://cmx-sdk-repo/ui/$env/
