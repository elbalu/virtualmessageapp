/* eslint-disable no-console */
import gulp from 'gulp';
import gwp from 'gulp-webpack';
import gls from 'gulp-live-server';
import gutil from 'gulp-util';
import env from 'gulp-env';
import mocha from 'gulp-mocha';
import del from 'del';
import template from 'gulp-template';
import notify from 'gulp-notify';
import packageConfig from './package.json';


// versioned filenames
// generate filenames with version number
const version = packageConfig.version;
const timestamp = Date.now().toString();
const mainFilename = `sdk-app-v${version}-${timestamp}.js`;
const vendorFilename = `sdk-vendor-v${version}-${timestamp}.js`;
const styleFilename = `sdk-style-${version}-${timestamp}.css`;

// set env vars based on command line args
env({
  vars: {
    NODE_ENV: gutil.env.env || process.env.NODE_ENV || 'mock',
    LOCALE: gutil.env.locale || process.env.LOCALE || 'en',
    DIST_DIR: gutil.env.dest || process.env.DIST_DIR || 'dist',
    SDK_WATCH: gutil.env.watch || false,
    SDK_MAIN_FILENAME: mainFilename,
    SDK_VENDOR_FILENAME: vendorFilename,
    SDK_STYLE_FILENAME: styleFilename,
    SDK_VERSION: packageConfig.version
  }
});

// webpackConfig must be imported after env()
// because it depends on env vars
const webpackConfig = require('./webpack.config');

// command line and env vars
const dest = process.env.DIST_DIR;
const testGlob = gutil.env.test || './test/unit/**/*.test.js';

/**
 *
 */
const printConfig = () => {
  // spit out env vars for reference
  console.log('-----configuration-------');
  console.log('dest:', process.env.DIST_DIR);
  console.log('version:', process.env.SDK_VERSION);
  console.log('env:', process.env.NODE_ENV);
  console.log('locale:', process.env.LOCALE);
  console.log('-------------------------');
};

/**
 * deletes all files in the dist
 * directory
 */
gulp.task('clean', () => {
  return del(['dist/*']);
});

/**
 * bundle and compile src using webpack. If the
 * environment is a livereloadEnvironment, enable
 * the watch flag in the webpack config
 */
gulp.task('build-js', () => {
  printConfig();
  return gulp.src('./src/js/index.js')
    .pipe(gwp(webpackConfig))
    .pipe(gulp.dest(dest))
    .pipe(notify({ message: 'Scripts task complete' }));
});

/**
 * copies index.html and replaces template
 * variables with javascript and css filenames
 */
gulp.task('html', () => {
  gulp.src(['public/index.html'])
    .pipe(template({
      main: mainFilename,
      vendor: vendorFilename,
      style: styleFilename
    }))
    .pipe(gulp.dest(dest));
});

/**
 * copies files from the public folder,
 * excluding index.html because it requires
 * templating. Also includes fontawesome fonts
 */
gulp.task('public', () => {
  gulp.src([
    'public/**/*',
    '!public/index.html'
  ])
    .pipe(gulp.dest(dest));
});

/**
 * starts a server and watches source
 * files for changes. Any time dist/ files
 * change, the server reloads the browser page
 */
 gulp.task('live-reload', () => {
   const server = gls.static(dest, 5000);
   gulp.watch([`${dest}/**.js`], (file) => {
     server.notify.apply(server, [file]);
   });
   server.start();
 });
// gulp.task('server', () => {
//   //  1. serve with default settings
//   const server = gls.static(dest, 5000);
//
//   //  use gulp.watch to trigger server actions(notify, start or stop)
//   gulp.watch(['dist/', 'dist/*'], (file) => {
//     server.notify.apply(server, [file]);
//   });
//   server.start();
// });

// gulp.task('live-reload', () => {
//     const server = gls.static(dest, 5000);
//     server.start();
//     gulp.watch([`${dest}/**.js`], (file) => {
//       console.log('file', file)
//         server.notify.apply(server, [file]);
//     });
// });

/**
 * runs unit tests
 */
gulp.task('test', () => {
  return gulp.src(testGlob)
    .pipe(mocha({
      reporter: 'spec',
      require: ['babel-core/register', 'test/utils/setup.js']
    }));
});

gulp.task('build', ['build-js', 'public', 'html']);
gulp.task('build-watch', ['build-js', 'public', 'html', 'live-reload']);
