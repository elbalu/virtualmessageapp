function sendResult(res, promise) {
  promise.then(function(result) {
    res.send(result);
  }).catch(function(err) {
    res.send({
      error: err.message
    });
  });
}

module.exports = {
  sendResult: sendResult
};
