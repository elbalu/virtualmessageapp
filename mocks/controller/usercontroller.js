const UserDataService = require('../services/userdataservice');

class UserController {

  static addUser(req, res) {
    const userService = new UserDataService();
    userService.addUser()
      .then((result) => {
        res.status(201).send(result);
      })
      .catch((error) => {
        console.log('Error while adding User ', error);
        const message = {
          message: 'Error Error while adding User .'
        };
        res.status(400).send(message);
      });
  }

  static updateUser(req, res) {
    const userService = new UserDataService();
    userService.updateUser()
      .then((result) => {
        res.status(201).send(result);
      })
      .catch((error) => {
        console.log('Error while updating User ', error);
        const message = {
          message: 'Error Error while updating User .'
        };
        res.status(400).send(message);
      });
  }

  static getUser(req, res) {
    const userId = req.params.id;
    const userService = new UserDataService();
    userService.getUser(userId)
      .then((result) => {
        res.status(201).send(result);
      })
      .catch((error) => {
        console.log('Error while getting User ', error);
        const message = {
          message: 'Error Error while getting User .'
        };
        res.status(400).send(message);
      });
  }
}

module.exports = UserController;
