const ZoneDataService = require('../services/zonedataservice');

class ZoneController {

  static addZone(req, res) {
    const zoneService = new ZoneDataService();
    zoneService.addZone()
      .then((result) => {
        res.status(201).send(result);
      })
      .catch((error) => {
        console.log('Error while adding ZOne ', error);
        const message = {
          message: 'Error Error while adding Zone .'
        };
        res.status(400).send(message);
      });
  }

  static updateZone(req, res) {
    const zoneService = new ZoneDataService();
    zoneService.updateZone()
      .than((result) => {
        res.status(200).send(result);
      })
      .catch((error) => {
        console.log('Error while adding ZOne ', error);
        const message = {
          message: 'Error Error while adding Zone .'
        };
        res.status(400).send(message);
      });
  }

  static getZoneById(req, res) {
    const zoneService = new ZoneDataService();
    const zoneId = req.params.id;
    zoneService.getZone(zoneId)
      .then((result) => {
        res.status(200).send(result);
      })
      .catch((error) => {
        console.log('Error while getting ZOne ', error);
        const message = {
          message: 'Error Error while getting Zone .'
        };
        res.status(400).send(message);
      });
  }
}

module.exports = ZoneController;
