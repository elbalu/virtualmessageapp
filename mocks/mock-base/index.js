'use strict';
var fs = require('fs'),
    join = require('path').join;

var Promise = require('bluebird'),
    _ = require('underscore'),
    $ = require('lodash');

var MockBase = (function() {
  function readCollection(collectionName) {
    return new Promise(function(resolve, reject) {
      fs.readFile(
        join(__dirname, 'collections', collectionName + '.json'),
        { encoding: 'utf8' },
        function(err, data) {
          if (err) return resolve([]);
          resolve(JSON.parse(data));
        }
      )
    });
  }

  function searchCollection(collectionName, query) {
    return new Promise(function(resolve, reject) {
      fs.readFile(
        join(__dirname, 'collections', collectionName + '.json'),
        { encoding: 'utf8' },
        function(err, data) {
          if (err) return resolve([]);
          let allData = JSON.parse(data);
          let filteredData = allData;
          if(query){
            filteredData = filterByQueryString(allData, query);
          }
          resolve(filteredData);
        }
      )
    });
  }

  function filterCollectionByKeyParam(collectionName, query) {
    return new Promise(function(resolve, reject) {
      fs.readFile(
        join(__dirname, 'collections', collectionName + '.json'),
        { encoding: 'utf8' },
        function(err, data) {
          var filteredData = [];
          if (err) return resolve([]);
          let allData = JSON.parse(data);
          if(query){
            filteredData = _.filter(allData,function(item){
                return item[query['key']].id === query.id
              })
          }
          resolve(filteredData);
        }
      )
    });
  }


    function traverseObj(obj){
      let arr = [];
      function iterate(obj, stack) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if (typeof obj[key] == "object") {
                        iterate(obj[key], stack);
                    } else {
                        arr.push(obj[key]);
                    }
                }
            }
            return arr;
        }

      return iterate(obj);
    }

  function filterByQueryString(allData, query){
    let newList = [];
    $.forOwn(allData, function(item) {
      var strArr = traverseObj(item);
      var matches = _.filter(strArr,function( s ) {
        if(typeof s === "string"){
         return (s.toLowerCase()).indexOf( query.toLowerCase() ) !== -1;
        }
        return false;
       });
       if(matches.length > 0){
         newList .push(item);
       }
    });
    return newList;
  }


  function writeCollection(collectionName, data) {
    return new Promise(function(resolve, reject) {
      fs.writeFile(
        join(__dirname, 'collections', collectionName + '.json'),
        JSON.stringify(data),
        function(err) {
          if (err) return reject(err);
          resolve({ success: true, data: data });
        }
      )
    });
  }

  function findLatestId(array) {
    if (array.length > 0)
      return array.map(function(item) { return item.id; })
        .reduce(function(prev, next) {
          if (prev > next) return prev;
          return next;
        });
    return 0;
  }

  function _MockBase(collectionName) {
    this.collectionName = collectionName;
  }

  function findInterface(method, query) {
    return readCollection(this.collectionName)
    .then(function(data) {
      if (!query) return data;
      return _[method](data, query);
    })
    .catch(function(err) {
      return Promise.resolve([]);
    });
  }

  function searchInterface(method, query) {
    return searchCollection(this.collectionName, query)
    .then(function(data) {
      if (!query) return data;
      return _[method](data, query);
    })
    .catch(function(err) {
      return Promise.resolve([]);
    });
  }

  function keyParamFilterInterface(method, query) {
    return filterCollectionByKeyParam(this.collectionName, query)
    .then(function(data) {
      return data;
    })
    .catch(function(err) {
      return Promise.resolve([]);
    });
  }

  _MockBase.prototype.read = function() {
    return readCollection(this.collectionName)
    .catch(function(error) {
      return Promise.resolve({err: error});
    });
  };

  _MockBase.prototype.write = function(data) {
    return writeCollection(this.collectionName, data)
      .catch(function(error) { return Promise.resolve({success: false, error: error}); });
  }

  _MockBase.prototype.find = _.partial(findInterface, 'where');
  _MockBase.prototype.findOne = _.partial(findInterface, 'findWhere');
  _MockBase.prototype.search = _.partial(searchInterface, 'where');
  _MockBase.prototype.filterByKeyParam = _.partial(keyParamFilterInterface, 'where');

  _MockBase.prototype.insert = function(newData) {
    return readCollection(this.collectionName)
    .then(function(data) {
      var id = findLatestId(data) + 1;
      if (newData instanceof Array) {
        newData = newData.map(function(item, index) {
          return _.assign(item, {
            id: id + index,
            updatedAt: Date.now()
          });
        });
      } else {
        Object.assign({}, newData, {
          id,
          updatedAt: Date.now()
        })
      }
      if (this.collectionName === 'poi') {
        const uData = {
          type: "FeatureCollection",
          features: data.features.concat(newData)
        }
        return writeCollection(this.collectionName, uData);
      } else if (this.collectionName === 'path') {
        console.log('data', data);
        console.log('newData', newData)
        return writeCollection(this.collectionName, newData);
      } else {
        return writeCollection(this.collectionName, data.concat(newData));
      }
    }.bind(this))
    .then(function() {
      return newData;
    })
    .catch(function(err) {
      return Promise.resolve({ success: false, error: err });
    });
  };

  _MockBase.prototype.insertWithId = function(newData, Id) {
    return readCollection(this.collectionName)
    .then(function(data) {
      if (newData instanceof Array) {
        newData = newData.map(function(item, index) {
          return _.assign(item, {
            id: Id,
            updatedAt: Date.now()
          });
        });
      } else {
        _.assign(newData, {
          id: Id,
          updatedAt: Date.now()
        })
      }
      return writeCollection(this.collectionName, data.concat(newData));
    }.bind(this))
    .then(function() {
      return newData;
    })
    .catch(function(err) {
      return Promise.resolve({ success: false, error: err });
    });
  };


  function removeByProp(prop, array) {
    return readCollection(this.collectionName)
    .then(function(data) {
      var props = _.pluck(array, prop);

      return writeCollection(
        this.collectionName,
        data.filter(function(item) {
          return !_.some(
            props,
            function(p) { return p === item[prop]; }
          );
        })
      );
    }.bind(this))
  }

  function removeByPropList(prop, list) {
      return readCollection(this.collectionName)
      .then(function(data) {
        var newData = data.filter(function(item) {
          return !_.some(list, function(sItem) {
            return item[prop] === sItem;
          });
        });

        return writeCollection(
          this.collectionName,
          newData
        );
      }.bind(this))
  }

  _MockBase.prototype.removeByPropList = removeByPropList;
  _MockBase.prototype.removeByProp = removeByProp;
  _MockBase.prototype.removeByIdList = _.partial(removeByPropList, 'id');
  _MockBase.prototype.removeById = _.partial(removeByProp, 'id');

  _MockBase.prototype.remove = function(query, multi) {
    var updateData;
    if (!query) return Promise.reject(new Error('Query required'));
    return readCollection(this.collectionName)
    .then(function(data) {
      if (this.collectionName === 'poi') {
        updateData = data.features.filter(p => p.properties.id !== query.id);
        delete updateData.id;
        const uData = {
          type: "FeatureCollection",
          features: updateData
        }
        return writeCollection(this.collectionName, uData);
      } else {
        var matched = [].concat(
          _[multi ? 'findWhere' : 'where'](data, query)
        );

        return writeCollection(
          this.collectionName,
          _.filter(data, function(item) {
            return !_.some(matched, function(fitem) {
              return item.id === fitem.id;
            });
          })
        );
      }

    }.bind(this))
  };

  _MockBase.prototype.update = function(query, update, multi) {
    var updateData;
    return readCollection(this.collectionName)
    .then(function(data) {
      if (this.collectionName === 'poi') {
        updateData = data.features.map((p) => {
          if (p.properties.id === query.id) return update;
          return p;
        });
        delete updateData.id;
        const uData = {
          type: "FeatureCollection",
          features: updateData
        }
        return writeCollection(this.collectionName, uData);
      } else {
        if (data.length === 0)
          return Promise.resolve({ success: false, data: data });
        updateData = _.map([].concat(
          _[multi ? 'where' : 'findWhere'](data, query)
        ), function(item) {
          delete update.id;
          return _.assign(item, update, {
            updatedAt: Date.now()
          });
        });
        return writeCollection(
          this.collectionName,
          _.map(data, function(ditem, index) {
            var initem = _.find(updateData, function(uitem) { return uitem.id === ditem.id; });
            if (initem)
              return _.assign(ditem, initem);
            return ditem;
          })
        );
      }



    }.bind(this))
    .then(function() {
      if (updateData.length === 1)
        return updateData[0];
      return updateData;
    });
  };

  _MockBase.prototype.updateByIdList = function(list, update) {
    return readCollection(this.collectionName)
    .then(function(data) {
      return writeCollection(
        this.collectionName,
        data.map(function(item) {
          if (_.some(list, function(id) { return item.id == id;})) {
            item = _.extend({}, item, update);
          }
          return item;
        })
      );
    }.bind(this));
  }

  return _MockBase;
})();

module.exports = MockBase;
