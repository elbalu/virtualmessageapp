const Model = require('./model');
const schema   = require('./schemas/message');
const validate = require('jsonschema').validate;

const _collection = 'messages';

class Message extends Model {
  static async get(params){
    let retval = await super.get(_collection, params, Message);
    return retval;
  }

  constructor(attributes){
    super(_collection, attributes, { timestamp : 1, sender : 1, recipient : 1 });
    this._selector = {
      sent      : false,
      sender    : this._attributes.sender,
      recipient : this._attributes.recipient
    }
  }

  async markAsSent() {
    this._attributes[0].sent = true;
    this.save();
    return '';
  }
}

module.exports = Message;

/*
Message = require('./message');

m = new Message({
  timestamp : new Date(),
  body      : 'You are late',
  sender    : {
    userClass   : 'professor',
    firstName   : 'john',
    lastName    : 'chambers',
    phoneNumber : '1234567890',
    macAddress  : '11:22:33:44:55:66'
  },
  recipient : {
    userClass   : 'professor',
    firstName   : 'chuck',
    lastName    : 'robbins',
    phoneNumber : '1234567890',
    macAddress  : '11:22:33:44:55:66'
  },

})
*/
