// TODO
const mongodb = require('mongodb');
const object  = require('lodash/object');
const cfg     = require('../config').mongodb;
//mongodb://<dbuser>:<dbpassword>@ds063899.mlab.com:63899/enghack-2018
const client  = mongodb.MongoClient;
const mdburl  = `mongodb://${cfg.user}:${cfg.pass}@${cfg.url}:${cfg.port}/${cfg.db}`;
console.log(mdburl);

// base class for model
class Model {
  static _doQuery(collection, params, action){
    let that = this;
    return new Promise((resolve, reject) => {
      client.connect(mdburl, (err, cli) => {
        if (err){
          reject(err);
        } else {
          console.log(that);
          let coll = cli.db(cfg.db).collection(collection);
          action(coll)
          .then(data => {
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
        }
      });
    });
  }

  static _createIndices(collection, indices){
    try {
      let indexCreated = this._doQuery(collection, indices, (col) => {
        return new Promise((resolve, reject) => {
          col.createIndex(indices, {unique : true}, (err, res) => {
            if (err){
              reject(err);
            } else {
              resolve(res);
            }
          });
        });
      });
      return indexCreated;
    } catch (err){
      throw(err);
    }
  }

  static async get(collection, params, klass){
    try {
      let result = await Model._doQuery(collection, params, (col) => {
        return new Promise((resolve, reject) => {
          col.find(params).toArray((err, items) => {
            if (err){
              reject(err);
            } else {
              resolve(items.map((item) => {
                return new klass(items);
              }));
            }
          });
        });
      });
      return result;
    } catch (err){
      throw err;
    }
  }

  constructor(collection, attributes, indices){
    // attributes will be an object according to the schema we define
    this.client = client;
    this._collection = collection;
    this._attributes = attributes;
    this._selector   = {};
    if (indices){
      Model._createIndices(this._collection, indices)
      .catch(err => {
        throw err;
      });
    }
  }

  async save(upsert=true){
    try {
      let that = this;
      let result = await Model._doQuery(this._collection, {}, (col) => {
        return new Promise((resolve, reject) => {
          col.update(that._selector, that._attributes, { upsert : upsert }, (err, result) => {
            if (err){
              reject(err);
            } else {
              resolve(result);
            }
          })
        });
      });
    } catch (err){
      throw err;
    }
  }

  async delete(){
    try {
      let that = this;
      let deleteResult = await Model._doQuery(this._collection, this._attributes, (err, result) => {
        return new Promise((resolve, reject) => {
          col.delete(that._attributes, (err, result) => {
            if (err){
              reject(err);
            } else {
              resolve(result);
            }
          });
        });
      });
      return deleteResult;
    } catch (err){
      throw err;
    }
  }
}

module.exports = Model;
