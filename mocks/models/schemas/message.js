// TODO
module.exports = {
    timestamp : { type   : 'Date' },
    sent      : { type   : 'boolean'},
    body      : { type   : 'string' },
    sender    : { '$ref' : '../user' },
    recipient : { '$ref' : '../user' },
    zone      : { '$ref' : '../zone' }
};
