module.exports = {
  id         : 'string',
  type       : 'string',
  properties : {
    zoneName : { type  : 'string' },
    zoneType : { type  : 'string' },
    zoneUser : {'$ref' : '../User'},
    details  : { type  : 'json' }
  }
};
