// TODO
const Model    = require('./model');
const schema   = require('./schemas/user');
const validate = require('jsonschema').validate;

const _collection = 'users';

class User extends Model {
  static async get(params){
    let retval = await super.get(_collection, params, User);
    return retval;
  }

  constructor(attributes){
    super(_collection, attributes, { firstName : 1, lastName : 1 });
    this._selector = {
      firstName : this._attributes.firstName,
      lastName  : this._attributes.lastName
    }
  }
}

module.exports = User;

/*
u = new User({
  userClass   : 'professor',
  firstName   : 'john',
  lastName    : 'chambers',
  phoneNumber : '1234567890',
  macAddress  : '11:22:33:44:55:66'
})
*/
