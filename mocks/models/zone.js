// TODO
const Model = require('./model');
const schema   = require('./schemas/zone');
const validate = require('jsonschema').validate;

const _collection = 'zones';

class Zone extends Model {
  static async get(params){
    let retval = await super.get(_collection, params, Zone);
    return retval;
  }

  constructor(attributes){
    super(_collection, attributes, { id : 1 });
    this._selector = {
      id : this._attributes.id
    }
  }
}

module.exports = Zone;
