const Router = require('express').Router();

const DB = require('../mock-base');
const uuid = require('uuid');
const c = require('../common/route-helpers');

Router

    .post('/', (req, res) => {
      c.sendResult(
          res,
          new DB('alerts').find()
      );
    })

    .get('/locations', (req, res) => {
      c.sendResult(
          res,
          new DB('alertsbylocation').read().then(function(data) {
            return data.location;
          })
      );
    })

    .get('/departments', (req, res) => {
      c.sendResult(
          res,
          new DB('alertsbylocation').read().then(function(data) {
            return data.department;
          })
      );
    })

    .get('/categories', (req, res) => {
      c.sendResult(
          res,
          new DB('alertsbylocation').read().then(function(data) {
            return data.category;
          })
      );
    })

    .get('/rules', (req, res) => {
      c.sendResult(
          res,
          new DB('alertsbylocation').read().then(function(data) {
            return data.rule;
          })
      );
    })

    .get('/priority', (req, res) => {
      c.sendResult(
          res,
          new DB('alertsbylocation').read().then(function(data) {
            return data.priority;
          })
      );
    })
    .get('/:userId', (req, res) => {
      c.sendResult(
          res,
          new DB('alerts').find()
      );
    })
    .delete('/:id', (req, res) => {
      c.sendResult(
        res,
        new DB('alerts').remove({ id: parseFloat(req.params.id) })
      );
    })

    .get('/comments/:alertId', (req, res) => {
      // const data = new DB('alerts').findOne({ id: parseInt(req.params.alertId, 10) });
      // c.sendResult(
      //   res,
      //   data.comments
      // );
      c.sendResult(
          res,
          new DB('alert-comments').find()
      );
    })
    // updates one alert
    .put('/:id', (req, res) => {
      c.sendResult(
        res,
        new DB('alert-comments').update({ id: parseFloat(req.params.id) }, req.body)
      );
    })

    .post('/comments', (req, res) => {
      new DB('alert-comments').insert(req.body.comment).then(() => {
        res.send(req.body);
      });
    })

    // updates one or more alerts
    .put('/', (req, res) => {
      const alerts = req.body;
      const updatedData = [];

      const promises = alerts.map(alert => new Promise((resolve) => {
        const id = alert.id;
        new DB('alerts').update({ id: parseFloat(id) }, alert).then(() => {
          updatedData.push(Object.assign({}, alert, { id }));
          resolve(true);
        });
      }));

      Promise.all(promises)
      .then(() => {
        res.send(updatedData);
      })
      .catch(console.error);
    })


    .get('/widgets/:userID', (req, res) => {
      c.sendResult(
          res,
          new DB('alertsWidgets').find()
      );
    })
    .put('/widgets/:id', (req, res) => {
      c.sendResult(
        res,
        new DB('alertsWidgets').update({ id: parseFloat(req.params.id) }, req.body)
      );
    })

    .post('/addWidget', (req, res) => {
      const newItem = req.body;
      const id = req.body.id;
      newItem.saved = true;
      // update allWidgets
      new DB('alertsWidgets').update({ id }, newItem)
    })
    .post('/forward', (req, res) => {
      // actual backend to send email with alerts
      res.send(req.body);
    });

module.exports = Router;
