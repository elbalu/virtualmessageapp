const Router = require('express').Router();

const DB = require('../mock-base'),
  c = require('../common/route-helpers');

Router
  .post('/token/controller/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('applications').find()
    );
  })
  .post('/token/controller', function(req, res) {
    c.sendResult(
      res,
      new DB('applications').find()
    );
  })
  .post('/token/connector/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('applications').find()
    );
  })
  .post('/token', function(req, res) {
    c.sendResult(
      res,
      new DB('applications').find()
    );
  })
module.exports = Router;
