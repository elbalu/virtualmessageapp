/**
 * areas mock route
 */
const Router = require('express').Router();

const DB = require('../mock-base');
const _ = require('lodash');
const c = require('../common/route-helpers');
const fs = require('fs')
const path = require('path');

Router
  .get('/basehierarchy', function(req, res) {
    c.sendResult(
      res,
      new DB('areas').find()
    );
  })
  .get('/features', function(req, res) {
    c.sendResult(
      res,
      new DB('floor-features').read()
    );
  })
  .get('/level/FLOOR', function(req, res) {
    c.sendResult(
      res,
      new DB('floors').read()
    );
  })
  .get('/features/:assetId', function(req, res) {
    c.sendResult(
      res,
      new DB('floor-features-asset').read()
    );
  })
  .get('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('floor').read()
    );
  })
  .post('/import', function(req, res) {
    c.sendResult(
      res,
      new DB('floorImport').read()
    );
  })
  .put('/floor/:id', function(req, res) {
    const id = parseFloat(req.params.id);
    const features = req.body;

    c.sendResult(
      res,
      new DB('floor-features').read().then(function(data) {
        return new DB('floor-features').write(
          data.map(function(di) {
            if (di.floor_id === id) {
              return _.extend({}, di, features);
            }
            return di;
          })
        );
      })
    );
  });

/**
 * GET retrieve a single floormap
 */
Router.get('/images/floor/:filename', (req, res) => {
  const imagePath = path.join(__dirname, '../../dist/floormaps/111.png');
  const floormap = fs.readFileSync(imagePath);
  c.sendResult(res, Promise.resolve(floormap));
});

module.exports = Router;
