const Router = require('express').Router();

const DB = require('../mock-base'),
  uuid = require('uuid'),
  c = require('../common/route-helpers');

Router

  .get('/', function(req, res) {
    c.sendResult(
      res,
      new DB('categories').find()
    );
  })
  .get('/icons', function(req, res) {
    c.sendResult(
      res,
      new DB('category-icons').find()
    );
  })
  .post('/', function(req, res) {
    var newItem = req.body;
    newItem.id = uuid.v1();

    new DB('categories').insert(newItem).then(function() {
      res.send(newItem);
    });
  })
  .put('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('categories').update({id: parseFloat(req.params.id)}, req.body)
    );
  })
  .delete('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('categories').remove({id: parseFloat(req.params.id)})
    );
  });

module.exports = Router;
