const Router = require('express').Router();

const DB = require('../mock-base');
const c = require('../common/route-helpers');

Router
  .get('/count/all', (req, res) => c.sendResult(
      res,
      new DB('count-all').find()
  ))
  .get('/count/campus/:id', (req, res) => c.sendResult(
      res,
      new DB('count-all').find()
  ))
  .get('/count/building/:id', (req, res) => c.sendResult(
      res,
      new DB('count-all').find()
  ))
  .get('/count/floor/:id', (req, res) => c.sendResult(
      res,
      new DB('count-all').find()
  ))
  .get('/count/building/:id', (req, res) => c.sendResult(
      res,
      new DB('count-all').find()
  ))
  .get('/count/', (req, res) => {
    const { query } = req;
    const { groupBy = '' } = query;
    if (groupBy === 'ssid') {
      return c.sendResult(
        res,
        new DB('count-all').find());
    }
    return {};
  })
  .get('/', (req, res) => {
    const { query } = req;
    const { deviceType = '', floorId = '' } = query;
    if (floorId.length > 0 && deviceType === 'CLIENT') {
      return c.sendResult(
        res,
        new DB('clients-geo').find());
    }
    c.sendResult(
      res,
      new DB('clients').find());
  });


module.exports = Router;
