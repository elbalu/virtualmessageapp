var Router = require('express').Router();

var DB = require('../mock-base'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

Router

  .get('/', function(req, res) {
    c.sendResult(
      res,
      new DB('cmx').find()
    );
  })
  .post('/', function(req, res) {
    var newItem = req.body;
    newItem.id = uuid.v1();

    new DB('cmx').insert(newItem).then(function() {
      res.send(newItem);
    });
  })
  .put('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('cmx').update({id: parseFloat(req.params.id)}, req.body)
    );
  })
  .delete('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('cmx').remove({id: parseFloat(req.params.id)})
    );
  });


module.exports = Router;
