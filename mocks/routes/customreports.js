var Router = require('express').Router();

var DB = require('../mock-base'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

Router

    .get('/', function(req, res) {
      c.sendResult(
          res,
          new DB('customreports').find()
      );
    });


module.exports = Router;
