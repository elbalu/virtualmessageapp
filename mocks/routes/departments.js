const router = require('express').Router();

const Db = require('../mock-base');
const uuid = require('uuid');
const routeHelpers = require('../common/route-helpers');

const tableName = 'departments';

/**
 * GET /departments
 * get all departments
 */
router.get('/', (req, res) => {
  routeHelpers.sendResult(res, new Db(tableName).find());
});

/**
 * GET /departments/:id
 * get a single department
 */
router.get('/:id', (req, res) => {
  const id = req.params.id;
  routeHelpers.sendResult(res, new Db(tableName).findOne({ id }));
});

/**
 * POST /departments
 * create a new department
 */
router.post('/', (req, res) => {
  const newDepartment = req.body;
  newDepartment.id = uuid.v1();

  new Db(tableName).insert(newDepartment).then(() => {
    res.send(newDepartment);
  });
});

/**
 * PUT /departments/:id
 * update a department
 */
router.put('/:id', (req, res) => {
  const departmentData = req.body;
  const id = parseInt(req.params.id, 10);
  routeHelpers.sendResult(
      res,
      new Db(tableName).update({ id }, departmentData)
  );
});

/**
 * DELETE /departments/:id
 * delete a department
 */
router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  routeHelpers.sendResult(
      res,
      new Db(tableName).remove({ id })
  );
});

module.exports = router;
