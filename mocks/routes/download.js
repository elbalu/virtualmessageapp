var Router = require('express').Router();
var fs = require('fs'),
  join = require('path').join,
  json2csv = require('json2csv');

var DB = require('../mock-base'),
    _ = require('lodash'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

Router

  .get('/:type', function(req, res) {
    var type = req.params.type;

    //get schema and create csv template
    fs.readFile(join(__dirname,'/../mock-base', 'collections', 'schemas' + '.json'),{ encoding: 'utf8' },
      function(err, data) {
        const schema = (err) ? err : JSON.parse(data)[type];
        const columns = schema.fields;
        const keys = _.map(columns,'key');
        var csv = json2csv({ data: [], fields: keys });
        res.setHeader('Content-disposition', 'attachment; filename=testing.csv');
        res.set('Content-Type', 'text/csv');
        res.status(200).send(csv);
      });
  })


module.exports = Router;
