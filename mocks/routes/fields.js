const Router = require('express').Router();
const DB = require('../mock-base');
const c = require('../common/route-helpers');

Router
  .get('/list/:id', (req, res) => {
    c.sendResult(
      res,
      new Promise((resolve) => {
        // get all asset fields
        return new DB('templates')
          .read()
          .then((templates) => {
            const assetTemplate = templates.ASSETS;
            if (req.params.id === 0) return resolve(assetTemplate);
            // get category template
            return new DB('categories')
              .findOne({ id: parseInt(req.params.id, 10) })
              .then((category) => {
                const categoryFields = (category && category.template) ? category.template.fields : [];
                const prefixedCategoryFields = categoryFields.map((f) => {
                  f.key = `categoryCustomData.${f.key}`;
                  return f;
                });
                assetTemplate.fields = assetTemplate.fields.concat(prefixedCategoryFields);
                assetTemplate.entityName = 'ASSET_DETAIL';
                return resolve(assetTemplate);
              });
          });
      })
    );
  })
  .get('/download', (req, res) => {
    c.sendResult(
        res,
        new Promise((resolve) => {
          // get all asset fields
          return new DB('templates')
          .read()
          .then((templates) => {
            const assetTemplate = templates.ASSETS;
            // get category template
            return new DB('categories')
            .findOne({ id: 1 })
            .then((category) => {
              const categoryFields = (category && category.template.fields) ? category.template.fields : [];
              const prefixedCategoryFields = categoryFields.map((f) => {
                f.key = `categoryCustomData.${f.key}`;
                return f;
              });
              assetTemplate.fields = assetTemplate.fields.concat(prefixedCategoryFields);
              const fields = assetTemplate.fields.filter(f => f).map(field => field.templateLabel);
              resolve(fields);
            });
          });
        })
    );
  })
  .get('/templates/:entityName', (req, res) => {
    c.sendResult(
      res,
      new DB('templates').read().then((data) => {
        return data[req.params.entityName];
      })
    );
  })
  .put('templates/:entityName', (req, res) => {
    c.sendResult(
      res,
      new DB('templates').read().then((data) => {
        data[req.params.entityName] = req.body;
        return new DB('templates').write(data);
      })
    );
  });

module.exports = Router;
