var Router = require('express').Router();

var DB = require('../mock-base'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

Router
  .get('/telemetryHistoryByType/:assetId/:type/:startTime/:endTime', function(req, res) {
    const assetId = req.params.assetId;
    c.sendResult(
        res,
        new DB('tag-telemetry').findOne({"assetId":parseInt(assetId)})
    )
  })

module.exports = Router;
