const Router = require('express').Router();

const DB = require('../mock-base');
const c = require('../common/route-helpers');
const loginData = require('../mock-base/collections/login.json');

Router
.post('/', (req, res) => {
  const token = loginData.assetToken;
  const tenantId = loginData.tenantId;
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('X-CMX-auth', token);
  res.setHeader('X-CMX-tenant', tenantId);
  res.header('Access-Control-Expose-Headers', ['X-CMX-auth', 'X-CMX-tenant']);
  const resItem = new DB('login').find();
  c.sendResult(
    res,
    resItem
  );
});

module.exports = Router;
