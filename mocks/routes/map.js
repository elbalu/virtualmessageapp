const Router = require('express').Router();
const path = require('path');

const DB = require('../mock-base');
const c = require('../common/route-helpers');

const filePath = '../../dist/floormaps';

Router
  .get('/basehierarchy', (req, res) => c.sendResult(
      res,
      new DB('basehierarchy').find()
  ))
  .get('/:id', (req, res) => c.sendResult(
      res,
      new DB('floor').find()
  ))
  .get('/level/:id', (req, res) => c.sendResult(
      res,
      new DB('floor').find()
  ))
  .get('/images/floor/:id', (req, res) => {
    const { params } = req;
    const { id } = params;
    const imgPath = `${filePath}/${id}`;
    const imagePath = path.join(__dirname, imgPath);
    return res.sendFile(imagePath);
  });


module.exports = Router;
