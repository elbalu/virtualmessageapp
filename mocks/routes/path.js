const Router = require('express').Router();

const DB = require('../mock-base');
const c = require('../common/route-helpers');

Router
  .get('/', (req, res) => c.sendResult(
      res,
      new DB('path').find()
  ))
  .post('/', function(req, res) {
    var newItem = req.body;
    // console.log('newItem', newItem)
    new DB('path').insert(newItem).then(function() {
      res.send(newItem);
    });
  })
  .put('/:id', function(req, res) {
    var newItem = req.body;
    new DB('path').update({id: parseFloat(req.params.id)}, req.body).then(function() {
      res.send(newItem);
    });
  })
  .delete('/:id', function(req, res) {
    new DB('path').remove({id: parseFloat(req.params.id)}, req.body).then(function() {
      res.send({ success: true, id: parseFloat(req.params.id) });
    });
  });

module.exports = Router;
