const Router = require('express').Router();

const DB = require('../mock-base');
const c = require('../common/route-helpers');

Router
  .get('/', (req, res) => c.sendResult(
      res,
      new DB('poi').find()
  ))
  .post('/', function(req, res) {
    var newItem = req.body;
    newItem.properties.id = Math.random(1000, 10);
    // console.log('newItem', newItem)
    new DB('poi').insert(newItem).then(function() {
      res.send(newItem);
    });
  })
  .put('/:id', function(req, res) {
    var newItem = req.body;
    new DB('poi').update({id: parseFloat(req.params.id)}, req.body).then(function() {
      res.send(newItem);
    });
  })
  .delete('/:id', function(req, res) {
    new DB('poi').remove({id: parseFloat(req.params.id)}, req.body).then(function() {
      res.send({ success: true, id: parseFloat(req.params.id) });
    });
  });

module.exports = Router;
