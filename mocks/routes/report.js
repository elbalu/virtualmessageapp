var Router = require('express').Router();

var DB = require('../mock-base'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

Router

    .get('/:id', function(req, res) {
      console.log('req.params.id', req.params.id)
      c.sendResult(
          res,
          new DB('reports').findOne()
      );
    });


module.exports = Router;
