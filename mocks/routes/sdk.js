var Router = require('express').Router();

var DB = require('../mock-base'),
    _ = require('lodash'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');
const fs = require('fs');
const join = require('path').join;

Router


.get('/locations', (req, res) => {
  c.sendResult(
      res,
      new DB('sdkbylocation').read().then(function(data) {
        return data.location;
      })
  );
})

.get('/departments', (req, res) => {
  c.sendResult(
      res,
      new DB('sdkbylocation').read().then(function(data) {
        return data.department;
      })
  );
})

.get('/categories', (req, res) => {
  c.sendResult(
      res,
      new DB('sdkbylocation').read().then(function(data) {
        return data.category;
      })
  );
})

  .get('/', function(req, res) {
    if (_.isEmpty(req.query)) {
       c.sendResult(
         res,
         new DB('sdk').find()
       );
    } else if (req.query.floorid && !req.query.category && !req.query.subcategory) {
       console.log('req.query.floorid', req.query.floorid)
       c.sendResult(
         res,
         new DB('sdkbyfloor').find({ floorId: parseInt(req.query.floorid) })
       );
    } else if (req.query.floorid && req.query.category && !req.query.subcategory) {
       console.log('req.query.floorid', req.query.floorid, req.query.category)
       c.sendResult(
         res,
         new DB('sdkbyfloor').find({ floorId: parseInt(req.query.floorid), category: req.query.category })
       );
    }else if (req.query.assetId) {
       console.log('req.query.assetId', req.query.assetId)
       c.sendResult(
         res,
         new DB('sdkbyfloor').find({ id: parseInt(req.query.assetId)})
       );
     }
  })
  .get('/search/favorites', function(req, res) {
    res.json(require('../mock-base/collections/search-favorites'));
  })
  .post('/search/favorites', function(req, res) {
    fs.writeFile(
        join(__dirname, '../mock-base/collections/search-favorites.json'),
        JSON.stringify(req.body.favorites),
        (err) => {
          if (err) return res.status(500).send(err);
          return res.sendStatus(200);
        }
    );
  })
  .get('/search/:categoryId/:key', function(req, res) {
    c.sendResult(
      res,
      new DB('sdk').search(req.params.key)
    );
  })
  .get('/category/:id', function(req, res) {
    c.sendResult(
        res,
        new DB('sdk').filterByKeyParam({
          "key":"category",
          "id": parseFloat(req.params.id)
        })
    );
  })
  .get('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('sdk').findOne({"id":parseInt(req.params.id)})
    );
  })


  .post('/', function(req, res) {
    var newItem = req.body;
    newItem.id = uuid.v1();

    new DB('sdk').insert(newItem).then(function() {
      res.send(newItem);
    });
  })
  .put('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('sdk').update({id: parseFloat(req.params.id)}, req.body)
    );
  })
  .delete('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('sdk').remove({id: parseFloat(req.params.id)})
    );
  })

  .post('/search', (req, res) => {
    c.sendResult(
        res,
        new DB('filteredAssets').find()
    );
  })


module.exports = Router;
