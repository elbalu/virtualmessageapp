var Router = require('express').Router();

var DB = require('../mock-base'),
    _ = require('lodash'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

Router

  .get('/', function(req, res) {
    c.sendResult(
      res,
      new DB('tags').find()
    );
  })
  .post('/', function(req, res) {
    var newItem = req.body;
    newItem.id = uuid.v1();

    new DB('tags').insert(newItem).then(function() {
      res.send(newItem);
    });
  })
  .put('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('tags').update({id: parseFloat(req.params.id)}, req.body)
    );
  })
  .delete('/:id', function(req, res) {
    c.sendResult(
      res,
      new DB('tags').remove({id: parseFloat(req.params.id)})
    );
  })
  .post('/search', (req, res) => {
    c.sendResult(
        res,
        new DB('tags').find()
    );
  });

module.exports = Router;
