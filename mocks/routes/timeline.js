var Router = require('express').Router();

var DB = require('../mock-base'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

Router
    .get('/asset/:assetId', function(req, res) {
      console.log("req", req.query.startTime, (req.query.startTime === '2017-06-18T07:00:00Z'));
      const assetId = req.params.assetId;
      if((req.query.startTime === '2017-06-18T07:00:00Z')) {
        console.log("req.query.startTime", req.query.startTime, "true")
        c.sendResult(
            res,
            new DB('location-history1').find()
          )
      } else {
        c.sendResult(
            res,
            new DB('location-history').find()
          )
      }

  })

module.exports = Router;
