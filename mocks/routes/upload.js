var path = require('path');

var Router = require('express').Router();
var upload = require('jquery-file-upload-middleware');

var DB = require('../mock-base'),
    c = require('../common/route-helpers');

var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var join = require('path').join;

Router

  .get('/:path', function(req, res) {
    res.status(404).send();
  })
  .post('/:path', function(req, res, next) {
    upload.fileHandler({
      uploadDir: function() {
        return path.join(__dirname, '..', '..', 'public', 'uploads', req.params.path);
      },
      uploadUrl: function() {
        return '/uploads/' + req.params.path;
      },
    })(req, res, next)
  })
  .post('/schema/:type', function(req, res, next) {
    const type = req.params.type;
    //upload file
    upload.fileHandler({
      uploadDir: function() {
        return path.join(__dirname, '..', '..', 'public', 'uploads', 'schema');
      }
    })(req, res, next)
    // read file after upload and update the schema
    upload.on('end', function (fileInfo, req, res) {
       converter.fromFile(join(__dirname, '..', '..', 'public', 'uploads', 'schema',fileInfo.name),function(err,result){
           console.log("result",result);
           new DB(type).insert(result);
       });
    });
  });


module.exports = Router;
