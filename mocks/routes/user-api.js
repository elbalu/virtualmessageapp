const express = require('express');
const UserController = require('../controller/usercontroller');


/* eslint-disable new-cap */
const router = express.Router();

router.post('/', UserController.addUser);

router.put('/', UserController.updateUser);

router.get('/:id', UserController.getUser);

module.exports = router;