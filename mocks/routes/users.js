var router = require('express').Router();

var DB = require('../mock-base'),
    _ = require('lodash'),
    uuid = require('uuid'),
    c = require('../common/route-helpers');

router.get('/', function(req, res) {
  c.sendResult(
    res,
    new DB('users').find()
  );
});

  router.post('/', function(req, res) {
  var newItem = req.body;
  newItem.id = uuid.v1();

  c.sendResult(res, new DB('users').insert(newItem));
});

router.put('/:id', function(req, res) {
  c.sendResult(
    res,
    new DB('users').update({ id: parseFloat(req.params.id) }, req.body)
  );
});

router.delete('/:id', function(req, res) {
  c.sendResult(
    res,
    new DB('users').remove({ id: parseFloat(req.params.id) })
  );
});

router.post('/current/user', function(req, res) {
  var newItem = req.body;
  if(!newItem.username){
    newItem.email ="superadmin";
    newItem.password ="cisco123";
  }
  var resItem = new DB('loginUser').findOne({"email":newItem.email,"password":newItem.password});
  c.sendResult(
    res,
    resItem
  );
});

router.get('/current/user', function(req, res) {
  var newItem = req.body;
  if(!newItem.username){
    newItem.email ="superadmin";
    newItem.password ="cisco123";
  }
  var resItem = new DB('loginUser').findOne( {"email": newItem.email, "password": newItem.password });
  c.sendResult(
    res,
    new DB('users').find()
  );
});

module.exports = router;
