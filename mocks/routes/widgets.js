
const Router = require('express').Router();

const DB = require('../mock-base');
const uuid = require('uuid');
const c = require('../common/route-helpers');

Router

.get('/', (req, res) => {
  const page = req.query.page;
  if (page === 'alerts') {
    c.sendResult(
        res,
        new DB('alertsWidgets').find()
    );
  } else {
    c.sendResult(
        res,
        new DB('assetWidgets').find()
    );
  }

})
.put('/allWidgets/:id', (req, res) => {
  c.sendResult(
    res,
    new DB('alertsWidgets').update({ id: parseFloat(req.params.id) }, req.body)
  );
})

.post('/addWidget', (req, res) => {
  const newItem = req.body;
  const id = req.body.id;
  newItem.saved = true;
  // update allWidgets
  new DB('alertsWidgets').update({ id }, newItem);
})
.get('/count', (req, res) => {
  res.send({ count: 123 });
})
.post('/forward', (req, res) => {
  // actual backend to send email with alerts
  res.send(req.body);
});

module.exports = Router;
