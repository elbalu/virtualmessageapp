const express = require('express');
const zoneController = require('../controller/zonecontroller');


/* eslint-disable new-cap */
const router = express.Router();

router.post('/', zoneController.addZone);

router.put('/', zoneController.updateZone);

router.get('/:id', zoneController.getZoneById);

module.exports = router;
