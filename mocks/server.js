const express = require('express');
const argv = require('minimist')(process.argv.slice(2));
const swagger = require('swagger-node-express');
const parser = require('body-parser');
const logger = require('morgan');
const cors = require('cors');
const twilioService = require('./services/twilioService');
const bodyparser = require('body-parser');
var moment = require('moment');
var message = require('./models/message');
var time = moment();
var time_stamp = time.format('YYYY-MM-DD HH:mm:ss Z');

// Get All Routes for Hackathon
const userRoute = require('./routes/user-api');
const zoneRoute = require('./routes/zone-api');
const NotificationService = require('./services/notificationservice');

const app = express();
const subpath = express();

const http = require('http').Server(app);
const io = require('socket.io')(http);

require('./socket')(io);

const configs = require('./config');

const appPort = configs.port;
const prefix = ':9876/api/am/v1';
const authPrefix = ':9876/api/configurations/v1';
const tokenAuth = ':9876/api/dms/v1';
const dmsPrefix = ':9876/api/dms/v1';
const apiServerPrefix = ':9876/api/apiserver/v1';
const mapPrefix = ':9876/api/configurations/v1';

// API path for Hackathon
const apiPath = '/api/hackathon/v1';

// var corsOptions = {
//   origin: 'http://cmx.am',
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// };


app
.use(logger('dev'))
.use('/v1', subpath)
.use('/', express.static('dist'))
.use(parser.urlencoded({ extended: true }))
.use(parser.json())
.use(cors())

.use(`${prefix}/maps`, require('./routes/areas'))
.use(`${authPrefix}/users`, require('./routes/users'))
.use(`${authPrefix}/user`, require('./routes/users'))
.use(`${dmsPrefix}/tenantaccess`, require('./routes/tenantaccess'))
.use(`${tokenAuth}/application`, require('./routes/application'))
.use(`${dmsPrefix}/config/controller`, require('./routes/wlcs'))
.use(`${dmsPrefix}/config/connector`, require('./routes/connectors'))
.use(`${apiServerPrefix}/clients`, require('./routes/clients'))
.use(`${mapPrefix}/maps`, require('./routes/map'))
.use(`${mapPrefix}/points`, require('./routes/poi'))
.use(`${mapPrefix}/routing`, require('./routes/path'))
.use(`${apiPath}/user`, require('./routes/user-api'));


app.post('/notify', function (req, res) {
  const body = {
    message: 'Notification received',
    value: req.body,
  };
  const notification = new NotificationService();
  notification.notification(req);
  res.send(body);
});

/* app.post('/sendSMS', function (req, res) {
  const body = {
    message: 'Send SMS request received',
    value: req.body,
  };
  twilioService.sendSMS('+1408-508-4926','+1256-670-1980', 'Hello Professor Martin, Came down to meet you.Let me know when available for discussion')
.then((message) => {
    console.log(message);
    res.send(message);
}).catch((err) => {
    console.log(err.message);
})
}); */

app.use(bodyparser.urlencoded({extended: false}));
app.post('/sms', (req, res) => {
  const messageBody = {
    body: req.body.Body,
    sender: req.body.From,
  }
var response = {
    body : req.body.Body +' Sender is : ' +req.body.From,
    sent : "false",
    zone : "Office",
    timestamp : new Date(),
    sender    : {
        "userClass"   : "Student",
        "firstName"   : "Shiva",
        "lastName"    : "Meka",
        "phoneNumber" : req.body.From,
        "macAddress"  : "c4:0b:db:d6:f8:91"
    },
    recipient : {
        "userClass"   : "Professor",
        "firstName"   : "Martin",
        "lastName"    : "Borstad",
        "phoneNumber" : "+16093675969",
        "macAddress"  : "00:0c:cc:40:18:3f"
    },
};
console.log(JSON.stringify(response));
res.send(JSON.stringify(response));
var messageSave = new message(response);
messageSave.save();
});


app.use(`${apiPath}/user`, userRoute);
app.use(`${apiPath}/zone`, zoneRoute);

app.get('/tm', function (req, res) {
  res.sendFile(__dirname + '/dist/index.html');
});

// Configure the API domain
const domain = 'localhost';
if (argv.domain !== undefined) {
  domain = argv.domain;
} else {
  console.log('No --domain=xxx specified, taking default hostname "localhost".')
}

// Configure the API port
const port = 8080;
if(argv.port !== undefined) {
  port = argv.port;
} else {
  console.log('No --port=xxx specified, taking default port ' + port + '.')
}

// Set and display the application URL
const applicationUrl = 'http://' + domain + ':' + port;
console.log('snapJob API running on ' + applicationUrl);


// Start the web server
app.listen(port);

http.listen(
  appPort,
  () => console.log('Mock-end is running on', appPort)
);
