const co = require('co');
const User = require('../models/user');
const zone = require('../models/zone');
const message = require('../models/message');
const twilioService = require('./twilioService');

class UserDataService {

  static async userData(macAddress){
    const param = {
      macAddress: macAddress,
    };
    const userInfo = await User.get(param);
    return userInfo;
  }
  /**
   *
   */
  notification(req) {
    return co(function *() {
      let result = '';
      let zoneId;
      let zoneName;
      try {
        console.log(JSON.stringify(req.body));
        const macAddress = req.body.assets[0].macAddresses[0];
        const state = req.body.assets[0].state;
        for (let i = 0; i < state.length; i += 1) {
          const userState = state[i];
          if (userState.key === 'location') {
            zoneId = userState.value.zone[0];
            zoneName = userState.value.zoneNameList[0];
          }
        }
        let params = {
          macAddress: macAddress,
        };
        const values = yield User.get(params);
        params = {
          zoneUser: macAddress,
        };
        const zoneValues = yield zone.get(params);
        console.log(zoneValues);

        params = {
          zone: zoneValues[0]._attributes[0].zoneName,
        };
        console.log('ZONE    ', params);

        const messageData = yield message.get(params);
        console.log('messageData    ', messageData.length);

        for (let i = 0; i < messageData.length; i ++) {
          const sender = messageData[0]._attributes[0].sender.phoneNumber;
          const recipient = messageData[0]._attributes[0].recipient.phoneNumber;
          const body = messageData[0]._attributes[0].body;
          console.log('recipient  ', recipient);
          console.log('sender     ', sender);

          yield twilioService.sendSMS(recipient, '+1256-670-1980', body);
        }
        // console.log('-------------    ', messageData[0])
        // yield messageData[0].markAsSent();

        // const message = yield message.get()
      } catch (error) {
        console.log('Error while Adding the user', error);
        throw error;
      }
      return result;
    });
  }
}

module.exports = UserDataService;
