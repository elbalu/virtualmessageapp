const accountSid = 'AC791f975c8c5058077b47a969e7c4a6dd';
const authToken = 'aed8b03d3bb2995fa1bb23a14207d533';
const client = require('twilio')(accountSid, authToken);

async function sendSMS(to, from, message){
    console.log('Send SMS');
    let appAuthToken;
    let messageRet = await client.messages
    .create({
        body: message,
        to: to,
        from: from
    });
  return messageRet;
};
module.exports.sendSMS = sendSMS;