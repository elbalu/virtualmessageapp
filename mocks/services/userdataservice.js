const co = require('co');

class UserDataService {

  /**
   *
   */
  addUser() {
    return co(function *() {
      let result = '';
      try {

      } catch (error) {
        console.log('Error while Adding the user', error);
        throw error;
      }
      return result;
    });
  }

  updateUser() {
    return co(function *() {
      let result = '';
      try {

      } catch (error) {
        console.log('Error while updating the user', error);
        throw error;
      }
      return result;
    });
  }

  /**
   *
   * @param userId
   * @returns {*}
   */
  getUser(userId) {
    return co(function *() {
      let result = '';
      try {

      } catch (error) {
        console.log('Error while getting the user', error);
        throw error;
      }
      return result;
    });
  }
}

module.exports = UserDataService;
