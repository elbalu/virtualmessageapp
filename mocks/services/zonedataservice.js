const co = require('co');

class ZoneDataService {

  /**
   *
   * @returns {*}
   */
  addZone() {
    return co(function *() {
      let result = '';
      try {

      } catch (error) {
        console.log('Error while Adding the zone', error);
        throw error;
      }
      return result;
    });
  }

  updateZone() {
    return co(function *() {
      let result = '';
      try {

      } catch (error) {
        console.log('Error while updating the zone', error);
        throw error;
      }
      return result;
    });
  }

  /**
   *
   * @param zoneId
   * @returns {*}
   */
  getZone(zoneId) {
    return co(function *() {
      let result = '';
      try {

      } catch (error) {
        console.log('Error while getting the zone details for Id: %d', error);
        throw error;
      }
      return result;
    });
  }

  getUsersByZone(zoneId) {
    return co(function *() {
      let result = '';
      try {

      } catch (error) {
        console.log('Error while getting the user details by ZoneId Id: %d', error);
        throw error;
      }
      return result;
    });
  }
}

module.exports = ZoneDataService;
