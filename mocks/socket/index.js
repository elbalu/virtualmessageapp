var _ = require('underscore'),
    Promise = require('bluebird');

var DB = require('../mock-base');

function main(io) {
  io.on('connection', _.partial(handleSocket, io));
  manageTestsEmmiter(io);
}

function manageTestsEmmiter(io) {
  setInterval(function() {
    new DB('tests').find()
    .then(function(tests) {
      io.to('room:manage-tests').emit('data', tests);
    });
  }, 10000);
}

function handleSocket(io, socket) {
  socket.on('room:join', function(roomName) {
    socket.join(roomName);
  });
}

module.exports = main;
