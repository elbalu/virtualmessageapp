import requests, os, sys
from requests.auth import HTTPBasicAuth
from argparse import ArgumentParser

# description: This script gets a token from
# a tenant management cloud using the provided
# username and password and then opens a browser
# tab with the default am ui url with the token
# set.
#
# author: Balu Loganathan <baepalap@cisco.com>

# create parser for this script
parser = ArgumentParser(description='log in and open local am ui');
parser.add_argument('-e', '--environment', default='sandbox', choices=['sandbox', 'test', 'production'], help='the backend environment to authenticate with');
parser.add_argument('-u', '--username', default='sajagade@cisco.com', help='username for a user in the given environment');
parser.add_argument('-p', '--password', default='admin', help='password for the given user. required if a username is given');

# parse args and generate token api url
args = parser.parse_args(sys.argv[1:]);
url = 'https://sdk-{env}-api.cmxdemo.com/api/tm/v1/account/local/login'.format(env=args.environment)
openCommand = 'open http://localhost:3000/#/?token={token}'

# get token and open ui in browser with token
res = requests.post(url, auth=HTTPBasicAuth(args.username, args.password))
if res.status_code is 200:
    token = res.json()['accountDetails'][0]['token']
    os.system(openCommand.format(token=token))
