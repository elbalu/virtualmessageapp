#!/bin/bash
PROJECT_NAME=$1
PORT=$2

echo "Deploy started!"
REMOTE_SERVER=root@107.170.222.104
PROCESS_LINE=$(ssh $REMOTE_SERVER "ps aux | grep \"node /var/www/$PROJECT_NAME/mocks/server.js\" | grep -v grep | awk '{print $2}'")
NODE_PID=$(echo $PROCESS_LINE | grep -v grep | awk '{print $2}')

echo "module.exports={\"port\":$PORT, \"domain\":\"localhost\"}" > mocks/config.js
npm run build:production
rm -rf mocks/node_modules
cp -R mocks/ dist/mocks
cd dist
tar -czf deploy-$PROJECT_NAME.tar.gz *
ssh $REMOTE_SERVER "kill -9 $NODE_PID"
ssh $REMOTE_SERVER "cd /var/www/$PROJECT_NAME/ && rm -rf ./* "
scp deploy-$PROJECT_NAME.tar.gz $REMOTE_SERVER:/var/www/$PROJECT_NAME/
rm deploy-$PROJECT_NAME.tar.gz
ssh $REMOTE_SERVER "cd /var/www/$PROJECT_NAME/ && tar -xzf deploy-$PROJECT_NAME.tar.gz && rm deploy-$PROJECT_NAME.tar.gz"
ssh $REMOTE_SERVER "cd /var/www/$PROJECT_NAME/mocks && npm install"
ssh $REMOTE_SERVER "sh -c \"nohup node /var/www/$PROJECT_NAME/mocks/server.js > /dev/null 2>&1 &\""
echo "Deploy finished!"
