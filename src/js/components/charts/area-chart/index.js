import React, { Component, PropTypes } from 'react';
import {
  Area,
  XAxis,
  YAxis,
  AreaChart,
  ReferenceLine,
  Tooltip,
} from 'recharts';
import * as Format from '../chart-formatter';

class AreaChartWidget extends Component {

  tooltip(props) {
    const { active, payload, label, xAxisType } = props;

    if (active) {
      const style = {
        padding: 6,
        backgroundColor: '#fff',
        border: '1px solid #ccc',
      };


      return (
        <div className="area-chart-tooltip" style={style}>
          <p>{`${payload[0].name} : `}<em>{this.axisLabelFormatter(payload[0].value)}</em></p>
          <p>{Format[xAxisType](label)}</p>
        </div>
      );
    }

    return null;
  }

  axisLabelFormatter(val) {
    const { yAxisType } = this.props;
    if (Format[yAxisType]) {
      return Format[yAxisType](val);
    }
    return `${val} ${yAxisType}`;
  }

  render() {
    const { data, icon, title, threshold, xAxisType, yAxisType, bgColor } = this.props;
    let { width, height } = this.props;

    if (!width) width = '100%';
    if (!height) height = '100%';

    return (

      <div className="area-chart-wrapper">
        <i className={`icon ${icon}`} />
        <h3 className="title">{title.toUpperCase()}</h3>
        <AreaChart
          data={data}
          height={height} width={width}
        >
          <XAxis
            dataKey="timestamp"
            tickFormatter={Format[xAxisType]}
          />
          <YAxis
            tickFormatter={v => this.axisLabelFormatter(v)}
          />
          <Tooltip
            xAxisType={xAxisType}
            yAxisType={yAxisType}
            content={props => this.tooltip(props)}
          />
          <ReferenceLine
            y={threshold}
            stroke="red"
            label=""
          />
          <Area
            type="monotone"
            dataKey="value"
            stroke="#8884d8"
            fill={bgColor}
          />
        </AreaChart>
      </div>
    );
  }
}

AreaChartWidget.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  data: PropTypes.instanceOf(Array),
  threshold: PropTypes.number,
  xAxisType: PropTypes.string,
  yAxisType: PropTypes.string,
  bgColor: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

AreaChartWidget.defaultProps = {
  title: '',
  icon: 'area-chart',
};

export default AreaChartWidget;
