/**
 * formatters for charts
 *
 */
import moment from 'moment';

export const timestamp = val => moment(val).format('h:mm A');
export const timestampWithSec = val => moment(val).format('h:mm:ss A');

// formatter for temperature in Celsius
export const temperatureC = val => `${val} ${String.fromCharCode(176)} C`;

// formatter for temperature in Fahreinheit
export const temperatureF = val => `${val} ${String.fromCharCode(176)} F`;
