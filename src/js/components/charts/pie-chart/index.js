import React, {
  Component,
  PropTypes,
} from 'react';
import {
  PieChart,
  Pie,
  Cell,
  Sector
} from 'recharts';
import { bindActionCreators } from 'redux';
import { FormattedMessage, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import _ from 'lodash';

import {
  pieChartColors,
} from '../../../misc/colors';
import Loading from '../../loading';
import * as pieChartActions from '../../../redux/actions/pie-chart-actions';

const initialState = {
  activeIndex: 0,
};

/**
 * PieChartWidget is a react component will
 * will render pie chart with given data
 * @class
 * @author Balu Loganthan <baepalap@cisco.com>
 * */

class PieChartWidget extends Component {


  static get contextTypes() {
    return {
      router: React.PropTypes.object.isRequired,
    };
  }

  /**
   * @constructor
   * @param {Object} props - React props
   */
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);
    this.onPieEnter = this.onPieEnter.bind(this);
    this.renderActiveShape = this.renderActiveShape.bind(this);
  }

  /**
   * @componentWillReceiveProps
   * @param  {Object} nextProps - React props
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.data.length > 0) {
      const highestValueIndex = _.indexOf(nextProps.data, _.maxBy(nextProps.data, o => o.value));
      this.setState({
        activeIndex: highestValueIndex
      });
    }
  }

  /**
   * onPieEnter - Set active Index of the pie
   * @param  {Array} data
   * @param  {Number} index
   */
  onPieEnter(data, index) {
    this.setState({
      activeIndex: index,
    });
  }

  /**
   * redirect - On Click of a Pie, frame the redirect
   * Query and redirect the use to filter page
   * @param  {Object} pieDetails
   * @return {String}
   */
  redirect(pieDetails) {
    const { currentFeature, widgetKey, widgetType, label } = this.props;
    const { id, name, payload } = pieDetails;
    const { redirect } = this.props.actions;
    redirect({ widgetKey, currentFeature, pieId: id, pieName: name, payload, widgetType, label });
  }

  /**
   * placeTextWithEllipsis - Pie chart title.
   * return ellipise text
   * @param  {String}  textString
   * @param  {Number}  width - Max size
   * @return {String}
   */
  placeTextWithEllipsis(textString, width) {
    return _.truncate(textString, {
      length: width
    });
  }
  /**
   * renderActiveShape - On mouse enter of pie, render the active
   * state with the outer arc
   * @param  {Object} props
   */
  renderActiveShape(props) {
    const { cx, cy, innerRadius, outerRadius, startAngle, endAngle,
      fill, payload } = props;

    return (
      <g>
        <text
          x={cx} y={cy} dy={0} className="pieTitle" textAnchor="middle" fill={fill}
        >
          <title>{payload.name}</title>
          {this.placeTextWithEllipsis(payload.name, 11)}
        </text>
        <text
          x={cx} y={cy} dy={24} className="pieCount" textAnchor="middle" fill={fill}
        >
          {payload.value}
        </text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + -1}
          outerRadius={outerRadius + 6}
          fill={fill}
        />
      </g>
    );
  }


  render() {
    const { data } = this.props;
    const recordCount = _.sumBy(data, o => o.value);
    if (!data) {
      return (
        <Loading key={Math.random(10)} />
      );
    }
    if (recordCount === 0) {
      return (
        <div className="pie-no-data">
          <h5><FormattedMessage id="chart.pie.noData" /></h5>
        </div>
      );
    }
    return (
      <div className="pie-chart-wrapper">
        <PieChart
          width={200} height={125}
          onMouseEnter={this.onPieEnter}
          onClick={e => this.redirect(e)}
        >
          <Pie
            activeIndex={this.state.activeIndex}
            activeShape={this.renderActiveShape}
            data={data}
            innerRadius={40}
            outerRadius={50}
            cursor="pointer"
            cx={'50%'}
            cy={'50%'}
          >
            {data.map((entry, index) =>
              <Cell
                key={index}
                fill={pieChartColors[index % pieChartColors.length]}
              />
            )}
          </Pie>
        </PieChart>
      </div>
    );
  }
}

PieChartWidget.propTypes = {
  data: PropTypes.instanceOf(Array).isRequired,
  currentFeature: PropTypes.shape({}),
  widgetKey: PropTypes.string,
  widgetType: PropTypes.string,
  actions: PropTypes.shape({
    redirect: PropTypes.func.isRequired,
  }),
};

PieChartWidget.defaultProps = {
  title: '',
  icon: 'pie-chart',
};
const mapStatetoProps = state => ({
  ...state.pieChartActions,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(pieChartActions, dispatch),
});

export default connect(mapStatetoProps, mapDispatchToProps)(injectIntl(PieChartWidget));
