
import React, { Component, PropTypes } from 'react';
import Highcharts from 'highcharts';
import moment from 'moment';

class ScatterChart extends Component {

  componentDidMount() {
    const { container,
      data,
      xAxisTitle,
      yAxisTitle,
     } = this.props;
    Highcharts.setOptions({
   // This is for all plots, change Date axis to local timezone
      global: {
        useUTC: false
      }
    });
    // Set container which the chart should render to.
    this.chart = Highcharts.chart(container, {
      chart: {
        type: 'scatter',
        zoomType: 'xy'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      credits: {
        enabled: false
      },
      xAxis: {
        title: {
          enabled: true,
          text: xAxisTitle
        },
        dateTimeLabelFormats: {
          millisecond: '%I:%M:%S %p',
          second: '%I:%M:%S %p',
          hour: '%I:%M %p',
          minute: '%I:%M %p'
        },
        startOnTick: true,
        endOnTick: true,
        showLastLabel: true,
        type: 'datetime'

      },
      yAxis: {
        title: {
          text: yAxisTitle
        },
        categories: data.categories
      },
      tooltip: {
        formatter: function() {
          return  `<b> ${this.series.name} </b><br/>` +
            moment(this.x).format('MMMM Do YYYY, h:mm:ss a');
        }
      },
      plotOptions: {
        scatter: {
          marker: {
            radius: 5,
            states: {
              hover: {
                enabled: true,
                lineColor: 'rgb(100,100,100)'
              }
            }
          },
          states: {
            hover: {
              marker: {
                enabled: false
              }
            }
          }
        }
      },
      series: data.series
    });
  }
  componentWillReceiveProps(props) {
    const { data } = props;
    const { series } = data;
    if (series.length === 0 && this.chart.series.length > 0) {
      while (this.chart.series.length > 0) {
        this.chart.series[0].remove(true);
      }
    } else if (series.length > 0 && this.chart.series.length === 0) {
      series.forEach((s) => {
        this.chart.addSeries(s);
      });
    } else {
      // first remove existing series
      while (this.chart.series.length > 0) {
        this.chart.series[0].remove();
      }
      // add new series
      series.forEach((s) => {
        this.chart.addSeries(s);
      });
      this.chart.yAxis[0].setCategories(props.data.categories);
    }
  }

  // Destroy chart before unmount.
  componentWillUnmount() {
    // this.chart.destroy();
  }

  // Create the div which the chart will be rendered to.
  render() {
    return React.createElement('div', { id: this.props.container });
  }


}

ScatterChart.propTypes = {
  data: PropTypes.shape({
    categories: PropTypes.instanceOf(Array),
  }),
  xAxisTitle: PropTypes.string,
  yAxisTitle: PropTypes.string,
  container: PropTypes.string.isRequired
};

ScatterChart.defaultProps = {
  xAxisTitle: '',
  yAxisTitle: '',
  xAxisUnit: '',
  YAxisUnit: '',
  categories: [],
};

export default ScatterChart;
