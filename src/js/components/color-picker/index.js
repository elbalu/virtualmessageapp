import React from 'react';
import PropTypes from 'prop-types';
import { Popover, Button } from 'antd';
import { CirclePicker } from 'react-color';
import _ from 'lodash';

/**
 * @class
 *
 * @name ColorPicker
 *
 * @description Button and Popover that provides color
 * options where the user can select an option. This component
 * should be extended to allow for other container types
 * (Popover, Modal, etc) and other picker types (CirclePicker,
 * SketchPicker, ChromePicker, etc).
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ColorPicker extends React.Component {
  /**
   * @constructor
   *
   * @param {Object} props - React props
   */
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  /**
   * @name ColorPicker#onChange
   *
   * @description Calls props.onChange with
   * the hex string value of the selected color.
   *
   * @param {{ hex:string }} color - Object with
   * property called hex that contains hex value.
   */
  onChange(color) {
    const { onChange } = this.props;
    onChange(color.hex);
  }

  /**
   * @name ColorPicker#getPicker
   *
   * @description Returns a react-color
   * color picker with the given color options
   * and selected color.
   *
   * @return {XML}
   */
  getPicker() {
    const { color, colors } = this.props;

    return (
      <CirclePicker
        color={{ hex: color }}
        onChange={this.onChange}
        colors={colors}
      />
    );
  }

  render() {
    const { disabled, style, color, children } = this.props;

    const content = (_.isNil(children)) ? (
      <Button disabled={disabled} style={style}>
        <span
          className="fa fa-paint-brush"
          style={{ color }}
        />
      </Button>
    ) : children;

    return (
      <Popover
        title="choose a color"
        content={this.getPicker()}
        trigger="click"
      >
        {content}
      </Popover>
    );
  }
}

ColorPicker.propTypes = {
  // onChange handler
  onChange: PropTypes.func,
  // selected color
  color: PropTypes.string,
  // is button disabled
  disabled: PropTypes.bool,
  // button css style
  style: PropTypes.shape(),
  // color options
  colors: PropTypes.arrayOf(PropTypes.string),
  // custom button
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ])
};

ColorPicker.defaultProps = {
  onChange: _.noop,
  color: null,
  disabled: false,
  style: {},
  colors: null
};

export default ColorPicker;
