import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import validate from 'validate.js';
import _get from 'lodash/get';
import _set from 'lodash/set';
import _unset from 'lodash/unset';
import _flatten from 'lodash/flatten';

import { Modal, Tabs } from 'antd';

import GeneralTab from './tabs/general-tab';
import CategoryTab from './tabs/category-tab';
import TemplateTab from './tabs/template-tab';
// import WlcTab from './tabs/wlc-tab';
// import ConnectorTab from './tabs/connector-tab';
import TagTab from './tabs/tag-tab';
import LocationTab from './tabs/location-tab';
import ScopeRoleTab from './tabs/scope-role-tab';

import Notification, { NOTIFICATION_TYPES } from '../../misc/models/notification';
import * as FormUtils from '../../misc/form-utils';
import config from '../../config';
import TemplateRenderer from '../../misc/renderers/template-renderer';
import Fields from '../form-generator/fields';

const entityNames = config.get('entityNames');
const TabPane = Tabs.TabPane;

export const MODES = {
  ADD: 1,
  EDIT: 2,
};

const initialState = {
  activeTab: 1,
  mode: MODES.ADD,
  entity: {},
};

/**
 * EntityModal is a modal container that is used to manage
 * entities (create/edit/delete). It is generalized to allow creation
 * of different entity types. It includes a form generator for
 * inputing information, a form builder for generating a schema,
 * and a section for generating rules. Each of these sections can
 * be disabled using props.
 *
 * @author: Benjamin Newcomer
 */
class EntityModal extends Component {

  /**
   * constructor. bind methods that will be called
   * by the dom and set initial state.
   *
   * @param props
   */
  constructor(props) {
    super(props);

    // initial state
    this.state = Object.assign({}, initialState);

    // bind methods that will
    // be called by the dom
    this.submit = this.submit.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
    this.nextTab = this.nextTab.bind(this);
    this.setTab = this.setTab.bind(this);
    this.reset = this.reset.bind(this);
    this.updateEntity = this.updateEntity.bind(this);
    this.updateCategory = this.updateCategory.bind(this);
    this.updateLocation = this.updateLocation.bind(this);
    this.getCategoryData = this.getCategoryData.bind(this);
    this.updateScopeRole = this.updateScopeRole.bind(this);
  }

  /**
   * @override
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (validate.isEmpty(nextProps)) return;

    const entity = Object.assign({}, nextProps.entity);
    const isVisibilityTransition = (this.props.visible !== nextProps.visible);
    const mode = (!validate.isEmpty(entity) && Object.prototype.hasOwnProperty.call(entity, 'id')) ? MODES.EDIT : MODES.ADD;

    if (isVisibilityTransition) {
      this.setState({ entity, mode, activeTab: initialState.activeTab });
    }
  }

  /**
   * sets the active tab (if user clicks on a tab
   * rather than advancing using the 'next' button)
   *
   * @param tab
   */
  setTab(tab) {
    this.setState({ activeTab: parseInt(tab, 10) });
  }

  /**
   * reset form builder schema and
   * entity modal state
   *
   * @param {function} [callback]
   */
  reset(callback) {
    // reset state
    this.state = Object.assign({}, initialState);
    if (callback) callback();
  }

  /**
   * advances the modal to the next tab
   */
  nextTab() {
    const activeTab = this.state.activeTab + 1;
    this.setState({ activeTab });
  }

  /**
   * @param entity
   */
  updateEntity(entity) {
    this.setState({ entity: { ...entity } });
  }

  /**
   * Updates the properties associated with site and location
   * properties of entity in this component's state.
   * When a sdk is static and a valid site with floor is
   * selected, the XY locations have to be enabled.
   *
   * @param entity
   */
  updateLocation(entity) {
    const staticLoc = entity.static;
    const site = entity.staticLocation;
    let x = '';
    let y = '';
    let xyDisabled = true;

    let isSiteAFloor = false;
    if (!validate.isEmpty(site)) {
      isSiteAFloor = true;
    }
    // if sdk is marked static and site is selected as a floor
    // then enable X,Y fields
    if ((staticLoc && isSiteAFloor)) {
      x = entity.x;
      y = entity.y;
      xyDisabled = false;
    }
    this.setState({
      entity: { ...entity, x, y },
      others: { xyDisabled }
    });
  }

  /**
   * prepares the necessary data for
   * updating the category tab
   */

  getCategoryData(catId, entity) {
    const { collections } = this.props;
    const selectedDepartment = entity.department;
    let category = entity.category || null;
    let selectedCategory = {};
    let updatedSubcategories = [];
    const categories = collections.DEPARTMENTS.find(i =>
      i.id === parseInt(selectedDepartment.id, 10)).categories || [];

    selectedCategory =
    categories.find(i => i.id === parseInt(catId, 10)) || {};
    category = (selectedCategory && selectedCategory.id) ? String(selectedCategory.id) : null;
    // check if selected category present in available categories
    // if not send null.This happens when user changes the department
    const availableSubcategories = selectedCategory.subcategories || [];
    const selectedSubcategories = entity.subcategories || [];
    // filter out subcategories that are not in available
    // subcategories. This happens when the user changes the category
    updatedSubcategories = selectedSubcategories.filter(
      s => availableSubcategories.includes(s),
    );

    return {
      category,
      subcategories: updatedSubcategories,
      selectedCategory,
      categories
    };
  }
  /**
   * Updates the category property of the entity in
   * this component's state. When a category is changed,
   * entity subcategories are updated to reflect the
   * new available subcategory options.
   *
   * @param entity
   */
  updateCategory(entity) {
    const selectedDepartment = entity.department;
    let categoryData = {};
    if (!validate.isDefined(selectedDepartment)) return;
    // validate if category belongs to department
    if (entity.category && !validate.isObject(entity.category)) {
      categoryData = this.getCategoryData(entity.category, entity);
    } else if (entity.category && validate.isObject(entity.category)) {
      const catId = entity.category.id;
      categoryData = this.getCategoryData(catId, entity);
    }

    let categoryCustomData = entity.categoryCustomData;

    if (categoryData.selectedCategory &&
      ('template' in categoryData.selectedCategory) &&
      categoryData.selectedCategory.template.fields.length === 0) {
      categoryCustomData = {};
    }
    this.setState({
      entity: { ...entity,
        category: categoryData.category,
        subcategories: categoryData.subcategories,
        categoryCustomData
      },
      others: {
        category: categoryData.selectedCategory,
        categories: categoryData.categories
      }
    });
  }

  /**
   * Updates the role property of the entity in
   * this component's state. When a department is changed,
   * entity categories are updated to reflect the
   * new available categories of the selected departments.
   *
   * @param entity
   */
  updateScopeRole(entity) {
    const { collections } = this.props;
    const selectedDepartments = (entity.departments === 'ALL') ? 'ALL' : (entity.departments || []);
    const selectedCategories = (entity.categories === 'ALL') ? 'ALL' : (entity.categories || []);
    let categories = entity.categories;
    let strCategories = [];

    // filter out categories that are not available in department
    // This happens when the user changes the department
    if (selectedDepartments !== 'ALL' && selectedDepartments.length > 0) {
      const deptIds = selectedDepartments.map(d => d.id);
      const departments = collections.DEPARTMENTS.filter((d) => {
        return deptIds.includes(d.id);
      });
      const categoryArr = departments.map(d => d.categories);
      const allCategories = _flatten(categoryArr).map(c => c.id);
      if (selectedCategories === 'ALL') {
        strCategories = allCategories.map(c => String(c));
        categories = strCategories;
      } else {
        // if selectedCategories not part of allCategories then remove
        selectedCategories.forEach((c) => {
          let id = c;
          if (validate.isObject(c)) id = String(c.id);
          if (allCategories.includes(parseInt(id, 10))) strCategories.push(id);
        });
        categories = strCategories;
      }
    }
    this.setState({
      entity: { ...entity,
        categories
      }
    });
  }

  /**
   * Once the user is done creating or editing an entity,
   * submit() will call the add() or edit() function,
   * depending on props given
   */
  submit() {
    const { intl, displayNotification } = this.props;
    let { template } = this.props;
    const { add, edit, hide } = this.props.actions;
    const { mode, entity } = this.state;
    const isEditMode = (mode === MODES.EDIT);
    const isAddMode = (mode === MODES.ADD);
    const hasCategoryTemplate = !validate.isEmpty(_get(entity, 'category.template'));
    const hasTemplate = !validate.isEmpty(entity.template);

    // password for add user ,newPassword for update password
    const passwordsMatch = ((entity.password || entity.newPassword) === entity.confirmPassword);
    const isUpdatingPassword = template.entityName === entityNames.password.key;

    if (!isUpdatingPassword && isEditMode) {
      // remove fields that should only be present when adding
      template = TemplateRenderer.rejectFieldsByType(template, Fields.Types.PASSWORD);
    }

    // Validate entity fields
    let validationErrors = FormUtils.validateEntity(entity, template);

    // if the entity has a category that has a template, validate
    // entity.customCategoryData against entity.category.template
    if (hasCategoryTemplate) {
      const categoryDataErrors = FormUtils.validateEntity(
        _get(entity, 'categoryCustomData', {}),
        entity.category.template,
      );
      validationErrors = validationErrors.concat(categoryDataErrors);
    }

    // if the entity has a key called oldPassword and another one called
    // newPassword, confirm that those are both equal
    if ((isUpdatingPassword || isAddMode) && !passwordsMatch) {
      validationErrors.push({
        key: 'password',
        status: FormUtils.VALIDATE_STATUSES.ERROR,
        message: intl.formatMessage({ id: 'passwordMismatch' }),
      });
    }

    // if the entity has validation errors,
    // present the errors to the user and do not
    // submit the entity
    if (validationErrors.length > 0) {
      const title = intl.formatMessage({ id: 'form.errors.prompt' });
      return displayNotification(
        Notification.fromValidationErrors(
          validationErrors.map(e => e.message),
          title
        )
      );
    }

    // set entityName equal to category name if
    // the entity is a category (categories have templates)
    if (hasTemplate) {
      _set(entity, 'template.entityName', entityNames.customCategory.key);
    }

    // if entity is a set of passwords, remove
    // currentPassword before sending because
    // this prop is not part of the update
    // password API
    if (isUpdatingPassword) {
      _unset(entity, 'confirmPassword');
    }


    // if there are no errors, submit entity
    if (isEditMode) edit(entity);
    else add(entity);

    hide();
    return this.reset();
  }

  /**
   * Renders the list of sections to show based
   * on props hasFields and hasRules
   */
  renderTabs() {
    const { sections, template } = this.props;
    const { entity, mode, others } = this.state;
    const tabs = [];
    const isEditMode = mode === MODES.EDIT;
    const isPasswordTemplate = (template.entityName === entityNames.password.key);

    // remove sensitive fields before displaying forms
    let processedTemplate = template;
    if (isEditMode && !isPasswordTemplate) {
      processedTemplate = TemplateRenderer.rejectFieldsByType(template, Fields.Types.PASSWORD);
    }

    if (sections.general !== false) {
      tabs.push(
        <GeneralTab
          title="General"
          template={processedTemplate}
          entity={entity}
          updateEntity={this.updateEntity}
          mode={mode}
        />,
      );
    }

    // if entity has a category include the category tab, which presents
    // a dropdown select of all categories. Once a category is chosen,
    // a list of additional fields (including subcategory) is presented
    if (sections.category) {
      tabs.push(
        <CategoryTab
          title="Department"
          entity={entity}
          others={others}
          onChange={this.updateCategory}
        />,
      );
    }

    // entity defines custom sdk fields (eg category, department)
    // This tab uses the form builder to allow a user to define a template
    // to be associated with this entity
    if (sections.template) {
      // give entity template the same title as overall template
      tabs.push(
        <TemplateTab
          title="Custom Asset Fields"
          template={entity.template}
          onChange={t => this.updateEntity({ ...entity, template: t })}
          category={entity}
        />,
      );
    }

    // if (sections.wlcs) {
    //   tabs.push(
    //     <WlcTab
    //       title="Wlcs"
    //       entity={entity}
    //       onChange={this.updateEntity}
    //     />,
    //   );
    // }

    // if (sections.connectors) {
    //   tabs.push(
    //     <ConnectorTab
    //       title="Connectors"
    //       entity={entity}
    //       onChange={this.updateEntity}
    //     />,
    //   );
    // }
    if (sections.tags) {
      tabs.push(
        <TagTab
          title="Tags"
          entity={entity}
          onChange={this.updateEntity}
        />,
      );
    }

    if (sections.location) {
      tabs.push(
        <LocationTab
          title="Static Location"
          entity={entity}
          others={others}
          onChange={this.updateLocation}
        />,
      );
    }

    if (sections.roles) {
      tabs.push(
        <ScopeRoleTab
          title="Location"
          entity={entity}
          others={others}
          onChange={this.updateScopeRole}
        />,
      );
    }

    // return sections
    return tabs;
  }

  /**
   * renderTabBar() creates a Tabs component
   * with tabs defined by EntityModal#renderTabs()
   *
   * @returns {*}
   */
  renderTabBar() {
    const { activeTab } = this.state;

    const tabs = this.renderTabs();
    if (tabs.length === 1) return tabs[0];

    return (
      <Tabs
        onTabClick={this.setTab}
        activeKey={String(activeTab)}
      >
        {tabs.map((t, index) => (
          <TabPane tab={t.props.title} key={String(index + 1)}>
            {t}
          </TabPane>
        ))}
      </Tabs>
    );
  }

  render() {
    const { title, visible, actions } = this.props;

    const onOk = this.submit;
    const okText = 'Save';
    const cancelText = 'Cancel';

    return (
      <Modal
        title={title}
        visible={visible}
        onCancel={actions.hide}
        onOk={onOk}
        okText={okText}
        cancelText={cancelText}
        width={800}
        height={600}
      >
        {this.renderTabBar()}
      </Modal>
    );
  }
}

EntityModal.propTypes = {
  // boolean arguments used to configure
  // the modal
  sections: PropTypes.shape({
    category: PropTypes.bool,
    template: PropTypes.bool,
    rules: PropTypes.bool,
    tags: PropTypes.bool,
    // wlcs: PropTypes.bool,
    // connectors: PropTypes.bool,
  }),

  // props describing the entity
  template: PropTypes.shape(),
  title: PropTypes.string,
  visible: PropTypes.bool,

  // functions to persist changes
  // made to the entity
  actions: PropTypes.shape({
    add: PropTypes.func,
    edit: PropTypes.func,
    hide: PropTypes.func,
  }),

  displayNotification: PropTypes.func,
  intl: intlShape,

  collections: PropTypes.shape(),
};

EntityModal.defaultProps = {
  actions: {},
  title: 'Entity',
  sections: {},
  template: TemplateRenderer.blankTemplate()
};

const mapStateToProps = state => ({
  collections: {
    DEPARTMENTS: [],
  },
});

export default connect(mapStateToProps, null)(injectIntl(EntityModal));
