import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import validate from 'validate.js';

import FormGenerator from '../../form-generator';
import Fields from '../../form-generator/fields';
import TemplateRenderer from '../../../misc/renderers/template-renderer';

class CategoryTab extends Component {
  
  getDepartmentCategories(department) {
    const { collections } = this.props;
    return collections.DEPARTMENTS.find(i =>
      i.id === parseInt(department.id, 10)).categories || [];
  }

  getSelectedCategory(categories, category) {
    if (validate.isObject(category)) {
      return categories.find(i => i.id === parseInt(category.id, 10)) || {};
    }
    return categories.find(i => i.id === parseInt(category, 10)) || {};
  }

  render() {
    const { entity, onChange } = this.props;
    // extract category and category schema from entity
    const categories = (entity.department) ? this.getDepartmentCategories(entity.department) : [];
    const category = entity.category || {};
    const selectedCategory = this.getSelectedCategory(categories, category);

    const subcategories = selectedCategory.subcategories || [];
    const template = selectedCategory.template || {};

    const selectCategoryTemplate = {
      fields: [
        {
          key: 'department',
          label: 'Department',
          type: Fields.Types.SELECT,
          collection: 'DEPARTMENTS',
          editable: true,
          required: true,
        },
        {
          key: 'category',
          label: 'Category',
          type: Fields.Types.SELECT,
          options: [...categories],
          editable: true,
          required: true,
        },
        {
          key: 'subcategories',
          label: 'Subcategories',
          type: Fields.Types.SELECT,
          options: [...subcategories],
          editable: true,
          multiple: true,
        },
      ],
    };

    const selectCategoryForm = (
      <FormGenerator
        template={selectCategoryTemplate}
        entity={entity}
        onChange={onChange}
      />
    );

    // if a category has been chosen
    // and the category defines a
    // template, create a form for
    // the newly added template (aka schema)
    const categoryTemplateForm = (template.fields && template.fields.length > 0) ? (
      <FormGenerator
        template={template}
        entity={entity.categoryCustomData}
        onChange={data => onChange({ ...entity, categoryCustomData: data })}
      />
  ) : '';

    return (
      <div>
        {selectCategoryForm}
        {categoryTemplateForm}
      </div>
    );
  }
}

CategoryTab.propTypes = {
  entity: PropTypes.shape().isRequired,
  others: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  collections: PropTypes.shape(),
};

const mapStateToProps = state => ({
  collections: {
    DEPARTMENTS: state.departments.departments,
  },
});

export default connect(mapStateToProps, null)(CategoryTab);
