import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import validate from 'validate.js';
import _find from 'lodash/find';

import FormGenerator from '../../form-generator';
import Fields from '../../form-generator/fields';
import ConnectorList from '../../tag-list';

import * as historyActions from '../../../redux/actions/history-actions';

/**
 * ConnectorTab is an Entity Modal tab for CONNECTOR.
 *
 */
class ConnectorTab extends Component {

  /**
   *
   * @returns {boolean}
   */
  componentWillMount() {
    const { entity } = this.props;

    // return false if there are no connectors to get history for
    if (!entity || !entity.connectors || entity.connectors.length === 0) return false;

    this.fetchTelemetryHistory(entity);
    return true;
  }

  fetchTelemetryHistory(entity) {
    const { getTelemetryHistoryData } = this.props.actions;
    entity.connectors.forEach((connector) => {
      if (!validate.isEmpty(connector.data)) {
        const connectorTelemetry = _find(connector.data, ['eventType', 'telemetry']);
        if (connectorTelemetry) {
          getTelemetryHistoryData(entity.id, connectorTelemetry.type, connector.id);
        }
      }
    });

    return true;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { onChange, entity, onValidate, template, history } = this.props;

    const selectConnectorTemplate = {
      fields: [{
        key: 'connectors',
        label: 'Connectors',
        description: 'Select one or more connectors',
        type: Fields.Types.SELECT,
        multiple: true,
        editable: true,
        collection: 'CONNECTORS',
      }],
    };

    const selectConnectorForm = (
      <FormGenerator
        template={selectConnectorTemplate}
        entity={entity}
        onChange={onChange}
        onValidate={onValidate}
      />
    );

    return (
      <div className="general">
        {selectConnectorForm}
        <ConnectorList tags={entity.connectors} template={template} history={history} />
      </div>
    );
  }
}

ConnectorTab.propTypes = {
  entity: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  history: PropTypes.shape(),
  onValidate: PropTypes.func,
  template: PropTypes.shape(),
  actions: PropTypes.shape(),
};

const mapStateToProps = state => ({
  template: state.templates.connectors,
  history: state.history.telemetry,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(historyActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConnectorTab);
