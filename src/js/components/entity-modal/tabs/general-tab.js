import React from 'react';
import PropTypes from 'prop-types';

import FormGenerator from '../../form-generator';

/**
 *
 * @param template
 * @param entity
 * @param updateEntity
 * @returns {XML}
 * @constructor
 */
class GeneralTab extends React.Component {
  render() {
    const { template, entity, updateEntity } = this.props;
    const generalFields = template.fields.filter(f => f.general);
    const generalTemplate = { ...template, fields: generalFields };
    return (
      <FormGenerator
        template={generalTemplate}
        entity={entity}
        onChange={updateEntity}
      />
    );
  }
}

GeneralTab.propTypes = {
  template: PropTypes.shape(),
  entity: PropTypes.shape().isRequired,
  updateEntity: PropTypes.func.isRequired,
};

export default GeneralTab;
