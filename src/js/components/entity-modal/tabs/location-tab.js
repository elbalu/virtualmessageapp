import React, { PropTypes } from 'react';
import validate from 'validate.js';

import FormGenerator from '../../form-generator';
import Fields from '../../form-generator/fields';

const LocationTab = ({ entity, onChange, others = {} }) => {
  let xyDisabled = true;
  if (!validate.isEmpty(entity.x) || !validate.isEmpty(entity.y)) {
    xyDisabled = false;
  } else if ('xyDisabled' in others) {
    xyDisabled = others.xyDisabled;
  }

  const locationTemplate = {
    fields: [
      {
        key: 'static',
        label: 'Static',
        type: Fields.Types.CHECKBOX,
        editable: true,
        required: false,
      },
      {
        key: 'staticLocation',
        label: 'Static Floor',
        type: Fields.Types.SELECT,
        collection: 'FLOORS',
        editable: true,
        required: false,
      },
      {
        key: 'x',
        label: 'Static X',
        type: Fields.Types.NUMBER,
        editable: true,
        required: false,
        disabled: xyDisabled
      },
      {
        key: 'y',
        label: 'Static Y',
        type: Fields.Types.NUMBER,
        editable: true,
        required: false,
        disabled: xyDisabled
      }
    ],
  };

  const locationForm = (
    <FormGenerator
      template={locationTemplate}
      entity={entity}
      onChange={onChange}
    />
  );

  return (
    <div>
      {
        'Please set sdk as static and select a floor to enable X and Y'
      }
      {locationForm}
    </div>
  );
};

LocationTab.propTypes = {
  entity: PropTypes.shape().isRequired,
  others: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
};

export default LocationTab;
