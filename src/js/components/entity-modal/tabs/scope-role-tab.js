import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import _flatten from 'lodash/flatten';

import FormGenerator from '../../form-generator';
import * as template from '../../../../resources/schemas/roles.json';


class ScopeRoleTab extends Component {
  render() {
    const { entity, onChange, collections } = this.props;
    let categories = [];
    let departments = (entity.departments === 'ALL') ? 'ALL' : (entity.departments || []);
    if (departments === 'ALL') {
      departments = collections.DEPARTMENTS;
    } else if (departments.length > 0) {
      // get selected department entities from collection
      const deptIds = departments.map(d => d.id);
      departments = collections.DEPARTMENTS.filter((d) => {
        return deptIds.includes(d.id);
      });
    }

    // get the categories from selected departments and populate
    // the categories field in template
    if (departments.length > 0) {
      const categoryArr = departments.map((d) => {
        const deptCategories = Object.assign([], d.categories);
        return deptCategories;
      });
      categories = _flatten(categoryArr);
    }

    // get categories field from template and append options
    // to that field
    const fields = template.fields;
    const categoryField = fields.find(i => i.key === 'categories');
    categoryField.options = categories;

    const scopeRoleTemplate = {
      fields
    };

    const roleForm = (
      <FormGenerator
        template={scopeRoleTemplate}
        entity={entity}
        onChange={onChange}
      />
    );

    return (
      <div>
        {roleForm}
      </div>
    );
  }

}

const mapStateToProps = state => ({
  collections: {
    DEPARTMENTS: state.departments.departments,
  },
});

ScopeRoleTab.propTypes = {
  entity: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  collections: PropTypes.shape(),
};

export default connect(mapStateToProps, null)(ScopeRoleTab);
