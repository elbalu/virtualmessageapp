import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import validate from 'validate.js';
import _find from 'lodash/find';

import FormGenerator from '../../form-generator';
import Fields from '../../form-generator/fields';
import TagList from '../../tag-list';

import * as historyActions from '../../../redux/actions/history-actions';

/**
 * TagTab is an Entity Modal tab that allows users to see
 * which tags are assigned to an sdk (or other entity) and assign
 * or unassign tags.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class TagTab extends Component {

  /**
   *
   * @returns {boolean}
   */
  componentWillMount() {
    const { entity } = this.props;

    // return false if there are no tags to get history for
    if (!entity || !entity.tags || entity.tags.length === 0) return false;

    this.fetchTelemetryHistory(entity);
    return true;
  }

  fetchTelemetryHistory(entity) {
    const { getTelemetryHistoryData } = this.props.actions;
    entity.tags.forEach((tag) => {
      if (!validate.isEmpty(tag.data)) {
        const tagTelemetry = _find(tag.data, ['eventType', 'telemetry']);
        if (tagTelemetry) {
          getTelemetryHistoryData(entity.id, tagTelemetry.type, tag.id);
        }
      }
    });

    return true;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { onChange, entity, onValidate, template, history } = this.props;

    const selectTagTemplate = {
      fields: [{
        key: 'tags',
        label: 'Tags',
        description: 'Select one or more tags',
        type: Fields.Types.SELECT,
        multiple: true,
        editable: true,
        collection: 'TAGS',
      }],
    };

    const selectTagForm = (
      <FormGenerator
        template={selectTagTemplate}
        entity={entity}
        onChange={onChange}
        onValidate={onValidate}
      />
    );

    return (
      <div className="general">
        {selectTagForm}
        <TagList tags={entity.tags} template={template} history={history} />
      </div>
    );
  }
}

TagTab.propTypes = {
  entity: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  history: PropTypes.shape(),
  onValidate: PropTypes.func,
  template: PropTypes.shape(),
  actions: PropTypes.shape(),
};

const mapStateToProps = state => ({
  template: state.templates.tags,
  history: state.history.telemetry,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(historyActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TagTab);
