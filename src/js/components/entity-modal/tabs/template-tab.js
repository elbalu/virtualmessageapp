import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import equal from 'deep-equal';
import config from '../../../config';

import TemplateBuilder from '../../form-builder';

import * as templatesActions from '../../../redux/actions/templates-actions';

const entityNames = config.get('entityNames');

/**
 *
 */
class TemplateTab extends Component {
  /**
   * @override
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = { template: props.template };
    this.addField = this.addField.bind(this);
    this.updateField = this.updateField.bind(this);
    this.removeField = this.removeField.bind(this);
  }

  /**
   * @override
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (!_get(nextProps, 'template.fields')) return;

    if (!equal(this.state.template, nextProps.template)) {
      this.setState({ template: nextProps.template });
    }
  }

  /**
   * adds a field to entity.template
   *
   * @param {Object} field
   */
  addField(field) {
    const { onChange, category } = this.props;
    const { template } = this.state;
    const fields = template.fields;

    // add field to template
    const nextFields = [...fields, templatesActions.formatNewField(field, category)];
    const nextTemplate = { ...template, fields: nextFields };

    this.setState({ template: nextTemplate }, () => {
      onChange(nextTemplate);
    });
  }

  /**
   * updates a field in entity.category.template
   *
   * @param {Object} field
   */
  updateField(field) {
    const { onChange } = this.props;
    const { template } = this.state;
    const fields = template.fields;

    // add field to template
    const nextFields = fields.map((f) => {
      if (f.key === field.key) return { ...field };
      return f;
    });
    const nextTemplate = { ...template, fields: nextFields };

    this.setState({ template: nextTemplate }, () => {
      onChange(nextTemplate);
    });
  }

  /**
   * removes a field in entity.category.template
   *
   * @param {Object} field
   */
  removeField(field) {
    const { onChange } = this.props;
    const { template } = this.state;
    const fields = template.fields;

    // add field to template
    const nextFields = fields.filter(f => f.key !== field.key);
    const nextTemplate = { ...template, fields: nextFields };

    this.setState({ template: nextTemplate }, () => {
      onChange(nextTemplate);
    });
  }

  /**
   * @override
   * @return {XML}
   */
  render() {
    const { template } = this.state;

    return (
      <TemplateBuilder
        customFieldsOnly
        template={template}
        actions={{
          add: this.addField,
          edit: this.updateField,
          remove: this.removeField,
        }}
      />
    );
  }
}

TemplateTab.propTypes = {
  template: PropTypes.shape(),
  onChange: PropTypes.func,
  category: PropTypes.shape()
};

TemplateTab.defaultProps = {
  template: {
    fields: [],
    entityName: entityNames.customCategory.key,
  },
};

export default TemplateTab;
