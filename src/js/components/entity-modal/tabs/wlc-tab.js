import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import validate from 'validate.js';
import _find from 'lodash/find';

import FormGenerator from '../../form-generator';
import Fields from '../../form-generator/fields';
import WlcList from '../../tag-list';

import * as historyActions from '../../../redux/actions/history-actions';

/**
 * WlcTab is an Entity Modal tab for WLC.
 *
 */
class WlcTab extends Component {

  /**
   *
   * @returns {boolean}
   */
  componentWillMount() {
    const { entity } = this.props;

    // return false if there are no wlcs to get history for
    if (!entity || !entity.wlcs || entity.wlcs.length === 0) return false;

    this.fetchTelemetryHistory(entity);
    return true;
  }

  fetchTelemetryHistory(entity) {
    const { getTelemetryHistoryData } = this.props.actions;
    entity.wlcs.forEach((wlc) => {
      if (!validate.isEmpty(wlc.data)) {
        const wlcTelemetry = _find(wlc.data, ['eventType', 'telemetry']);
        if (wlcTelemetry) {
          getTelemetryHistoryData(entity.id, wlcTelemetry.type, wlc.id);
        }
      }
    });

    return true;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { onChange, entity, onValidate, template, history } = this.props;

    const selectWlcTemplate = {
      fields: [{
        key: 'wlcs',
        label: 'Wlcs',
        description: 'Select one or more wlcs',
        type: Fields.Types.SELECT,
        multiple: true,
        editable: true,
        collection: 'WLCS',
      }],
    };

    const selectWlcForm = (
      <FormGenerator
        template={selectWlcTemplate}
        entity={entity}
        onChange={onChange}
        onValidate={onValidate}
      />
    );

    return (
      <div className="general">
        {selectWlcForm}
        <WlcList tags={entity.wlcs} template={template} history={history} />
      </div>
    );
  }
}

WlcTab.propTypes = {
  entity: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  history: PropTypes.shape(),
  onValidate: PropTypes.func,
  template: PropTypes.shape(),
  actions: PropTypes.shape(),
};

const mapStateToProps = state => ({
  template: state.templates.wlcs,
  history: state.history.telemetry,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(historyActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(WlcTab);
