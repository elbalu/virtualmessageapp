import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

/**
 * Action is a single link that performs
 * an action when clicked. Entity Widget has
 * a section for actions that can be performed
 * on the entity to which the page is dedicated.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Action extends React.Component {
  render() {
    const { className, action, children, extra } = this.props;

    return (
      <div className={`action ${className}`}>
        <div className="content">
          <a onClick={action}>
            {children}
          </a>
        </div>
        <div className="extra">
          {extra}
        </div>
      </div>
    );
  }
}

Action.propTypes = {
  className: PropTypes.string,
  action: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  extra: PropTypes.element
};

Action.defaultProps = {
  className: '',
  action: _.identity,
  children: null,
  extra: null
};

export default Action;
