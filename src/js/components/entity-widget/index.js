import React, { PropTypes, Component } from 'react';
import { injectIntl, intlShape, FormattedNumber, FormattedMessage } from 'react-intl';
import { Upload, Popover, Button, Checkbox, Modal } from 'antd';

import _ from 'lodash';
import pluralize from 'pluralize';

import Notification from '../../misc/models/notification';
import Action from './action';
import * as UserUtils from '../../misc/user-utils';
import * as ImportUtils from '../../misc/import-utils';
import url from '../../misc/url';

const initialState = {
  uploadOptionsVisibility: false,
  overwrite: true,
};
/**
 * EntityWidget is a react component that gives
 * a summary and quick actions for the entity to which
 * a page is dedicated.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class EntityWidget extends Component {
  /**
   * EntityWidget#downloadTemplateFile directs the browser to
   * download the file that contains the sdk template
   */
  static downloadTemplateFile() {
    global.window.location.href = `${url.download}?authtoken=${UserUtils.getToken()}`;
  }

  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.toggleUploadOptionsVisibility = this.toggleUploadOptionsVisibility.bind(this);
  }

  toggleUploadOptionsVisibility(visible) {
    this.setState({ uploadOptionsVisibility: visible });
  }

  toggleCheckBox(e) {
    this.setState({ overwrite: e.target.checked });
  }

  /**
   * upload overwrite button renderer
   */
  uploadOverwriteButton() {
    const { reloadEntities, entityType, displayNotification } = this.props;
    // handler that is called when upload is finished
    const onUpload = (info) => {

      // close the upload options Popover
      this.toggleUploadOptionsVisibility(false);

      // // parseUploadResponse will display an error message
      // // if upload failed. otherwise it does nothing
      ImportUtils.parseUploadResponse(info.file.response);

      // reload data if upload is done
      if (info.file.status === 'done') {
        setTimeout(() => reloadEntities(), 900);
      }

      // display error to user
      if (info.file.status === 'error') {
        displayNotification(Notification.fromError(info.file.response));
      }
    };

    const authHeader = `JWT ${UserUtils.getToken()}`;

    return (<Upload
      name="file"
      action={url.import[entityType]}
      accept={(entityType === 'maps') ? '.gz' : 'text/csv'}
      showUploadList={false}
      headers={{
        authorization: authHeader,
        tenantid: UserUtils.getTenantId()
      }}
      data={{
        overwrite: this.state.overwrite
      }}
      scope={this}
      onChange={onUpload}
    >
      <Button size="small" type="primary"><FormattedMessage id="template.import" /></Button>
    </Upload>);
  }

  /**
   * renderer for upload overwrite
   * @param props
   */
  uploadOverwriteContent() {
    return (
      <div className="upload-options">
        <Checkbox
          className="overwrite-option"
          checked={this.state.overwrite}
          onChange={e => this.toggleCheckBox(e)}
        >
          <FormattedMessage id="sdk.overwite.text" />
        </Checkbox>
        <div className="text-center">
          {this.uploadOverwriteButton()}
        </div>
      </div>
    );
  }

  /**
   * customActions returns custom action components
   *
   * @return {XML|null}
   */
  customActions(customActions) {
      // if customActions were given, render each action
      // into its own column
      const customActionComponents = !_.isEmpty(customActions) ? (
          customActions.map((a, i) => <Action key={i} action={a().props.onClick} className={a().props.attachClass}>
              {a()}
              <span>{a().props.label}</span>
          </Action>)
      ) : null;
      return customActionComponents;
  }

  /**
   * EntityWidget#actions returns a list of actions
   * that can be performed based on user permissions.
   *
   * @return {XML}
   */
  actions() {
    const { reloadEntities, entityType, add, displayNotification,
      invite, uploadOverwrite, customActions } = this.props;
    const addAccess = UserUtils.hasAccess(entityType, 'add');
    const buttons = [];

    // add entity button. User must have add access.
    // This button is hidden for maps because there is
    // a different workflow for uploading maps
    if (addAccess && entityType !== 'maps') {
      buttons.push(
        <Action action={add} className="create">
          <i className="fa fa-plus" title="create" />
          {invite ? <FormattedMessage id="invite" /> : <FormattedMessage id="add" />}
        </Action>
      );
    }

    buttons.push(this.customActions(customActions));

    // upload entities and download templates actions.
    // User must have add access and this EntityWidget
    // must be able to reload the entities on the page.
    // The upload button is available on the maps page,
    // but the download template button is hidden
    if (addAccess && _.isFunction(reloadEntities)) {
      // handler that is called when upload is finished
      const onUpload = (info) => {
        // // parseUploadResponse will display an error message
        // // if upload failed. otherwise it does nothing
        ImportUtils.parseUploadResponse(info.file.response);

        // reload data if upload is done
        if (info.file.status === 'done') {
          setTimeout(() => reloadEntities(), 900);
        }

        // display error to user
        if (info.file.status === 'error') {
          displayNotification(Notification.fromError(info.file.response));
        }
      };

      // download template link. This link is not
      // available on the maps page
      const templateLink = (entityType !== 'maps') ? (
        <a onClick={EntityWidget.downloadTemplateFile} hidden={entityType === 'maps'}>
          <FormattedMessage id="template.download" />
        </a>
      ) : null;

      const authHeader = `JWT ${UserUtils.getToken()}`;

      const mapConditional = (entityType === 'maps') ? (<Upload
            name="mapFile"
            action={url.import[entityType]}
            accept={ '.gz' }
            showUploadList={false}
            headers={{
                authorization: authHeader,
                tenantid: UserUtils.getTenantId(),
              }}
            scope={this}
            onChange={onUpload}
        >
          <i className="fa fa-cloud-upload" title="upload" />
          <FormattedMessage id="template.import" />
        </Upload>) : (<Upload
            name="file"
            action={url.import[entityType]}
            accept={ 'text/csv' }
            showUploadList={false}
            headers={{
                authorization: authHeader,
                tenantid: UserUtils.getTenantId(),
              }}
            scope={this}
            onChange={onUpload}
        >
          <i className="fa fa-cloud-upload" title="upload" />
          <FormattedMessage id="template.import" />
        </Upload>);

      // upload entities button
      // if uploadOverwrite true then show the overwrite options
      // vefore upload, if not show upload directly
      const uploadButton = uploadOverwrite ? (
        <Action className="upload" extra={templateLink}>
          <Popover
            content={this.uploadOverwriteContent()}
            trigger="click"
            placement="right"
            visible={this.state.uploadOptionsVisibility}
            onVisibleChange={this.toggleUploadOptionsVisibility}
          >
            <i className="fa fa-cloud-upload" title="upload" />
            <FormattedMessage id="template.import" />
          </Popover>
        </Action>
      ) : (
        <Action className="upload" extra={templateLink}>
          { mapConditional }
        </Action>
      );

      buttons.push(uploadButton);
    }

    return (
      <ul>
        {buttons.map((a, i) => (
          <li key={i}>{a}</li>
        ))}
      </ul>
    );
  }

  render() {
    const { icon, title, entities } = this.props;
    const count = _.size(entities);
    const pluralizedTitle = pluralize(title, count);

    return (
      <div className="entity-widget-wrapper">
        <div className="entity-widget">
          <div className="icon">
            <span className="fa-stack">
              <i className="fa fa-circle fa-stack-2x" />
              <i className={`${icon} fa-stack-1x`} />
            </span>
          </div>

          <div className="title">
            <div className="ew-count">
              <h1><FormattedNumber value={count} /></h1>
            </div>
            <div className="ew-title"><h5>{pluralizedTitle}</h5></div>
          </div>

          <div className="actions">
            {this.actions()}
          </div>
        </div>
      </div>
    );
  }
}


EntityWidget.propTypes = {
  icon: PropTypes.string,
  invite: PropTypes.bool,
  title: PropTypes.string.isRequired,
  entities: PropTypes.instanceOf(Array),
  add: PropTypes.func,
  customActions: PropTypes.arrayOf(PropTypes.func),
  entityType: PropTypes.string.isRequired,
  reloadEntities: PropTypes.func,
  displayNotification: PropTypes.func,
  uploadOverwrite: PropTypes.bool,

  // i18n support
  intl: intlShape,
};

EntityWidget.defaultProps = {
  icon: '',
  title: '',
  entities: [],
  customActions: [],
  uploadOverwrite: false
};

export default injectIntl(EntityWidget);
