import React from 'react';
import { Button } from 'antd';

import UserSnapWidget from '../../misc/usersnap-widget';

/**
 * Feedback is a React component that
 * renders a single button. When that button
 * is clicked, the usersnap feedback widget opens.
 * This component could be expanded in the future
 * to incldue other feedback libraries or enable
 * use of multiple usersnap widgets.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Feedback extends React.Component {
  /**
   * Feedback.open uses the UserSnap
   * JavaScript API to open the
   * feedback modal.
   */
  static open() {
    UserSnapWidget.open();
  }

  /**
   * Feedback#render returns a single button.
   *
   * @override
   * @return {XML}
   */
  render() {
    return (
      <div className="feedback clickable" onClick={Feedback.open}>
        <span className="icon fa fa-comment" />
        <span className="label">Feedback</span>
      </div>
    );
  }
}

export default Feedback;

