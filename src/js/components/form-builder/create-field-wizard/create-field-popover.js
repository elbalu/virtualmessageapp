import React, { PropTypes } from 'react';
import { Popover } from 'antd';

const CreateFieldPopover = ({ children, visible, hide, title, prompt }) => {
  const wrappedContent = <div>{children}</div>;

  return (
    <Popover
      title={title}
      trigger="click"
      visible={visible}
      onVisibleChange={hide}
      content={wrappedContent}
      placement="bottom"
    >
      <span className="add-field-prompt clickable">{prompt}</span>
    </Popover>
  );
};

CreateFieldPopover.propTypes = {
  title: PropTypes.string,
  prompt: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.instanceOf(Array),
  ]).isRequired,
  hide: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
};

export default CreateFieldPopover;
