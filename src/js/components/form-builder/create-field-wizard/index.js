import React, { PropTypes, Component } from 'react';
import { Button, Tabs } from 'antd';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';

import Notification from '../../../misc/models/notification';
import CreateFieldPopover from './create-field-popover';
import FormGenerator from '../../form-generator';

import Fields from '../../form-generator/fields';
import * as FormUtils from '../../../misc/form-utils';
import * as notificationConstants from '../../../redux/constants/notification-constants';

const TabPane = Tabs.TabPane;

const initialState = {
  visible: false,
  type: null,
  field: {},
};

/**
 * CreateFieldWizard allows users to create a field and
 * add it to the template that they are building or
 * editing (template/schema)
 *
 * @author: Benjamin Newcomer
 */
class CreateFieldWizard extends Component {
  /**
   * constructor. bind methods that will be called
   * by the DOM.
   *
   * @param props
   */
  constructor(props) {
    super(props);

    this.state = { ...initialState };

    this.setFieldType = this.setFieldType.bind(this);
    this.updateField = this.updateField.bind(this);
    this.submitField = this.submitField.bind(this);
    this.handleVisibleChange = this.handleVisibleChange.bind(this);
    this.reset = this.reset.bind(this);
    this.hide = this.hide.bind(this);
  }

  /**
   * setFieldType stores the field type chosen
   * from a list of field types
   *
   * @param {string} nextType see Fields.Types
   */
  setFieldType(nextType) {
    const { type } = this.state;

    // if type has changed, reset field state
    if (type !== nextType) {
      this.setState({ field: initialState.field });
    }

    // set state for new field type
    this.setState({ type: nextType });
  }

  /**
   * hide() closes the modal and reset
   * the field form
   */
  hide() {
    this.setState({ visible: false }, () => this.reset());
  }

  /**
   * reset() resets the state
   * by erasing the chosen field type
   * and field. Because this reset is used
   * in a render method, it must have a quiet
   * mode (that does not trigger a render) to
   * avoid an infinite loop. triggerRender = false
   * will not trigger a render.
   *
   * @param {boolean} triggerRender True if reset()
   *  should trigger a state transition
   */
  reset(triggerRender = true) {
    const resetState = Object.assign({}, this.state, {
      field: initialState.field,
      type: initialState.type,
    });

    if (triggerRender) this.setState(resetState);
    else this.state = resetState;
  }

  /**
   * updateField stores updated field
   * info passed up from the field form
   *
   * @param updatedField
   */
  updateField(updatedField) {
    this.setState({ field: { ...updatedField } });
  }

  /**
   * Finish generating the field definition
   * and submit it
   */
  submitField() {
    const { field, type } = this.state;
    const { add, entityName, intl, displayNotification } = this.props;
    const fieldComponent = Fields[type];
    const template = fieldComponent.config.template;

    const validationErrors = FormUtils.validateEntity(field, template);

    // show validation errors
    if (validationErrors.length > 0) {
      const title = intl.formatMessage({ id: 'form.errors.prompt' });
      return displayNotification(Notification.fromValidationErrors(validationErrors, title));
    }

    // set field.type = this.state.type
    // set validation type
    // set entity name
    field.type = type;
    field.validation = fieldComponent.config.defaultValidation;
    field.entityName = entityName;

    this.reset();
    this.hide();
    return add(field);
  }

  /**
   *
   * @param visible
   */
  handleVisibleChange(visible) {
    if (!visible) this.hide();
    else this.setState({ visible });
  }

  /**
   * renderFieldTypeOptions renders a radio group
   * of field types that can be created
   *
   * @returns {XML}
   */
  renderFieldTypeOptions() {
    return (
      <ul className="field-type-list">
        {
          Object.keys(Fields)
          .filter(type => Fields[type].config.isUserDefinable)
          .map((type) => {
            const { label, icon } = Fields[type].config;
            return (
              <li key={type} onClick={() => this.setFieldType(type)} >
                <span><i className={icon} />{label}</span>
              </li>
            );
          })
        }
      </ul>
    );
  }

  /**
   * renderFieldForm creates a FormGenerator using the
   * template provided by the field type chosen
   *
   * @returns {*}
   */
  renderFieldForm() {
    const { field, type } = this.state;

    // field will be null until a
    // field type is chosen from the list
    if (!type) return this.reset(false);

    const fieldComponent = Fields[type];
    const template = fieldComponent.config.template;
    const description = fieldComponent.config.description;

    return (
      <div>
        <p>{description}</p>
        <FormGenerator
          template={template}
          entity={field}
          onChange={this.updateField}
        />
      </div>
    );
  }

  render() {
    const { visible, type } = this.state;
    const { intl, prompt } = this.props;

    // if field type has not been chosen, activeKey = 1,
    // if field type has been chosen, activeKey = 2
    const fieldWasSelected = (type !== null);
    const activeKey = (fieldWasSelected) ? '2' : '1';

    const label = (fieldWasSelected) ? Fields[type].config.label : null;
    const title = (fieldWasSelected) ?
      intl.formatMessage({ id: 'fields.add.prompt.withLabel' }, { label }) :
      intl.formatMessage({ id: 'fields.add.prompt' });

    return (
      <CreateFieldPopover
        title={title}
        visible={visible}
        hide={this.handleVisibleChange}
        prompt={prompt}
      >
        <Tabs activeKey={activeKey} className="create-field-wizard">

          <TabPane tab={null} key="1" disabled={activeKey !== '1'}>
            {this.renderFieldTypeOptions()}
            <div className="buttons">
              <Button type="default" onClick={this.hide}>
                <FormattedMessage id="cancel" />
              </Button>
            </div>
          </TabPane>

          <TabPane tab={null} key="2" disabled={activeKey !== '2'}>
            {this.renderFieldForm()}
            <div className="buttons">
              <Button type="primary" onClick={this.submitField}>
                <FormattedMessage id="add" />
              </Button>
              <Button type="default" onClick={this.reset}>
                <FormattedMessage id="back" />
              </Button>
            </div>
          </TabPane>

        </Tabs>
      </CreateFieldPopover>
    );
  }
}

CreateFieldWizard.propTypes = {
  add: PropTypes.func,
  entityName: PropTypes.string,
  prompt: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),

  displayNotification: PropTypes.func,

  // i18n support
  intl: intlShape,
};

export default injectIntl(CreateFieldWizard);
