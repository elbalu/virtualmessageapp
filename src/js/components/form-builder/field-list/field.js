import React from 'react';
import { Collapse } from 'antd';

import { FormattedMessage } from 'react-intl';
import validate from 'validate.js';

import Fields from '../../form-generator/fields';
import FormGenerator from '../../form-generator';
import Logger from '../../../misc/logger';

const Panel = Collapse.Panel;

/**
 * Field is a class that represents one field in
 * the Field List component. It returns a collapse panel with
 * a form (custom fields only) for editing and deleting fields.
 *
 * @param field
 * @param update
 * @param remove
 * @return {XML}
 * @constructor
 */
class Field {

  /**
   * @constructor
   * @param {Object} field
   * @param {function} update
   * @param {function} remove
   * @return {XML}
   */
  static render(field, { update, remove }) {
    if (field.isDefault) return Field.renderDefaultFieldPanel(field);
    return Field.renderCustomFieldPanel(field, { update, remove });
  }

  /**
   * returns a panel that display information about
   * a default field.
   *
   * @param {Object} field
   * @return {XML}
   */
  static renderDefaultFieldPanel(field) {
    const { key, label, required, type } = field;
    const fieldComponent = Fields[type];

    if (!validate.isDefined(fieldComponent)) return null;

    const icon = fieldComponent.config.icon;
    const typeLabel = fieldComponent.config.label;
    const requiredString = (required) ?
      <FormattedMessage id="yes" /> :
      <FormattedMessage id="no" />;

    const panelHeader = (
      <div>
        <i className={icon} />
        <span className="header">{label}</span>
      </div>
    );

    return (
      <Panel key={key} header={panelHeader}>
        <p className="right">
          <FormattedMessage id="fields.editDefault.prompt" />
        </p>
        <p className="field-type">
          <FormattedMessage id="fields.type.label" values={{ type: typeLabel }} />
        </p>
        <p className="field-required">
          <FormattedMessage id="fields.required" values={{ required: requiredString }} />
        </p>
      </Panel>
    );
  }

  /**
   * render a panel that display information and gives actions
   * allowing a user to edit or remove a custom field.
   *
   * @param {Object} field
   * @param {function} update
   * @param {function} remove
   * @return {XML}
   */
  static renderCustomFieldPanel(field, { update, remove }) {
    const { key, label, required, type } = field;
    const fieldComponent = Fields[type];


    if (!validate.isDefined(fieldComponent)) {
      Logger.error('TagList renderPanel', `field ${type} not found`);
      return null;
    }

    const icon = fieldComponent.config.icon;
    const template = fieldComponent.config.template;

    const requiredString = (required) ?
      <FormattedMessage id="yes" /> :
      <FormattedMessage id="no" />;

    const panelHeader = (
      <div>
        <i className={icon} />
        <span className="header">{label}</span>
        <i className="fa fa-close clickable remove-field-button" onClick={() => remove(field)} />
      </div>
    );

    return (
      <Panel key={key} header={panelHeader}>
        <p className="field-type">
          <FormattedMessage id="fields.type.label" values={{ type }} />
        </p>
        <p className="field-required">
          <FormattedMessage id="fields.required" values={{ required: requiredString }} />
        </p>
        <FormGenerator
          template={template}
          entity={field}
          onChange={update}
        />
      </Panel>
    );
  }
}

export default Field;
