import React, { PropTypes, Component } from 'react';
import { Collapse } from 'antd';
import { FormattedMessage } from 'react-intl';

import Field from './field';

class FieldList extends Component {
  /**
   * renderCollapse() renders a collapse
   * component with panels based on input
   * fields array
   *
   * @param {Array} fields
   * @returns {XML}
   */
  renderCollapse(fields) {
    const { update, remove } = this.props;

    if (fields.length === 0) return null;

    return (
      <Collapse accordion className="field-list">
        {fields.map(field => Field.render(field, { update, remove }))}
      </Collapse>
    );
  }

  /**
   * @override
   * @return {*}
   */
  render() {
    const { fields, customFieldsOnly } = this.props;

    if (fields.length === 0) return null;

    const defaultFields = fields.filter(f => f.isDefault && !f.hidden && f.editable);
    const customFields = fields.filter(f => !f.isDefault);

    const defaultFieldsSection = (
      <div className="field-list-wrapper" hidden={customFieldsOnly}>
        <h3 className="header">
          <FormattedMessage id="fields.default.title" />
        </h3>
        {this.renderCollapse(defaultFields)}
      </div>
    );

    const customFieldsSection = (
      <div className="field-list-wrapper last">
        <h3 className="header" hidden={customFieldsOnly}>
          <FormattedMessage id="fields.custom.title" />
        </h3>
        {this.renderCollapse(customFields)}
      </div>
    );

    return (
      <div className="field-list-group">
        {defaultFieldsSection}
        {customFieldsSection}
      </div>
    );
  }
}

FieldList.propTypes = {
  fields: PropTypes.instanceOf(Array),
  customFieldsOnly: PropTypes.bool,
  remove: PropTypes.func,
  update: PropTypes.func,
};

FieldList.defaultProps = {
  customFieldsOnly: false,
};

export default FieldList;
