import React, { Component, PropTypes } from 'react';

import { FormattedMessage } from 'react-intl';

import FieldList from './field-list';
import CreateFieldWizard from './create-field-wizard';

/**
 * TemplateBuilder is a composite component
 * that provides a workflow for users to create
 * new templates or edit existing ones.
 *
 * @author: Benjamin Newcomer
 */
class TemplateBuilder extends Component {
  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { template, customFieldsOnly, displayNotification } = this.props;
    const { add, edit, remove } = this.props.actions;

    const prompt = <FormattedMessage id="fields.none.prompt" />;
    const fields = template.fields || [];

    return (
      <div className="form-builder">
        <div className="field-list-wrapper">
          <FieldList
            fields={fields}
            customFieldsOnly={customFieldsOnly}
            update={edit}
            remove={remove}
          />
        </div>
        <div className="create-field-wizard-wrapper">
          <CreateFieldWizard
            add={add}
            entityName={template.entityName}
            prompt={prompt}
            displayNotification={displayNotification}
          />
        </div>
      </div>
    );
  }
}

TemplateBuilder.propTypes = {
  customFieldsOnly: PropTypes.bool,
  template: PropTypes.shape(),
  displayNotification: PropTypes.func,
  actions: PropTypes.shape({
    add: PropTypes.func,
    edit: PropTypes.func,
    remove: PropTypes.func,
  }),
};

TemplateBuilder.defaultProps = {
  template: { fields: [] },
};

export default TemplateBuilder;
