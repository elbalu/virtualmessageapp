import React, { PropTypes } from 'react';
import { Cascader } from 'antd';
import config from '../../../config';

const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const cascaderConfig = {
  parse: (value) => {
    if (!value || Object.keys(value).length === 0) return undefined;
    if (value instanceof Array) return value.map(v => this.parse(v));
    if (value instanceof Object) return String(value.id);
    return value;
  },
  template: {},
  label: 'Option Tree',
  icon: 'fa fa-code-fork',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: false,
};

/**
 * Cascader field. This field can be used when a tree
 * of options is presented to the user (like selecting
 * a zone from cmapus > building > floor > zone)
 *
 * @author Benjamin Newcomer
 */
const CascaderField = ({ value, field, onChange }) => (
  <Cascader
    options={field.options}
    changeOnSelect
    onChange={onChange}
    showSearch
    placeholder="Please select"
    size="large"
    defaultValue={cascaderConfig.parse(value)}
  />
);

CascaderField.config = cascaderConfig;

CascaderField.propTypes = {
  field: PropTypes.shape(),
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.instanceOf(Array),
    PropTypes.string,
  ]),
};

CascaderField.defaultProps = {
  value: [],
};

export default CascaderField;
