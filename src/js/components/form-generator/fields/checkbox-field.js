import React, { PropTypes } from 'react';
import { Checkbox } from 'antd';
import validate from 'validate.js';
import config from '../../../config';

const fieldTypes = config.get('fieldTypes');
const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const checkboxConfig = {
  parse: (value) => {
    if (!validate.isDefined(value)) return false;
    return value;
  },
  template: {
    fields: [
      {
        key: 'label',
        label: 'Label',
        required: true,
        editable: true,
        type: fieldTypes.TEXT,
      },
      {
        key: 'description',
        label: 'Help Text',
        description: 'Text that describes this field',
        editable: true,
        type: fieldTypes.TEXT,
      },
      {
        key: 'required',
        label: 'Required',
        type: fieldTypes.CHECKBOX,
        editable: true,
      },
    ],
  },
  label: 'Checkbox',
  icon: 'fa fa-check-square',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: false,
  description: 'A checkbox allows the user to make a binary choice by checking or unchecking the checkbox.',
};

/**
 * Checkbox field
 *
 * @author Benjamin Newcomer
 */
const CheckboxField = ({ value, onChange }) => (
  <Checkbox
    onChange={e => onChange(e.target.checked)}
    checked={checkboxConfig.parse(value)}
  />
);

CheckboxField.config = checkboxConfig;

CheckboxField.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default CheckboxField;
