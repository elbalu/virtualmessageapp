import React, { PropTypes } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import config from '../../../config';

const fieldTypes = config.get('fieldTypes');
const validationTypes = config.get('validationTypes');

// static data used when configuring or
// using this field
const dateFieldConfig = {
  parse: (value) => {
    if (!value) return null;
    return moment(value);
  },
  template: {
    fields: [
      {
        key: 'label',
        label: 'Label',
        required: true,
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'description',
        label: 'Help Text',
        description: 'Text that describes this field',
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'required',
        label: 'Required',
        type: fieldTypes.CHECKBOX,
        editable: true,
      },
    ],
  },
  label: 'Date',
  icon: 'fa fa-calendar-o',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: true,
  description: 'A date field allows the user to select a date which includes day, month, and year',
  placeholder: 'Click to select a date',
};

/**
 * converts moment object to utc string.
 * If date is not a moment object (usually
 * null), null is returned
 *
 * @param {moment} date
 * @return {null|String}
 */
const dateToStringOrNull = (date) => {
  if (date instanceof moment) return date.utc().format();
  return null;
};

/**
 * DateField is a React component that provides
 * a date picker and accepts value and onChange props
 * to connect to a form.
 *
 * @param value
 * @param onChange
 * @param field
 * @constructor
 */
const DateField = ({ value, field, onChange }) => (
  <DatePicker
    isClearable
    onChange={date => onChange(dateToStringOrNull(date))}
    utcOffset={moment(new Date()).utcOffset()}
    selected={dateFieldConfig.parse(value)}
    placeholderText={field.description || dateFieldConfig.placeholder}
    className="ant-input"
  />
);

DateField.config = dateFieldConfig;

DateField.propTypes = {
  onChange: PropTypes.func,
  field: PropTypes.shape(),
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
  ]),
};

export default DateField;
