import React, { PropTypes } from 'react';
import moment from 'moment';
import DatePicker from 'antd/lib/date-picker';
import LocaleProvider from 'antd/lib/locale-provider';
import enUS from 'antd/lib/locale-provider/en_US';
import config from '../../../config';

const validationTypes = config.get('validationTypes');
const dateFormat = config.get('dateFormat');

// staic data used when configuring or
// using this field
const datetimeConfig = {
  parse: value => moment(value),
  template: {},
  label: 'Datetime',
  icon: 'fa fa-clock-o',
  defaultValidation: validationTypes.DATETIME,
  isUserDefinable: false,
  description: 'A Datetime field allows the user to select a date and time which includes the day, month, year, hour, and minute.',
};

/**
 * DatetimeField is a React Component that provides date and time
 * pickers and props that take in a datetime and pass up
 * a datetime when a user selects one.
 *
 * @param value
 * @param onChange
 * @constructor
 */
const DatetimeField = ({ value, onChange }) => (
  <div>
    <LocaleProvider locale={enUS} >
      <DatePicker
        showTime
        onChange={onChange}
        format={dateFormat}
        value={datetimeConfig.parse(value)}
      />
    </LocaleProvider>
  </div>
);

DatetimeField.config = datetimeConfig;

DatetimeField.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
  ]),
};

DatetimeField.defaultProps = {
  value: moment(),
};

export default DatetimeField;
