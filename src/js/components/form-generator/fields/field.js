import React, { PropTypes } from 'react';
import { Form } from 'antd';
import _lowerCase from 'lodash/lowerCase';

import Fields from './index';

/**
 * Field wrapper that provides uniform styling
 * including labeling fields and marking fields
 * that are required
 *
 * @author Benjamin Newcomer
 */
const Field = ({ field, value, onChange }) => {
  const fieldType = field.type;
  const fieldComponent = Fields[fieldType];
  const className = `${field.key}-field ${_lowerCase(field.type)}-type-field`;

  return (
    <Form.Item
      className={className}
      label={field.label}
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 14 }}
      required={field.required}
    >
      {React.createElement(fieldComponent, { field, value, onChange })}
    </Form.Item>
  );
};

Field.propTypes = {
  onChange: PropTypes.func,
  field: PropTypes.shape(),
  value: PropTypes.any,
};

export default Field;
