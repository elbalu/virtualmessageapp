import React, { PropTypes } from 'react';
import { Button, Message, Upload } from 'antd';
import config from '../../../config';

import url from '../../../misc/url';

const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const fileConfig = {
  parse: value => value,
  template: {},
  label: 'File Upload',
  icon: 'fa fa-cloud-upload',
  isUserDefinable: false,
  defaultValidation: validationTypes.ANY,
  description: 'A checkbox allows the user to make a binary choice by checking or unchecking the checkbox.',
};

/**
 * FileUploadField is a React Component that provides
 * a file upload widget which allows users to select files
 * from their local file system. Once files are uploaded,
 * the component passes up a list of the uploaded files.
 *
 * @param field
 * @param value
 * @param onChange
 * @returns {XML}
 * @constructor
 */
const FileUploadField = ({ field, value, onChange }) => {
  const fileList = FileUploadField.parse(value);

  const onChangeWrapper = (info) => {
    // check if file upload is done
    if (info.file.status === 'done') {
      fileList.push(info.file);
      onChange(fileList);
      Message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      Message.error(`${info.file.name} file upload failed.`);
    } else if (info.file.status === 'removed') {
      onChange(fileList.filter(f => f.uid !== info.file.uid));
    }
  };

  return (
    <Upload
      name={field.key}
      action={`${url.upload}/${field.path}`}
      listType="picture"
      fileList={fileConfig.parse(value)}
      onChange={onChangeWrapper}
    >
      <Button type="ghost">
        <i className="fa fa-upload" />
        Click to upload
      </Button>
    </Upload>
  );
};


FileUploadField.config = fileConfig;

FileUploadField.propTypes = {
  field: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.instanceOf(Array),
};

FileUploadField.defaultProps = {
  value: [],
};

export default FileUploadField;
