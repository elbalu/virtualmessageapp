import React, { PropTypes, Component } from 'react';
import validate from 'validate.js';
import { Tag } from 'antd';

import { enableSelectedOptions, removeDisabledOptions } from '../../../misc/form-utils';

const CheckableTag = Tag.CheckableTag;

// staic data used when configuring or
// using this field
const selectConfig = {
  parse: (value) => {
    if (!value) return [];
    if (value instanceof Array) return value;
    return [];
  },

};

/**
 * Icon Selector field
 *
 * @author Balu Loganathan
 */
class IconField extends Component {

  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.updateValue = this.updateValue.bind(this);
  }


  /**
   * update value and perform validation
   * @param value
   */
  updateValue(inputValue) {
    const { value, onChange } = this.props;
    const parsedValue = selectConfig.parse(value);

    // if not selected then select the option
    if (parsedValue.indexOf(inputValue) < 0) {
      onChange([...parsedValue, inputValue]);
    } else {
      // if already selected then deselect the option
      parsedValue.splice(parsedValue.indexOf(inputValue), 1);
      onChange(parsedValue);
    }
  }

  /**
   * render a select field with options.
   * is multiple by default
   */
  renderSelector() {
    const { field, value } = this.props;
    const options = field.options;
    let parsedValue = selectConfig.parse(value);

    if (validate.isEmpty(parsedValue)) {
      parsedValue = [];
    }

    // make sure that any options that are selected (currently in
    // value) are not disabled so that they can be deselected.
    // also make sure that selected values appear in the
    // list of options
    let processedOptions = enableSelectedOptions(options, parsedValue);
    processedOptions = removeDisabledOptions(processedOptions);

    return (
      <div>
        {
            processedOptions.map(o => (
              <CheckableTag
                key={o.value}
                checked={parsedValue.indexOf(o.value) > -1}
                onChange={checked => this.updateValue(o.value, checked)}
              >
                <i className={o.iconCls} />
              </CheckableTag>
            ))
          }
      </div>
    );
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { field } = this.props;
    const { options } = field;
    const noneText = 'None';

    // if there are no options, return an empty div
    if (validate.isEmpty(options)) {
      return <div>{noneText}</div>;
    }

    // single select
    return this.renderSelector();
  }
}

IconField.config = selectConfig;

IconField.propTypes = {
  field: PropTypes.shape(),
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Array),
    PropTypes.object,
  ]),
};

IconField.defaultProps = {
  selectAll: false,
};

export default IconField;
