import config from '../../../config';

import TextField from './text-field';
import NumberField from './number-field';
import SelectField from './select-field';
import RadioField from './radio-field';
import CheckboxField from './checkbox-field';
import FileUploadField from './file-field';
import TelField from './tel-field';
import DateField from './date-field';
import OptionsField from './options-field';
import ParagraphField from './paragraph-field';
import PasswordField from './password-field';
import DatetimeField from './datetime-field';
import TreeSelectField from './tree-select-field';
import ResourceField from './resource-field';
import CascaderField from './cascader-field';
import IconField from './icon-field';

const validationTypes = config.get('validationTypes');
const fieldTypes = config.get('fieldTypes');
const validationConstraints = config.get('validationConstraints');

/**
 * Fields is an object that aggregates all form generator
 * fields. It also includes an array of field type constants,
 * validation type constants, and validation constraints that are
 * used by the validate.js library to perform validation.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */

// Object that groups all
// field components by type
const Fields = {
  [fieldTypes.TEXT]: TextField,
  [fieldTypes.NUMBER]: NumberField,
  [fieldTypes.CHECKBOX]: CheckboxField,
  [fieldTypes.SELECT]: SelectField,
  [fieldTypes.RADIO]: RadioField,
  [fieldTypes.FILE]: FileUploadField,
  [fieldTypes.CASCADER]: CascaderField,
  [fieldTypes.TEL]: TelField,
  [fieldTypes.DATE]: DateField,
  [fieldTypes.DATETIME]: DatetimeField,
  [fieldTypes.OPTIONS]: OptionsField,
  [fieldTypes.PARAGRAPH]: ParagraphField,
  [fieldTypes.PASSWORD]: PasswordField,
  [fieldTypes.TREE_SELECT]: TreeSelectField,
  [fieldTypes.LOCATION]: TreeSelectField,
  [fieldTypes.RESOURCES]: ResourceField,
  [fieldTypes.ICON]: IconField,
};
// expose FieldTypes as prop on Fields,
// but make it non-enumerable
Object.defineProperty(Fields, 'Types', {
  value: fieldTypes,
});
// expose field validations as prop on Fields,
// but make it non-enumerable
Object.defineProperty(Fields, 'ValidationTypes', {
  value: validationTypes,
});
// expose validation constraints as prop on Fields,
// but make it non-enumerable
Object.defineProperty(Fields, 'ValidationConstraints', {
  value: validationConstraints,
});

export default Fields;
