import React, { PropTypes } from 'react';
import { InputNumber } from 'antd';
import config from '../../../config';

const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const numberConfig = {
  parse: (value) => {
    if (!value) return null;
    return String(value);
  },
  template: {},
  label: 'Number',
  icon: 'fa fa-calculator',
  defaultValidation: validationTypes.NUMBER,
  isUserDefinable: false,
  description: 'A Number field allows the user to select a number',
};

/**
 * NumberField is a React Component that provides an input optimized
 * for entering numbers. If field.max and field.min are set, these are
 * used to enforce a max and min number on the component.
 *
 * @param field
 * @param value
 * @param onChange
 * @constructor
 */
const NumberField = ({ field, value, onChange }) => {
  return (
    <InputNumber
      max={field.max}
      min={field.min}
      disabled={field.disabled}
      onChange={onChange}
      value={numberConfig.parse(value)}
    />
  );
}

NumberField.config = numberConfig;

NumberField.propTypes = {
  field: PropTypes.shape(),
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default NumberField;
