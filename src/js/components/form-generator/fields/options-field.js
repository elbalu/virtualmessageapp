import React, { PropTypes, Component } from 'react';
import { Input, Tag, Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import config from '../../../config';

const validationTypes = config.get('validationTypes');

const initialState = {
  inputValue: '',
  inputVisible: false,
};

// staic data used when configuring or
// using this field
const optionsConfig = {
  parse: (value) => {
    if (!value) return [];
    if (value instanceof Array) return value;
    return [];
  },
  template: {},
  label: 'Options',
  icon: 'fa fa-list',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: false,
};

/**
 * options field. Used to create arrays
 * of string options
 *
 * @author Benjamin Newcomer
 */
class OptionsField extends Component {

  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.handleInputRef = this.handleInputRef.bind(this);
    this.showInput = this.showInput.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.addOption = this.addOption.bind(this);
    this.removeOption = this.removeOption.bind(this);
  }

  /**
   * stores a reference to the
   * given input
   *
   * @param inputRef
   */
  handleInputRef(inputRef) {
    this.input = inputRef;
  }

  /**
   * show input that allows user to
   * create a new option
   */
  showInput() {
    this.setState({ inputVisible: true }, () => this.input.focus());
  }

  /**
   * updates state.inputValue
   *
   * @param {String} inputValue
   */
  handleInputChange(inputValue) {
    this.setState({ inputValue });
  }

  /**
   * addOption() adds an option to
   * the current array of options
   */
  addOption() {
    const { value, onChange } = this.props;
    const { inputValue } = this.state;
    const noInputValue = !inputValue;
    const duplicateValue = value.indexOf(inputValue) > -1;

    if (!noInputValue && !duplicateValue) {
      onChange([...value, inputValue]);
    }

    this.setState(Object.assign({}, initialState));
  }

  /**
   * removeOption() removes an option from
   * the current array of options
   *
   * @param option string
   */
  removeOption(option) {
    const { value, onChange } = this.props;
    const newValue = value.filter(v => v !== option);
    onChange([...newValue]);
  }

  render() {
    const { value } = this.props;
    const { inputValue, inputVisible } = this.state;
    const maxOptionLength = 10;

    const newTagInput = (
      <Input
        className="options-field-input"
        ref={this.handleInputRef}
        type="text"
        size="small"
        value={inputValue}
        onChange={e => this.handleInputChange(e.target.value)}
        onBlur={this.addOption}
        onPressEnter={this.addOption}
      />
    );

    const newTagButton = (
      <Button
        className="options-field-button"
        size="small"
        type="dashed"
        onClick={this.showInput}
      >
        <FormattedMessage id="addOption" />
      </Button>
    );

    return (
      <div className="options-field">
        {value.map((tag) => {
          const isLongTag = tag.length > maxOptionLength;
          const tagTitle = isLongTag ? `${tag.slice(0, maxOptionLength)}...` : tag;

          return (
            <Tag
              closable
              title={tag}
              key={tag}
              afterClose={() => this.removeOption(tag)}
            >
              {tagTitle}
            </Tag>
          );
        })}
        {(inputVisible) ? newTagInput : newTagButton}
      </div>
    );
  }
}

OptionsField.config = optionsConfig;

OptionsField.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.instanceOf(Array),
  field: PropTypes.shape(),
};

OptionsField.defaultProps = {
  value: [],
};

export default OptionsField;
