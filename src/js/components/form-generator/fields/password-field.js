import React, { PropTypes, Component } from 'react';
import { Input } from 'antd';
import config from '../../../config';

const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const passwordConfig = {
  parse: (value) => {
    if (!value) return null;
    return String(value);
  },
  template: {},
  label: 'Password',
  icon: 'fa fa-font',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: false,
};

/**
 * Password input field
 *
 * @author Benjamin Newcomer
 */
class PasswordField extends Component {
  /**
   * @override
   * @constructor
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = { value: props.value };
    this.updateValue = this.updateValue.bind(this);
  }

  /**
   * updates state.value if the incoming
   * value is different than the stored one.
   * if they are the same, do not update in order
   * to avoid unnecessary re-rendering.
   *
   * @override
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const currentValue = this.state.value;
    const nextValue = nextProps.value;

    if (currentValue !== nextValue) {
      this.setState({ value: nextValue });
    }
  }

  /**
   * updates state.value and passes
   * value to onChange function
   *
   * @param {string} value
   */
  updateValue(value) {
    const { onChange } = this.props;

    this.setState({ value });
    onChange(value);
  }

  /**
   * @override
   * @return {XML}
   */
  render() {
    const { field } = this.props;
    const { value } = this.state;

    return (
      <Input
        type="password"
        className="data-input"
        onChange={e => this.updateValue(e.target.value)}
        value={passwordConfig.parse(value)}
        required={field.required}
        placeholder={field.description}
      />
    );
  }
}

PasswordField.config = passwordConfig;

PasswordField.propTypes = {
  onChange: PropTypes.func.isRequired,
  field: PropTypes.shape().isRequired,
  value: PropTypes.string,
};

export default PasswordField;
