import React, { PropTypes } from 'react';
import Radio, { Group } from 'antd/lib/radio';
import config from '../../../config';

const validationTypes = config.get('validationTypes');

const RadioGroup = Group;

// staic data used when configuring or
// using this field
const radioConfig = {
  parse: (value) => {
    if (!value) return null;
    if (value instanceof Object) return String(value.id);
    return String(value);
  },
  template: {},
  label: 'Radio Button Group',
  icon: 'fa fa-list-ul',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: false,
  description: 'A radio group provides a list of options and allows the user to select only one.',
};

/**
 * RadioField is a React Component that provides a group
 * of radio buttons and passes up the button that is
 * selected.
 *
 * @param value
 * @param onChange
 * @param field
 * @constructor
 */
const RadioField = ({ value, onChange, field }) => (
  <RadioGroup
    className="data-input"
    onChange={e => onChange(e.target.value)}
    value={radioConfig.parse(value)}
  >
    {field.options.map(o =>
      <Radio
        className="radio-vertical"
        key={o.value}
        value={o.value}
      >
        {o.label}
      </Radio>,
    )}
  </RadioGroup>
);

RadioField.config = radioConfig;

RadioField.propTypes = {
  field: PropTypes.shape(),
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape(),
  ]),
};

export default RadioField;
