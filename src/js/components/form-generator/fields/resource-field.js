import React, { PropTypes, Component } from 'react';
import Radio, { Group } from 'antd/lib/radio';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import config from '../../../config';

const validationTypes = config.get('validationTypes');

const RadioGroup = Group;

const permissions = ['READ_ONLY', 'READ_WRITE'];

// staic data used when configuring or
// using this field
const resourceConfig = {
  parse: value => value,
  template: {},
  defaultValidation: validationTypes.ANY,
  isUserDefinable: false,
};

/**
 * Resource field. This field is used to create
 * sets of radio-groups to assign permissions
 * to each resource like 'ADD_ASSET','DELET_ASSET' etc
 *
 * @author Balu Loganathan
 */
class ResourceField extends Component {

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { field, onChange, value } = this.props;
    const { options } = field;


    this.selections = {};

    const onChangeWrapper = (selectedValues, resourceType) => {
      this.selections[resourceType] = selectedValues;
      onChange(this.selections);
    };

    const fetchGroupOptions = () => {
      let optionsList = [];
      optionsList = options.map((item, index) =>
        (
          <Row key={index}>
            <Col span={8}>{item.label}</Col>
            <Col span={16}>
              <Row>
                <RadioGroup
                  key={item.value}
                  className="resource-list-row"
                  onChange={e => onChangeWrapper(e.target.value, item.value)}
                >
                  {permissions.map((o, i) => {
                    const checked = (value) ? value[item.value] : false
                    return (<Radio checked={true} key={`${item.value}_${i}`} value={o} />);
                  }
                  )}
                </RadioGroup>
              </Row>
            </Col>
          </Row>
        ),
      );
      return optionsList;
    };

    return (
      <div className="resources">
        <Row className="header">
          <Col span={8}>&nbsp;</Col>
          { permissions.map(o => <Col key={`header_${o}`} span={8}>{o}</Col>) }
        </Row>
        {fetchGroupOptions()}
      </div>
    );
  }
}

ResourceField.config = resourceConfig;

ResourceField.propTypes = {
  field: PropTypes.shape(),
  onChange: PropTypes.func,
  value: PropTypes.string,
};

ResourceField.defaultProps = {
  value: '',
};

export default ResourceField;
