import React, { PropTypes, Component } from 'react';
import validate from 'validate.js';
import { Row, Col, Checkbox, Select } from 'antd';
import config from '../../../config';
import { enableSelectedOptions, removeDisabledOptions } from '../../../misc/form-utils';

const fieldTypes = config.get('fieldTypes');
const validationTypes = config.get('validationTypes');
const Option = Select.Option;

/**
 * filterOption() compares an option component
 * to the inputValue. See ant.design Select
 * filterOption for more information.
 *
 * @note children is expected to be the inner HTML
 * of each Option component, which should be
 * a string (if search is desired)
 *
 * @param {string} inputValue
 * @param {string|XML} option
 * @returns boolean
 */
const filterOption = (inputValue, option) => (
    option.props.children.includes(inputValue)
);

// staic data used when configuring or
// using this field
const selectConfig = {
  parse: (value) => {
    if (validate.isEmpty(value) || value === 0) return undefined;
    if (validate.isArray(value)) return value.map(v => selectConfig.parse(v));
    if (validate.isObject(value)) return String(value.id || value.name);
    return value;
  },
  template: {
    fields: [
      {
        key: 'label',
        label: 'Label',
        required: true,
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'description',
        label: 'Help Text',
        description: 'Text that describes this field',
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'options',
        label: 'Options',
        required: true,
        type: fieldTypes.OPTIONS,
        editable: true,
      },
      {
        key: 'required',
        label: 'Required',
        type: fieldTypes.CHECKBOX,
        editable: true,
      },
      {
        key: 'multiple',
        label: 'Multiple',
        type: fieldTypes.CHECKBOX,
        editable: true,
      },
    ],
  },
  label: 'Dropdown',
  icon: 'fa fa-bars',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: true,
  description: 'A dropdown provides a list of options and allows the user to select one or more depending on whether or not Multiple is enabled.',
  placholder: 'Click to see options',
};

/**
 * Select field
 *
 * @author Benjamin Newcomer
 */
class SelectField extends Component {

  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.updateValue = this.updateValue.bind(this);
    this.selectAll = this.selectAll.bind(this);
  }


  /**
   * set initial value call updateValue
   * to trigger initial validation
   *
   * @override
   */
  componentWillMount() {
    const { value } = this.props;
    if (value === 'ALL') this.selectAll();
    else this.updateValue(selectConfig.parse(value));
  }

  /**
   * update value and perform validation
   * @param value
   */
  updateValue(value) {
    if (value && (value.includes(undefined) || value.includes('ALL'))) return;
    const { onChange, field } = this.props;
    const optionsEmpty = !field.options || field.options.length === 0;

    if (optionsEmpty) onChange(null);
    else onChange(value);
  }

  /**
   * update the stored value to select all
   * or deselect all
   *
   * @param deselect
   */
  selectAll(deselect = false) {
    const { field } = this.props;
    const { options } = field;
    let updatedValue;

    if (deselect) updatedValue = [];
    else updatedValue = options.map(o => o.value);

    this.updateValue(updatedValue);
  }

  /**
   * render multi select with a checkbox to
   * select all
   *
   * @returns {XML}
   */
  renderMultipleSelectAll() {
    const { value, field } = this.props;
    const optionsLength = field.options.length;
    const parsedValue = selectConfig.parse(value);
    const allSelected = (parsedValue &&
      (parsedValue === 'ALL' || parsedValue.length === optionsLength));
    const allText = 'All';

    return (
      <Row type="flex" justify="space-between">
        <Col span={21}>
          {this.renderSelect()}
        </Col>
        <Col span={3}>
          <span className="select-all-checkbox">
            <Checkbox
              onChange={e => this.selectAll(!e.target.checked)}
              checked={allSelected}
              disabled={field.disabled}
            >
              {allText}
            </Checkbox>
          </span>
        </Col>
      </Row>
    );
  }

  /**
   * render a select field with options. can
   * be multiple depending on field.type.multiple
   */
  renderSelect() {
    const { field, value } = this.props;
    const options = field.options;
    const multiple = field.multiple;
    const allowClear = !field.required;
    let parsedValue = selectConfig.parse(value);

    if (multiple && validate.isEmpty(parsedValue)) {
      parsedValue = [];
    }

    if (!multiple && validate.isEmpty(parsedValue)) {
      parsedValue = null;
    }

    // make sure that any options that are selected (currently in
    // value) are not disabled so that they can be deselected.
    // also make sure that selected values appear in the
    // list of options
    let processedOptions = enableSelectedOptions(options, parsedValue);
    processedOptions = removeDisabledOptions(processedOptions);

    const className = `data-input${(multiple) ? ' multiple' : ''}`;

    return (
      <Select
        className={className}
        style={field.style ? field.style : {}}
        multiple={multiple}
        allowClear={allowClear}
        tokenSeparators={[',']}
        size="large"
        onChange={this.updateValue}
        value={parsedValue}
        filterOption={filterOption}
        placeholder={field.description || selectConfig.placholder}
        disabled={field.disabled}
      >
        {
          processedOptions.map(o => (
            <Option
              key={o.value}
              value={String(o.value)}
              disabled={o.disabled}
            >
              {o.label}
            </Option>
          ))
        }
      </Select>
    );
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { field } = this.props;
    const { multiple, options, defaultSelectAll, all } = field;
    const noneText = 'None';

    // if there are no options, return a disabled select
    if (validate.isEmpty(options)) {
      return <Select
        disabled
        style={field.style ? field.style : {}}
        placeholder={noneText}
      />;
    }

    // if multiselect is enabled
    if (multiple && (defaultSelectAll || all)) {
      return this.renderMultipleSelectAll();
    }

    // single select
    return this.renderSelect();
  }
}

SelectField.config = selectConfig;

SelectField.propTypes = {
  field: PropTypes.shape(),
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Array),
    PropTypes.object,
  ]),
};

SelectField.defaultProps = {
  selectAll: false,
};

export default SelectField;