import React, { PropTypes } from 'react';
import Input from 'antd/lib/input';
import config from '../../../config';

const fieldTypes = config.get('fieldTypes');
const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const telConfig = {
  parse: value => value,
  template: {
    fields: [
      {
        key: 'label',
        label: 'Label',
        required: true,
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'description',
        label: 'Help Text',
        description: 'Text that describes this field',
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'required',
        label: 'Required',
        type: fieldTypes.CHECKBOX,
        editable: true,
      },
    ],
  },
  label: 'Phone Number',
  icon: 'fa fa-phone',
  defaultValidation: validationTypes.TEL,
  isUserDefinable: true,
  description: 'A phone number field allows the user to enter a phone number',
  placeholder: 'XXX-XXX-XXXX',
};

/**
 * Phone Number input field (html5 input type tel)
 *
 * @author Benjamin Newcomer
 */
const TelField = ({ value, onChange, field }) => (
  <Input
    type="tel"
    onChange={e => onChange(e.target.value)}
    value={telConfig.parse(value)}
    placeholder={field.description || telConfig.placeholder}
  />
);

TelField.config = telConfig;

TelField.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  field: PropTypes.shape(),
};

export default TelField;
