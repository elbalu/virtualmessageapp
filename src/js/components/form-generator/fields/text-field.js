import React, { PropTypes, Component } from 'react';
import Input from 'antd/lib/input';
import config from '../../../config';

const fieldTypes = config.get('fieldTypes');
const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const textConfig = {
  parse: (value) => {
    if (!value) return null;
    if (value instanceof Array) return value.map(v => textConfig.parse(v)).join(',');
    if (value instanceof Object) return value.name || value.label;
    return String(value);
  },
  template: {
    fields: [
      {
        key: 'label',
        label: 'Label',
        required: true,
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'description',
        label: 'Help Text',
        description: 'Text that describes this field',
        type: fieldTypes.TEXT,
        editable: true,
      },
      {
        key: 'required',
        label: 'Required',
        type: fieldTypes.CHECKBOX,
        editable: true,
      },
    ],
  },
  label: 'Text',
  icon: 'fa fa-font',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: true,
  description: 'A text fields allows the user to enter any text',
  placeholder: '',
};

/**
 * Text input field
 *
 * @author Benjamin Newcomer
 */
class TextField extends Component {
  /**
   * @override
   * @constructor
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = { value: props.value };
    this.updateValue = this.updateValue.bind(this);
  }

  /**
   * updates state.value if the incoming
   * value is different than the stored one.
   * if they are the same, do not update in order
   * to avoid unnecessary re-rendering.
   *
   * @override
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const currentValue = this.state.value;
    const nextValue = nextProps.value;

    if (currentValue !== nextValue) {
      this.setState({ value: nextValue });
    }
  }

  /**
   * updates state.value and passes
   * value to onChange function
   *
   * @param {string} value
   */
  updateValue(value) {
    const { onChange } = this.props;

    this.setState({ value });
    onChange(value);
  }

  /**
   * @override
   * @return {XML}
   */
  render() {
    const { field } = this.props;
    const { value } = this.state;

    return (
      <Input
        className="data-input"
        disabled={field.disabled}
        onChange={e => this.updateValue(e.target.value)}
        value={textConfig.parse(value)}
        required={field.required}
        placeholder={field.description || textConfig.placeholder}
      />
    );
  }
}

TextField.config = textConfig;

TextField.propTypes = {
  onChange: PropTypes.func.isRequired,
  field: PropTypes.shape().isRequired,
  value: PropTypes.string,
};

export default TextField;
