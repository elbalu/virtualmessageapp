import React, { PropTypes, Component } from 'react';
import _isEmpty from 'lodash/isEmpty';
import _xor from 'lodash/xor';
import validate from 'validate.js';
import TreeSelect from 'antd/lib/tree-select';
import Checkbox from 'antd/lib/checkbox';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Select from 'antd/lib/select';
import config from '../../../config';

const validationTypes = config.get('validationTypes');

// staic data used when configuring or
// using this field
const treeConfig = {
  parse: (value) => {
    if (!value || Object.keys(value).length === 0) return undefined;
    if (value instanceof Array) return value.map(v => treeConfig.parse(v));
    if (value instanceof Object) return String(value.id);
    return value;
  },
  template: {},
  label: 'Option Tree',
  icon: 'fa fa-code-fork',
  defaultValidation: validationTypes.ANY,
  isUserDefinable: false,
};


/**
 * Tree select field. This field can be used when a tree
 * of options is presented to the user (like selecting
 * a zone from cmapus > building > floor > zone)
 * This allows the user to do multiple selections in contrast
 * to cascader which allows single selection only.
 *
 * @author Balu Loganathan
 */
class TreeSelectField extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.updateValue = this.updateValue.bind(this);
  }


  /**
   * set initial value call updateValue
   * to trigger initial validation
   *
   * @override
   */
  componentWillMount() {
    const { value } = this.props;

    if (value === 'ALL') this.selectAll();
    //else this.updateValue(value);
  }

  /**
   * update the stored value to select all
   * or deselect all
   *
   * @param deselect
   */
  selectAll(deselect = false) {
    const { field } = this.props;
    const options = field.options;
    let updatedValue;

    if (deselect) updatedValue = [];
    else updatedValue = options.map(o => o.value);

    this.updateValue(updatedValue);
  }

  /**
   * update value and perform validation
   * @param value
   */
  updateValue(value) {
    const { onChange, field } = this.props;
    const optionsEmpty = !field.options || field.options.length === 0;

    if (value === 'ALL') {
      onChange('ALL');
    } else if (optionsEmpty) {
      onChange(null);
    } else {
      onChange(value);
    }
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { field, value } = this.props;
    const { options } = field;
    let allSelected = true;
    const multiple = field.multiple || false;

    if (value !== 'ALL' && multiple) {
      // compare the first level id's of geodata and selected values
      // if noth are equal that means all the campuses are selected
      const firstLevelNodeArr = options.map(v => v.key);
      const selValueArr = value.map(v => v.id);
      allSelected = _isEmpty(_xor(firstLevelNodeArr, selValueArr));
    }

    const noneText = 'None';
    const allText = 'All';

    // if there are no options, return a disabled select
    if (validate.isEmpty(options)) {
      return <Select disabled placeholder={noneText} />;
    }

    const SHOW_PARENT = TreeSelect.SHOW_PARENT;
    return (
      <Row type="flex" justify="space-between">
        <Col span={21}>

          <TreeSelect
            className="data-input tree-select"
            treeData={options}
            onChange={this.updateValue}
            multiple={multiple}
            treeCheckable={multiple}
            showCheckedStrategy={SHOW_PARENT}
            searchPlaceholder={field.description || 'Please select'}
            value={treeConfig.parse(value)}
            disabled={field.disabled}
          />

        </Col>
        {
          (multiple) ? (
            <Col span={3}>
              <span className="select-all-checkbox">
                <Checkbox
                  onChange={e => this.selectAll(!e.target.checked)}
                  checked={allSelected}
                  disabled={field.disabled}
                >
                  {allText}
                </Checkbox>
              </span>
            </Col>
          ) : ''
        }
      </Row>
    );
  }
}

TreeSelectField.config = treeConfig;

TreeSelectField.propTypes = {
  field: PropTypes.shape(),
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Array),
  ]),
};

TreeSelectField.defaultProps = {
  value: [],
};

export default TreeSelectField;
