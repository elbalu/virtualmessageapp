import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import _set from 'lodash/set';
import _get from 'lodash/get';
import _find from 'lodash/find';
import _isObject from 'lodash/isObject';
import validate from 'validate.js';
import equal from 'deep-equal';

import Field from './fields/field';

import Logger from '../../misc/logger';
import Fields from './fields';
import { findDeep } from '../../misc/helpers';
import * as FormUtils from '../../misc/form-utils';

/**
 * Generator is a React component that generates
 * forms based on a defined JSON format. The Generator
 * supports a number of different field types like input,
 * select, radio, and others. It does not provide form submission
 * behavior, but rather accepts template, entity, and onChange
 * props. The template is the json that is used to define
 * the form. Entity is the data structure that the form
 * will map to, and onChange is a function that accepts
 * the entity and is used to pass an updated entity back
 * up to the parent anytime a form value is changed. The main
 * focus of this component is to provide a flexible API for
 * creating forms that reduces development time while still
 * creating a rich, interactive experience for users.
 *
 *
 * @prop {Object} template A Template object that defines forms fields
 * @prop {Object} entity A JSON object that contains form field values
 * @prop {function({Object})} onChange A function that accepts the entity and
 *    is used to pass updated entity state to the parent
 *
 * @author Benjamin Newcomer
 */
class Generator extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = { template: { ...props.template } };
    this.updateField = this.updateField.bind(this);
  }

  /**
   * componentWillReceiveProps() updates the template
   * stored in state with the new incoming template.
   * A merge is required because this.state.template
   * contains validation data, but the incoming
   * template will not.
   *
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    const { template } = this.state;
    const nextTemplate = nextProps.template;

    if (!nextTemplate || !nextTemplate.fields) return;
    if (equal(template, nextTemplate)) return;

    this.setState({ template: nextTemplate });
  }

  /**
   * Generates an array of select or radiogroup compatible
   * options from the given options object which can be an
   * object, a string, an array of strings or an array of
   * objects.
   *
   * @param {Object} field Object representing a field
   * @returns {{ options: Array, collection: * }}
   */
  generateOptions(field) {
    const { collections } = this.props;
    const fieldOptionsIsArray = validate.isArray(field.options);
    const fieldHasCollection = validate.isDefined(field.collection);
    const collectionIsString = (typeof field.collection === 'string');
    const collectionName = field.collection;
    const fieldIsTreeSelect = (field.type === Fields.Types.TREE_SELECT);
    const fieldIsIcon = (field.type === 'ICON');
    const collectionIsTags = (field.collection === 'TAGS');

    let options = field.options;
    let collection = null;

    // generate options if it is a icon field
    if (fieldIsIcon) options = options.map(c => FormUtils.generateIconOption(c));
    // generate options for each object in options array
    else if (fieldOptionsIsArray) options = options.map(o => FormUtils.generateOption(o));

    // format collection into options array and replace
    // field.collection with array
    if (fieldHasCollection && collectionIsString) {
      collection = collections[collectionName];
      const collectionIsDefined = validate.isDefined(collection);

      if (!collectionIsDefined) {
        Logger.error('FormGenerator#generateOptions', `collection ${collectionName} does not exist`);
      }

      // parse options in different ways depending on
      // field type and key (which specifies data format)
      if (fieldIsTreeSelect) options = FormUtils.generateTreeOptions(collection);
      else if (collectionIsTags) options = FormUtils.optionsFromTagsCollection(collection);
      else options = collection.map(c => FormUtils.generateOption(c));
    }

    return { options, collection };
  }

  /**
   * generate a single form field
   *
   * @param fieldConfig
   */
  generateField(fieldConfig) {
    const { entity } = this.props;
    const { key, label } = fieldConfig;

    // merge field config data with list of options
    // and generate props required to create form field
    const { options, collection } = this.generateOptions(fieldConfig);
    const mergedField = { ...fieldConfig, options, collection };
    const isPolicyField = (key === 'policy');
    const fieldValue = _get(entity, key);
    if (isPolicyField && fieldValue) {
      this.updateUserForm(String(fieldValue.id), entity);
    }
    const onChange = (value) => {
      if (isPolicyField) {
        this.updateUserForm(value, entity);
      }
      this.updateField(mergedField, value);
    };

    // create the form input
    return (
      <Field
        key={label}
        value={fieldValue}
        onChange={onChange}
        field={mergedField}
      />
    );
  }

  /**
   * update user form data
   * this needs certain fields to be disabled for admin access
   *
   * @param {id} policy
   * @param {Object} entity
   */
  updateUserForm(policy, entity) {
    const { template } = this.state;
    const { POLICIES } = this.props.collections;
    const fieldsList = _get(template, 'fields', []);
    const effectedFields = ['categories', 'departments', 'sites'];
    const admin = POLICIES.find(p => p.name === 'AdminAccess');
    if (admin && (policy === String(admin.id))) {
      effectedFields.forEach((key) => {
        const field = _find(fieldsList, ['key', key]);
        field.disabled = true;
        field.required = false;
        entity[key] = undefined;
      });
    } else {
      effectedFields.forEach((key) => {
        const field = _find(fieldsList, ['key', key]);
        field.disabled = false;
        field.required = true;
      });
    }
  }

  /**
   * prepares location data in the format
   * required by the backend
   *
   * @param {id} leafId
   * @param {id} nodeId
   * @param {Object} leaf
   */
  prepareLocationData(leafId, nodeId, leaf) {
    let locationLabel = '';
    const ancestors = (leaf.relationshipData.ancestors) ?
    leaf.relationshipData.ancestors : [];
    const ancestorsIds = (leaf.relationshipData.ancestorsIds) ?
    Object.assign([], leaf.relationshipData.ancestorsIds) : [];
    ancestorsIds.push(nodeId);

    locationLabel = ancestors.map(o => `${o} -> `);
    locationLabel.push(leaf.name);

    const hierarchy = ['campus', 'building', 'floor'];

    const nodeVal = {
      name: locationLabel.join(''),
      id: leafId,
      campus: [],
      building: [],
      floor: [],
      level: leaf.level
    };

    ancestorsIds.forEach((id, i) => {
      nodeVal[hierarchy[i]] = [id];
    });
    return nodeVal;
  }

  /**
   * update form data. map from id to
   * collection entry if necessary
   *
   * @param {Object} field
   * @param {*} value
   */
  updateField(field, value) {
    const { onChange, entity } = this.props;
    const valueIsEmpty = validate.isEmpty(value);
    const valueIsArray = validate.isArray(value);
    const fieldHasCollection = !validate.isEmpty(field.collection);
    const fieldIsTreeSelect = (field.type === Fields.Types.TREE_SELECT);
    let parsedValue = value;

    // for fields with collection attached
    // map value (id) to full entity
    if (fieldHasCollection && !valueIsEmpty) {
      const collection = field.collection;

      if (valueIsArray) {
        // handle case where value is an array of ids
        parsedValue = value.map(v => collection.find(c => String(c.id) === String(v)));
      } else {
        // handle case where value is a single id
        parsedValue = collection.find(c => String(c.id) === String(value));
      }

      // if field is a tree-select,
      // value required additional
      // processing
      if (fieldIsTreeSelect) {
        if (value instanceof Array) {
          parsedValue = [];

          // fetch from tree object using id
          value.forEach((leafId) => {
            const nodeId = _isObject(leafId) ? leafId.id : leafId;
            const leaf = findDeep(collection, { id: nodeId });
            if (leaf) {
              parsedValue.push(this.prepareLocationData(leafId, nodeId, leaf));
            }
          });
        } else {
          const nodeId = value;
          const leaf = findDeep(collection, { id: nodeId });
          if (leaf) {
            parsedValue = this.prepareLocationData(value, nodeId, leaf);
          }
        }
      }
    }

    // pass updated entity up to parent
    onChange({ ...entity, [field.key]: parsedValue });
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { template } = this.state;

    // extract schema fields list
    const fieldsList = _get(template, 'fields', []);

    return (
      <div>
        {fieldsList
          .filter(f => f.editable)
          .map(f => this.generateField(f))}
      </div>
    );
  }
}

Generator.propTypes = {
  template: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  entity: PropTypes.shape(),

  // provided by redux
  collections: PropTypes.shape(),
};

Generator.defaultProps = {
  entity: {},
};

// the collection names here must match
// the choices listed for field collection
// see Asset Manager Design Wiki for details
const mapStateToProps = state => ({
  collections: {
    DEPARTMENTS: [],
    CATEGORIES: [],
    CATEGORY_ICONS: [],
    TAGS: [],
    ROLES: [],
    RESOURCES: [],
    POLICIES: [],
    LOCATIONS: [],
    FLOORS: [],
    ASSETS: [],
    USERS: [],
    CONTROLLERS: state.wlcs.wlcs,
    CONNECTORS: state.connectors.connectors
  },
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Generator);
