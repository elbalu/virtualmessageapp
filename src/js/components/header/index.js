import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import validate from 'validate.js';

import Row from 'antd/lib/row';
import Col from 'antd/lib/col';

import UserWidget from './user-widget';

import * as usersActions from '../../redux/actions/users-actions';

const initialState = {
  passwordModal: {
    entity: {},
    visible: false,
  },
};

/**
 * Header is a React component that manages and
 * generates the Asset Management app header. This
 * includes the logo/title section and alerts
 * and user widgets.
 *
 * @class
 */
class Header extends Component {
  /**
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    const { getUserFields } = props.usersActions;

    this.state = initialState;
    this.togglePasswordModalVisibility = this.togglePasswordModalVisibility.bind(this);

    getUserFields();
  }

  /**
   * toggles the visibility of the password modal when a
   * user wants to change their password.
   *
   * @param {Object} nextEntity
   */
  togglePasswordModalVisibility(nextEntity) {
    const { passwordModal } = this.state;
    const visible = !passwordModal.visible;
    const entity = (validate.isObject(nextEntity)) ? { id: nextEntity.id } : passwordModal.entity;

    this.setState({ passwordModal: { entity, visible } });
  }

  render() {
    const { intl } = this.props;

    return (
      <Row className="app-header" align="middle" type="flex" justify="start">

        <Col span={12}>
          <div className="title">
            <div className="nav-bar-logo sdk-icon-cisco" />
            <span className="app-header-title">
              <FormattedMessage id="appName" />
            </span>
          </div>
        </Col>

        <Col span={12}>
          <Row align="middle" type="flex" justify="space-between">

            <Col span={6} offset={10} />

            <Col span={8}>
              <UserWidget />
            </Col>

          </Row>
        </Col>
      </Row>
    );
  }
}

Header.propTypes = {

  notificationActions: PropTypes.shape({
    display: PropTypes.func,
  }),
  // i18n support
  intl: intlShape,
};

const mapStatetoProps = state => ({
  ...state.alerts,
  ...state.login,
  passwordTemplate: state.users.passwordTemplate,
});

const mapDispatchToProps = dispatch => ({
  usersActions: bindActionCreators(usersActions, dispatch),
});

export default connect(mapStatetoProps, mapDispatchToProps)(injectIntl(Header));
