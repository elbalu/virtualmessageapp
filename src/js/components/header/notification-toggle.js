import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';

import Switch from 'antd/lib/switch';

import { toggleNotifications } from '../../redux/actions/settings-actions';

/**
 * Toggle switch for notification settings
 */
const NotificationsToggle = ({ notificationsAreEnabled, toggleNotificationsAction, intl }) => {
  const label = intl.formatMessage({ id: 'notifications' });
  const help = intl.formatMessage({ id: 'notifications.help' });

  return (
    <span className="notifications-toggle" title={help}>

      <label htmlFor="notification-toggle">
        <FormattedMessage id="notifications" />
      </label>

      <Switch
        id="notification-toggle"
        checked={notificationsAreEnabled}
        onChange={toggleNotificationsAction}
      />

    </span>
  );
};

NotificationsToggle.propTypes = {
  // provided by redux
  notificationsAreEnabled: PropTypes.bool,
  toggleNotificationsAction: PropTypes.func,
  // i18n support
  intl: intlShape,
};

const mapStatetoProps = state => ({
  notificationsAreEnabled: state.settings.notifications,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  toggleNotificationsAction: value => dispatch(toggleNotifications(value)),
}, dispatch);

export default connect(mapStatetoProps, mapDispatchToProps)(injectIntl(NotificationsToggle));
