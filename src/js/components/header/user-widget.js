import React, { PropTypes, Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Popover from 'antd/lib/popover';
import _isArray from 'lodash/isArray';

import { formatName } from '../../misc/user-utils';
import * as loginActions from '../../redux/actions/login-actions';

const initialState = {
  visible: false,
};

/**
 * Header user widget. This react component displays a username and
 * a dropdown menu with information and actions related to settings
 * and user management (things like a logout button, global auto-refresh
 * settings and account management).
 *
 * @author Benjamin Newcomer
 */
class UserWidget extends Component {
  /**
   * @override
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);
    this.hide = this.hide.bind(this);
    this.handleVisibleChange = this.handleVisibleChange.bind(this);
  }

  /**
   * hides the popover
   */
  hide() {
    this.setState({ visible: false });
  }

  /**
   * sets state.visible to param visible
   *
   * @param {Boolean} visible
   */
  handleVisibleChange(visible) {
    this.setState({ visible });
  }

  /**
   * returns a list of information and actions
   * that users can take regarding settings and
   * user account.
   *
   * @return {XML}
   */
  renderDropdown() {
    const { children, user } = this.props;
    const { logout } = this.props.loginActions;
    const widgets = _isArray(children) ? children : [children];

    return (
      <ul className="user-widget-list">

        <li><span>{`Welcome, ${user.firstName}`}</span></li>

        {widgets.map((w, index) => (<li key={index}>{w}</li>))}

        <li className="sign-out">
          <a onClick={logout}>
            <FormattedMessage id="login.logout" />
          </a>
        </li>

      </ul>
    );
  }

  /**
   * @override
   * @return {XML}
   */
  render() {
    const { user } = this.props;
    const { visible } = this.state;

    return (
      <div className="header-widget">
        <Popover
          placement="bottom"
          visible={visible}
          onVisibleChange={this.handleVisibleChange}
          content={this.renderDropdown()}
          trigger="hover"
        >
          <span className="username hoverable">
            <span>{formatName(user.firstName, user.lastName)}</span>
            <i className="fa fa-caret-down" />
          </span>
        </Popover>
      </div>
    );
  }
}

UserWidget.propTypes = {
  user: PropTypes.shape(),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  loginActions: PropTypes.shape({
    logout: PropTypes.func,
  }),
};

const mapStateToProps = state => ({
  user: state.login.currentUser,
});

const mapDispatchToProps = dispatch => ({
  loginActions: bindActionCreators(loginActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserWidget);
