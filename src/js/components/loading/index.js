import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Icon } from 'antd';

const initialState = {
  stillLoading: false
};

/**
 * @class
 *
 * @name Loading
 *
 * @description Loading is a versatile component
 * that communicates to the user that a resource
 * is loading. Can show text or an icon.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Loading extends React.Component {
  /**
   * @constructor
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @name componentDidMount
   *
   * @description Starts the timer
   */
  componentDidMount() {
    this.startTimer(this.props.wait);
  }

  /**
   * @name componentWillReceiveProps
   *
   * @override
   *
   *
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const prevLoading = this.props.loading;
    const nextLoading = nextProps.loading;

    // if loading prop changes from false
    // to true, start timer
    if (!prevLoading && nextLoading) {
      this.setState({ stillLoading: initialState.stillLoading });
      this.startTimer(nextProps.wait);
    }
  }

  /**
   * @name getMessage
   *
   * @return {XML} - A loading-related
   * message
   */
  getMessage() {
    const { icon, timeout } = this.props;
    const { stillLoading } = this.state;

    if (timeout) return <span>there was a problem contacting the server</span>;

    if (stillLoading) {
      if (icon) {
        return (
          <div>
            <div className="icon-wrapper"><Icon type="loading" /></div>
            <div>still loading...</div>
          </div>
        );
      }

      return <span>still loading...</span>;
    }

    if (icon) return <Icon type="loading" />;
    return <span>loading...</span>;
  }

  getStyle(noPadding) {
    if(noPadding) {
      return { padding: 'initial' }
    } else return {};
  }

  /**
   * @name wait
   *
   * @description Starts a timer and changes
   * state.stillLoading when the timer is done.
   *
   * @param {number} wait - Wait time in seconds
   */
  startTimer(wait) {
    setTimeout(() => this.setState({ stillLoading: true }), wait * 1000);
  }

  /**
   * @name render
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { loading, noPadding } = this.props;

    return (
      <div className="loading-wrapper" hidden={!loading} style={ this.getStyle(noPadding) }>
        {this.getMessage()}
      </div>
    );
  }
}

Loading.propTypes = {
  // whether to use icon or text
  icon: PropTypes.bool,
  // how long to wait before showing
  // the 'still loading' message (seconds)
  wait: PropTypes.number,
  // loading completed due to timeout
  timeout: PropTypes.bool,
  // start loading timer when value changes
  loading: PropTypes.bool
};

Loading.defaultProps = {
  icon: false,
  wait: 10,
  timeout: false,
  loading: false
};

export default Loading;
