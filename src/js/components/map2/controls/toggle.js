import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';

/**
 * @name ToggleControl
 *
 * @description Implementation of Mapbox IControl that
 * renders a button and registers a handler for
 * button clicks.
 *
 * @implements IControl
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ToggleControl {
  /**
   * @constructor
   *
   * @param {string} className - Class name of control
   * button.
   *
   * @param {function} onClick - Callback for when
   * control is clicked.
   *
   * @param {string} title - Button tooltip text
   *
   * @param {boolean} [active] - Default active state
   * for toggle control. Defaults to true.
   */
  constructor(className, onClick, title, active = false) {
    this.className = className;
    this.title = title;
    this.onClickCallback = onClick;
    this.active = active;
    this.onClick = this.onClick.bind(this);
  }

  /**
   * @name onClick
   *
   * @description Updates active state and calls
   * this.onClick if a click handler was provided.
   */
  onClick() {
    this.active = !this.active;
    const button = this.container.firstElementChild;

    // update control class
    button.className = this.getClassName();

    // call callback
    if (_.isFunction(this.onClickCallback)) {
      this.onClickCallback(this.active);
    }
  }

  /**
   * @name getClassName
   *
   * @description Returns a compound className composed
   * of the class name given in the constructor and the
   * active className.
   *
   * @return {string}
   */
  getClassName() {
    return `${this.className} ${this.active ? 'active' : ''}`;
  }

  /**
   * @name onAdd
   *
   * @description Creates and returns a DOM
   * node that has the control button as a child.
   *
   * @param {Object} map - Mapbox map
   */
  onAdd(map) {
    this.map = map;
    const container = global.document.createElement('div');
    container.className = 'mapboxgl-ctrl-group mapboxgl-ctrl';

    const button = (
      <button
        onClick={this.onClick}
        className={this.getClassName()}
        title={this.title}
      />
    );

    ReactDOM.render(button, container);
    this.container = container;
    return container;
  }

  /**
   * @name onRemove
   *
   * @description Removes this control from
   * the DOM.
   */
  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}

export default ToggleControl;
