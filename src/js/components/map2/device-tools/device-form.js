import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col, Icon, Input, Button, Select, Slider, Radio } from 'antd';
import Maki from 'maki';
import update from 'immutability-helper';

import FormGenerator from '../../form-generator';
import Location from '../../../misc/map/location';
import ColorPicker from '../../color-picker';
import Fields from '../../form-generator/fields';
import { zoneColors } from '../../../misc/colors';

const { Option, OptGroup } = Select;
const { TextArea } = Input;
const RadioGroup = Radio.Group;

const initialState = {
  currentPOI: {},
  modifiedPOI: {}
};

const marks = {
  0: '0',
  10: '10',
  20: '20',
  30: '30',
  40: '40',
  50: '50',
  60: '60'
};

const uuidMarks = {
  0: '0',
  25: '8000',
  50: '16000',
  75: '32000',
  100: '64000'
};

/**
 * @name DeviceForm
 *
 * @description A sidebar with tools to manage a single zone.
 * This tool can be used to create or edit zones. Users can
 * edit the zone name, select a color, and save or cancel. The
 * tool is designed to slide in and out using CSS.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class DeviceForm extends React.Component {
  /**
   * @constructor
   *
   * @param {Object} props - {@link ZoneForm.propTypes}
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeUUID = this.onChangeUUID.bind(this);
    this.onChangeUUIDMin = this.onChangeUUIDMin.bind(this);
    this.onChangeUUIDMax = this.onChangeUUIDMax.bind(this);
    this.onChangeLongDesc = this.onChangeLongDesc.bind(this);
    this.onChangeShortDesc = this.onChangeShortDesc.bind(this);
    this.onChangeIcon = this.onChangeIcon.bind(this);
    this.onChangeRadius = this.onChangeRadius.bind(this);
    this.onChangeNotificationRadius = this.onChangeNotificationRadius.bind(this);
    this.onChangeMinMax = this.onChangeMinMax.bind(this);
    this.onChangeEnable = this.onChangeEnable.bind(this);
    this.onChangePushMessage = this.onChangePushMessage.bind(this);
    this.onChangeColor = this.onChangeColor.bind(this);
    this.onChangeRange = this.onChangeRange.bind(this);
    this.onChangeKeyWords = this.onChangeKeyWords.bind(this);
    this.onChangeMacAddress = this.onChangeMacAddress.bind(this);
    this.onOverwriteLocation = this.onOverwriteLocation.bind(this);
    this.save = this.save.bind(this);
    this.renderTags = this.renderTags.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { selectedPOI, poi } = this.props;
    const { selectedPOI: sPOI, location } = nextProps;

    if (selectedPOI !== sPOI) {
      this.setState({ currentPOI: sPOI, modifiedPOI: sPOI });
    }


  }
  /**
   * @name onChangeName
   *
   * @description Updates the selected zone's
   * name.
   *
   * @param {Proxy} e - React event object
   */
  onChangeName(e) {
    const { modifiedPOI } = this.state;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        name: e.target.value
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeUUID(e) {
    this.setState(update(this.state, {
      modifiedPOI: {
        properties: {
          uuid: {
            id: {
              $set: e.target.value
            }
          }
        }
      }
    }));
  }

  onChangeUUIDMin(e) {
    this.setState(update(this.state, {
      modifiedPOI: {
        properties: {
          uuid: {
            min: {
              $set: e.target.value
            }
          }
        }
      }
    }));
  }

  onChangeUUIDMax(e) {
    this.setState(update(this.state, {
      modifiedPOI: {
        properties: {
          uuid: {
            max: {
              $set: e.target.value
            }
          }
        }
      }
    }));
  }

  onChangeIcon(v) {
    const { modifiedPOI } = this.state;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        icon: v
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeKeyWords(v) {
    const { modifiedPOI } = this.state;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        keyWords: v
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeLongDesc(e) {
    const { modifiedPOI } = this.state;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        description: {
          ...modifiedPOI.properties.description,
          long: e.target.value
        }
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeShortDesc(e) {
    const { modifiedPOI } = this.state;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        description: {
          ...modifiedPOI.properties.description,
          short: e.target.value
        }
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeRadius(v) {
    const { modifiedPOI } = this.state;
    const { onRadiusChange } = this.props;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        radius: v
      }
    });
    onRadiusChange(v);
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeNotificationRadius(v) {
    const { modifiedPOI } = this.state;
    const { onNotificationRadiusChange } = this.props;
    this.setState(update(this.state, {
      modifiedPOI: {
        properties: {
          notification: {
            radius: {
              $set: v
            }
          }
        }
      }
    }));
    onNotificationRadiusChange(v);
  }

  onChangeMinMax(v) {
    const c = this.calculateToMinMax(v)
    const { modifiedPOI } = this.state;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        uuid: {
          ...modifiedPOI.properties.uuid,
          minMax: c
        }
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeEnable(e) {
    const { modifiedPOI } = this.state;
    const enabledValue = e.target.value;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        notification: {
          ...modifiedPOI.properties.notification,
          enabled: enabledValue
        }
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangePushMessage(e) {
    const { modifiedPOI } = this.state;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        notification: {
          ...modifiedPOI.properties.notification,
          message: e.target.value
        }
      }
    });
    this.setState({ modifiedPOI: newPOI });
  }

  onChangeRange(e) {
    const { zone, setZone } = this.props;
    const { value } = e.target;
    const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
    if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      if (Location.isDevice(zone)) {
        // update device name immutably
        setZone(zone.updateDeviceRange(_.toNumber(value)));
      }
    }
  }

  onChangeMacAddress(e) {
    const { zone, setZone } = this.props;
    const isDevice = Location.isDevice(zone);
    var regexp = /^(([A-Fa-f0-9]{2}[:]){5}[A-Fa-f0-9]{2}[,]?)+$/i;
    const { value } = e.target;
    if (regexp.test(value) || value !== '' || value !== '-') {
      if (isDevice) {
        // update device mac address immutable
        setZone(zone.updateDeviceMacAddr(value));
      }
    }
  }

  /**
   * @name onChangeColor
   *
   * @description Updates the selected zone's
   * color.
   *
   * @param {string} color - New color as a hex string.
   */
  onChangeColor(color) {
    const { zone, setZone } = this.props;
    const isDevice = Location.isDevice(zone);

    if (isDevice) {
      // update zone color immutably
      setZone(zone.updateColor(color));
    }
  }

  onOverwriteLocation(e) {
    const { zone, setZone } = this.props;
    const isDevice = Location.isDevice(zone);
    if (isDevice) {
      const isChecked = e.target.checked;
      setZone(zone.updateOverrideLocation(isChecked));
    }
  }
  /**
   * @name save
   *
   * @description Saves an exisiting zone. Used
   * to update the name, color, and coordinates
   * of a zone. After saving, the zone is deselected
   * and the zone tool is closed.
   */
  save() {
    const { poiActions, location } = this.props;
    const { addPOI, updatePOI } = poiActions;
    const { modifiedPOI, currentPOI } = this.state;
    const isNew = !currentPOI.properties.name;
    const newPOI = Object.assign({}, modifiedPOI, {
      ...modifiedPOI,
      properties: {
        ...modifiedPOI.properties,
        floorId: location.getId()
      }
    })
    if (isNew) {
      addPOI(newPOI);
    } else {
      updatePOI(newPOI);
    }
  }

  renderIcons() {
    return Object.keys(Maki.layouts.streets).map((c) => {
      return (
        <OptGroup label={c}>
          {
            Maki.layouts.streets[c].map(p => (
              <Option value={p}>
                <img src={`/svgs/${p}.svg`} alt={p} />&#160;{p}
              </Option>
            ))
          }
        </OptGroup>
      );
    });
  }

  calculateToMinMax(minMax) {
    // eg: 50 to 32000
    const [min, max] = minMax;
    const calc = val => (64000 * val) / 100;
    return [calc(min), calc(max)];
  }

  calculateFromMinMax(minMax) {
    // eg: 32000 to 50
    const [min, max] = minMax;
    const calc = val => (100 * val) / 64000;
    return [calc(min), calc(max)];
  }

  renderTags() {
    const { tags } = this.props;
    if (!tags || tags.length < 0) return null;
    return tags.map(t => (
      <Option key={t}>{t}</Option>
    ));
  }
  /**
   * @name render
   * @override
   */
  render() {
    const { poiActions, tags, xValue, yValue } = this.props;
    const { setSelectedPOI } = poiActions;
    const { modifiedPOI } = this.state;
    const isDevice = (modifiedPOI && !!modifiedPOI.properties);

    if (!isDevice) {
      return null;
    }

    const { properties } = modifiedPOI;
    const { notification, uuid, coordinates } = properties;
    const [x, y] = coordinates;
    const { id, min, max } = uuid;
    const { enabled, message, radius: notificationRadius } = notification;
    const { name, description, radius } = properties;
    const isNew = !name;

    const activeClass = isDevice ? 'open' : '';
    const title = isNew ? 'Add POI' : 'Edit POI';
    const hasValidationErrors = _.isEmpty(name);


    return (
      <div className={`zone-tool ${activeClass}`}>
        <div className="inner">
          {/* header */}
          <Row className="header" type="flex" align="middle" justify="space-between">
            <Col span={12}>
              <h5 className="title">{title}</h5>
            </Col>
            <Col span={2}>
              <Icon
                className="right-arrow clickable-2"
                type="close"
                onClick={() => setSelectedPOI(null)}
              />
            </Col>
          </Row>

          {/* content */}
          <div className="content">
            <div>
              <h6>X: {_.round(x, 2)} &#160; Y: {_.round(y, 2)}</h6>
            </div>
            {/* name */}
            <div className="name">
              Name: <span className="red">*</span>
              <Input
                className="name-input"
                placeholder="Name"
                value={name}
                maxLength="150"
                onChange={e => this.onChangeName(e)}
              />
            </div>
            <div className="range">
              Icon:
              <Select
                defaultValue={properties.icon}
                value={properties.icon}
                style={{ width: '100%' }}
                onChange={this.onChangeIcon}
              >
                {this.renderIcons()}
              </Select>
            </div>
            <div className="range">
              Key words:
              <Select
                mode="tags"
                size="default"
                placeholder="Please select"
                defaultValue={properties.keyWords}
                value={properties.keyWords}
                style={{ width: '100%' }}
                onChange={this.onChangeKeyWords}
              >
                {this.renderTags()}
              </Select>
            </div>
            <div className="range">
              Long description:
              <TextArea
                rows={2}
                className="description-input"
                placeholder="Description"
                value={description.long}
                onChange={e => this.onChangeLongDesc(e)}
              />
            </div>
            <div className="range">
              Short description:
              <TextArea
                rows={2}
                className="description-input"
                placeholder="Description"
                value={description.short}
                onChange={e => this.onChangeShortDesc(e)}
              />
            </div>
            <div className="range">
              Destination radius (feet):
              <Slider
                marks={marks}
                defaultValue={radius}
                value={radius}
                style={{ width: '90%' }}
                max={60}
                onChange={this.onChangeRadius}
              />
            </div>
            <div className="divider" />
            <div className="range">
              Notification status:
              <RadioGroup
                onChange={e => this.onChangeEnable(e)}
                defaultValue={enabled}
              >
                <Radio value={'true'}>Enabled</Radio>
                <Radio value={'false'}>Disabled</Radio>
              </RadioGroup>
            </div>
            <div className="range">
              Notification message:
              <TextArea
                rows={4}
                className="description-input"
                placeholder="Push notification message"
                value={message}
                onChange={e => this.onChangePushMessage(e)}
              />
            </div>
            <div className="range">
              Notification radius (feet):
              <Slider
                marks={marks}
                defaultValue={notificationRadius}
                value={notificationRadius}
                style={{ width: '90%' }}
                max={60}
                onChange={this.onChangeNotificationRadius}
              />
            <div className="divider" />

            </div>
            <div className="name">
              UUID:
              <Input
                className="name-input"
                placeholder="Name"
                value={id}
                maxLength="15"
                onChange={e => this.onChangeUUID(e)}
              />
            </div>
            <div className="name">
              Major:
              <Input
                className="name-input"
                placeholder="Name"
                value={max}
                maxLength="15"
                onChange={e => this.onChangeUUIDMax(e)}
              />
            </div>
            <div className="name">
              Minor:
              <Input
                className="name-input"
                placeholder="Name"
                value={min}
                maxLength="15"
                onChange={e => this.onChangeUUIDMin(e)}
              />
            </div>
          </div>
        </div>
        {/* buttons */}
        <Row className="buttons" type="flex" justify="end">
          <Col span={5}>
            <Button
              type="primary"
              size="small"
              className={`save clickable ${hasValidationErrors ? 'disabled' : ''}`}
              onClick={hasValidationErrors ? _.noop : this.save}
            >
              Save
            </Button>
          </Col>
          <Col span={5}>
            <Button
              size="small"
              className="cancel clickable"
              onClick={() => setSelectedPOI(null)}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

DeviceForm.propTypes = {
  // selected zones (or null for new zone)
  zone: PropTypes.instanceOf(Location),
  // save zone (update)
  save: PropTypes.func,
  // save zone (create),
  add: PropTypes.func,
  // select or update zone
  setZone: PropTypes.func
};

DeviceForm.defaultProps = {
  zone: null,
  save: _.noop,
  add: _.noop,
  setZone: _.noop
};

export default DeviceForm;
