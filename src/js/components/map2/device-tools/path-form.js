import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col, Icon, Input, Button, Select, Slider, Radio, Menu } from 'antd';
import Maki from 'maki';

import { zoneColors } from '../../../misc/colors';

const initialState = {
  initialPathCollection: {}
};

/**
 * @name PathForm
 *
 * @description A sidebar with tools to manage a single zone.
 * This tool can be used to create or edit zones. Users can
 * edit the zone name, select a color, and save or cancel. The
 * tool is designed to slide in and out using CSS.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class PathForm extends React.Component {
  /**
   * @constructor
   *
   * @param {Object} props - {@link ZoneForm.propTypes}
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.save = this.save.bind(this);
    this.getPath = this.getPath.bind(this);
    this.removePath = this.removePath.bind(this);
    this.renderPathContent = this.renderPathContent.bind(this);
    this.getPairs = this.getPairs.bind(this);
    this.reset = this.reset.bind(this);
  }

  componentDidMount() {
    const { pathCollection } = this.props;
    this.setState({ initialPathCollection: pathCollection });
  }
  /**
   * @name save
   *
   * @description Saves an exisiting zone. Used
   * to update the name, color, and coordinates
   * of a zone. After saving, the zone is deselected
   * and the zone tool is closed.
   */
  save() {
    const { pathActions, pathCollection, location } = this.props;
    const { addPath } = pathActions;
    if (pathCollection.floorId === '') {
      const newPathCollection = Object.assign({}, pathCollection, {
        ...pathCollection,
        floorId: location.getId()
      });
      addPath(newPathCollection);
    } else {
      addPath(pathCollection);
    }
  }

  getPath(f) {
    const { pathActions } = this.props;
    const { setSelectedPath } = pathActions;
    setSelectedPath(f);
  }

  removePath(f) {
    const { pathActions } = this.props;
    const { deletePath } = pathActions;
    this.save();
    setTimeout(() => {
      deletePath(f);
    }, 100)
  }

  renderPathContent() {
    const { selectedPath } = this.props;
    if (selectedPath && selectedPath.properties) {
      return this.renderSelectedPath();
    }
    return this.renderPathPoints();
  }

  renderPathPoints() {
    const { pathCollection } = this.props;
    const { features } = pathCollection;
    if (features.length <= 0) {
      return (
        <h6>Add paths by clicking on the map</h6>
      );
    }

    return features.map((f) => {
      const { properties } = f;
      const { id, coordinates } = properties;
      const [y, x] = coordinates;

      return (
        <Row className="points clickable" type="flex" align="middle" justify="space-between">
          <Col span={12} onClick={() => this.getPath(f)}>
            <h6>
              <span className="pointIcon">{id}</span>
              <span className="pointXy">x: {x}&#160;&#160;y: {y}</span>
            </h6>
          </Col>
          <Col span={2}>
            <Icon
              className="right-arrow clickable-2 red"
              type="close-circle-o"
              onClick={() => this.removePath(f)}
            />
          </Col>
        </Row>
      );
    });
  }

  getPairs(id) {
    const { pathCollection } = this.props;
    const { features } = pathCollection;
    const ids = features.map(f => f.properties.id);

    return ids.filter(e => e !== id).map(el => [id, el]);
  }

  hOverPair(v) {
    const { pathActions } = this.props;
    const { setActivePair } = pathActions;
    setActivePair(v);
  }

  addPair(v) {
    const { addPair } = this.props;
    addPair(v);
  }

  removePair(v) {
    const { removePair } = this.props;
    removePair(v);
  }

  reset(type = '') {
    const { initialPathCollection } = this.state;
    const { pathActions, closePathPanel, path } = this.props;
    const { addPath } = pathActions;
    addPath(path);
    if (type === 'closePanel') {
      closePathPanel('close');
    }
  }

  renderPathPointIcon(isPathDrawn, v) {
    if (isPathDrawn) {
      return (
        <div
          className="clickable"
          onClick={() => this.removePair(v)}
        >
          <Icon
            className="right-arrow clickable-2 red"
            type="close-circle-o"
          />
          <span>&#160;Remove Path</span>
        </div>
      );
    }
    return (
      <div
        className="clickable"
        onClick={() => this.addPair(v)}
      >
        <Icon
          className="right-arrow clickable-2 green"
          type="check-circle-o"
        />
        <span>&#160;Add Path</span>
      </div>
    );
  }
  renderSelectedPath() {
    const { selectedPath, pathCollection, pathActions } = this.props;
    const { pairs: allPairs } = pathCollection;
    const { properties } = selectedPath;
    if (!properties) return null;

    const { id, coordinates } = properties;
    const [y, x] = coordinates;

    const pairs = this.getPairs(id);
    return (
      <div>
        <Row className="points point-header" type="flex" align="middle" justify="space-between">
          <Col span={12}>
            <h6>
              <span className="pointIcon">{id}</span>
              <span className="pointXy">x: {x}&#160;&#160;y: {y}</span>
            </h6>
          </Col>
          <Col span={2}>
            <Icon
              className="clickable-2"
              type="close"
              onClick={() => pathActions.setSelectedPath([])}
            />
          </Col>
        </Row>
        {
          pairs.map((v) => {
            const isPathDrawn = allPairs.filter(f => f.toString() === v.toString() || f.reverse().join().toString() === v.toString()).length > 0;
            return (
            <Row className="points clickable path-row" type="flex" align="middle" justify="space-between" onMouseOver={() => this.hOverPair(v)} >
              <Col span={4}>
                <h6>
                  <span className="pointIcon">{v[0]}</span>
                  <Icon
                    className="clickable-2 red"
                    type="arrow-right"
                  />&#160;
                  <span className="pointIcon">{v[1]}</span>
                </h6>
              </Col>
              <Col span={12}>
                {this.renderPathPointIcon(isPathDrawn, v)}
              </Col>
            </Row>
            )
          })
       }
      </div>
    );
  }

  /**
   * @name render
   * @override
   */
  render() {
    const { pathCollection } = this.props;
    if (!pathCollection.features) {
      return null;
    }
    const { features } = pathCollection;

    return (
      <div className={`zone-tool open path`}>
        <div className="inner">
          {/* header */}
          <Row className="header" type="flex" align="middle" justify="space-between">
            <Col span={12}>
              <h5 className="title">Path points</h5>
            </Col>
            <Col span={2}>
              <Icon
                className="right-arrow clickable-2"
                type="close"
                onClick={() => this.reset('closePanel')}
              />
            </Col>
          </Row>

          {/* content */}
          <div className="content" onMouseLeave={() => this.hOverPair([])}>
            {/* name */}
            { this.renderPathContent() }


            {/* buttons */}


          </div>

        </div>
        <Row className="buttons" type="flex" justify="end">
          <Col span={5}>
            <Button
              type="primary"
              size="small"
              className="save clickable"
              onClick={this.save}
            >
              Save
            </Button>
          </Col>
          <Col span={5}>
            <Button
              size="small"
              className="cancel clickable"
              onClick={this.reset}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

PathForm.propTypes = {
  // save zone (update)
  save: PropTypes.func,
  // save zone (create),
  add: PropTypes.func,
};

PathForm.defaultProps = {
  save: _.noop,
  add: _.noop,
};

export default PathForm;
