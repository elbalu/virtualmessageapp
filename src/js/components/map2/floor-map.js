/* eslint-disable react/style-prop-object */
import React from 'react';
import Maki from 'maki';
import PropTypes from 'prop-types';
import * as MapboxGL from 'mapbox-gl';
import ReactMapboxGl, { Layer, Feature } from 'react-mapbox-gl';
import { Icon, Alert } from 'antd';
import _ from 'lodash';
import update from 'immutability-helper';

import Location from '../../misc/map/location';
import FloorMapUtil from '../../misc/map/floor-map-util';
import { styles } from '../../misc/map/styles';

import ToggleControl from './controls/toggle';
import config from '../../config';
import POIForm from './device-tools/device-form';
import PathForm from './device-tools/path-form';


const mapboxConfig = config.get('mapbox');
const accessToken = mapboxConfig.accessToken;
//  empty mapbox style linking to our own icon set
const defaultStyle = styles;

const defaultContainerStyle = { width: '90vw', height: '100%' };
const defaultBounds = [[-90, -90], [90, 90]];
const maxErrorReloads = 10;
const maxZoom = 18;

const states = {
  MOUNTING: 'mounting',
  INITIALIZING: 'initializing',
  LOADING: 'loading',
  READY: 'ready'
};

const initialState = {
  map: null,
  state: states.MOUNTING,
  drawing: false,
  errorReloads: 0,
  mouse: {
    x: 0,
    y: 0
  },
  layers: {
    zones: true,
    radius: false,
    poi: false,
    path: false
  },
  xValue: 0,
  yValue: 0,
  radius: 0,
  notificationRadius: 0,
  poiInProgress: false,
  poi: {
    type: 'Feature',
    properties: {
      id: null,
      name: null,
      icon: 'marker',
      keyWords: [],
      floorId: '',
      description: {
        long: '',
        short: ''
      },
      radius: 0,
      notification: {
        enabled: 'true',
        message: '',
        radius: 0
      },
      coordinates: [],
      uuid: {
        id: '',
        min: '0',
        max: '64000'
      }
    },
    geometry: {
      type: 'Point',
      coordinates: []
    }
  },
  pathCollection: {
    type: 'FeatureCollection',
    features: [],
    floorId: '',
    pairs: []
  },
  path: {
    type: 'Feature',
    properties: {
      id: 1,
      coordinates: [],
    },
    geometry: {
      type: 'Point',
      coordinates: []
    }
  },
  pathInProgress: false,
  newPath: {},
  previousPath: 0,
  nextPath: 0
};

const Mapbox = ReactMapboxGl({
  accessToken,
});

/**
 * WorldMap is a react component that wraps the
 * ReactMapboxGl component and manages viewing and
 * editing floor maps.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class FloorMap extends React.Component {

  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = initialState;

    this.mapDidRender = this.mapDidRender.bind(this);
    this.mapDidLoad = this.mapDidLoad.bind(this);
    this.handleMapError = this.handleMapError.bind(this);
    this.mapClick = this.mapClick.bind(this);
    this.mapLocationPointer = this.mapLocationPointer.bind(this);

    this.togglePOILayer = this.togglePOILayer.bind(this);
    this.getPOILayer = this.getPOILayer.bind(this);
    this.onClickPOI = this.onClickPOI.bind(this);
    this.onDrageEndPOI = this.onDrageEndPOI.bind(this);
    this.getSelectedPOILayer = this.getSelectedPOILayer.bind(this);
    this.getRadiusLayer = this.getRadiusLayer.bind(this);
    this.onRadiusChange = this.onRadiusChange.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.getSelectedRadiusLayer = this.getSelectedRadiusLayer.bind(this);
    this.onNotificationRadiusChange = this.onNotificationRadiusChange.bind(this);
    this.toggleRadiusLayer = this.toggleRadiusLayer.bind(this);

    this.togglePathLayer = this.togglePathLayer.bind(this);
    this.onClickPath = this.onClickPath.bind(this);
    this.getPathPointsLayer = this.getPathPointsLayer.bind(this);
    this.drawActivePair = this.drawActivePair.bind(this);
    this.getCordsForActivePair = this.getCordsForActivePair.bind(this);
    this.addPair = this.addPair.bind(this);
    this.removePair = this.removePair.bind(this);
    this.getCordsForAddedPair = this.getCordsForAddedPair.bind(this);
    this.closePathPanel = this.closePathPanel.bind(this);

    this.onPathPointDragEnd = this.onPathPointDragEnd.bind(this);
    this.hOverPathPoint = this.hOverPathPoint.bind(this);
    this.hLeavePathPoint = this.hLeavePathPoint.bind(this);
  }

  /**
   * WorldMap#componentDidUpdate checks
   * the location prop and, if the location is a
   * floor, requests the floor map image and adds
   * it to the mapbox map.
   *
   * @override
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const { map, state } = this.state;
    const { radius, location, notificationRadius, path } = this.props;
    const nextLocation = nextProps.location;
    const currentLocation = location;
    const hasNewLocation = !Location.same(currentLocation, nextLocation);
    const isFloor = Location.isFloor(nextLocation);
    const isInitializing = state === states.INITIALIZING;
    const canLoadImage = this.isReady() || isInitializing;

    if (radius !== nextProps.radius) {
      this.setState({ radius: nextProps.radius });
    }

    if (notificationRadius !== nextProps.notificationRadius) {
      this.setState({ notificationRadius: nextProps.notificationRadius });
    }

    if (path && nextProps.path) {
      if (path.features !== nextProps.path.features) {
        this.setState({ pathCollection: nextProps.path })
      }
      if (path.floorId === '') {
        const newPathCollection = Object.assign({}, path, {
          ...path,
          floorId: location.getId()
        });
        this.setState({ pathCollection: newPathCollection });
      }
    }
    if (canLoadImage && hasNewLocation && isFloor) {
      // if a floor has been selected, request the new floor image
      map.loadImage(nextLocation, defaultBounds);
      this.setState({ state: states.LOADING });
    }
  }

  renderIcons() {
    return (
          Maki.layouts.all.all.map(c => (
            <span>
              <div>{c}</div>
              <img src={`/svgs/${c}.svg`} alt={c} />
            </span>
          )
        )
    );
  }

  onRadiusChange(radius) {
    this.setState({ radius });
  }

  onNotificationRadiusChange(radius) {
    this.setState({ notificationRadius: radius });
  }
  /**
   * WorldMap#componentDidUpdate removes old
   * image sources from the map once a new
   * map is selected.
   *
   * @override
   */
  componentDidUpdate(prevProps) {
    const { map } = this.state;
    const prevLocation = prevProps.location;
    const currentLocation = this.props.location;
    const prevWidth = prevProps.width;
    const currentWidth = this.props.width;
    const hasOldImage = !Location.same(prevLocation, currentLocation) &&
      Location.isFloor(prevLocation);

    if (map) {
      // if the parent container changed, resize the map
      if (prevWidth !== currentWidth) map.mapbox.resize();

      // if there is an old floor image, remove it
      if (hasOldImage) map.removeImage(prevLocation.getImage());
    }
  }

  getPointerLocation() {
    const xyNode = (
      <div>
        <span style={{ padding: '10px' }}>x : <span ref={n => (this.xNode = n)}>0</span></span>
        <span>y : <span ref={n => (this.yNode = n)}>0</span></span>
      </div>
    );
    return (
      <div className="xy-container right">
        <p>{xyNode}</p>
      </div>
    );
  }

  /**
   * Floormap#getMessage returns a message or
   * icon to give the user feedback when a map
   * cannot be displayed because a map does not
   * exist or is loading.
   *
   * @return {XML|null}
   */
  getMessage() {
    const { location } = this.props;
    let message = null;

    // no message if there is a map to display
    if (this.hasFloorMap()) return null;

    // loading icon if something is loading or initializing
    if (!this.isReady()) message = <Icon type="loading" />;

    // message if a floor was selected but it does
    // not have a map
    if (this.isReady() && Location.isFloor(location) && !location.hasImage()) {
      message = <span>we do not have a map for this floor</span>;
    }

    const messageClassName = `controls
      controls-center
      controls-middle message
      ${_.isNil(message) ? 'hidden' : ''}
    `;

    return (
      <div className={messageClassName}>
        {message}
      </div>
    );
  }

  /**
   * WorldMap#getFloorLayer returns a
   * react-mapbox-gl.Layer component with
   * the image that corresponds to the selected
   * floor.
   *
   * @return {XML}
   */
  getFloorLayer() {
    const { location } = this.props;
    const name = location.getImage().imageName;

    return (
      <Layer
        key="floor-image"
        id="floor-image"
        type="raster"
        sourceId={name}
        before="background"
      />
    );
  }

  onClickPOI(f) {
    this.props.selectPoi(f);
    this.setState({ radius: f.properties.radius });
  }

  onDrageEndPOI(f) {
    this.props.selectPoi(f);
    this.setState({ radius: f.properties.radius });
  }

  hLeavePathPoint() {
    const { pathActions } = this.props;
    const { setActivePair } = pathActions;
    const { previousPath, nextPath } = this.state;
    if (previousPath > 0 && nextPath === 0) {
      setActivePair([]);
    }
  }

  onClickPath(e) {
    const path = e.feature.properties;
    const { previousPath, nextPath } = this.state;

    const parsedKeys = ['geometry', 'properties']
    const parsedObject = Object.keys(path).reduce((r, o) => {
      if (parsedKeys.indexOf(o) > -1) {
        r[o] = JSON.parse(e.feature.properties[o]);
      } else {
        r[o] = e.feature.properties[o];
      }
    	return r;
    }, {});
    const { pathActions } = this.props;
    const { setSelectedPath } = pathActions;

    if (previousPath <= 0) {
      this.setState({ previousPath: parsedObject.properties.id });
    }


    if (previousPath > 0) {
      this.addPair([previousPath, parsedObject.properties.id]);
      this.setState({ previousPath: 0 });
    }

    setSelectedPath(parsedObject);
  }

  getNewId(paths) {
    const ids = paths.map(p => p.properties.id);
    const max = _.isEmpty(ids) ? 0 : Math.max(...ids);
    return max + 1;
  }

  getPreviousId(paths) {
    const ids = paths.map(p => p.properties.id);
    const max = _.isEmpty(ids) ? 0 : Math.max(...ids);
    return max;
  }

  mapClick(mapp, e) {
    const map = this.map;
    const { layers, poi, path, pathCollection } = this.state;
    const { features } = pathCollection;
    const allFeaturesCoords = features.map(f => f.properties.coordinates);
    const { lngLat } = e;
    const xy = map.mapToFloorCoords(lngLat.lng, lngLat.lat);
    const x = Math.round(xy.x * 100) / 100;
    const y = Math.round(xy.y * 100) / 100;

    const isCoordsCloser = allFeaturesCoords.filter(f => (f[0] <= x + 2 && f[0] >= x - 2) && (f[1] <= y + 2 && f[1] >= y - 2)).length > 0;
    this.setState({ xValue: x, yValue: y });
    const isPOIEnabled = layers.poi;
    const isPathEnabled = layers.path;
    const previousId = this.getPreviousId(pathCollection.features);
    const newId = this.getNewId(pathCollection.features);

    if (isPathEnabled && !isCoordsCloser) {
      const np = Object.assign({}, path, {
        ...path,
        properties: {
          ...path.properties,
          id: newId,
          coordinates: [x, y]
        },
        geometry: {
          ...path.geometry,
          coordinates: [lngLat.lng, lngLat.lat]
        }
      });
      const newPC = Object.assign({}, pathCollection, {
        ...pathCollection,
        features: [
          ...pathCollection.features,
          np
        ]
      })
      this.setState({ pathCollection: newPC });

      if (previousId !== 0) {
        this.addPair([previousId, newId]);
      }
    }

    if (isPOIEnabled) {
      const newPOI = Object.assign({}, poi, {
        ...poi,
        properties: {
          ...poi.properties,
          coordinates: [x, y]
        },
        geometry: {
          ...poi.geometry,
          coordinates: [lngLat.lng, lngLat.lat]
        }
      })
      this.setState({ radius: newPOI.properties.radius, notificationRadius: newPOI.properties.notification.radius });

      this.setState({ poiInProgress: true });
      this.props.selectPoi(newPOI)
    }
  }

  onDragEnd(lngLat) {
    const { selectedPOI, poiActions } = this.props;
    if (!selectedPOI) return null;
    const { setSelectedPOI } = poiActions;
    const { lat, lng } = lngLat.lngLat;
    const map = this.map;
    const xy = map.mapToFloorCoords(lng, lat);
    const x = Math.round(xy.x * 100) / 100;
    const y = Math.round(xy.y * 100) / 100;
    this.setState({ xValue: x, yValue: y });
    const updatedPOI = Object.assign({}, selectedPOI, {
      ...selectedPOI,
      properties: {
        ...selectedPOI.properties,
        coordinates: [x, y]
      },
      geometry: {
        ...selectedPOI.geometry,
        coordinates: [lng, lat]
      }
    })
    setSelectedPOI({ ...updatedPOI });
  }

  onPathPointDragEnd(lngLat, id) {
    const { pathCollection } = this.state;
    const { features } = pathCollection;

    const { lat, lng } = lngLat.lngLat;
    const map = this.map;
    const xy = map.mapToFloorCoords(lng, lat);
    const x = Math.round(xy.x * 100) / 100;
    const y = Math.round(xy.y * 100) / 100;

    const modifiedFeatureArray = features.map((f) => {
      if (f.properties.id !== id) return f;
      const newFeature = f;
      newFeature.properties.coordinates = [x, y];
      newFeature.geometry.coordinates = [lng, lat];
      return newFeature;
    });

    const modifiedPathCollection = Object.assign({}, pathCollection, {
      ...pathCollection,
      features: modifiedFeatureArray
    });

    this.setState({ pathCollection: modifiedPathCollection });
  }

  getPOILayer() {
    const { poi, selectedPOI } = this.props;
    const { features } = poi;
    if (!features) return null;
    const groups = features.reduce((r, f) => {
      const { properties } = f;
      const { icon } = properties;
      if (!(icon in r)) {
        r[icon] = [];
      }
      r[icon].push(f)
      return r;
    }, {});
    return Object.keys(groups).map(g => (
      <Layer
        type="symbol"
        id={`poi-layer-${g}`}
        layout={{ 'icon-image': g }}

      >
        {
          groups[g].map(c => (
            <Feature
              coordinates={c.geometry.coordinates}
              draggable={true}
              properties={c}
              onClick={() => this.onClickPOI(c)}
            />
          ))
      }
      </Layer>
    ));
  }

  getPathPointsLayer() {
    const { pathCollection } = this.state;
    const { features } = pathCollection;
    if (features.length <= 0) {
      return (
        <div />
      );
    }
    return features.map((f) => {
      return (
        <Layer
          type="symbol"
          id={`path-layer-${f.properties.id}`}
          layout={{ 'text-field': f.properties.id.toString(), 'text-padding': 5, 'text-ignore-placement': true }}
          paint={{ 'text-halo-color': '#abc233', 'text-halo-width': 200, 'text-color': '#fff' }}
        >
          <Feature
            coordinates={f.geometry.coordinates}
            draggable={true}
            properties={f}
            onClick={this.onClickPath}
            onDragEnd={(e) => this.onPathPointDragEnd(e, f.properties.id)}
            onMouseEnter={() => this.hOverPathPoint(f.properties.id)}
            onMouseLeave={() => this.hLeavePathPoint()}
          />
        </Layer>
      )
    });
  }
  getRadiusLayer() {
    if (!this.state.layers.radius) {
      return (
        <div />
      );
    }
    const { poi, selectedPOI } = this.props;
    const { features } = poi;
    const groups = features.reduce((r, f) => {
      const { properties } = f;
      const { icon } = properties;
      if (!(icon in r)) {
        r[icon] = [];
      }
      r[icon].push(f)
      return r;
    }, {});
    return Object.keys(groups).map((g) => {
      return groups[g].map((c) => {
        return (
          <Layer
            type="circle"
            id={`radius-layer-${g}`}
            paint={{ 'circle-color': '#049FD9', 'circle-radius': c.properties.radius * 2, 'circle-opacity': 0.2 }}

          >
            <Feature
              coordinates={c.geometry.coordinates}
              properties={c}
            />
          </Layer>
        )
      })
    });
  }

  getSelectedRadiusLayer() {
    const { selectedPOI } = this.props;
    const { radius } = this.state;
    if (!selectedPOI) {
      return (
        <div />
      );
    }
    const { properties, geometry } = selectedPOI;
    const map = this.map;
    return (
      <Layer
        type="circle"
        id={`radius-layer-${properties.id}`}
        paint={{ 'circle-color': '#049FD9', 'circle-radius': radius * 2, 'circle-opacity': 0.2 }}
      >
        <Feature
          coordinates={geometry.coordinates}
        />
      </Layer>
    )
  }

  getSelectedNotificationRadiusLayer() {
    const { selectedPOI } = this.props;
    const { notificationRadius } = this.state;
    if (!selectedPOI) {
      return (
        <div />
      );
    }
    const { properties, geometry } = selectedPOI;
    const map = this.map;
    return (
      <Layer
        type="circle"
        id={`notification-radius-layer-${properties.id}`}
        paint={{ 'circle-color': '#CF1F2F', 'circle-radius': notificationRadius * 2, 'circle-opacity': 0.2 }}
      >
        <Feature
          coordinates={geometry.coordinates}
        />
      </Layer>
    )
  }

  getSelectedPOILayer() {
    const { selectedPOI } = this.props;

    if (!selectedPOI) {
      return (
        <div />
      );
    }
    const { properties, geometry } = selectedPOI;
    return (
      <Layer
        type="symbol"
        id={'poi-layer-selected-poi'}
        layout={{ 'icon-image': 'marker' }}
      >
        <Feature
          coordinates={geometry.coordinates}
          draggable={true}
          onDragEnd={this.onDragEnd}
        />
      </Layer>
    )
  }

  getCordsForActivePair() {
    const { pathCollection } = this.state;
    const { activePair } = this.props;

    const { features } = pathCollection;
    const cords = features.reduce((r, f) => {
      if (activePair.indexOf(f.properties.id) >= 0) {
        r.push(f.geometry.coordinates)
      }
      return r;
    }, []);
    return cords;
  }

  getCordsForAddedPair(pair) {
    const { pathCollection } = this.state;

    const { features } = pathCollection;

    const cords = features.reduce((r, f) => {
      if (pair.indexOf(f.properties.id) >= 0) {
        r.push(f.geometry.coordinates);
      }
      return r;
    }, []);
    return cords;
  }

  hOverPathPoint(id) {
    const { pathActions } = this.props;
    const { setActivePair } = pathActions;
    const { previousPath, nextPath } = this.state;
    if (previousPath > 0 && nextPath === 0) {
      setActivePair([previousPath, id]);
    }
  }

  addPair(v) {
    const { pathCollection } = this.state;
    const newPath = Object.assign({}, pathCollection, {
      ...pathCollection,
      pairs: [
        ...pathCollection.pairs,
        v
      ]
    });
    this.setState({ pathCollection: newPath });
  }

  removePair(v) {
    const { pathCollection } = this.state;
    const { pairs } = pathCollection;

    let newPairs = pairs.filter(p => p.toString() !== v.toString());

    newPairs = newPairs.filter(p => p.reverse().join().toString() !== v.toString());
    const newPath = Object.assign({}, pathCollection, {
      ...pathCollection,
      pairs: [
        ...newPairs
      ]
    });
    this.setState({ pathCollection: newPath });
  }

  closePathPanel(action) {
    const { layers } = this.state;

    if (action === 'close') {
      this.setState(update(this.state, {
        layers: {
          path: {
            $set: false
          }
        }
      }));
    }
  }
  drawActivePair() {
    const { activePair } = this.props;
    if (activePair <= 0) {
      return null;
    }
    const coords = this.getCordsForActivePair(activePair);
    return (
      <Layer
        type="line"
        id={`line-layer-${coords[0]}`}
        paint={{ 'line-color': '#abc233', 'line-width': 2, 'line-opacity': 0.4 }}
        layout={{ 'line-join': 'round', 'line-cap': 'round' }}
      >
        <Feature
          coordinates={coords}
        />
      </Layer>
    );
  }

  drawAddedPair() {
    const { pathCollection } = this.state;
    const { pairs } = pathCollection;
    if (pairs.length <= 0) {
      return null;
    }
    return (
      <Layer
        type="line"
        id={`line-add-layer-${pairs[0][0]}`}
        paint={{ 'line-color': '#abc233', 'line-width': 2, 'line-opacity': 1 }}
        layout={{ 'line-join': 'round', 'line-cap': 'round' }}
      >
        {
          pairs.map(p => {
            const coords = this.getCordsForAddedPair(p);
            return (
              <Feature
                coordinates={coords}
              />
            )
          })
        }
      </Layer>
    );
  }

  renderPathForm() {
    const { layers, pathCollection } = this.state;
    const { pathActions, selectedPath, path, location } = this.props;
    if (!layers.path) {
      return null;
    }
    return (
      <PathForm
        pathCollection={pathCollection}
        location={location}
        pathActions={pathActions}
        path={path}
        selectedPath={selectedPath}
        addPair={this.addPair}
        removePair={this.removePair}
        closePathPanel={this.closePathPanel}
      />
    );
  }
  /**
   * @name toggleZonesLayer
   *
   * @description Toggles the visibility of
   * the zones layer by updating component
   * state.
   *
   * @param {boolean} visible
   */
  toggleZonesLayer(visible) {
    this.setState(update(this.state, {
      layers: {
        zones: {
          $set: visible
        }
      }
    }));
  }

  /**
   * @name togglePOILayer
   *
   * @description Toggles the visibility of
   * the zones layer by updating component
   * state.
   *
   * @param {boolean} visible
   */
  togglePOILayer(visible) {
    this.setState(update(this.state, {
      layers: {
        poi: {
          $set: visible
        }
      }
    }));
  }

  /**
   * @name togglePOILayer
   *
   * @description Toggles the visibility of
   * the zones layer by updating component
   * state.
   *
   * @param {boolean} visible
   */
  togglePathLayer(visible) {
    this.setState(update(this.state, {
      layers: {
        path: {
          $set: visible
        }
      }
    }));
  }


  /**
   * @name togglePOILayer
   *
   * @description Toggles the visibility of
   * the radius layer by updating component
   * state.
   *
   * @param {boolean} visible
   */
  toggleRadiusLayer(visible) {
    this.setState(update(this.state, {
      layers: {
        radius: {
          $set: visible
        }
      }
    }));
  }

  /**
   * Floormap#isReady returns true if
   * the state of this component is ready.
   *
   * @return {boolean}
   */
  isReady() {
    return this.state.state === states.READY;
  }

  /**
   * Floormap#hasFloorMap returns true if
   * a floor has been selected and it has
   * a floor map.
   *
   * @return {boolean}
   */
  hasFloorMap() {
    const { location } = this.props;
    return this.isReady() && Location.isFloor(location) && location.hasImage();
  }

  /**
   * @name mapDidRender
   *
   * @description Updates component state
   * whenever the mapbox Map renders and is
   * fully loaded. This function is also called
   * when a floor map image is finished loading.
   */
  mapDidRender() {
    const { map } = this.state;
    if (map.mapbox.loaded() && !this.isReady()) {
      this.setState({
        map: _.clone(this.state.map),
        state: states.READY,
        errorReloads: 0
      });
    }
  }

  /**
   * @name mapLocationPointer
   *
   * @description displays x,y of
   * the placed pointer on mouse move
   */
  mapLocationPointer(e) {
    const map = this.map;
    const xy = map.mapToFloorCoords(e.lngLat.lng, e.lngLat.lat);
    const x = this.xNode;
    const xValue = Math.round(xy.x * 100) / 100;
    const yValue = Math.round(xy.y * 100) / 100;
    // this.setState({ xValue, yValue });
    x.innerHTML = xValue;
    const y = this.yNode;
    y.innerHTML = yValue;
  }

  /**
   * @name handleMapError
   *
   * @description Generic handler for all
   * Mapbox map errors. Currently handles error
   * when floor image does not load. This method
   * reloads the image, up to maxErrorReloads times.
   *
   * @param {{error: {message: string}}} e
   */
  handleMapError(e) {
    const { errorReloads } = this.state;
    const message = _.get(e, 'error.message', '');
    const sourceErrorRegex = /^layers.floor-image: source .+" not found$/;
    const isSourceError = !_.isEmpty(message.match(sourceErrorRegex));

    if (isSourceError && errorReloads < maxErrorReloads) {
      // reload floor map image
      const { location } = this.props;
      const { map } = this.state;
      map.loadImage(location, defaultBounds);
      this.state.errorReloads += 1;
      this.state.state = states.LOADING;
    } else {
      console.error(e);
    }
  }


  /*
  * Selecting a feature needs a handler on both the cluster circle and the individual icon layer
  * Because there is no z-index property and mouse events fire on all layers (overlapping), we need
  * to make sure we are only firing the click event on one layer. This layer is determined by
  * the mouseover event. If overlapping layers, the layer the mouse entered into first will be
  * the default focused layer
  *
  * Features from the same layer will never overlap, since clustering is enabled
  *
  * If you cant select the feature you desire because of overlapping, try hiding the conflicting
  * layers from the control panel
   */

  /**
   * @name mapDidLoad
   *
   * @description Stores a reference to the
   * mapbox map and a reference to an instance of
   * WorldMap.
   *
   * @param {Map} mapbox - Mapbox map instance. This
   * object provides the Mapbox GL JS APIs.
   */
  mapDidLoad(mapbox) {
    const { toggleLayers } = this.props;
    const map = new FloorMapUtil(mapbox);
    global.window.map = mapbox;
    this.map = map;


    // add navigation control
    mapbox.addControl(new MapboxGL.NavigationControl(), 'top-left');

    // register a listener that updates
    // component state each time the map renders.
    mapbox.on('render', this.mapDidRender);

    // show x,y on mouse move
    mapbox.on('mousemove', this.mapLocationPointer);

    // register map error handler
    mapbox.on('error', this.handleMapError);

    mapbox.addControl(new ToggleControl(
      'mapboxgl-icon fa fa-map-pin',
      this.togglePOILayer,
      'Create POI by clicking on map'
    ), 'top-left');

    mapbox.addControl(new ToggleControl(
      'mapboxgl-icon fa fa-exchange',
      this.togglePathLayer,
      'Draw path by clicking on map'
    ), 'top-left');


    // update state from INITIALIZING
    // to READY to trigger the initial
    // load of the default location
    this.setState({
      map,
      state: states.INITIALIZING
    }, () => mapbox.resize());
  }

  /**
   *
   * @return {XML}
   */
  render() {
    const layers = this.hasFloorMap() ? [this.getFloorLayer()] : [];
    const POIlayers = this.hasFloorMap() ? [this.getPOILayer()] : [];
    // const radiuslayers = this.hasFloorMap() ? [this.getRadiusLayer()] : [];
    const selectedPOIlayers = this.hasFloorMap() ? [this.getSelectedPOILayer()] : [];
    const selectedRadiusLayer = this.hasFloorMap() ? [this.getSelectedRadiusLayer()] : [];
    const selectedNotificationRadiusLayer = this.hasFloorMap() ? [this.getSelectedNotificationRadiusLayer()] : [];
    const activePairPath = this.hasFloorMap() ? [this.drawActivePair()] : [];
    const addedPairPath = this.hasFloorMap() ? [this.drawAddedPair()] : [];
    const pathLayer = this.hasFloorMap() ? [this.getPathPointsLayer()] : [];
    const iconLayer = this.hasFloorMap() ? [this.renderIcons()] : [];
    const { save, remove, location, zone, setZone, draw, selectedPOI, poiActions, tags } = this.props;
    const { xValue, yValue } = this.state;
    return (
      <div className="floor-map">

        <div>
          <POIForm
            selectedPOI={selectedPOI}
            location={location}
            poiActions={poiActions}
            save={save}
            add={this.addZone}
            remove={remove}
            tags={tags}
            xValue={xValue}
            yValue={yValue}
            onRadiusChange={this.onRadiusChange}
            onNotificationRadiusChange={this.onNotificationRadiusChange}
          />
          {this.renderPathForm()}
        </div>
        <Mapbox
          style={defaultStyle}
          accessToken={accessToken}
          containerStyle={defaultContainerStyle}
          onStyleLoad={this.mapDidLoad}
          maxZoom={maxZoom}
          onClick={this.mapClick}
        >
          {layers}
          {selectedRadiusLayer}
          {selectedNotificationRadiusLayer}
          {selectedPOIlayers}
          {POIlayers}
          {activePairPath}
          {addedPairPath}
          {pathLayer}
        </Mapbox>
        {this.getPointerLocation()}
      </div>
    );
  }
}

FloorMap.propTypes = {
  // whether or not layers can be toggled
  toggleLayers: PropTypes.bool,
  // currently selected location
  location: PropTypes.instanceOf(Location),
  // selected zone
  width: PropTypes.string,
  radius: PropTypes.number,
  save: PropTypes.func,
  remove: PropTypes.func,
  setZone: PropTypes.func,
  draw: PropTypes.func,
  tags: PropTypes.arrayOf(PropTypes.object)
};

FloorMap.defaultProps = {
  toggleLayers: false,
  location: null,
  zone: null,
  save: _.noop,
  remove: _.noop,
  add: _.noop,
  setZone: _.noop,
  width: '100vw',
  draw: true,
  onSelectAsset: _.noop
};

export default FloorMap;
