import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Location from '../../../misc/map/location';
import ZoneDrawControl from '../../../misc/map/zone-draw-control';
import FloorMapUtil from '../../../misc/map/floor-map-util';

/**
 * @name Zones
 *
 * @description Zones is a list of zones
 * that can be drawn on a Mapbox map. This
 * component manages selecting, drawing, editing, and
 * styling zones that have been drawn on a
 * floor map.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Zones extends React.Component {
  /**
   * @constructor
   *
   * @param {Object} props - React props
   */
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  /**
   * @name componentWillReceiveProps
   *
   * @description Checks if props.map is loaded
   * and initializes the ZoneDrawControl once
   * it is.
   *
   * @param {Object} nextProps - React props
   */
  componentWillReceiveProps(nextProps) {
    const currentMap = this.props.map;
    const nextLocations = nextProps.zones;
    const nextMap = nextProps.map;
    const nextLocation = nextProps.zone;
    const currentDraw = this.props.draw;
    const nextDraw = nextProps.draw;
    const visible = nextProps.visible;

    // initialize zone drawings once map is loaded
    if (!currentMap && nextMap) {
      this.mapDidLoad(nextMap);
    }

    if ((currentDraw !== nextDraw) && this.control) {
      this.control.enableDraw(nextDraw);
    }

    // draw the zones given in props.zones
    if (currentMap) {
      if (!visible) this.drawZones([]);
      else this.drawZones(nextLocations, nextLocation);
    }
  }

  /**
   * @name componentWillUnmount
   *
   * @description Unmounts the ZoneDrawControl.
   * This control is added using a mapbox API
   * outside of the normal React lifecycle so this
   * hook is required to prevent multiple instances
   * of the ZoneDrawControl from being mounted.
   */
  componentWillUnmount() {
    this.control.unmount();
  }

  /**
   * @name onChange
   *
   * @description Updates the vertices
   * of the current zone using the GeoJSON
   * feature passed in.
   *
   * @param {Object} feature - GeoJSON Polygon
   * Feature
   */
  onChange(feature) {
    const { zone, setZone, map } = this.props;

    if (zone && map) {
      // update zone with new vertices immutably
      const type = _.get(feature, 'geometry.type');
      if (type === 'Point') {
        setZone(zone.updatePointCoordinate(feature, map.mapToFloorCoords));
      } else {
        setZone(zone.updateVerticesFromPolygon(feature, map.mapToFloorCoords));
      }
    }
  }

  /**
   * @name onSelect
   *
   * @description When a feature is
   * selected, this method calls the setZone
   * prop function with the selected zone.
   *
   * @param {Object} feature - GeoJSON Polygon
   * Feature
   */
  onSelect(feature) {
    const { setZone, zones, map, zone } = this.props;
    const id = _.get(feature, 'id');
    const currentId = zone ? zone.getId() : null;
    let nextZone = _.find(zones, z => z.getId() === id);
    const isNewZone = !_.isNil(feature) && _.isNil(nextZone);

    if (id !== currentId) {
      if (feature) {
        if (isNewZone) {
          // create new zone
          const type = _.get(feature, 'geometry.type');
          if (type === 'Point') {
            nextZone = new Location(Location.LEVELS.DEVICE);
            nextZone = nextZone.updatePointCoordinate(feature, map.mapToFloorCoords);
          } else {
            nextZone = new Location(Location.LEVELS.ZONE);
            nextZone = nextZone.updateVerticesFromPolygon(feature, map.mapToFloorCoords);
          }
        }
      } else {
        // handle deselecting a zone during zone creation
        // and clicking on an already selected feature
        if (isNewZone) nextZone = zone; // deselecting a new zone causes bug
        else nextZone = null;
      }

      setZone(nextZone);
    }
  }

  /**
   * @name mapDidLoad
   *
   * @description Once the mapbox map is ready,
   * this method creates a new ZoneDrawControl
   * and draws the zones given in props.zones.
   * It will also select one of the zones if
   * props.zone is set.
   *
   * @param {FloorMapUtil} map - FloorMAp
   */
  mapDidLoad(map) {
    const { draw } = this.props;

    // initialize class that manages the
    // interaction with the mapbox map
    this.control = this.control || new ZoneDrawControl(
      map.mapbox,
      map.floorToMapCoords,
      draw
    );

    // mount the zone draw control on the
    // newly loaded map
    this.control.mount();

    // register a listener to select a
    // zone when its polygon is clicked
    this.control.onSelect(this.onSelect);

    // register a listener to respond
    // when a zone polygon has been updated
    this.control.onChange(this.onChange);
  }

  /**
   * @name drawZones
   *
   * @param {Location[]} zones - List
   * of zones to draw.
   *
   * @param {Location} zone -
   * Currently selected zone
   *
   * @description Draws all of the zones
   * given in props.zones or passed in.
   */
  drawZones(zones, zone) {
    if (!_.isNil(this.control)) {
      this.control.setZones(zones, zone);
    }
  }

  /**
   * @name render
   *
   * @description Render just returns null. This
   * method must be implemented for all React
   * components, but this component renders dom
   * nodes by interacting with Mapbox APIs rather
   * than returning any JSX like most components.
   *
   * @return {null}
   */
  render() { return null; }
}

Zones.propTypes = {
  visible: PropTypes.bool,
  // list of zones
  zones: PropTypes.arrayOf(PropTypes.instanceOf(Location)),
  // selected zone
  zone: PropTypes.instanceOf(Location),
  // select zone
  setZone: PropTypes.func,
  // FloorMapUtil
  map: PropTypes.instanceOf(FloorMapUtil),
  // whether or not to add draw button
  draw: PropTypes.bool
};

Zones.defaultProps = {
  visible: true,
  zones: [],
  zone: null,
  setZone: _.noop,
  map: null,
  draw: true
};

export default Zones;
