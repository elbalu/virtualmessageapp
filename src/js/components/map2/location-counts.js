import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Location from '../../misc/map/location';
import Loading from '../../components/loading';

const defaultDefaultValue = {
    clients: 0,
};

/**
 * LocationCounts shows counts for device types for selected location
 *
 * @class
 * @author Joshua Mayer <josmayer@cisco.com>
 */
class LocationCounts extends React.Component {

    /**
     * @constructor
     *
     * @param {Object} props
     */
    constructor(props) {
        super(props);
    }

    getDeviceCount(deviceType, selectedLoc, countsObj, floorCountsObj) {
        if(_.isEmpty(countsObj) && _.isEmpty(floorCountsObj)) {
            return (
                <Loading icon loading noPadding/>
            );
        }
        if(selectedLoc && selectedLoc.data) {
            if(selectedLoc.data.level == "FLOOR") {
                //use floorCountsObj
                return floorCountsObj.features.length;
            } else {
                //use countsObj
                if(countsObj && countsObj.results) {
                    return countsObj.results.counts[deviceType];
                } else return 0;
            }
        } else {
            if(countsObj && countsObj.results) {
                return countsObj.results.counts[deviceType];
            } else return 0;
        }
    }


    /**
     * @name LocationCounts#render
     *
     * @override
     * @return {XML}
     */
    render() {
        const { selectedLocation, activeCounts, activeFloorClients, activeFloorRogues, activeFloorRogueAPs, activeFloorTags } = this.props;
        const graphic = (
            <div>
                <div className="device-count-wrapper">
                    <span className="fa fa-mobile-phone device-count-icon"></span>
                    <span className="device-count-value">{ this.getDeviceCount("clients", selectedLocation, activeCounts, activeFloorClients) }</span>
                </div>
                <div className="device-count-wrapper">
                    <span className="fa fa-user-times device-count-icon"></span>
                    <span className="device-count-value">{ this.getDeviceCount("rogue_clients", selectedLocation, activeCounts, activeFloorRogues) }</span>
                </div>
                <div className="device-count-wrapper">
                    <span className="fa fa-user-secret device-count-icon"></span>
                    <span className="device-count-value">{ this.getDeviceCount("rogue_aps", selectedLocation, activeCounts, activeFloorRogueAPs) }</span>
                </div>
                <div className="device-count-wrapper">
                    <span className="fa fa-tag device-count-icon"></span>
                    <span className="device-count-value">{ this.getDeviceCount("tags", selectedLocation, activeCounts, activeFloorTags) }</span>
                </div>
            </div>
        );

        return (
            <div className="counts-list">
                {graphic}
            </div>
        );
    }
}

LocationCounts.propTypes = {
    // currently selected location
    selectedLocation: PropTypes.instanceOf(Location),
    activeCounts: PropTypes.object,
    activeFloorTags: PropTypes.object,
    activeFloorRogues: PropTypes.object,
    activeFloorRogueAPs: PropTypes.object,
    activeFloorClients: PropTypes.object,
};

LocationCounts.defaultProps = {
    selectedLocation: {},
    activeCounts: {},
    activeFloorTags: {},
    activeFloorRogues: {},
    activeFloorRogueAPs: {},
    activeFloorClients: {},
};

export default LocationCounts;
