import React from 'react';
import PropTypes from 'prop-types';
import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete';
import { Input, Popover, Icon } from 'antd';
import _ from 'lodash';

import Location from '../../../misc/map/location';

const initialState = {
  address: '',
  visible: false
};

class EditAddress extends React.Component {
  constructor(props) {
    super(props);
    const address = props.location ? props.location.getAddress() : '';
    this.state = { ...initialState, address };
    this.onChange = this.onChange.bind(this);
    this.setVisible = this.setVisible.bind(this);
    this.save = this.save.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const currentLocation = this.props.location;
    const nextLocation = nextProps.location;

    if (!Location.same(currentLocation, nextLocation) && nextLocation) {
      this.setState({ address: nextLocation.getAddress() });
    }
  }

  onChange(address) {
    this.setState({ address });
  }

  /**
   * @name setVisible
   *
   * @description Toggles the Edit
   * Address popover and resets
   * this.state.address on close.
   *
   * @param {boolean} visible
   */
  setVisible(visible) {
    // reset address on hide
    if (visible === false) {
      const { location } = this.props;
      const address = location ? location.getAddress() : '';
      this.setState({ address });
    }

    // hide or show popover
    this.setState({ visible });
  }

  save() {
    const { save, location } = this.props;
    const { address } = this.state;

    if (location) {
      // update location address
      location.setAddress(address);

      // geocode address to get lat/lng
      geocodeByAddress(address, (err, lngLat) => {
        if (!err) location.setLngLat(lngLat);
        save(location);
      });

      // reset and close form
      this.setVisible(false);
    }
  }

  render() {
    const { address, visible } = this.state;
    const { inline, location } = this.props;
    const savedAddress = location ? location.getAddress() : null;
    const inlineClass = inline ? 'inline' : '';

    const content = (
      <div>
        <PlacesAutocomplete
          hideLabel
          value={address || ''}
          onChange={this.onChange}
        >
          <Input />
        </PlacesAutocomplete>
        <div className="buttons">
          <a onClick={this.save}>Save</a>
          <a onClick={() => this.setVisible(false)}>Cancel</a>
        </div>
      </div>
    );

    return (
      <div className="edit-address" onClick={e => e.stopPropagation()}>

        {/* address display */}
        <div className={`address ${inlineClass}`} hidden={!savedAddress} title={savedAddress}>
          {savedAddress}
        </div>

        {/* missing address text */}
        <span className="address warning" hidden={savedAddress}>
          <span className="icon fa fa-exclamation-triangle" />
          &nbsp;missing address
        </span>

        {/* address edit popover */}
        <Popover
          title="edit address"
          trigger="click"
          content={content}
          overlayClassName="address-edit-form"
          visible={visible}
          onVisibleChange={this.setVisible}
        >
          <Icon
            type="edit"
            className="clickable"
          />
        </Popover>

      </div>
    );
  }
}

EditAddress.propTypes = {
  // location to set address for
  location: PropTypes.instanceOf(Location),
  // callback to save location
  save: PropTypes.func,
  // flag that keeps address on one
  // line if true, wraps if false
  inline: PropTypes.bool
};

EditAddress.defaultProps = {
  location: null,
  save: _.noop,
  inline: false
};

export default EditAddress;
