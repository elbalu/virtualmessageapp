import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Alert } from 'antd';
import _ from 'lodash';
import pluralize from 'pluralize';

import Location from '../../../misc/map/location';
import LocationSelector from './selector';
import Loading from '../../loading';
import CampusListItem from './list-items/campus-list-item';
import BuildingListItem from './list-items/building-list-item';
import FloorListItem from './list-items/floor-list-item';
import ZoneListItem from './list-items/zone-list-item';
import DeviceListItem from './list-items/device-list-item';
import APListItem from './list-items/ap-list-item';
import LocationDetails from './location-details';
import ZoneWizard from '../zone-tools/zone-wizard';

const MenuItem = Menu.Item;
const MenuItemGroup = Menu.ItemGroup;

/**
 * @class
 *
 * @name LocationMenu
 *
 * @description Menu for selecting floors from
 * a location tree. Includes the ability to edit
 * buildings using the Editor component.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class LocationMenu extends React.Component {
  /**
   * @constructor
   *
   * @param props
   */
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.onSelectZone = this.onSelectZone.bind(this);
    this.getPois = this.getPois.bind(this);
    this.deletePoi = this.deletePoi.bind(this);
    this.clonePoi = this.clonePoi.bind(this);
  }

  /**
   * LocationMenu#onSelect accepts location id
   * and sets it as the selected location.
   *
   * @param {{ key:string }} e - Ant Design Menu
   * onSelect event.
   */
  onSelect(e) {
    const { onSelect } = this.props;
    onSelect(e.key);
  }

  /**
   * @name LocationMenu#onSelectZone
   *
   * @description Accepts location id
   * and sets it as the selected zone. Does not
   * update the location menu.
   *
   * @param {{ key:string }} e - Ant Design Menu
   * onSelect event.
   */
  onSelectZone(e) {
    const { onSelectZone, selectedLocation } = this.props;
    const zones = selectedLocation.getZones();
    const zone = _.find(zones, z => z.getId() === e.key);
    const devices = selectedLocation.getDevices();
    const device = _.find(devices, y => y.getId() === e.key);
    if (device) {
      onSelectZone(device);
    } else {
      onSelectZone(zone);
    }
  }

  /**
   * @name getMenuItem
   *
   * @description Gets a menu item content
   * component by location level.
   *
   * @param {Location} location
   *
   * @return {XML}
   */
  getMenuItem(location) {
    const { save, remove } = this.props;
    switch (true) {
      case Location.isCampus(location):
        return <CampusListItem location={location} />;
      case Location.isBuilding(location):
        return <BuildingListItem location={location} save={save} />;
      case Location.isFloor(location):
        return <FloorListItem location={location} />;
      case Location.isDevice(location):
        return <DeviceListItem location={location} remove={remove} />;
      case Location.isAP(location):
        return <APListItem location={location} />;
      case Location.isZone(location): default:
        return <ZoneListItem location={location} remove={remove} />;

    }
  }

  /**
   * @name getMenu
   *
   * @description Returns an Ant Design
   * menu listing the children of the
   * selectedLocation.
   *
   * @return {XML}
   */
  getMenu() {
    const { selectedLocation, locations } = this.props;
    const isFloor = Location.isFloor(selectedLocation);
    const selectedId = selectedLocation ? [selectedLocation.getId()] : [];
    var children = selectedLocation ? selectedLocation.getChildren() : locations;

    children = _.filter(children, (child) => {
      return child.getLevel() !== 'AP';
    });

    children = _.filter(children, (child) => {
      return child.getLevel() !== 'DEVICE';
    });

    const listItems = _.map(children, c => (
      <MenuItem key={c.getId()} level={c.getLevel()}>
        {this.getMenuItem(c)}
      </MenuItem>
    ));

    // split up items by level (type)
    const sections = _.groupBy(listItems, i => pluralize(_.capitalize(i.props.level)));

    // add warnings
    const warnings = this.getWarnings();
    if (!_.isEmpty(warnings)) sections.warnings = warnings;

    if (isFloor) {
      const pois = this.getPois();
      if (!_.isEmpty(pois)) sections.pois = pois;
    }
    return (
      <Menu
        onClick={isFloor ? this.onSelectZone : this.onSelect}
        selectedKeys={selectedId}
      >
        {_.map(sections, (items, key) => (
          <MenuItemGroup key={key} title={(isFloor) ? (key === 'pois' ? 'POIs' : key) : ''}>
            {items}
          </MenuItemGroup>
        ))}
      </Menu>
    );
  }

  /**
   * @name getWarnings
   *
   * @description Returns a list of warnings
   * in the context of the selected location.
   * Includes things like 'no zones', 'no exciters',
   * and 'no floor map'
   *
   * @return {Array}
   */
  getWarnings() {
    const { selectedLocation } = this.props;
    const warnings = selectedLocation ? selectedLocation.getWarnings() : [];
    return warnings.map(w => (
      <Alert
        key={w.message}
        message={w.message}
        description={w.description}
        type="warning"
        showIcon
      />
    ));
  }
  selectPoi(p) {
    this.props.selectPoi(p);
    this.props.onRadiusSelect(p.properties.radius);
  }

  deletePoi(p) {
    const { poiActions } = this.props;
    const { deletePOI } = poiActions;
    deletePOI(p)
  }

  clonePoi(p) {
    const { poiActions } = this.props;
    const { addPOI } = poiActions;
    const [lng, lat] = p.geometry.coordinates;
    const newPOI = Object.assign({}, p, {
      ...p,
      properties: {
        ...p.properties,
        id: null,
        name: `${p.properties.name}-Copy`
      },
      geometry: {
        ...p.properties.geometry,
        coordinates: [lng + 0.1, lat + 0.1]
      }
    });
    addPOI(newPOI);
  }

  getPois() {
    const { poi, selectedPOI } = this.props;
    const selectedPOIActive = selectedPOI && !!selectedPOI.properties;
    if (!poi) return null;
    const { features } = poi;
    if (!features) {
      return null;
    }
    return features.map((f) => {
      let className = 'poi clickable';
      if (selectedPOIActive) {
        className += (f.properties.id === selectedPOI.properties.id) ? ' active' : '';
      }
      return (
        <div className={className}>
          <img src={`/svgs/${f.properties.icon}.svg`} alt={f.properties.icon} onClick={() => this.selectPoi(f)} />
          <span className="poiName" onClick={() => this.selectPoi(f)}>{f.properties.name}</span>
          <div className="actions">
            <span className="fa fa-times red" onClick={() => this.deletePoi(f)} title="delete" />
          </div>
        </div>
      )
    }
    );
  }
  /**
   * LocationMenu#render returns a tree of
   * locations.
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { locations, selectedLocation, onSelect, save } = this.props;
    const isBuilding = Location.isBuilding(selectedLocation);

    return (
      <div className="location-menu">
        <Loading icon loading={!locations} />
        <div className="content" hidden={!locations}>
          <div className="selector">
            <LocationSelector
              locations={locations}
              selectedLocation={selectedLocation}
              onSelect={onSelect}
            />
          </div>
          <div className="details" hidden={!isBuilding}>
            <LocationDetails location={selectedLocation} save={save} />
          </div>
          {this.getMenu()}
        </div>
      </div>
    );
  }
}

LocationMenu.propTypes = {
  // list of all locations
  locations: PropTypes.arrayOf(PropTypes.object),
  // on selecting a location
  onSelect: PropTypes.func,
  // select a zone
  onSelectZone: PropTypes.func,
  // selected zone
  selectedZone: PropTypes.instanceOf(Location),
  // currently chosen location
  selectedLocation: PropTypes.instanceOf(Location),
  // save a location (for updating building address)
  save: PropTypes.func,
  // remove a location (for deleting zones)
  remove: PropTypes.func
};

LocationMenu.defaultProps = {
  locations: [],
  onSelect: _.noop,
  onSelectZone: _.noop,
  selectedZone: null,
  selectedLocation: null,
  save: _.noop,
  remove: _.noop
};

export default LocationMenu;
