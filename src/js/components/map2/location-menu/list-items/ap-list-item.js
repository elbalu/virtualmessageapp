import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Popconfirm, Icon } from 'antd';

import Location from '../../../../misc/map/location';
import ListItem from './list-item';

const defaultColor = 'gray';
const nameMaxLength = 15;

/**
 * @class
 *
 * @name APListItem
 *
 * @description Location Menu Item representing
 * an AP.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 * @author Joshua Mayer <josmayer@cisco.com>
 */
class APListItem extends React.Component {
    /**
     * @name render
     *
     * @override
     *
     * @return {XML}
     */
    render() {
        const { location, remove } = this.props;
        const name = location ? location.getName() : 'no name';
        const color = location ? location.getColor() : defaultColor;

        const title = (
            <span>
        <i className="color fa fa-circle" style={{ color }} />
        <span className="ap-name" title={name}>
          {(_.size(name) > nameMaxLength) ? `${name.slice(0, nameMaxLength)}...` : name}
        </span>
      </span>
        );

        return (
            <ListItem className="floor-list-item" name={title} />
        );
    }
}

APListItem.propTypes = {
    // this location [zone]
    location: PropTypes.instanceOf(Location),

};

APListItem.defaultProps = {
    location: null,
};

export default APListItem;
