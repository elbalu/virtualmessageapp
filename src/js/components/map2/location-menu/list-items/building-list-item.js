import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Location from '../../../../misc/map/location';
import ListItem from './list-item';
import EditAddress from '../edit-address';

/**
 * @class
 *
 * @name BuildingListItem
 *
 * @description Location Menu Item for a building
 * location. Allows the user to add or edit the
 * building address.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class BuildingListItem extends React.Component {
  /**
   * @name getAddressInput
   *
   * @description Returns a
   * textarea element that autocompletes
   * addresses with save and cancel buttons.
   *
   * @return {XML}
   */
  getAddressInput() {
    const { location, save } = this.props;
    return <EditAddress inline location={location} save={save} />;
  }

  /**
   * @name render
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { location } = this.props;
    const name = location ? location.getName() : 'no name';
    const numFloors = location ? location.getCount(Location.LEVELS.FLOOR) : 0;

    // counts
    const counts = [{ icon: 'icon fontello fontello-floor', count: numFloors }];

    return (
      <ListItem
        className="building-list-item"
        name={name}
        counts={counts}
        extra={this.getAddressInput()}
      />
    );
  }
}

BuildingListItem.propTypes = {
  // selected building
  location: PropTypes.instanceOf(Location),
  // save building
  save: PropTypes.func
};

BuildingListItem.defaultProps = {
  location: null,
  save: _.noop
};

export default BuildingListItem;
