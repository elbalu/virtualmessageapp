import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import pluralize from 'pluralize';

import Location from '../../../../misc/map/location';
import ListItem from './list-item';

/**
 * @class
 *
 * @name CampusListItem
 *
 * @description Location Menu Item representing
 * a campus. Displays campus name, number
 * of buildings and number of buildings missing
 * addresses.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class CampusListItem extends React.Component {
  /**
   * @name render
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { location } = this.props;
    const name = location ? location.getName() : 'no name';
    const buildings = location ? location.getChildren() : [];
    const numBuildings = location ? location.getCount(Location.LEVELS.BUILDING) : 0;

    // counts
    const counts = [{ icon: 'icon fa fa-building-o', count: numBuildings }];

    // warnings
    const warnings = [];
    const buildingsWithoutAddresses = _.filter(buildings, b => !b.hasAddress());
    if (!_.isEmpty(buildingsWithoutAddresses)) {
      const count = _.size(buildingsWithoutAddresses);
      const message = `${count} ${pluralize('building', count)} missing address`;
      warnings.push({ icon: 'icon fa fa-exclamation-triangle', message });
    }

    return (
      <ListItem
        className="campus-list-item"
        name={name}
        counts={counts}
        warnings={warnings}
      />
    );
  }
}

CampusListItem.propTypes = {
  location: PropTypes.instanceOf(Location)
};

CampusListItem.defaultProps = {
  location: null
};

export default CampusListItem;
