import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Popconfirm, Icon } from 'antd';

import Location from '../../../../misc/map/location';
import ListItem from './list-item';

const defaultColor = 'gray';
const nameMaxLength = 15;

/**
 * @class
 *
 * @name ZoneListItem
 *
 * @description Location Menu Item representing
 * a zone. Allows the user to edit the color or
 * name and delete the zone.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class DeviceListItem extends React.Component {
  /**
   * @name render
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { location, remove } = this.props;
    const name = location ? location.getName() : 'no name';
    const color = location ? location.getColor() : defaultColor;

    const title = (
      <span>
        <i className="color fa fa-circle" style={{ color }} />
        <span className="zone-name" title={name}>
          {(_.size(name) > nameMaxLength) ? `${name.slice(0, nameMaxLength)}...` : name}
        </span>
        <Popconfirm
          title="Are you sure you want to delete this chokepoint?"
          okText="Yes"
          cancelText="No"
          onConfirm={() => remove(location)}
          onClick={e => e.stopPropagation()}
        >
          <Icon
            type="delete"
            title="remove"
            className="remove clickable red"
          />
        </Popconfirm>
      </span>
    );

    return (
      <ListItem className="floor-list-item" name={title} />
    );
  }
}

DeviceListItem.propTypes = {
  // this location [zone]
  location: PropTypes.instanceOf(Location),
  // delete the zone
  remove: PropTypes.func
};

DeviceListItem.defaultProps = {
  location: null,
  remove: _.noop
};

export default DeviceListItem;
