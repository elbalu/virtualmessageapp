import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Location from '../../../../misc/map/location';
import ListItem from './list-item';

/**
 * @class
 *
 * @name FloorListItem
 *
 * @description Location Menu Item representing
 * a floor. List the floor name and number of zones
 * and exciters.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class FloorListItem extends React.Component {
  /**
   * @name render
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { location } = this.props;
    const name = location ? location.getName() : 'no name';
    const numZones = location ? location.getCount(Location.LEVELS.ZONE) : 0;
    const numDevice = location ? location.getCount(Location.LEVELS.CHOKEPOINT) : 0;

    // counts
    const zonesCount = { icon: 'icon fa fa-object-ungroup', count: numZones };
    const deviceCount = { icon: 'icon fa fa-microchip', count: numDevice };

    const counts = [];
    counts.push(zonesCount);
    counts.push(deviceCount)

    return (
      <ListItem className="floor-list-item" name={name} counts={counts} />
    );
  }
}

FloorListItem.propTypes = {
  location: PropTypes.instanceOf(Location)
};

FloorListItem.defaultProps = {
  location: null
};

export default FloorListItem;