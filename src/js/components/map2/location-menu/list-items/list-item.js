import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

/**
 * @class
 *
 * @name ListItem
 *
 * @description A Location Menu Item. Displays
 * a name, stats, warnings, and additional info.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ListItem extends React.Component {
  /**
   * @name render
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { name, className, counts, warnings, extra } = this.props;

    return (
      <div className={`list-item ${className}`} >

        {/* name */}
        <h5 className="name clickable">{name}</h5>

        {/* counts like floors, zones, etc */}
        <p className="counts" hidden={_.isEmpty(counts)}>
          {_.map(counts, (c) => {
            if (React.isValidElement(c)) return React.cloneElement(c, { key: c.icon });
            return <span key={c.icon}><span className={c.icon} /> x {c.count}</span>;
          })}
        </p>

        {/* warnings like 5 buildings missing address */}
        <p className="warnings" hidden={_.isEmpty(warnings)}>
          {_.map(warnings, (w) => {
            if (React.isValidElement(w)) return React.cloneElement(w, { key: w.icon });
            return <span key={w.message}><span className={w.icon} /> {w.message}</span>;
          })}
        </p>

        {/* extra like the building address input */}
        <div className="extra" hidden={_.isNil(extra)}>
          {extra}
        </div>
      </div>
    );
  }
}

ListItem.propTypes = {
  name: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  className: PropTypes.string,
  counts: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.shape({
      icon: PropTypes.string,
      count: PropTypes.number
    })
  ])),
  warnings: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.shape({
      icon: PropTypes.string,
      message: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
      ])
    }),
    PropTypes.element
  ])),
  extra: PropTypes.element
};

ListItem.defaultProps = {
  name: '',
  className: '',
  counts: [],
  warnings: [],
  extra: null
};

export default ListItem;
