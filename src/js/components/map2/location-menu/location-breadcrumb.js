import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Breadcrumb } from 'antd';

import Location from '../../../misc/map/location';

const BreadcrumbItem = Breadcrumb.Item;

const allCampuses = 'All';

/**
 * @class
 *
 * @name LocationBreadcrumb
 *
 * @description LocationBreadcrumb navigation specifically
 * for the LocationMenu component.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class LocationBreadcrumb extends React.Component {
  /**
   * @name render
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { onSelect, selectedLocations } = this.props;
    const isAllCampuses = _.isEmpty(_.compact(selectedLocations));

    // prepend selected locations with null to represent
    // all campuses and remove the last element because
    // that is the selected location and does not belong
    // in the breadcrumb
    const locations = [null, ...selectedLocations.slice(0, -1)];

    return (
      <div
        className="location-breadcrumb"
        style={{ visibility: isAllCampuses ? 'hidden' : 'visible' }}
      >
        <Breadcrumb>
          {_.map(locations, (l) => {
            const id = l ? l.getId() : null;
            const name = l ? l.getName() : allCampuses;

            return (
              <BreadcrumbItem key={name}>
                <a onClick={() => onSelect(id)}>{name}</a>
              </BreadcrumbItem>
            );
          })}
        </Breadcrumb>
      </div>
    );
  }
}

LocationBreadcrumb.propTypes = {
  // list of selected location and parents starting from root
  selectedLocations: PropTypes.arrayOf(PropTypes.instanceOf(Location)),
  // callback when a location link is clicked
  onSelect: PropTypes.func
};

LocationBreadcrumb.defaultProps = {
  selectedLocations: [],
  onSelect: _.noop
};

export default LocationBreadcrumb;
