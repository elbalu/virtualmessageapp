import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Location from '../../../misc/map/location';
import EditAddress from './edit-address';

class LocationDetails extends React.Component {
  render() {
    const { location, save } = this.props;
    const isBuilding = Location.isBuilding(location);

    if (isBuilding) {
      return (
        <div className="location-details">
          <EditAddress location={location} save={save} />
        </div>
      );
    }

    return null;
  }
}

LocationDetails.propTypes = {
  location: PropTypes.instanceOf(Location),
  save: PropTypes.func,
  remove: PropTypes.func
};

LocationDetails.defaultProps = {
  location: null,
  save: _.noop,
  remove: _.noop
};

export default LocationDetails;
