import React from 'react';
import PropTypes from 'prop-types';
import { Cascader } from 'antd';
import _ from 'lodash';

import Location from '../../../misc/map/location';
import LocationBreadcrumb from './location-breadcrumb';

const allCampuses = 'All Campuses';

const initialState = {
  selectedIds: null
};

/**
 * LocationMenu is a tree of locations, displaying
 * campuses, buildings, and floors as a tree.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class LocationSelector extends React.Component {
  /**
   * @constructor
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.onSelect = this.onSelect.bind(this);
    this.onClick = this.onClick.bind(this);
    this.selectParent = this.selectParent.bind(this);
  }

  /**
   * @name LocationMenu#onSelect
   *
   * @description Stores selectedIds
   *
   * @param {string[]} selectedIds - List of ids
   * representing the map hierarchy path to the
   * selected location.
   */
  onSelect(selectedIds) {
    this.state.selectedIds = selectedIds;
  }

  /**
   * @name onClick
   *
   * @description Selects a location
   * using props.onSelect when the selector
   * menu is clicked
   */
  onClick() {
    const { onSelect } = this.props;
    const { selectedIds } = this.state;

    onSelect(_.last(selectedIds));
    this.setState({ selectedIds: null });
    this.cascader.handlePopupVisibleChange(false);
  }

  /**
   * @name selectParent
   *
   * @description Finds the parent of the selected location
   * and selects it. If the selected location is a campus
   * this method resets the selector.
   */
  selectParent() {
    const { selectedLocation, locations, onSelect } = this.props;

    if (!_.isNil(selectedLocation)) {
      // reset selector
      if (Location.isCampus(selectedLocation)) onSelect(null);

      // get and select parent
      const parent = selectedLocation.findParent(locations);
      if (parent) onSelect(parent.getId());
    }
  }

  /**
   * @name LocationMenu#render
   *
   * @description Returns a tree of
   * locations.
   *
   * @override
   *
   * @return {XML}
   */
  render() {
    const { locations, selectedLocation, onSelect, centered } = this.props;
    const treeData = _.map(locations, l => l.toTreeData(3, false));
    const selectedLocations = selectedLocation ? selectedLocation.findPath(locations) : [null];
    const selectedLocationPath = _.map(selectedLocations, l => (l ? l.getId() : null));

    // add 'All Campuses' option to treeData
    treeData.unshift({ value: null, label: allCampuses, isLeaf: true });

    return (
      <div className={`location-selector ${centered ? 'centered' : ''}`}>

        <LocationBreadcrumb onSelect={onSelect} selectedLocations={selectedLocations} />

        <div className="content">
          <Cascader
            changeOnSelect
            allowClear={false}
            expandTrigger="hover"
            value={selectedLocationPath}
            onChange={this.onSelect}
            options={treeData}
            getPopupContainer={() => global.document.getElementById('location-selector-options-container')}
            ref={(c) => { this.cascader = c; }}
            displayRender={label => _.last(label)}
          />
          <div
            id="location-selector-options-container"
            onClick={this.onClick}
          />
        </div>

      </div>
    );
  }
}

LocationSelector.propTypes = {
  // locations tree
  locations: PropTypes.arrayOf(PropTypes.object),
  // callback when a location is selected
  onSelect: PropTypes.func,
  // currently selected location
  selectedLocation: PropTypes.instanceOf(Location),
  // boolean, selector will be centered if true
  centered: PropTypes.bool
};

LocationSelector.defaultProps = {
  locations: [],
  onSelect: _.noop,
  selectedLocation: {},
  centered: false
};

export default LocationSelector;
