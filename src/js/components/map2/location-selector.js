import React from 'react';
import PropTypes from 'prop-types';
import { Cascader } from 'antd';
import _ from 'lodash';

import Location from '../../misc/map/location';
import Loading from '../../components/loading';

const defaultDefaultValue = {
  value: '*',
  label: 'All Campuses',
  isLeaf: true
};

/**
 * LocationMenu is a tree of locations, displaying
 * campuses, buildings, and floors as a tree.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class LocationSelector extends React.Component {
  /**
   * @name loadData
   *
   * @description Marks the selected option as
   * loading. This does not change the UI, but
   * prevents the Cascader from disappearing the
   * menu each time an option is selected. This is
   * useful when we are loading children because we
   * want to select a floor and load and present
   * zones.
   *
   * @param {Object[]} selectedOptions - And Design
   * Cascader options array.
   */
  static loadData(selectedOptions) {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;
  }

  /**
   * @constructor
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }

  /**
   * @name LocationMenu#onSelect
   *
   * @description Accepts location id
   * and sets it as the selected location.
   *
   * @param {string[]} selectedIds - List of ids
   * representing the map hierarchy path to the
   * selected location.
   */
  onSelect(selectedIds) {
    const { onSelect } = this.props;
    onSelect(_.last(selectedIds));
  }

  /**
   * @name LocationMenu#render
   *
   * @description Returns a tree of
   * locations.
   *
   * @override
   * @return {XML}
   */
  render() {
    const { locations, selectedLocation, trigger, defaultValue } = this.props;
    const treeData = _.map(locations, l => l.toTreeData());
    const selectedLocationPath = !_.isNil(selectedLocation) ?
      _.map(selectedLocation.findPath(locations), l => l.getId()) : _.compact([defaultValue.value]);

    // add default option to treeData
    treeData.unshift(defaultValue);

    // show tree with locations or show
    // loading icon if locations have not
    // been fetched yet
    const tree = (!locations) ? (
      <Loading icon loading />
    ) : (
      <Cascader
        changeOnSelect
        loadData={LocationSelector.loadData}
        expandTrigger={trigger}
        placeholder="select a location"
        className="locations-list"
        value={selectedLocationPath}
        onChange={this.onSelect}
        options={treeData}
      />
    );

    return (
      <div className="locations-list">
        {tree}
      </div>
    );
  }
}

LocationSelector.propTypes = {
  // locations tree
  locations: PropTypes.arrayOf(PropTypes.object),
  // callback when a location is selected
  onSelect: PropTypes.func,
  // currently selected location
  selectedLocation: PropTypes.instanceOf(Location),
  // Ant Design Cascader expandTrigger
  trigger: PropTypes.string,
  // label for 'All' option
  defaultValue: PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string
  })
};

LocationSelector.defaultProps = {
  locations: [],
  onSelect: _.noop,
  selectedLocation: {},
  trigger: 'click',
  defaultValue: defaultDefaultValue
};

export default LocationSelector;
