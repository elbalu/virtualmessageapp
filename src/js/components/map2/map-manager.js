import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import LocationMenu from './location-menu';
import FloorMap from './floor-map';
import WorldMap from './world-map';
import Location from '../../misc/map/location';

/* eslint-disable quotes */
/* eslint-disable quote-props */

const initialState = {
  width: '0',
  mapWidth: '100%',
  radius: 0
};

/**
 * MapManager is a lightweight react component that acts as a
 * parent for the WorldMap and WorldMap components.
 * The MapManager is responsible for switching between
 * maps by selecting locations.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class MapManager extends React.Component {
  /**
   * @constructor
   *
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.radiusSelect = this.radiusSelect.bind(this);
  }

  /**
   * @name componentWillReceiveProps
   *
   * @description Checks if the menu boolean
   * has changed and adjusts the width of
   * the map to compensate.
   *
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const { menu } = nextProps;

    if (menu) this.setState({ width: '78%' });
    else this.setState({ width: '100%' });
  }

  radiusSelect(radius) {
    this.setState({ radius })
  }
  /**
   * @override
   *
   * @return {XML}
   */
  render() {
    const { width, mapWidth, radius } = this.state;
    const { save, selectedLocation, selectLocation, activeFloorClients, activeFloorRogues, activeFloorRogueAPs, activeFloorTags, selectDevice,
      locations, remove, add, selectZone, selectedZone, menu, toggleLayers, poi, onSelectPoi, selectedPOI, poiActions, tags, path, pathActions, selectedPath, activePair } = this.props;
    const isFloor = Location.isFloor(selectedLocation);
    return (
      <div className="map-manager">
        {/* location menu */}
        <div className="menu" hidden={!menu}>
          <LocationMenu
            locations={locations}
            selectedLocation={selectedLocation}
            onSelect={selectLocation}
            selectedZone={selectedZone}
            onSelectZone={selectZone}
            save={save}
            remove={remove}
            poi={poi}
            poiActions={poiActions}
            selectPoi={onSelectPoi}
            selectedPOI={selectedPOI}
            onRadiusSelect={this.radiusSelect}
          />
        </div>

        {/* world map container */}
        <div
          className={`map-container ${isFloor ? '' : 'active'}`}
          style={{ width, overflow: 'hidden' }}
        >
          <WorldMap
            width={width}
            location={selectedLocation}
            locations={locations}
            onSelect={selectLocation}
          />
        </div>

        {/* floor map container */}
        <div
          id="map-container"
          className={`map-container ${isFloor ? 'active' : ''}`}
          style={{ width, overflow: 'hidden' }}
        >
          <FloorMap
            toggleLayers={toggleLayers}
            draw={menu}
            width={mapWidth}
            poi={poi}
            path={path}
            selectedPath={selectedPath}
            pathActions={pathActions}
            activePair={activePair}
            poiActions={poiActions}
            selectPoi={onSelectPoi}
            selectedPOI={selectedPOI}
            location={selectedLocation}
            save={save}
            remove={remove}
            add={add}
            radius={radius}
            tags={tags}
          />
        </div>
      </div>
    );
  }
}

MapManager.propTypes = {
  // list of locations
  locations: PropTypes.arrayOf(PropTypes.object),
  // default selected location (from url)
  selectedLocation: PropTypes.instanceOf(Location),
  // select location
  selectLocation: PropTypes.func,
  // select zone
  selectZone: PropTypes.func,
  // selected zone
  selectedZone: PropTypes.instanceOf(Location),
  // action creator to save a location
  save: PropTypes.func,
  // action creator to remove location
  remove: PropTypes.func,
  // action creator to create new location
  add: PropTypes.func,
  // boolean to show location menu
  menu: PropTypes.bool,
  // devices to draw (optional)
  selectDevice: PropTypes.func,
  // whether or not floormap layers can be toggled
  toggleLayers: PropTypes.bool
};

MapManager.defaultProps = {
  locations: [],
  selectedLocation: null,
  selectLocation: _.noop,
  save: _.noop,
  remove: _.noop,
  add: _.noop,
  menu: false,
  onSelectAsset: _.noop,
  toggleLayers: false
};

export default MapManager;
