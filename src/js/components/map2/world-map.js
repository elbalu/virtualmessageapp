/* eslint-disable react/style-prop-object */
import React from 'react';
import PropTypes from 'prop-types';
import mapboxgl, { NavigationControl } from 'mapbox-gl';
import ReactMapboxGl, { Marker, Layer, Feature } from 'react-mapbox-gl';
import { Alert, Tooltip } from 'antd';
import _ from 'lodash';

import Location from '../../misc/map/location';
import WorldMapUtil from '../../misc/map/world-map-util';
import config from '../../config';
import * as colors from '../../misc/colors';

const mapboxConfig = config.get('mapbox');
const accessToken = mapboxConfig.accessToken;
const defaultStyle = 'mapbox://styles/mapbox/streets-v9';
const defaultContainerStyle = { width: '100%', height: '100%' };
const maxZoom = 18;
// const defaultBounds = [
//   [-156.3048257876484, -85.0511300000003],
//   [311.6952109879056, 85.05113000000196]
// ];
const campusColor = colors.green;
const activeCampusColor = colors.ciscoBlue;

const states = {
  INITIALIZING: 'initializing',
  MOUNTING: 'mounting',
  LOADING: 'loading',
  READY: 'ready'
};

const initialState = {
  state: states.INITIALIZING,
  map: null
};

const POSITION_CIRCLE_PAINT = {
  'circle-stroke-width': 4,
  'circle-radius': 10,
  'circle-blur': 0.15,
  'circle-color': '#3770C6',
  'circle-stroke-color': 'white'
};

const Mapbox = ReactMapboxGl({
  accessToken,
});

/**
 * @class
 *
 * @name WorldMap
 *
 * @description React component that wraps the
 * ReactMapboxGl component and manages viewing
 * campuses and buildings.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 * @author Balu Loganathan  <baepalap@cisco.com>
 */
class WorldMap extends React.Component {
  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.mapDidLoad = this.mapDidLoad.bind(this);
    this.getMarkerForLocation = this.getMarkerForLocation.bind(this);
    this.getPolygonForCampus = this.getPolygonForCampus.bind(this);
    this.mapDidRender = this.mapDidRender.bind(this);
    this.mapDidZoom = this.mapDidZoom.bind(this);
  }

  /**
   * @name WorldMap#componentWillReceiveProps
   *
   * @description checks the location prop and,
   * if the location has changed, goes to that
   * location on the map.
   *
   * @override
   *
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const { locations } = nextProps;
    const nextLocation = nextProps.location;
    const currentLocation = this.props.location;
    const isNewLocation = !Location.same(currentLocation, nextLocation);
    const isBuilding = Location.isBuilding(nextLocation);
    const isCampus = Location.isCampus(nextLocation);
    const allCampus = Location.allCampus(nextLocation);

    // update map bounds when the selected location or
    // the selected location address changes
    if ((isNewLocation && (isBuilding || isCampus || allCampus)) || isBuilding) {
      this.setMapBounds(nextLocation, locations);
    }
  }

  /**
   * @name componentDidUpdate
   *
   * @description Resizes the map if the
   * parent container width has changed.
   *
   * @override
   */
  componentDidUpdate(prevProps) {
    const { map } = this.state;
    const prevWidth = prevProps.width;
    const currentWidth = this.props.width;

    if (map && prevWidth !== currentWidth) {
      map.mapbox.resize();
    }
  }

  /**
   * @name getBoundsForLocations
   *
   * @param {Location[]} locations - List of locations
   * that have {lnglat} positions that should be
   * fitted on the map.
   *
   * @return {mapboxgl.LngLatBounds} - Lng lat bounds
   * that fit all of the locations given.
   */
  getBoundsForLocations(locations) {
    // get only locations that have an address
    const locationsWithAddress = _.filter(locations, l => l.hasAddress());
    if (_.isEmpty(locationsWithAddress)) return null;

    // extand lnglat bounds to fit all of the addresses
    const bounds = new mapboxgl.LngLatBounds();
    _.each(locationsWithAddress, l => bounds.extend(l.getLngLat()));
    return bounds;
  }

  /**
   * @name getLayers
   *
   * @description Returns a list of all the
   * layers on the map.
   *
   * @return {Layer[]|Layer|null}
   */
  getLayers() {
    const { locations, location } = this.props;
    const isAllCampuses = _.isNil(location);
    const isCampus = Location.isCampus(location);
    const isBuilding = Location.isBuilding(location);

    // at the campus level, campuses are represented
    // by rounded polygons enclosing the buildings
    if (isAllCampuses || isCampus || isBuilding) {
      return _.map(locations, this.getPolygonForCampus);
    }

    return null;
  }

  /**
   * @name getPolygonForCampus
   *
   * @description Generates a rounded polygon
   * Layer that encloses all of the building
   * in the given campus and a label layer
   * that shows the campus name overlayed
   * on the polygon.
   *
   * @param {Location} location - Campus
   *
   * @return {XML}
   */
  getPolygonForCampus(location) {
    const { onSelect } = this.props;
    const selectedLocation = this.props.location;
    const isSelected = Location.same(location, selectedLocation);
    const name = location ? location.getName() : '';
    const id = location ? location.getId() : null;
    const { polygon } = WorldMapUtil.getCoordinatesForCampus(location);
    const color = (isSelected) ? activeCampusColor : campusColor;

    if (!polygon) return null;

    // polygon with background color
    // and a popup with the name
    return (
      <div key={`polygon-${id}`}>
        <Layer
          type="fill"
          key={`${name}-shape`}
          paint={{
            'fill-color': color,
            'fill-opacity': 0.4
          }}
        >
          <Feature
            coordinates={polygon}
            onClick={() => onSelect(id)}
          />
        </Layer>
      </div>
    );
  }

  /**
   * @name getMarkerForLocation
   *
   * @description Returns a react-mapboxgl
   * Marker for the given location. Uses lnglat
   * for buildings and finds the center of
   * building lnglats for campuses.
   *
   * @param {Location} location
   *
   * @param {number[]} [coordinates] -
   * Coordinates of the location. If not provided, they
   * will be calculated from the location provided.
   *
   * @return {XML}
   */
  getMarkerForLocation(location, coordinates) {
    const selectedLocation = this.props.location;
    const { onSelect } = this.props;
    const isSelected = Location.same(selectedLocation, location);
    const name = location.getName();
    const id = location.getId();
    const activeClass = (isSelected) ? 'active' : '';
    let lngLat = coordinates;
    let icon;

    // get info for a campus marker
    if (Location.isCampus(location)) {
      if (!lngLat) {
        const bounds = this.getBoundsForLocations(location.getChildren());
        lngLat = bounds ? bounds.getCenter().toArray() : null;
      }
      icon = (
        <span className={`campus fa-stack fa-lg ${activeClass}`}>
          <i className="fa fa-circle fa-stack-2x" />
          <i className="fa fa-map-o fa-stack-1x fa-inverse" />
        </span>
      );
    }

    // get info for a building marker
    if (Location.isBuilding(location)) {
      if (!lngLat) {
        lngLat = location.getLngLat();
      }
      icon = (
        <span className={`building fa-stack fa-lg ${activeClass}`}>
          <i className="fa fa-circle fa-stack-2x" />
          <i className="fa fa-building-o fa-stack-1x fa-inverse" />
        </span>
      );
    }

    if (_.isNil(lngLat)) return null;

    const locationSummary = (
      <div>
        <div>{`lat: ${lngLat[1]}`}</div>
        <div>{`lng: ${lngLat[0]}`}</div>
      </div>
    );

    return (
      <Marker
        className="marker"
        key={`marker-${name}`}
        onClick={() => onSelect(id)}
        coordinates={lngLat}
      >
        <Tooltip title={locationSummary}>
          <div className="name"><span>{name}</span></div>
          <div className="icon">{icon}</div>
        </Tooltip>
      </Marker>
    );
  }

  /**
   * @name getMarkers
   *
   * @description Returns a list of all of
   * the Markers that should be on the map.
   * Depends on the currently selected location
   * and list of locations in props.
   *
   * @return {Array}
   */
  getMarkers() {
    const { locations, location } = this.props;
    const isAllCampuses = _.isNil(location);
    const isCampus = Location.isCampus(location);
    const isBuilding = Location.isBuilding(location);

    // if no locations are selected, show markers
    // for each campus with clustering
    if (isAllCampuses) {
      return _.compact(_.map(locations, c => this.getMarkerForLocation(c)));
    }

    // if a campus or building is selected show markers
    // for buildings (there will be polygons for campuses)
    if (isAllCampuses || isCampus || isBuilding) {
      return (_.compact(_.flatten(_.map(locations, campus => (
        _.map(campus.getChildren(), b => this.getMarkerForLocation(b))
      )))));
    }

    return [];
  }

  /**
   * @name setMapBounds
   *
   * @description Finds a bounding box for the map
   * that will fit either all campuses or the
   * selected campus or building.
   *
   * @param {Location} [selectedLocation] - Currently
   * selected location
   *
   * @param {Location[]} [locations] - All locations
   */
  setMapBounds(selectedLocation = this.props.location, locations = this.props.locations) {
    const { map } = this.state;
    const isAllCampuses = _.isNil(selectedLocation);
    const isCampus = Location.isCampus(selectedLocation);
    const isBuilding = Location.isBuilding(selectedLocation);
    let bounds = null;
    if (map) {
      if (isAllCampuses) {
        // initialize map bounds for all campuses
        const allBuildings = _.flatten(_.map(locations, l => l.getChildren()));
        bounds = this.getBoundsForLocations(allBuildings);
      } else if (isCampus) {
        // set selected building bounds for campus
        const buildings = selectedLocation.getChildren();
        bounds = this.getBoundsForLocations(buildings);
      } else if (isBuilding) {
        // set selected building bounds for building
        bounds = this.getBoundsForLocations([selectedLocation]);
      }

      if (!bounds) {
        //set to New York city if no location addresses are present
        bounds = [[-73.9876, 40.7661], [-73.9397, 40.8002]];
        map.mapbox.fitBounds(bounds, { padding: 280, duration: 1000 });
      }
      else map.mapbox.fitBounds(bounds, { padding: 280, duration: 1000 });
    }
  }

  /**
   * @name getWarnings
   *
   * @description Gets a list of Alerts
   * for appropriate warnings like
   * missing address for the selected
   * location.
   *
   * @return {XML[]}
   */
  getWarnings() {
    const { location } = this.props;
    if (_.isNil(location)) return [];

    const hasLngLat = location.hasLngLat();
    const isCampus = Location.isCampus(location);
    const isBuilding = Location.isBuilding(location);
    const warnings = [];

    // campus missing address
    if (!hasLngLat && isCampus) {
      warnings.push(
        <Alert
          showIcon
          type="warning"
          key="campus-address-warning"
          message="This campus does not have an address"
          description="Make sure at least one building has an address"
        />
      );
    }

    // building missing address
    if (!hasLngLat && isBuilding) {
      warnings.push(
        <Alert
          showIcon
          type="warning"
          key="building-address-warning"
          message="This building does not have an address"
          description="You can add a location using the menu on the left"
        />
      );
    }

    return warnings;
  }

  /**
   * @name isReady
   *
   * @return {boolean} - True if
   * the state of this component is ready.
   */
  isReady() {
    return this.state.state === states.READY;
  }

  /**
   * @name mapDidRender
   *
   * @description Updates component state
   * whenever the mapbox Map renders and is
   * fully loaded.
   */
  mapDidRender() {
    const { map } = this.state;

    if (map.mapbox.loaded() && !this.isReady()) {
      this.setState({
        map: _.clone(this.state.map),
        state: states.READY
      });
    }
  }

  /**
   * @name mapDidZoom
   *
   * @description Updates selected
   * locations whenever zoom is changed.
   */
  mapDidZoom() {}

  /**
   * WorldMap#mapDidLoad stores a reference to the
   * mapbox map and a reference to an instance of
   * WorldMap.
   *
   * @param {WorldMap} mapbox - Mapbox map instance. This
   * object provides the Mapbox GL JS APIs.
   */
  mapDidLoad(mapbox) {
    global.window.worldmap = mapbox;
    // add navigation controls
    mapbox.addControl(new NavigationControl(), 'top-left');

    // register a listener that updates
    // component state each time the map renders.
    mapbox.on('render', this.mapDidRender);

    // register listener for when map zooms
    mapbox.on('zoomend', this.mapDidZoom);

    // update state from INITIALIZING to
    // READY, store map reference
    this.setState({
      map: new WorldMapUtil(mapbox),
      state: states.READY
    }, () => {
      mapbox.resize();
      this.setMapBounds();
    });
  }

  /**
   *
   * @return {XML}
   */
  render() {
    const { map, zoom } = this.state;
    const markers = this.getMarkers();
    const layers = this.getLayers();
    const warnings = this.getWarnings();
    const hasWarnings = !_.isEmpty(warnings);

    return (
      <div className="world-map">
        <div className={`message ${hasWarnings ? 'active' : ''}`}>
          {warnings}
        </div>

        <div
          className="world-map-container"
          hidden={!map}
        >
          <Mapbox
            style={defaultStyle}
            accessToken={accessToken}
            containerStyle={defaultContainerStyle}
            onStyleLoad={this.mapDidLoad}
            maxZoom={maxZoom}
          >
            {layers}
            {markers}
            <Layer
              type="circle"
              id="position-marker"
              paint={POSITION_CIRCLE_PAINT}
            >
              <Feature
                coordinates={[10, 10]}
                draggable={true}
                onDragEnd={this.onDragEnd}
              />
            </Layer>
          </Mapbox>
        </div>
      </div>
    );
  }
}

WorldMap.propTypes = {
  // currently selected location
  location: PropTypes.instanceOf(Location),
  // list of all locations,
  locations: PropTypes.arrayOf(PropTypes.instanceOf(Location)),
  // select a new location
  onSelect: PropTypes.func,
  // width of map container
  width: PropTypes.string
};

WorldMap.defaultProps = {
  location: null,
  locations: [],
  onSelect: _.noop,
  width: '78%'
};

export default WorldMap;
