import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col, Icon, Input, Button } from 'antd';

import Location from '../../../misc/map/location';
import ColorPicker from '../../color-picker';
import { zoneColors } from '../../../misc/colors';

/**
 * @name ZoneForm
 *
 * @description A sidebar with tools to manage a single zone.
 * This tool can be used to create or edit zones. Users can
 * edit the zone name, select a color, and save or cancel. The
 * tool is designed to slide in and out using CSS.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ZoneForm extends React.Component {
  /**
   * @constructor
   *
   * @param {Object} props - {@link ZoneForm.propTypes}
   */
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeColor = this.onChangeColor.bind(this);
    this.save = this.save.bind(this);
  }

  /**
   * @name onChangeName
   *
   * @description Updates the selected zone's
   * name.
   *
   * @param {Proxy} e - React event object
   */
  onChangeName(e) {
    const { zone, setZone } = this.props;
    const isZone = Location.isZone(zone);
    const name = _.get(e, 'target.value');

    if (isZone) {
      // update zone name immutably
      setZone(zone.updateName(name));
    }
  }

  /**
   * @name onChangeColor
   *
   * @description Updates the selected zone's
   * color.
   *
   * @param {string} color - New color as a hex string.
   */
  onChangeColor(color) {
    const { zone, setZone } = this.props;
    const isZone = Location.isZone(zone);

    if (isZone) {
      // update zone color immutably
      setZone(zone.updateColor(color));
    }
  }

  /**
   * @name save
   *
   * @description Saves an exisiting zone. Used
   * to update the name, color, and coordinates
   * of a zone. After saving, the zone is deselected
   * and the zone tool is closed.
   */
  save() {
    const { zone, save, add, setZone } = this.props;
    const isNew = Location.isNew(zone);

    if (isNew) add(zone);
    else save(zone);
    setZone(null);
  }

  /**
   * @name render
   * @override
   */
  render() {
    const { setZone, zone } = this.props;
    const isZone = Location.isZone(zone);
    const isNew = Location.isNew(zone);
    const color = isZone ? zone.getColor() : null;
    const name = isZone ? zone.getName() : '';
    const activeClass = isZone ? 'open' : '';
    const title = isNew ? 'Add Zone' : 'Edit Zone';
    const hasValidationErrors = _.isEmpty(name) || _.isEmpty(color);

    return (
      <div className={`zone-tool ${activeClass}`}>
        <div className="inner">
          {/* header */}
          <Row className="header" type="flex" align="middle" justify="space-between">
            <Col span={12}>
              <h5 className="title">{title}</h5>
            </Col>
            <Col span={2}>
              <Icon
                className="right-arrow clickable-2"
                type="verticle-left"
                onClick={() => setZone(null)}
              />
            </Col>
          </Row>

          {/* content */}
          <div className="content">

            {/* name */}
            <div className="name">
              <Input
                className="name-input"
                placeholder="Zone name"
                value={name}
                maxLength="15"
                onChange={this.onChangeName}
              />
            </div>

            {/* color */}
            <Row className="color" style={{ color }} type="flex" justify="space-between">
              <Col span={12}>
                <ColorPicker
                  style={{ width: '20%' }}
                  color={color}
                  onChange={this.onChangeColor}
                  colors={zoneColors.map(_.toLower)}
                >
                  <span className="clickable">Choose a color</span>
                </ColorPicker>
              </Col>
              <Col span={2}>
                <span className="fa fa-circle icon" />
              </Col>
            </Row>

            {/* buttons */}
            <Row className="buttons" type="flex" justify="end">
              <Col span={5}>
                <Button
                  type="primary"
                  size="small"
                  className={`save clickable ${hasValidationErrors ? 'disabled' : ''}`}
                  onClick={hasValidationErrors ? _.noop : this.save}
                >
                  Save
                </Button>
              </Col>
              <Col span={5}>
                <Button
                  size="small"
                  className="cancel clickable"
                  onClick={() => setZone(null)}
                >
                  Cancel
                </Button>
              </Col>
            </Row>

          </div>
        </div>
      </div>
    );
  }
}

ZoneForm.propTypes = {
  // selected zones (or null for new zone)
  zone: PropTypes.instanceOf(Location),
  // save zone (update)
  save: PropTypes.func,
  // save zone (create),
  add: PropTypes.func,
  // select or update zone
  setZone: PropTypes.func
};

ZoneForm.defaultProps = {
  zone: null,
  save: _.noop,
  add: _.noop,
  setZone: _.noop
};

export default ZoneForm;
