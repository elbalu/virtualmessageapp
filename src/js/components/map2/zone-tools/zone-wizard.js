import React from 'react';
import PropTypes from 'prop-types';
import { Steps } from 'antd';

const Step = Steps.Step;

/**
 * @name ZoneWizard
 *
 * @description A set of progress steps
 * that walks a user through creating a
 * zone on a floor map.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ZoneWizard extends React.Component {
  render() {
    const { step } = this.props;
    const activeClass = (step === -1) ? '' : 'active';

    return (
      <Steps direction="vertical" size="small" className={`zone-wizard ${activeClass}`} current={step}>
        <Step title="Draw" description="Click to create new vertices. Double click to finish." />
        <Step title="Name and Color" description="Choose a name and a color." />
        <Step title="Done" description="You are done!" />
      </Steps>
    );
  }
}

ZoneWizard.propTypes = {
  step: PropTypes.number
};

ZoneWizard.defaultProps = {
  step: 0
};

export default ZoneWizard;