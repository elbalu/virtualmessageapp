import React, { PropTypes, ContextTypes, Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import validate from 'validate.js';

import { toggleSubmenu } from '../../redux/actions/navigation-actions';

const navItemWithSubClass = 'with-subitem';
const navItemClass = 'nav-bar-item';
const linkClass = 'link';

const initialState = {
  open: true,
};

class NavBarItem extends Component {
  /**
   *
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);
    this.toggle = this.toggle.bind(this);
  }

  /**
   *
   */
  getSubItems() {
    const { children, currentPath } = this.props;

    if (validate.isEmpty(children)) return null;

    return children.map((child, index) => (
      <NavBarItem
        key={index}
        item={child}
        currentPath={currentPath}
      />
    ));
  }

  /**
   * toggle menu. If menu has no
   * children this is a noop
   */
  toggle() {
    const { children } = this.props;
    const { open } = this.state;
    const hasChildren = !validate.isEmpty(children);

    if (!hasChildren) return;

    this.setState({ open: !open });
  }

  /**
   *
   * @returns {XML}
   */
  render() {
    const { item, children, currentPath } = this.props;
    const { path, icon, title, hidden, disabled } = item;
    // const { open } = this.state;
    const open = true; // disable submenu toggling

    const isActive = item.path === currentPath.substring(1);
    const hasChildren = !validate.isEmpty(children);
    const itemClass = `${navItemClass}
      ${(hasChildren) ? navItemWithSubClass : ''}
      ${(hidden) ? ' hidden' : ''}
      ${(open) ? 'open' : ''}
      ${(isActive) ? 'active' : ''} `;
    const routeClass = `${linkClass}
      ${(disabled) ? 'disabled' : ''}`;

    return (
      <div className={itemClass}>
        {
          (hasChildren) ?
            <div className="placeHolder">
              <i className={icon} />
              <span>{title}</span>
            </div> :
            <Link
              id={path}
              to={path}
              className={routeClass}
              onClick={() => (hasChildren)?  null :this.toggle(item)}
            >
              <i className={icon} />
              <span>{title}</span>
            </Link>
        }
        <div className="nav-bar-item-list">
          {this.getSubItems()}
        </div>
      </div>
    );
  }
}

NavBarItem.propTypes = {
  item: PropTypes.shape(),
  children: PropTypes.arrayOf(PropTypes.object),
  currentPath: PropTypes.string,
};

NavBarItem.defaultProps = {
  item: {},
};

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  toggle: id => dispatch(toggleSubmenu(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBarItem);
