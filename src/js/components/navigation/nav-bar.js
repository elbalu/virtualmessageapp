import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import NavBarItem from './nav-bar-item';

/**
 * NavBar is a React component that manages
 * a navigation sidebar. It is given a navConfig
 * object and uses that to display a list of links.
 *
 * @class
 * @author Benjamin Newcomer
 */
class NavBar extends Component {
  /**
   * NavBar#getNavItems generates navigation
   * links from props.navConfig.
   *
   * @return {XML}
   */
  getNavItems() {
    const { navConfig, currentPath } = this.props;

    return navConfig.map((item, index) => {
      if (item.separator) return <div key={index} className="nav-bar-separator" />;

      return (
        <NavBarItem
          key={index}
          item={item}
          currentPath={currentPath}
        >
          {item.children}
        </NavBarItem>
      );
    });
  }

  /**
   * NavBar#render returns the nav bar
   * component.
   *
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div className="app-nav-bar">
        <div className="nav-bar-header" />
        {this.getNavItems()}
      </div>
    );
  }
}

NavBar.propTypes = {
  navConfig: PropTypes.instanceOf(Array),
  currentPath: PropTypes.string
};

const mapStateToProps = state => ({ navConfig: state.navigation });

export default connect(mapStateToProps)(NavBar);
