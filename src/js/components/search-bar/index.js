import React, { Component, PropTypes } from 'react';
import Menu from 'antd/lib/menu';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Input from 'antd/lib/input';
import Dropdown from 'antd/lib/dropdown';
import validate from 'validate.js';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';

const initialState = {
  searchParam: '',
};

/**
 * SearchBar is a component that provides
 * a text input, search button, reset button,
 * save/remove favorites buttons, and a list of
 * favorite searches. The SearchBar does not
 * implement any searches, but rather focuses on
 * UI functionality.
 *
 * @prop {function(string)} onSearch a callback that
 *  accepts a search term
 * @prop {Array} favoriteSearches A string array
 *  of favorite search terms
 * @prop {function(Array)} editFavoriteSearches a
 *  callback that accepts a string array and updates
 *  the favoriteSearches passed in
 *
 * @author Benjamin Newcomer
 */
class SearchBar extends Component {
  /**
   * constructor() sets initial state and
   * binds methods that will be called by the
   * DOM
   *
   * @override
   * @param {Array} props React props
   */
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);

    this.search = this.search.bind(this);
    this.saveFavoriteSearch = this.saveFavoriteSearch.bind(this);
    this.removeFavoriteSearch = this.removeFavoriteSearch.bind(this);
    this.reset = this.reset.bind(this);
  }

  /**
   * search() takes the stored searchParam
   * and passes it to the parent using
   * the provided onSearch() callback
   */
  search() {
    const { onSearch } = this.props;
    const { searchParam } = this.state;
    onSearch(searchParam);
  }

  /**
   * reset() clears the search term and
   * passes an empty string to the parent
   * to notify the parent of the reset
   */
  reset() {
    const { onSearch } = this.props;
    this.updateSearchParam('');
    onSearch('');
  }

  /**
   * saveFavoriteSearch() adds the current
   * searchParam to the favoriteSearches array
   * and passes the new array to the parent
   * using the editFavoriteSearches() callback
   */
  saveFavoriteSearch() {
    const { favoriteSearches, editFavoriteSearches } = this.props;
    const { searchParam } = this.state;
    if (searchParam) {
      if (!favoriteSearches) editFavoriteSearches([searchParam]);
      else editFavoriteSearches([...favoriteSearches, searchParam]);
    }
  }

  /**
   * updateSearchParam() updates
   * this.state.searchParam
   *
   * @param {string} value Search param
   */
  updateSearchParam(value) {
    const { onSearch } = this.props;
    this.setState({ searchParam: value });
    if (validate.isEmpty(value)) {
      onSearch('');
    }
  }

  /**
   * removeFavoriteSearch() remove the current
   * searchParam from the favoriteSearches array,
   * passes the new array to the parent using
   * the editFavoriteSearches() callback, and then
   * clears the search input
   */
  removeFavoriteSearch() {
    const { favoriteSearches, editFavoriteSearches } = this.props;
    const { searchParam } = this.state;

    const updatedSearches = favoriteSearches.filter(f => f !== searchParam);
    editFavoriteSearches(updatedSearches);
    this.reset();
  }

  /**
   * renderFavoriteSearches() returns a dropdown
   * menu that lists favorite searches and sets
   * the search when clicked
   *
   * @returns {XML}
   */
  renderFavoriteSearches() {
    const { favoriteSearches, onSearch } = this.props;

    if (!favoriteSearches || favoriteSearches.length === 0) {
      return (
        <Menu className="favorite-search">
          <Menu.Item><FormattedMessage id="search.favorites.none" /></Menu.Item>
        </Menu>
      );
    }

    const favItems = favoriteSearches.map(searchParam => (
      <Menu.Item
        key={searchParam}
      >
        <div><span>{searchParam}</span></div>
      </Menu.Item>
    ));

    return (
      <Menu
        onSelect={({ key }) => {
          this.updateSearchParam(key);
          onSearch(key);
        }}
      >
        {favItems}
      </Menu>
    );
  }

  /**
   * renderButtons() renders all actionable buttons
   * (save/remove favorite search, search, reset)
   *
   * @returns {XML}
   */
  renderButtons() {
    const { searchParam } = this.state;
    const { favoriteSearches, intl } = this.props;

    const isFavoriteSearch = favoriteSearches.includes(searchParam);
    const isSearchParamEmpty = validate.isEmpty(searchParam);
    const favoriteClass = (isFavoriteSearch) ? 'favorite' : null;
    const favoriteSearchClickHandler = (isFavoriteSearch) ?
        this.removeFavoriteSearch : this.saveFavoriteSearch;

    // i18n localized strings
    const clearTitle = intl.formatMessage({ id: 'clear' });
    const searchTitle = intl.formatMessage({ id: 'search' });
    const saveFavoriteTitle = intl.formatMessage({ id: 'search.favorites.save' });

    return (
      <span className="search-buttons right">
        <i
          className="fa fa-times search-reset-button clickable"
          title={clearTitle}
          onClick={this.reset}
          hidden={isSearchParamEmpty}
        />

        <i
          className="fa fa-search search-button clickable"
          title={searchTitle}
          onClick={this.search}
        />
        <i
          className={`fa fa-heart search-save-favorite-button clickable ${favoriteClass}`}
          title={saveFavoriteTitle}
          onClick={favoriteSearchClickHandler}
        />

      </span>
    );
  }

  /**
   * renderFavoritesDropdown() return a dropdown
   * that lists favorite searches
   *
   * @returns {*}
   */
  renderFavoritesDropdown() {
    const { favoriteSearches, editFavoriteSearches } = this.props;
    const areFavoritesEnabled = favoriteSearches && editFavoriteSearches;

    if (!areFavoritesEnabled) return null;

    return (
      <Dropdown overlay={this.renderFavoriteSearches()}>
        <a className="hyperlink">
          <FormattedMessage id="favorites" />
          &nbsp;
          <i className="fa fa-caret-down" />
        </a>
      </Dropdown>
    );
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { intl } = this.props;
    const { searchParam } = this.state;

    // i18n localized string
    const searchPlaceholder = intl.formatMessage({ id: 'search' });

    return (
      <div className="search-bar">
        <Row>
          <Col span={21}>
            <Row>

              <Col span={21}>
                <Input
                  className="search-input"
                  placeholder={searchPlaceholder}
                  onInput={e => this.updateSearchParam(e.target.value)}
                  onPressEnter={this.search}
                  value={searchParam}
                />
              </Col>

              <Col span={3}>
                <div className="search-actions">
                  {this.renderButtons()}
                </div>
              </Col>

            </Row>
            <div className="underline" />
          </Col>

          <Col span={3} className="search-favorites-dropdown ">
            <Dropdown overlay={this.renderFavoriteSearches()}>
              <a className="hyperlink">
                <FormattedMessage id="search.favorites" />
                &nbsp;
                <i className="fa fa-caret-down" />
              </a>
            </Dropdown>
          </Col>
        </Row>
      </div>
    );
  }
}

SearchBar.propTypes = {
  onSearch: PropTypes.func.isRequired,
  favoriteSearches: PropTypes.arrayOf(PropTypes.string),
  editFavoriteSearches: PropTypes.func,

  // i18n support
  intl: intlShape.isRequired,
};

export default injectIntl(SearchBar);
