import React, { Component, PropTypes } from 'react';

import SortableListItem from './item';

const initialState = {
  draggingIndex: null,
};

/**
 * SortableList uses the react-sortable
 * components to create a sortable list
 * with draggable items.
 *
 * @author Benjamin Newcomer
 */
class SortableList extends Component {
  /**
   * @override
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState, { items: props.items });
    this.updateState = this.updateState.bind(this);
  }

  /**
   * uses the updateListOrder hook to notify parent
   * when items are reordered. Also updates state to
   * track when an item is being dragged.
   *
   * @param {{ items:Object[], draggingIndex:number }} nextState
   */
  updateState(nextState) {
    const { updateListOrder } = this.props;
    if (nextState.items) updateListOrder(nextState.items.map(i => i.key));
    this.setState(Object.assign({}, this.state, {
      draggingIndex: nextState.draggingIndex,
    }));
  }

  /**
   * @override
   */
  render() {
    const { items, className } = this.props;
    const { draggingIndex } = this.state;

    return (
      <ul className={`sortable-list ${className}`} >
        {items.map((item, index) => (
          <SortableListItem
            childProps={{
              onClick: item.props.onClick,
              hidden: item.props.hidden,
            }}
            key={index}
            updateState={this.updateState}
            items={items}
            outline="list"
            draggingIndex={draggingIndex}
            sortId={index}
          >
            {item}
          </SortableListItem>
        ))}
      </ul>
    );
  }
}

SortableList.Item = SortableListItem;

SortableList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.element).isRequired,
  updateListOrder: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default SortableList;
