import React, { PropTypes } from 'react';
import { sortable } from 'react-sortable';

/**
 * SortableListItem is a wrapper around any React
 * element that implements sortable functionality
 * provided by react-sortable
 *
 * @param props
 * @constructor
 * @author Benjamin Newcomer
 */
const SortableListItem = props => (
  <li {...props} className="sortable-list-item draggable">
    {props.children}
  </li>
);

SortableListItem.propTypes = {
  children: PropTypes.element.isRequired,
};

export default sortable(SortableListItem);
