# Table Component

## Description
The Table component manages data tables and actions performed on the table data. This Table is built on top of the Ant.Design Table component and leverages the Ant Table functionality as much as possible.

## Usage

### Supplying Data
See https://ant.design/components/table/ for documentation regarding table data. Table passes the data through to the Ant.Design Table with any alterations using the dataSource prop.
 
### Filtering
Columns can be filtered using the Ant.Design filtering functionality. You can enable filtering on a specific column by supplying that column's key in the filters prop string array. The Table will generate a list of unique values for the column and provide them as filter options.

### Actions
Table provides several different types of actions. 

##### Actions Column
Table gives per-row action buttons in a column that is stuck to the right-hand side of the table. Edit and delete actions are provided by default and more actions can be provided by passing an array of functions that render action buttons using the actions prop. Each action function is passed an entire table row data object that it can use to render the button and configure itself. Actions placed here must operate on one and only one row.

##### Bulk Actions
Table allows for the selection of multiple rows that actions can be performed in bulk. To provide bulk actions, pass an array of functions that render buttons using the bulkActions prop. Each bulk action function is passed a list of keys representing the rows that are currently selected. Each action given here must operate on one or more rows.

##### Custom Actions
Additional actions can be added to Table Actions, which sits just on top of the table itself. To provide custom actions, pass an array of functions that render buttons using the customActions prop. Each function in list is called with no arguments and should return a button or link that performs an action. These actions are more flexible so the responsibility of the buttons is up to the developer.