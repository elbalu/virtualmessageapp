import React from 'react';
import PropTypes from 'prop-types';
import enUS from 'antd/lib/locale-provider/en_US';
import { injectIntl } from 'react-intl';
import { LocaleProvider, Table as BaseTable, Modal } from 'antd';
import _ from 'lodash';

import TableActions from './table-actions';
import ColumnManager from './table-actions/column-manager';
import BulkActions from './table-actions/bulk-actions';
import * as tableUtils from '../../misc/table-utils/index';
import * as preferenceUtils from '../../misc/table-utils/preference-utils';

const defaultPaginationConfig = {
  showSizeChanger: true,
  pageSizeOptions: ['10', '20', '40', '80'],
};

const initialState = {
  columns: [],
  storageKey: '',
  selectedRowKeys: [],
  filteredInfo: {},
  paginationConfig: {
    pageSize: 10,
    current: 1
  },
};

/**
 * Table is a React component that generates a table
 * based on an input data source and column configuration
 * object. The column configuration object defines settings
 * for columns, actions, and more.
 *
 * @class
 */
class Table extends React.Component {
  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);
    this.updateColumn = this.updateColumn.bind(this);
    this.updateColumns = this.updateColumns.bind(this);
    this.updatePagination = this.updatePagination.bind(this);
    this.updateCurrentPage = this.updateCurrentPage.bind(this);
    this.removeRow = this.removeRow.bind(this);
    this.clearRowSelection = this.clearRowSelection.bind(this);
    this.handleTableChange = this.handleTableChange.bind(this);
    this.handleRowSelection = this.handleRowSelection.bind(this);
    this.resetFilters = this.resetFilters.bind(this);

  }

  componentDidMount() {
    this.prepareColumns(this.props);
  }

  /**
   * Table#componentWillReceiveProps updates the table
   * columns with columns generated from the passed in template.
   *
   * @param {Object} nextProps new react properties
   */
  componentWillReceiveProps(nextProps) {
    this.prepareColumns(nextProps);
    this.clearRowSelection();
  }

  /**
   * Table#prepareColumns checks if a new template has been
   * passed in through props. If it has, this method generates
   * columns from the template and replaces its current column
   * config with the new columns. It also updates the saved
   * column config in local storage.
   *
   * @param {Object} props - React props
   */
  prepareColumns(props) {
    const { template, count, params } = props;
    let { columns, paginationConfig } = this.state;

    // if there is no template, then do nothing
    if (!template.fields) return;

    const noColumns = (columns.length === 0);
    const fieldsAreDifferent = !tableUtils.compareFields(columns, template.fields);
    const storageKey = template.entityName;

    // if there are no columns yet... or a new template has been passed in,
    // generate columns again
    if (noColumns || fieldsAreDifferent) {
      // check for ls saved columns and pagination config
      const columnVisibility = preferenceUtils.retrieveColumns(storageKey);
      // const savedPaginationConfig = preferenceUtils.retrievePaginationConfig(storageKey);

      // save new columns and optionally restore
      // saved pagination config
      columns = this.generateColumns(columnVisibility, props);
      // paginationConfig = savedPaginationConfig || paginationConfig;
      paginationConfig.total = count;

      // if page params current page is not same as state paginationConfig
      // current page then reset the current page to first page
      // This mismatch could happen when tables current page is 2 and selected widget
      // has only one page of data
      if (params && (paginationConfig.current > (params.pagination.page + 1))) {
        paginationConfig.current = 1;
      }
    }

    // now that we have columns and pagination config, save them
    // in local storage and update the table state
    preferenceUtils.saveColumns(storageKey, columns);
    preferenceUtils.savePaginationConfig(storageKey, paginationConfig);

    // save component state
    this.setState({ storageKey, columns, paginationConfig });
  }


  /**
   * Update the properties of a single column. This change is
   * persisited by updating the column config in local
   * storage.
   *
   * @param {Object} column
   */
  updateColumn(column) {
    const { columns, storageKey } = this.state;

    const updatedColumns = columns.map((c) => {
      if (c.key === column.key) return column;
      return c;
    });

    preferenceUtils.saveColumns(storageKey, updatedColumns);
    this.setState({ columns: updatedColumns });
  }

  /**
   * updateColumns simply replaces the columns
   * state property with the input nextColumns.
   * Also saves columns in local storage
   *
   * @param nextColumns
   */
  updateColumns(nextColumns) {
    const { template } = this.props;
    const storageKey = template.entityName;

    preferenceUtils.saveColumns(storageKey, nextColumns);
    this.setState({ columns: nextColumns });
  }

  /**
   * updatePagination() persists page size option
   * when a user changes the page size.
   *
   * @param {Number} current Current page number (starts with 1)
   * @param {Number} pageSize Number of rows per page
   */
  updatePagination(current, pageSize) {
    const { paginationConfig, storageKey } = this.state;
    const updatedPaginationConfig = Object.assign({}, paginationConfig, { pageSize, current });
    this.setState({ paginationConfig: updatedPaginationConfig }, () => {
      // preferenceUtils.savePaginationConfig(storageKey, updatedPaginationConfig);
    });
  }

  /**
   * Table#updateCurrentPage changes the current page for
   * the table.
   *
   * @param nextPage
   */
  updateCurrentPage(nextPage) {
    const { paginationConfig, storageKey } = this.state;
    const updatedPaginationConfig = Object.assign({}, paginationConfig, { current: nextPage });
    this.setState({ paginationConfig: updatedPaginationConfig }, () => {
      // preferenceUtils.savePaginationConfig(storageKey, updatedPaginationConfig);
    });
  }

  clearRowSelection() {
    this.setState({ selectedRowKeys: [] });
  }

  /**
   * actionColumn() adds action buttons to column
   * config using columnUtils
   *
   * @param {XML[]} actions - Array of buttons
   */
  actionColumn(actions) {
    const { edit } = this.props;
    const remove = (this.props.remove) ? this.removeRow : undefined;
    return tableUtils.actionColumn({ edit, remove, id: 0 }, actions);
  }

  /**
   * TODO
   * @param row
   */
  removeRow(row) {
    const { remove, template, rowKey } = this.props;
    const label = row.title || row.label || row.name || row.serial || row.firstName || '';
    const deleteText = `Are you sure you want to delete ${label} ?`;
    const removeParam = (rowKey) ? row[rowKey] : row.id;
    Modal.confirm({
      title: 'Delete',
      content: deleteText,
      onOk: () => remove(removeParam),
    });
  }

  /**
   * generateColumns() uses columnUtils#buildColumns() to
   * generate column defiition from a schema/template object
   *
   * @param {Array} [columnVisibility] Array of column keys with visibility flag
   *  all columns that should be visible
   * @param {Object} props
   * @returns {Array}
   */
  generateColumns(columnVisibility, props) {
    const { template, hasActions, details, showEditableOnly,
        remove, edit, dataSource, collections, actions, filters, fetchData } = props;
    const { filteredInfo } = this.state;
    // build array of columns
    const columns = tableUtils.buildColumns({
      template,
      details,
      columnVisibility,
      dataSource,
      collections,
      showEditableOnly,
      filteredInfo: filters || filteredInfo,
      // flag to set if sort and filters are handled by backend
      actionsIntegrated: !(fetchData) === false,
    });

    // append a column for actions
    const showActions = hasActions && (
      _.isFunction(remove) ||
      _.isFunction(edit) ||
      !_.isEmpty(actions)
    );
    if (showActions) {
      columns.push(this.actionColumn(actions));
    }

    return columns;
  }

  /**
   * prepareParams() converts the in built table
   * params to server readable format
   *
   * @param {Object} paging
   * @param {Object} filters
   * @param {Object} sorter
   * @returns {Object}
   */
  prepareParams(paging, filters, sorter) {
    const params = {};
    if (Object.keys(paging).length > 0) {
      params.pagination = {
        page: (paging.current - 1),
        limit: paging.pageSize,
      };
    }
    if (Object.keys(sorter).length > 0) {
      params.sort = {
        key: sorter.columnKey,
        direction: (sorter.order === 'ascend') ? 'ASC' : 'DESC'
      };
    }
    return {
      params,
      pageSize: paging.pageSize || 10
    };
  }


  /**
   * handles changes event from table generated by
   * filter selections and paging changes
   *
   * @param {Object} paging
   * @param {Object} filters
   */
  handleTableChange(paging, filters, sorter) {
    const { fetchData, setParams, filterSelect } = this.props;
    if (!(_.isEmpty(filters))) {
      this.setState({ filteredInfo: filters }, () => {
        if (!fetchData) {
          // then filtering and sorting done by table
          this.prepareColumns(this.props);
        }
        if (filterSelect) {
          filterSelect(filters);
        }
        // on any table change refresh the data
        if (fetchData) {
          // fetch filtered and sorted data from backend
          const params = this.prepareParams(paging, filters, sorter);
          setParams(params);
          fetchData();
        }
        // this.prepareColumns(this.props);
      });
    }
  }

  /**
   * handles clearing of table filters
   *
   * @param {Object} paging
   * @param {Object} filters
   */
  resetFilters() {
    const { filterSelect } = this.props;
    const { filteredInfo } = initialState;
    this.setState({ filteredInfo }, () => {
      this.prepareColumns(this.props);
      if (filterSelect)filterSelect(filteredInfo);
    });
  }

  /**
   * Table#handleRowSelection accepts a list of
   * selected table row keys. This method is called when
   * a user selects or deselects a row (for bulk edit). The
   * sdk page maintains selected row data so that it can
   * provide bulk edit actions like enable/disable and delete.
   *
   * @param {Array} selectedRowKeys - List of selected row keys
   */
  handleRowSelection(selectedRowKeys) {
    this.setState({ selectedRowKeys });
  }

  /**
   * Table#rowSelection returns a configuration object
   * for the Ant Table row selection. Row selection is
   * managed interally if bulk actions are provided. If
   * no bulk actions are provided, but a rowSelection object
   * is provided, that is used.
   *
   * @return {*}
   */
  rowSelection() {
    const { bulkActions, rowSelection } = this.props;

    if (bulkActions && !_.isEmpty(bulkActions.actions) && !bulkActions.customBulk) {
      return {
        onChange: this.handleRowSelection,
        selectedRowKeys: this.state.selectedRowKeys
      };
    }

    if (_.isObject(rowSelection)) return rowSelection;

    return null;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { dataSource, rowKey, hasEditAccess, openTemplateModal,
          bulkActions, customActions, template, customFields, entityType,
          fetchData, showEditableOnly } = this.props;

    const { columns, paginationConfig } = this.state;
    const columnsToShow = columns.filter(c => c.show);
    const { add, save, remove } = this.props.fieldActions;
    const scroll = (columnsToShow.length > 7) ? { x: columnsToShow.length * 200 } : undefined;

    const mergedPaginationConfig = _.assign(
      {
        onShowSizeChange: this.updatePagination,
        onChange: this.updateCurrentPage,
      },
      defaultPaginationConfig,
      paginationConfig,
    );

    const tableActions = (
      <TableActions
        actions={{
          openTemplateModal: (hasEditAccess) ? openTemplateModal : null,
          resetFilters: this.resetFilters,
          columns: {
            save: this.updateColumn,
            saveAll: this.updateColumns
          },
          fields: { add, save, remove },
          filters: { fetchData },
        }}
        bulkActions={bulkActions}
        selectedRowKeys={this.state.selectedRowKeys}
        customActions={customActions}
        template={template}
        columns={columns}
        filterOption={this.props.filterOption}
        filters={this.props.filters}
        search={this.props.search}
        customFields={customFields}
        entityType={entityType}
        download={this.props.download}
        showEditableOnly={showEditableOnly}
      />
    );

    return (
      <div className="table-wrapper">
        <div className="table">
          <LocaleProvider locale={enUS}>
            <BaseTable
              title={() => tableActions}
              rowKey={rowKey || 'id'}
              rowSelection={this.rowSelection()}
              rowClassName={(record, index) => `table-row table-row-${index}`}
              pagination={mergedPaginationConfig}
              columns={columnsToShow}
              dataSource={dataSource}
              scroll={scroll}
              onChange={this.handleTableChange}
            />
          </LocaleProvider>
        </div>
      </div>
    );
  }
}

Table.propTypes = {
  // data related props
  template: PropTypes.shape().isRequired,
  dataSource: PropTypes.arrayOf(PropTypes.object).isRequired,
  filters: PropTypes.instanceOf(Array),
  filterOption: PropTypes.shape(),
  rowKey: PropTypes.string,

  // fields actions
  fieldActions: PropTypes.shape({
    add: PropTypes.func,
    save: PropTypes.func,
    remove: PropTypes.func
  }),

  // visibility options
  showEditableOnly: PropTypes.bool,
  hideableColumns: PropTypes.bool,
  hasActions: PropTypes.bool,
  hasEditAccess: PropTypes.bool,

  // data manipulation
  remove: PropTypes.func,
  edit: PropTypes.func,
  details: PropTypes.func,
  openTemplateModal: PropTypes.func,
  filterSelect: PropTypes.func,

  // table row selection options
  rowSelection: PropTypes.shape({
    onChange: PropTypes.func,
    selectedRowKeys: PropTypes.arrayOf(PropTypes.any)
  }),

  // ability to add custom fields
  customFields: PropTypes.bool,

  /**
   * actions is an array of functions that
   * accept a table row and return a clickable
   * component that performs an action
   */
  actions: PropTypes.arrayOf(PropTypes.func),

  /**
   * array of functions where each accepts a list
   * of selected row keys and returns a component
   * that performs an action
   */
  bulkActions: PropTypes.shape({}),

  /**
   * array of functions where each returns a component
   * that performs an action
   */
  customActions: PropTypes.arrayOf(PropTypes.func),

  /**
   * props to enable search
   */
  search: PropTypes.shape({
    onSearch: PropTypes.func,
    favoriteSearches: PropTypes.arrayOf(PropTypes.string),
    editFavoriteSearches: PropTypes.func,
  }),

  /**
   * props to enable download
   */
  download: PropTypes.shape({}),

  /**
   * props to enable adding custom field
   * based on user role
   */
  entityType: PropTypes.string,
};

Table.defaultProps = {
  hasActions: true,
  hideableColumns: true,
  searchable: false,
  template: {},
  filters: null,
  filterOption: {
    categories: [],
    departments: [],
    locations: []
  },
  dataSource: [],
  showEditableOnly: false,
  hasEditAccess: false,
  fieldActions: {
    add: _.noop,
    save: null,
    remove: _.noop
  },
  search: {
    onSearch: null,
    favoriteSearches: [],
    editFavoriteSearches: _.noop
  },
  download: null,
  customFields: false,
  entityType: null
};

export {
  TableActions,
  BulkActions,
  ColumnManager
};

export default injectIntl(Table);
