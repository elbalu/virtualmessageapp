import React from 'react';
import PropTypes from 'prop-types';
import { Popover, Button } from 'antd';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';

const initialState = {
  visible: false
};

/**
 * BulkActions is a React component that provides a single button
 * with a Popover that gives the user a list of actions that can
 * be performed on a list of entities. This list of entities is
 * typically created using the row selection feature of the Table
 * component.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class BulkActions extends React.Component {
  /**
   * @constructor
   * @param {Object} props - React props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.handleVisibilityChange = this.handleVisibilityChange.bind(this);
  }

  /**
   * BulkActions#handleVisibilityChange is a click
   * handler that updates component state and
   * controls the visibility of the bulk actions
   * popover.
   *
   * @param {boolean} visible
   */
  handleVisibilityChange(visible) {
    this.setState({ visible });
  }

  /**
   * BulkActions#renderContent returns the
   * content of the BulkActions Popover. This
   * is where users will click to perform actions
   * on the entities they have selected.
   *
   * @return {XML}
   */
  content() {
    const { actions, selectedRowKeys } = this.props;

    return (
      <div className="bulk-actions-wrapper">
        <div className="title"><h5>Bulk Edit</h5></div>
        <ul className="bulk-actions">
          {actions.map((a, index) => (
            <li className="bulk-action" key={index}>{a(selectedRowKeys)}</li>
          ))}
        </ul>
      </div>
    );
  }

  /**
   * @override
   * @return {XML}
   */
  render() {
    const { actions } = this.props;
    if (_.isEmpty(actions)) return null;

    return (
      <Popover
        content={this.content()}
        trigger="click"
        placement="bottomRight"
        visible={this.state.visible}
        onVisibleChange={this.handleVisibilityChange}
      >
        <span title="bulk edit" className="clickable icomoon icomoon-bulk-edit" />
      </Popover>
    );
  }
}

BulkActions.propTypes = {
  // list of functions that each accept a list of
  // selected row keys and an index
  actions: PropTypes.arrayOf(PropTypes.func),
  // list of the keys (usually id) of selected table
  // rows
  selectedRowKeys: PropTypes.arrayOf(PropTypes.any)
};

BulkActions.defaultProps = {
  actions: [],
  selectedRowKeys: []
};

export default BulkActions;
