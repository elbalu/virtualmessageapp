import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';

import Popover from 'antd/lib/popover';
import SortableList from '../../sortable-list';

const getIconClass = show => ((show) ? 'fa-eye' : 'fa-eye-slash red');
const minNumColumns = 2;

const initialState = {
  visible: false,
};

/**
 * ColumnManager works with the Table component and manages
 * table columns. It provides functionality to show/hide columns
 * and reorder columns using hooks provided by its parent component.
 *
 * @deprecated Use {@link ColumnManager2}
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ColumnManager extends Component {
  /**
   * @override
   * @constructor
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);
    this.handleVisibleChange = this.handleVisibleChange.bind(this);
    this.updateColumnOrder = this.updateColumnOrder.bind(this);
  }

  /**
   * show/hide the column manager
   *
   * @param {boolean} visible
   */
  handleVisibleChange(visible) {
    this.setState({ visible });
  }

  /**
   * uses the onReorder hook to
   * send an updated list of column
   * order when a column item is dragged.
   *
   * @param {String[]} columnKeys
   */
  updateColumnOrder(columnKeys) {
    const { columns, onReorder } = this.props;
    const updatedColumns = columnKeys.map(key => columns.find(c => c.key === key));
    onReorder(updatedColumns);
  }

  /**
   * returns a draggable sortable list of
   * columns
   *
   * @return {XML}
   */
  renderDropdownContent() {
    const { columns, onUpdateColumn } = this.props;
    const numVisibleColumns = columns.filter(c => c.show && c.key !== 'actions').length;

    const columnItems = columns
      .map((column) => {
        const toggleColumnVisibility = () => onUpdateColumn({ ...column, show: !column.show });
        const className = `fa ${getIconClass(column.show)}`;
        const hidden = column.key === 'actions';
        const disabled = numVisibleColumns <= minNumColumns && column.show;

        return (
          <span
            key={column.key}
            hidden={hidden}
            onClick={(!disabled) ? toggleColumnVisibility : null}
            className={(disabled) ? 'disabled' : null}
          >
            <i className={className} />
            <span>{column.title}</span>
            <i className="fa fa-arrows pull-right" />
          </span>
        );
      });

    return (
      <div className="column-manager-content">
        <div className="message">
          <FormattedMessage id="columnManagerDescription" />
        </div>
        <SortableList
          className="column-manager-list"
          updateListOrder={this.updateColumnOrder}
          items={columnItems}
        />
      </div>
    );
  }

  /**
   * @override
   * @return {XML}
   */
  render() {
    const { visible } = this.state;

    return (
      <div className="column-toolbar">
        <Popover
          content={this.renderDropdownContent()}
          onVisibleChange={this.handleVisibleChange}
          visible={visible}
        >
          <a className="hyperlink">
            <FormattedMessage id="table.columns.hideShow" />
            &nbsp;
            <i className="fa fa-caret-down" />
          </a>
        </Popover>
      </div>
    );
  }
}

ColumnManager.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  onReorder: PropTypes.func,
  onUpdateColumn: PropTypes.func,
};

export default ColumnManager;
