import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
// import { Template, Form } from 'form-generator';
import { Button } from 'antd';

import fieldTemplates from './field-templates.json';
import config from '../../../../../config';

const entityNames = config.get('entityNames');

const initialState = {
  field: {}
};

/**
* FieldForm is a React component that accepts a field object
* and gives the user forms to edit the field. This form is used
* to edit field properties and the show/hide column property.
*
* @class
* @author Benjamin Newcomer <bnewcome@cisco.com>
*/
class FieldForm extends React.Component {
  /**
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = { ...initialState, field: props.field };
    this.onChange = this.onChange.bind(this);
    this.save = this.save.bind(this);
  }

  /**
   * FieldForm#onChange updates the field
   * in component state
   *
   * @param {Object} field
   */
  onChange(field) {
    this.setState({ field });
  }

  /**
   * FieldForm#save saves the field value
   * using props.save
   */
  save() {
    const { save } = this.props;
    const { field } = this.state;
    save(field);
  }

  /**
   * FieldForm#render returns two forms: one
   * for editing the field object and one for
   * editing column properties (show/hide). Field
   * properties are saved in the backend and column
   * properties are saved in localStorage.
   * @return {XML}
   */
  render() {
    const { className } = this.props;
    const { field } = this.state;
    const isDefault = field.isDefault;

    // if the field is a default field, return a
    // message instead of a form because default
    // fields are not user editable
    if (isDefault) {
      return (
        <div className="default-message">
          <span>default fields cannot be edited</span>
        </div>
      );
    }

    // if a save function was not provided,
    // give a message instructing the user to go
    // to another page (a config page) to edit fields
    if (!_.isFunction(this.props.save)) {
      return (
        <div className="default-message">
          <span>fields can only be edited from config pages</span>
        </div>
      );
    }

    // if the field type is not supported, return null
    if (_.isEmpty(fieldTemplates[field.type])) return null;

    // generate the form template for editing this field
    // and check for validationErrors
    // const template = new Template(fieldTemplates[field.type]);
    // const validationErrors = template.validate(field);

    // generate a save button that is disabled and
    // provides feedback if there are form errors
    const saveButton = (_.isEmpty(validationErrors)) ? (
      <Button
        icon="check-circle-o"
        className="save"
        onClick={this.save}
      >
        save
      </Button>
    ) : (
      <Button
        disabled
        icon="exclamation-circle-o"
        className="save disabled"
        onClick={this.save}
      >
        save
      </Button>
    );

    return (
      <div className={`field-form ${className}`}>
        {/* <Form
          template={template}
          value={field}
          onChange={this.onChange}
          inputWidth={19}
        /> */}
        <div className="buttons">
          {saveButton}
        </div>
      </div>
    );
  }
}

FieldForm.propTypes = {
  field: PropTypes.shape(),
  save: PropTypes.func,
  className: PropTypes.string
};

FieldForm.defaultProps = {
  field: {},
  save: null,
  className: ''
};

export default FieldForm;
