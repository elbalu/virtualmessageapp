import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import update from 'immutability-helper';

import SortableList from '../../../../sortable-list';
import ColumnListItem from './item';

/**
 * ColumnList is a list of columns/fields that gives
 * a summary of the fields in a template along with
 * actions like reordering, hiding, and editing fields.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ColumnList extends React.Component {
  constructor(props) {
    super(props);
    this.toggleVisibility = this.toggleVisibility.bind(this);
    this.reorder = this.reorder.bind(this);
  }

  /**
   * ColumnListItem#toggleVisibility toggles the
   * visibility for a single column by changing
   * that column's show property and then saving
   * the whole list of columns.
   *
   * @param {Object} column - Column to toggle
   */
  toggleVisibility(column) {
    const { onChange, list } = this.props.columns;
    onChange(list.map((c) => {
      if (c.key !== column.key) return c;
      return update(column, { show: { $apply: show => !show } });
    }));
  }

  /**
   * ColumnListItem#reorder saves the list of
   * reordered columns keys by updating the ordering
   * of the whole column list.
   *
   * @param {String[]} keys - List of column keys
   */
  reorder(keys) {
    const { list, onChange } = this.props.columns;
    const reorderedList = keys.map(key => _.find(list, ['key', key]));
    // add action column back everytime the columns are re ordered
    const actionColumn = _.find(list, ['key', 'actions']);
    if (actionColumn) reorderedList.push(actionColumn);
    onChange(reorderedList);
  }

  /**
   * ColumnListItem#render returns a Collapse with
   * one Panel.
   *
   * @return {XML}
   */
  render() {
    const fields = this.props.fields.list;
    const columns = this.props.columns.list;
    const { remove, save } = this.props.fields;

    // generate a set of list items from the columns
    // and fields passed in
    const items = _.compact(columns.map((column) => {
      // find corresponding field
      const field = _.find(fields, ['key', column.key]);
      if (_.isUndefined(field)) return null;

      return (
        <ColumnListItem
          key={column.key}
          customFields={this.props.customFields}
          column={column}
          field={field}
          remove={remove}
          toggleVisibility={this.toggleVisibility}
          save={save}
        />
      );
    }));

    return (
      <div className="column-list-wrapper">
        <SortableList
          className="column-list"
          updateListOrder={this.reorder}
          items={items}
        />
      </div>
    );
  }
}

ColumnList.propTypes = {
  fields: PropTypes.shape({
    list: PropTypes.arrayOf(PropTypes.object),
    save: PropTypes.func,
    remove: PropTypes.func,
  }),
  columns: PropTypes.shape({
    list: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func
  }),
  customFields: PropTypes.bool
};

ColumnList.defaultProps = {
  fields: {
    list: [],
    save: null,
    remove: _.noop
  },
  columns: {
    list: [],
    onChange: _.noop
  },
  customFields: true
};

export default ColumnList;
