import React from 'react';
import PropTypes from 'prop-types';
import { Collapse, Popconfirm } from 'antd';
import _ from 'lodash';

import FieldForm from './field-form';

const Panel = Collapse.Panel;

/**
 * ColumnListItem is a representation of a single
 * field/column pair that can be edited, deleted,
 * and toggled (visibility).
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ColumnListItem extends React.Component {
  /**
   * ColumnListItem#render returns a Collapse with
   * one Panel.
   *
   * @return {XML}
   */
  render() {
    const { column, remove, save, field, toggleVisibility, customFields } = this.props;

    // field form for editing a field
    const form = (
      <FieldForm
        className={`field-form-${field.key}`}
        field={field}
        save={save}
      />
    );

    // the visibility toggle is an open eye when the
    // column is visible and a slashed eye when the
    // column is hidden
    const visibilityToggle = (column.show) ? (
      <span
        title="visible"
        className="fa fa-eye clickable visibility"
        onClick={() => toggleVisibility(column)}
      />
    ) : (
      <span
        title="hidden"
        className="fa fa-eye-slash red clickable visibility visibility-hidden"
        onClick={() => toggleVisibility(column)}
      />
    );

    // the remove button confirms that users want to
    // remove this field. default fields cannot be
    // deleted so null is given instead of the button
    const removeButton = (!field.isDefault && customFields) ? (
      <Popconfirm
        title="Are you sure you want to delete this field?"
        onConfirm={() => remove(field)}
        okText="yes"
        cancelText="no"
      >
        <span
          hidden={field.isDefault}
          className="fa fa-times clickable"
          title="remove"
        />
      </Popconfirm>
    ) : null;

    const draggableIcon = (
      <span className="draggable-icon clickable">
        <i className="fa fa-ellipsis-v" />
        <i className="fa fa-ellipsis-v" />
      </span>
    );

    // the header contains the field name, reordering icon,
    // visibility toggle, and delete button
    const header = (
      <div className="header">
        {draggableIcon}
        <span>{field.label}</span>
        <span className="right" onClick={e => e.stopPropagation()}>
          <span onClick={e => e.stopPropagation()}>{removeButton}</span>
          {visibilityToggle}
        </span>
      </div>
    );

    return (
      <Collapse key={field.key} className="column-list-item">
        <Panel header={header} key="1">
          {form}
        </Panel>
      </Collapse>
    );
  }
}

ColumnListItem.propTypes = {
  field: PropTypes.shape(),
  save: PropTypes.func,
  remove: PropTypes.func,
  column: PropTypes.shape(),
  toggleVisibility: PropTypes.func,
  customFields: PropTypes.bool
};

ColumnListItem.defaultProps = {
  field: null,
  save: null,
  remove: _.noop,
  column: [],
  toggleVisibility: _.noop,
  customFields: true
};

export default ColumnListItem;
