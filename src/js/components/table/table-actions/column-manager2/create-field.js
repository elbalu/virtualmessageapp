import React from 'react';
import PropTypes from 'prop-types';
import { Popover, Button } from 'antd';
import _ from 'lodash';

import FieldForm from './column-list/field-form';
import Field from '../../../../misc/models/field';

const initialState = {
  visible: false
};

class CreateField extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.onVisibleChange = this.onVisibleChange.bind(this);
  }

  onVisibleChange() {
    this.setState({ visible: !this.state.visible });
  }

  render() {
    const { save } = this.props;

    const form = (
      <FieldForm
        field={Field.blank()}
        save={save}
      />
    );

    return (
      <Popover
        content={form}
        trigger="click"
        onVisibleChange={this.onVisibleChange}
        visible={this.state.visible}
      >
        <Button>Add a new column</Button>
      </Popover>
    );
  }
}

CreateField.propTypes = {
  save: PropTypes.func
};

CreateField.defaultProps = {
  save: _.noop
};

export default CreateField;
