import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'antd';
import _ from 'lodash';
import * as UserUtils from '../../../../misc/user-utils';
import { FormattedMessage } from 'react-intl';

import ColumnList from './column-list';
import CreateFieldWizard from '../../../form-builder/create-field-wizard';

const initialState = {
  visible: false,
  columns: []
};

/**
 * ColumnManager2 is a React component that renders a modal to manage
 * table fields/columns. It allows columns to be created, removed,
 * edited, hidden and sorted.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ColumnManager2 extends React.Component {
  /**
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = { ...initialState, columns: props.columns };
    this.changeColumns = this.changeColumns.bind(this);
    this.toggleVisibility = this.toggleVisibility.bind(this);
  }

  /**
   * ColumnManager2#componentWillReceiveProps updates
   * the columns list in component state with
   * incoming nextProps.columns.
   *
   * @override
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    this.setState({ columns: nextProps.columns });
  }

  /**
   * ColumnManager2#changeColumns updates the
   * columns list in component state.
   */
  changeColumns(columns) {
    const { onChange } = this.props.columns;
    onChange(columns);
  }

  /**
   * ColumnManager2#toggleVisibility toggles
   * modal visibility
   */
  toggleVisibility() {
    this.setState({ visible: !this.state.visible });
  }

  /**
   * ColumnManager2#render returns a link that opens the
   * ColumnManager modal when clicked.
   *
   * @override
   * @return {XML}
   */
  render() {
    const fields = this.props.fields.list;
    const columns = this.props.columns.list;
    const { add, save, remove, entityName } = this.props.fields;
    const { customFields, entityType } = this.props;

    const addAcess = UserUtils.hasAccess(entityType, 'add');

    const newFieldPropmt = <span><span className="fa fa-plus" /> add a new field</span>;

    return (
      <span className="column-manager-wrapper">
        <span
          onClick={this.toggleVisibility}
          title="manage columns"
          className="clickable icomoon icomoon-settings"
        />
        <Modal
          title="Manage Columns"
          visible={this.state.visible}
          className="column-manager"
          footer={false}
          onCancel={this.toggleVisibility}
          ref={(ref) => { this.modal = ref; }}
        >
          <ColumnList
            customFields={customFields}
            fields={{
              save,
              remove,
              list: fields
            }}
            columns={{
              list: columns,
              onChange: this.changeColumns
            }}
          />
          { (customFields && addAcess) ? (
            <div className="create-field">
              <CreateFieldWizard entityName={entityName} add={add} prompt={newFieldPropmt} />
            </div>
          ) : '' }
        </Modal>
      </span>
    );
  }
}

/* eslint-disable react/no-unused-prop-types */
ColumnManager2.propTypes = {
  fields: PropTypes.shape({
    list: PropTypes.arrayOf(PropTypes.object),
    entityName: PropTypes.string,
    save: PropTypes.func,
    remove: PropTypes.func,
    add: PropTypes.func
  }),
  columns: PropTypes.shape({
    list: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func
  }),
  customFields: PropTypes.bool,
};

ColumnManager2.defaultProps = {
  fields: {
    list: [],
    save: null,
    remove: _.noop,
    add: _.noop
  },
  columns: {
    list: [],
    onChange: _.noop
  }
};

export default ColumnManager2;
