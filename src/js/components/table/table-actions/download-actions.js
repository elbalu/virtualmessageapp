import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

/**
 * DownloadActions is a table action that downloads
 * table data in various formats like csv, pdf, xls
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class DownloadActions extends React.Component {
  /**
   * @override
   * @return {*}
   */
  render() {
    const { download } = this.props;
    if (_.isEmpty(download)) return null;

    const types = [];
    Object.keys(download).forEach((key) => {
      types.push(
        <span
          onClick={download[key]}
          title={`Download ${key}`}
          key={key}
          className={`icomoon icomoon-${key}-download clickable`}
        />
      );
    });
    return (
      <span>
        { types }
      </span>
    );
  }
}

DownloadActions.propTypes = {
  download: PropTypes.shape()
};

DownloadActions.defaultProps = {
  download: null,
};

export default DownloadActions;
