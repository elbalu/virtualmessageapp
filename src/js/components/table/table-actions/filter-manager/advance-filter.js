import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col, Button, Select, Input, Icon, TreeSelect, DatePicker } from 'antd';
import _ from 'lodash';
import * as operators from './operators';
import { generateSelectorFields, convertOptionsToSelectors } from '../../../../misc/filter-utils';

import SelectField from './fields/select-field';
import DefaultField from './fields/default-field';
import TemperatureField from './fields/temperature-field';
import HumidityField from './fields/humidity-field';
import BatteryField from './fields/battery-field';
import DateField from './fields/date-field';
import BasicField from './fields/basic-field';


const fieldObjs = {
  TEXT: { key: '', type: 'TEXT', operator: 'contains', values: [] },
  PARAGRAPH: { key: '', type: 'TEXT', operator: 'contains', values: [] },
  SELECT: { key: '', type: 'ENUM', operator: 'selection', values: [] },
  LOCATION: { key: '', type: 'ENUM', operator: 'selection', values: [] },
  TREE_SELECT: { key: '', type: 'ENUM', operator: 'selection', values: [] },
  TEL: { key: '', type: 'TEXT', operator: 'contains', values: [] },
  DATE: { key: '', type: 'DATE', operator: 'before', values: [] },
  DATETIME: { key: '', type: 'DATE', operator: 'before', values: [] },
  ENUM: { key: '', type: 'ENUM', operator: 'selection', values: [] },
  tagBattery: { key: '', type: 'NUMBER', operator: 'below', values: [], units: 'percentRemaining' },
  tagHumidity: { key: '', type: 'NUMBER', operator: 'below', values: [], units: 'percent' },
  tagTemperature: { key: '', type: 'NUMBER', operator: 'below', values: [], units: 'celsius' }
};


const collectionMapping = {
  category: 'categories',
  department: 'departments',
  location: 'locations',
  site: 'locations',
  staticLocation: 'locations',
  subcategories: 'subcategories'
};

const defaultFieldKeys = ['department', 'category', 'location', 'rule.name'];
const Option = Select.Option;
let nextId = 0;

const initialState = {
  attributes: [],
  filters: {},
  fieldsTemplate: []
};


class AdvanceFilter extends React.Component {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);
    this.onOperatorChange = this.onOperatorChange.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
  }

  componentWillMount() {
    const { params, template } = this.props;
    this.setState({ fieldsTemplate: template });
    const filters = {};
    // clear existing filters and add new filter
    this.setState({ filters }, () => {
      // add first attribute by default
      if (params) {
        this.prepareFilters(params);
      }
    });
  }

  updatedTemplate(filters) {
    const keys = filters.map(o => o.key);
    const { fieldsTemplate } = this.state;
    let updatedTemplate = Object.assign([], fieldsTemplate);
    updatedTemplate = updatedTemplate.map((o) => {
      const item = o;
      if (keys.includes(o.key)) item.assigned = true;
      else item.assigned = false;
      return item;
    });
    this.setState({ fieldsTemplate: updatedTemplate });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params !== this.props.params) {
      this.updatedTemplate(nextProps.params.fields);
      this.prepareFilters(nextProps.params);
    }
  }

  prepareFilters(params) {
    const fields = _.get(params, 'fields');
    const filters = {};

    const { fieldsTemplate } = this.state;
    defaultFieldKeys.forEach((key) => {
      const fieldTpl = _.find(fieldsTemplate, ['key', key]);
      if (fieldTpl) fieldTpl.assigned = true;
      const filteredTpl = _.find(fields, ['key', key]);
      if (fieldTpl) {
        if (filteredTpl) {
          filters[++nextId] = filteredTpl;
        } else {
          let fieldType = (fieldTpl.options && fieldTpl.options.length > 0) ? 'ENUM' : fieldTpl.type || fieldTpl.key;
          if (fieldTpl.key === 'tags') {
            fieldType = 'TEXT'
          }
          const field = Object.assign({}, fieldObjs[fieldType]);
          field.key = key;
          field.label = fieldTpl.label;
          filters[++nextId] = field;
        }

      }
    });

    fields.forEach((field, i) => {
      if(!(defaultFieldKeys.includes(field.key))) {
        filters[++nextId] = field;
      }
    });
    // check if any of category, department or location is part of template and
    //  is present in filters if not pre fill those filters
    this.setState({
      filters
    });
  }


  generateAttributeField(fieldTpl) {
    if (fieldTpl.key === 'category' || fieldTpl.key === 'department'
      || fieldTpl.key === 'location' || fieldTpl.key === 'site' || fieldTpl.key === 'staticLocation') {
      // return BasicField;
      return BasicField;
    } else if ((fieldTpl.options && fieldTpl.options.length > 0) || fieldTpl.key === 'subcategories') {
      return SelectField;
      // create type selection field
    } else if (fieldTpl.key === 'tagTemperature') {
      // create date field with operators
      return TemperatureField;
    } else if (fieldTpl.key === 'tagHumidity') {
      // create date field with operators
      return HumidityField;
    } else if (fieldTpl.key === 'tagBattery') {
      // create date field with operators
      return BatteryField;
    } else if (fieldTpl.type === 'DATE' || fieldTpl.type === 'DATETIME') {
      // create date field with operators
      return DateField;
    }
    return DefaultField;
  }

  onAttributeChange(filterId, val, prevKey) {
    const { change } = this.props;
    const { fieldsTemplate } = this.state;
    const { filters } = this.state;

    // set prev tpl to unassigned
    const prevTpl = _.find(fieldsTemplate, ['key', prevKey]);
    prevTpl.assigned = false;

    const fieldTpl = _.find(fieldsTemplate, ['key', val]);
    fieldTpl.assigned = true;

    // if fieldTpl has options then consider it as ENUM/SELECTION
    // else consider the field.type
    let fieldType = (fieldTpl.options && fieldTpl.options.length > 0) ? 'ENUM' : fieldTpl.type || fieldTpl.key;
    if (fieldTpl.key === 'tags') {
      fieldType = 'TEXT';
    }
    const field = Object.assign({}, fieldObjs[fieldType]);
    field.key = val;
    field.label = fieldTpl.label;
    filters[filterId] = field;

    // refresh filters with new fields
    // filters[filterId]['key'] = val;
    this.setState({ filters });
    change(filters);
  }

  onOperatorChange(filterId, val) {
    const { change } = this.props;
    const { filters } = this.state;
    filters[parseInt(filterId)]['operator'] = val;
    this.setState({ filters });
    change(filters);
  }

  onValueChange(filterId, val) {
    const { change } = this.props;
    const { filters } = this.state;
    const updatedFilters = Object.assign({}, filters);
    updatedFilters[parseInt(filterId)]['values'] = (typeof val === 'object') ? val : [val];
    this.setState({ filters: updatedFilters });
    change(filters);
  }

  onUnitsChange(filterId, val) {
    const { change } = this.props;
    const { filters } = this.state;
    filters[parseInt(filterId)]['units'] = val;
    this.setState({ filters });
    change(filters);
  }

  removeFilter(e, filterId, key) {
    const { change } = this.props;
    e.stopPropagation();
    const { filters, fieldsTemplate } = this.state;
    delete filters[parseInt(filterId)];
    const fieldTpl = _.find(fieldsTemplate, ['key', key]);
    fieldTpl.assigned = false;
    this.setState({ filters, fieldsTemplate });
    change(filters);
  }

  renderAttribute(filterId, filter) {
    const { collections } = this.props;
    const { fieldsTemplate } = this.state;
    const fieldTpl = _.find(fieldsTemplate, ['key', filter.key]);
    fieldTpl.assigned = true;
    const field = this.generateAttributeField(fieldTpl);
    let options;
    if (fieldTpl.options && fieldTpl.options.length) {
      options = convertOptionsToSelectors(fieldTpl.options);
    } else if ((fieldTpl.type === 'SELECT' || fieldTpl.type === 'LOCATION' || fieldTpl.type === 'TREE_SELECT')) {
      const data = (fieldTpl.key !== 'location' || fieldTpl.type !== 'TREE_SELECT') ? collections[collectionMapping[fieldTpl.key]] : collections.locations;
      if (fieldTpl.key === 'location' || fieldTpl.key === 'site' || fieldTpl.key === 'staticLocation') {
        options = _.map(data, l => l.toTreeData(3, false));
      } else if (fieldTpl.key === 'subcategories') {
        options = collections[collectionMapping[fieldTpl.key]];
      } else {
        options = generateSelectorFields(data);
      }
    }

    const unAssignedTpls = _.filter(fieldsTemplate, o => (!o.assigned));
    return (
      <Row className={'filter-item'}>
        <Col span="8">
          <Col span="2">
            <Icon
              type="close"
              className="remove-filter clickable"
              onClick={e => this.removeFilter(e, filterId, fieldTpl.key)}
            />
          </Col>
          <Col span="20">
            <Select
              placeholder="Select Attribute"
              getPopupContainer={triggerNode => triggerNode.parentNode}
              onChange={v => this.onAttributeChange(filterId, v, fieldTpl.key)}
              value={filter.label}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              optionFilterProp={'children'}
            >
              {unAssignedTpls.map(option => (
                <Option
                  className="fg-range-unit"
                  optionLabelProp={'label'}
                  value={option.key}
                >
                  {option.label}
                </Option>
            ))}
            </Select>
          </Col>
        </Col>
        <Col span="16">
          {React.createElement(field, {
            key: filterId,
            operator: filter.operator,
            operatorType: filter.type,
            options: options || [],
            unit: '',
            value: filter.values,
            units: filter.units,
            onOperatorChange: val => this.onOperatorChange(filterId, val),
            onValueChange: val => this.onValueChange(filterId, val),
            onUnitsChange: val => this.onUnitsChange(filterId, val),
          })}

        </Col>
      </Row>
    );
  }

  generateAttributeType() {
    const { filters, fieldsTemplate } = this.state;
    // get the first unassigned Tpl
    const firstField = _.find(fieldsTemplate, o => !(o.assigned));
    if (!firstField) return false;
    const filterId = ++nextId;
    // if fieldTpl has options then consider it as ENUM/SELECTION
    // else consider the field.type
    const fieldType = (firstField.options && firstField.options.length > 0) ? 'ENUM' : firstField.type || firstField.key;
    const field = Object.assign({}, fieldObjs[fieldType]);
    field.key = firstField.key;
    field.label = firstField.label;
    filters[filterId] = field;

    this.setState({ filters });
    return this.renderAttribute(filterId, firstField);
  }

  addFilter() {
    const { attributes } = this.state;
    const attribute = this.generateAttributeType();
    if (!attribute) return;
    attributes.push(attribute);
    this.setState({ attributes });
  }


  render(){
    const { collections, params } = this.props;
    const { fieldsTemplate } = this.state;
    const { filters } = this.state;
    if( _.isEmpty(fieldsTemplate)) return null;

    const attributes = [];
    Object.keys(filters).forEach((key) => {
      attributes.push(this.renderAttribute(key, filters[key]));
    });

    return (
      <div>
        {attributes}
        <Button
          size="small"
          className="add-attribute"
          onClick={() => this.addFilter()}
        >
          + Add Filter
        </Button>
      </div>
    );
  }
}

//
// AdvanceFilter.propTypes = {
//
// };
//
// AdvanceFilter.defaultProps = {
//
// };

const mapStateToProps = state => ({
  collections: {
    categories: [],
    subcategories: [],
    departments: [],
    locations: [],
  }
});

export default connect(mapStateToProps, null)(AdvanceFilter);
