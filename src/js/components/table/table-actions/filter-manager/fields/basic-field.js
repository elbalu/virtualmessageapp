import React, { PropTypes } from 'react';
import { TreeSelect } from 'antd';
import DefaultField from './default-field';
import { generateStructuredFields, generateTreeValue } from '../../../../../misc/filter-utils';

const SHOW_PARENT = TreeSelect.SHOW_PARENT;

class BasicField extends DefaultField {

  onChange(v) {
    const { onValueChange } = this.props;
    const structuredVal = generateStructuredFields(v);
    onValueChange(structuredVal);
  }

  renderInputElement() {
    const { value, options } = this.props;
    const tProps = {
      style: {
        width: '75%'
      },
      treeData: options,
      multiple: true,
      treeCheckable: true,
      labelInValue: true,
      showCheckedStrategy: SHOW_PARENT,
      treeNodeFilterProp: 'label',
      searchPlaceholder: 'Select',
    };

    const data = generateTreeValue(value);

    return (
      <TreeSelect
        {...tProps}
        value={data}
        onChange={v => this.onChange(v)}
        filterTreeNode={
          (input, option) => option.props.title.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        getPopupContainer={triggerNode => triggerNode.parentNode}
      />
    );
  }

}

DefaultField.propTypes = {
  operator: PropTypes.string.isRequired,
  value: PropTypes.instanceOf(Array).isRequired,
  operatorType: PropTypes.string.isRequired,
  onOperatorChange: PropTypes.func.isRequired,
  onValueChange: PropTypes.func.isRequired,
};

BasicField.defaultProps = {
  options: []
};

export default BasicField;
