import React, { PropTypes } from 'react';
import Select from 'antd/lib/select';
import { Input, DatePicker } from 'antd';
import _ from 'lodash';
import { DATE } from '../operators';
import DefaultField from './default-field';


const Option = Select.Option;

const InputGroup = Input.Group;

const conditions = DATE;

const WHICH = {
  VALUE_A: '0',
  VALUE_B: '1',
  CONDITION: 'condition',
  UNITS: 'units'
};

/**
 * @class
 *
 * @name DateField
 *
 * @description A field that handles inputs dates both
 * individual dates and date ranges
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class DateField extends DefaultField {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  /**
   * onChange calls props.update with one parameter
   * with format
   * {
   *   value: [lower:number, upper:number],
   *   condition:string,
   *   units:string
   * }
   *
   * @param {number|string} nextValue - Single value to
   * apply to current range value.
   *
   * @param {string} which - String path to the range value
   * property that should be updated.
   */
  onChange(nextValue, which) {
    const { onValueChange, value, onOperatorChange } = this.props;


    // create copy of value
    const updatedValue = [...value];

    // if condition was changed from or to Between, create or remove value[1]
    if (which === WHICH.CONDITION) {
      if (which !== 'between') {
        // set second value to empty
        _.set(updatedValue, '1', '');
        onValueChange(updatedValue);
      }
      onOperatorChange(nextValue);
    } else {
      // update with new value. could be new numeric value,
      // new condition, or new units
      _.set(updatedValue, which, nextValue);
      onValueChange(updatedValue);
    }
  }

  /**
   * render generates an Ant.Design InputGroup with one Select for
   * the range condition, one Select for units, and two Inputs. One
   * Input may be hidden depending on the selected range condition.
   *
   * @return {XML}
   */
  render() {
    const { value, field, operator, unit, onOperatorChange } = this.props;


    // get condition and units with defaults applied
    const conditionValue = operator || conditions[0];

    let valueA;
    let valueB;

    // set values based on selected condition
    switch (conditionValue) {
      case 'after':
      case 'before':
        valueA = value[0] || '';
        valueB = null;
        break;
      case 'between':
        valueA = value[0] || '';
        valueB = value[1] || '';
        break;
      default: {
        const conditionsStr = _.values(conditions).join(', ');
        throw new Error(`DateField condition ${conditionValue} not found. options are ${conditionsStr}`);
      }
    }

    // show both number inputs only if condition is 'Between'.
    // Depending on whether or not both values are shown, we
    // set the max of the first field and min of the second
    // field such that valueA > valueB
    const showValueB = conditionValue === 'between';
    const valueAWidth = showValueB ? '37.5%' : '50%';
    const valueBWidth = showValueB ? '37.5%' : '0';

    return (
      <InputGroup compact>
        <Select
          style={{ width: '25%' }}
          onChange={v => this.onChange(v, WHICH.CONDITION)}
          value={conditionValue}
          getPopupContainer={triggerNode => triggerNode.parentNode}
        >
          {_.values(conditions).map(c =>
            <Option key={c.value} value={c.value}>{c.label}</Option>
          )}
        </Select>
        <DatePicker
          style={{ width: valueAWidth }}
          value={valueA}
          isClearable
          showToday={false}
          selected={valueA}
          onChange={n => this.onChange(n, WHICH.VALUE_A)}
          placeholderText={'Select Date'}
          getCalendarContainer={triggerNode => triggerNode.parentNode}
        />
        {(() => {
          return showValueB ?
              (
                <DatePicker
                  style={{ width: valueBWidth }}
                  value={valueB}
                  showToday={false}
                  isClearable
                  selected={valueB}
                  onChange={n => this.onChange(n, WHICH.VALUE_B)}
                  placeholderText={'Select Date'}
                  getCalendarContainer={triggerNode => triggerNode.parentNode}
                />
              ) : null;
        })()
      }
      </InputGroup>
    );
  }
  }

DateField.propTypes = {
  operator: PropTypes.string.isRequired,
  value: PropTypes.instanceOf(Array).isRequired,
  operatorType: PropTypes.string.isRequired,
  onOperatorChange: PropTypes.func.isRequired,
  onValueChange: PropTypes.func.isRequired,
};

export default DateField;
