import React, { Component, PropTypes } from 'react';
import Select from 'antd/lib/select';
import { Input } from 'antd';
import * as operators from '../operators';

const Option = Select.Option;
const InputGroup = Input.Group;

/**
 * @class
 *
 * @name DefaultField
 *
 * @description A field that handles all default inputs
 * for filters
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */

class DefaultField extends Component {

  renderOperatorSelector() {
    const { operator, operatorType, onOperatorChange } = this.props;
    const operatorOptions = operators[operatorType];
    return (
      <Select
        style={{ width: '25%' }}
        onChange={v => onOperatorChange(v)}
        value={operator}
        filterOption={operator}
        placeholder={'Select'}
        getPopupContainer={triggerNode => triggerNode.parentNode}
      >
        {
          operatorOptions.map(o => (
            <Option
              key={o.value}
              value={o.value}
            >
              {o.label}
            </Option>
          ))
        }
      </Select>
    );
  }

  renderInputElement() {
    const { value, onValueChange } = this.props;
    const fieldVal = value[0] || '';
    return (
      <Input
        style={{ width: '50%' }}
        type="text"
        onChange={e => onValueChange(e.target.value)}
        value={fieldVal}
        className="fg-input fg-text"
      />
    );
  }

  render() {
    return (
      <InputGroup compact>
        {
         this.renderOperatorSelector()
        }
        {this.renderInputElement()}
      </InputGroup>
    );
  }


}
DefaultField.propTypes = {
  operator: PropTypes.string.isRequired,
  value: PropTypes.instanceOf(Array).isRequired,
  operatorType: PropTypes.string.isRequired,
  onOperatorChange: PropTypes.func.isRequired,
  onValueChange: PropTypes.func.isRequired,
};

DefaultField.defaultProps = {
  options: [],
};

export default DefaultField;
