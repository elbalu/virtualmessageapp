import React, { PropTypes } from 'react';
import Select from 'antd/lib/select';
import { Input, InputNumber } from 'antd';
import _ from 'lodash';
import { HUMIDITY } from '../operators';
import DefaultField from './default-field';

const units = [
  'percent'
];

const Option = Select.Option;
const InputGroup = Input.Group;

const conditions = HUMIDITY;

const WHICH = {
  VALUE_A: '0',
  VALUE_B: '1',
  CONDITION: 'condition',
  UNITS: 'units'
};

/**
 * @class
 *
 * @name HumidityField
 *
 * @description A field that handles both
 * individual inputs and values between a range
 * considering the units
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class HumidityField extends DefaultField {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  /**
   * onChange calls props.update with one parameter
   * with format
   * {
   *   value: [lower:number, upper:number],
   *   condition:string,
   *   units:string
   * }
   *
   * @param {number|string} nextValue - Single value to
   * apply to current range value.
   *
   * @param {string} which - String path to the range value
   * property that should be updated.
   */
  onChange(nextValue, which) {
    const { onValueChange, value, onOperatorChange } = this.props;


    // create copy of value
    const updatedValue = [...value];

    // if condition was changed from or to Between, create or remove value[1]
    if (which === WHICH.CONDITION) {
      if (which !== 'between') {
        // set second value to empty
        _.set(updatedValue, '1', '');
        onValueChange(updatedValue);
      }
      onOperatorChange(nextValue);
    } else {
      // update with new value. could be new numeric value,
      // new condition, or new units
      _.set(updatedValue, which, nextValue);
      onValueChange(updatedValue);
    }
  }

  /**
   * render generates an Ant.Design InputGroup with one Select for
   * the range condition, one Select for units, and two Inputs. One
   * Input may be hidden depending on the selected range condition.
   *
   * @return {XML}
   */
  render() {
    const { value, operator, unit, onOperatorChange } = this.props;


    // get condition and units with defaults applied
    const conditionValue = operator || conditions[0];
    const unitsValue = unit || units[0];

    let valueA;
    let valueB;

    // set values based on selected condition
    switch (conditionValue) {
      case 'above':
      case 'below':
        valueA = value[0] || '';
        valueB = null;
        break;
      case 'between':
        valueA = value[0] || '';
        valueB = value[1] || '';
        break;
      default: {
        const conditionsStr = _.values(conditions).join(', ');
        throw new Error(`HumidityField condition ${conditionValue} not found. options are ${conditionsStr}`);
      }
    }

    // show both number inputs only if condition is 'Between'.
    // Depending on whether or not both values are shown, we
    // set the max of the first field and min of the second
    // field such that valueA > valueB
    const showValueB = conditionValue === 'between';
    const valueAWidth = showValueB ? '25%' : '50%';
    const valueBWidth = showValueB ? '25%' : '0';
    const unitsWidth = '25%';
    const min = -Infinity;
    const max = 100;
    const unitsDisabled = units.length <= 1;

    return (
      <InputGroup compact>
        <Select
          style={{ width: '25%' }}
          onChange={v => this.onChange(v, WHICH.CONDITION)}
          value={conditionValue}
          getPopupContainer={triggerNode => triggerNode.parentNode}
        >
          {_.values(conditions).map(c =>
            <Option key={c.value} value={c.value}>{c.label}</Option>
          )}
        </Select>
        <InputNumber
          style={{ width: valueAWidth }}
          value={String(valueA)}
          onChange={n => this.onChange(n, WHICH.VALUE_A)}
          className="fg-range-value"
          min={min}
          max={max}
        />
        {(() => {
          return showValueB ?
              (
                <InputNumber
                  style={{ width: valueBWidth }}
                  value={String(valueB)}
                  onChange={n => this.onChange(n, WHICH.VALUE_B)}
                  className="fg-range-value"
                  min={min}
                  max={max}
                />
              ) : null;
        })()}
        <Select
          style={{ width: unitsWidth }}
          placeholder="Units"
          className="fg-range-units"
          onChange={v => this.onChange(v, WHICH.UNITS)}
          disabled={unitsDisabled}
          value={unitsValue}
          getPopupContainer={triggerNode => triggerNode.parentNode}
        >
          {units.map(option => (
            <Option
              className="fg-range-unit"
              value={option}
              key={option}
            >
              {option}
            </Option>
          ))}
        </Select>
      </InputGroup>
    );
  }
  }

HumidityField.propTypes = {
  operator: PropTypes.string.isRequired,
  value: PropTypes.instanceOf(Array).isRequired,
  operatorType: PropTypes.string.isRequired,
  onOperatorChange: PropTypes.func.isRequired,
  onValueChange: PropTypes.func.isRequired,
};

export default HumidityField;
