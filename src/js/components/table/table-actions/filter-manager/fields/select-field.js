import React, { PropTypes } from 'react';
import Select from 'antd/lib/select';
import DefaultField from './default-field';

const Option = Select.Option;

class SelectField extends DefaultField {

  onChange(values) {
    const { onValueChange } = this.props;
    const structuredVal = values.map((v) => {
      return {
        id: v,
        name: v,
      };
    });
    onValueChange(structuredVal);
  }

  renderInputElement() {
    const { value, options } = this.props;
    const fieldVal = value.map(v => v.id);
    return (
      <Select
        style={{ width: '75%' }}
        showSearch
        multiple
        allowClear
        tokenSeparators={[',']}
        onChange={v => this.onChange(v)}
        optionFilterProp="children"
        filterOption={
          (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        placeholder={'Select'}
        value={fieldVal}
        getPopupContainer={triggerNode => triggerNode.parentNode}
      >
        {
          options.map(o => (
            <Option
              key={o.id || o.name}
              value={o.id || o.name}
            >
              {o.name}
            </Option>
          ))
        }
      </Select>
    );
  }

}

SelectField.propTypes = {
  operator: PropTypes.string.isRequired,
  value: PropTypes.instanceOf(Array).isRequired,
  operatorType: PropTypes.string.isRequired,
  onOperatorChange: PropTypes.func.isRequired,
  onValueChange: PropTypes.func.isRequired,
};

SelectField.defaultProps = {
  options: []
};

export default SelectField;
