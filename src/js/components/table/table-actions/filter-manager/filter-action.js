import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Modal, Button, TreeSelect, Row, Col, Icon, Popover, Card } from 'antd';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import * as widgetActions from '../../../../redux/actions/widget-actions';
import * as tableActions from '../../../../redux/actions/table-actions';
import Location from '../../../../misc/map/location';
import AdvanceFilter from './advance-filter';
import { generateStructuredFields, getBasicFilter, generateFieldsFilter, getAdvanceFilter } from '../../../../misc/filter-utils'

const initialState = {
  visible: false,
  columns: [],
  fields: [],
  selectedFilter: [{value: '', label: ''}]
};

class FilterAction extends React.Component {
  /**
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = {...initialState, columns: props.columns, fields: _.get(props.filterOption.params, 'fields')};
    this.onFilter = this.onFilter.bind(this);
    this.onClearFilter = this.onClearFilter.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onFilter = () => {
    const {fetchData} = this.props.actions;
    const { resetPaging, setParams, addFields } = this.props.tableActions;
    // reset paging
    resetPaging() ;

    const data = this.state.fields;
    // filter out the fields which dont have values
    const fields = _.filter( data, function(item) {
      return item.values.length > 0
    });
    addFields(fields);
    fetchData();
    this.hide();
  }

  onClearFilter = () => {
    const { fetchData } = this.props.actions;

    const { clearParams, addFields } = this.props.tableActions;
    // reset paging
    clearParams() ;
    // reset params

    this.setState({fields: []});
    let fields = []
    addFields(fields);
    fetchData();

  }

  hide = () => {
    this.setState({
      visible: false,
    });
  }

  handleVisibleChange = (visible) => {
    this.setState({visible});
  }

  getButtons() {
    return (
      <Row type="flex" justify="end" gutter={16}>
        <Col >
          <Button
            size="large"
            className=" buttons cancel clickable"
            onClick={() => this.onClearFilter()}
          >
            Clear all
          </Button>
        </Col>
        <Col>
          <Button
            size="large"
            type="primary"
            className=" buttons cancel"
            onClick={() => this.onFilter()}
          >
            Filter
          </Button>
        </Col>
      </Row>
    );
  }

  onChange(fields) {
    this.setState({fields});
  }

  render() {
    const { fields } = this.state;
    const { showEditableOnly } = this.props;
    const { params } = this.props.table;
    const text = <div className="filter-action">
      <Row type="flex" justify="start"><span className="titleHeader">Filters</span></Row>
    </div>;

    const content = (
      <Row style={{width:'650px'}} className='structured-filter'>
        <Row>
          <AdvanceFilter ref="advanced" filterOption={this.props.filterOption}
             params={params}
             template={getAdvanceFilter(this.props.template.fields, showEditableOnly)}
             change={this.onChange} selectedFilter={this.state.selectedFilter}/>
        </Row>
        <Row> {this.getButtons()}</Row>
      </Row>
    );


    return (
      <span className="filter-action">
        <Popover content={content} title={text} trigger="click" placement="bottomRight"
                 visible={this.state.visible} onVisibleChange={this.handleVisibleChange}>
          <Icon
            type="filter"
            style={{fontSize: 32}}
            className={(fields && (Object.keys(fields)).length>0) ? 'clickable blue' : 'clickable'}
          />
        </Popover>
      </span>
    );
  }
}

/* eslint-disable react/no-unused-prop-types */
FilterAction.propTypes = {
  fields: PropTypes.shape({
    list: PropTypes.arrayOf(PropTypes.object),
    entityName: PropTypes.string,
    save: PropTypes.func,
    remove: PropTypes.func,
    add: PropTypes.func
  }),
  columns: PropTypes.shape({
    list: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func
  }),
  customFields: PropTypes.bool,
};

FilterAction.defaultProps = {
  fields: {
    list: [],
    save: null,
    remove: _.noop,
    add: _.noop
  },
  filterOption: {
    params: []
  },
  template: [],
  columns: {
    list: [],
    onChange: _.noop
  }
};

const mapStatetoProps = state => ({
  table: state.table,
});

const mapDispatchToProps = dispatch => ({
  tableActions: bindActionCreators(tableActions, dispatch),
});

export default connect(mapStatetoProps, mapDispatchToProps)(FilterAction);
