import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal, Button, TreeSelect,Row, Col, Icon, Popover, Card, Tree } from 'antd';
import _ from 'lodash';
import Location from '../../../../../misc/map/location'
import { generateStructuredFields, generateFieldsFilter,generateSelectorFields, getSelectedData, generateFieldsFilterFor } from '../../../../../misc/filter-utils'


const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const tProps = {
  multiple: true,
  treeCheckable: true,
  showCheckedStrategy: SHOW_PARENT,
  searchPlaceholder: 'Please select',
  labelInValue: true,
  treeNodeFilterProp: "label",
  allowClear: true,
  style: {
    width: 300,
  },
};


class BasicItem extends React.Component {

  constructor(props) {
    super(props)
    const { selectedFields ,template } = props;
    this.onChange = this.onChange.bind(this);
    if( !_.isEmpty(template)){
      this.state = {
        value: getSelectedData(_.filter(selectedFields.fields, ['key', template.key])),
      };
    }
  }

  componentWillReceiveProps(nextProps) {
    const { selectedFields } = nextProps;
    if(selectedFields  !== this.props.selectedFields){
      this.setState({
        value: getSelectedData(_.filter(selectedFields.fields, ['key', this.props.template.key])),
      });
    }
  }

  getDataName(key){
    if(key === "category")
      return "categories";
    if(key === "location")
      return "locations";
    if(key === "department")
      return "departments";
  }

  onChange = (value) => {
    const { template, change } = this.props;
    this.setState({value});
    const values = generateFieldsFilterFor(template.label, template.key, generateStructuredFields(value));
    change(values, template.key );
  }

  render() {
    const { template, data } = this.props;
    const selectorData = _.get(data, this.getDataName(template.key));
    const treeProps = {
      ...tProps,
      treeData: (template.key !== 'location') ? generateSelectorFields(selectorData) : _.map(selectorData, l => l.toTreeData(3, false)) ,
      value: this.state.value,
      onChange: this.onChange,
    };

    return (
      <Col>
        <div className="small">
          <span>{template.label}</span>
          <br />
          <TreeSelect {...treeProps} getPopupContainer={triggerNode => triggerNode.parentNode} />
          <br />
          <br />
        </div>
      </Col>
    );
  }
}

BasicItem.propTypes = {
};

BasicItem.defaultProps = {
  data:[],
  template:[],
  change: _.noop,
  selectedFields: []
};

export default BasicItem;
