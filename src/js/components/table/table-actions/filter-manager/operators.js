export const TEXT = [{
  label: 'Contains',
  value: 'contains'
}, {
  label: 'Not Contains',
  value: 'notContains'
}, {
  label: 'Equals',
  value: 'equals'
}, {
  label: 'Not Equals',
  value: 'notEquals'
}];
export const ENUM = [{
  label: 'Equals',
  value: 'selection'
}];
export const SELECT = [{
  label: 'Selection',
  value: 'selection'
}];
export const LOCATION = [{
  label: 'Equals',
  value: 'selection'
}];
export const DATE = [{
  label: 'Before',
  value: 'before'
}, {
  label: 'Between',
  value: 'between'
}, {
  label: 'After',
  value: 'after'
}];
export const DATETIME = Object.assign([], DATE);
export const BATTERY = [{
  label: 'Below',
  value: 'below'
}, {
  label: 'Between',
  value: 'between'
}, {
  label: 'Above',
  value: 'above'
}];
export const TEMPERATURE = Object.assign([], BATTERY);
export const HUMIDITY = Object.assign([], BATTERY);
export const TREE_SELECT = Object.assign([], LOCATION);
