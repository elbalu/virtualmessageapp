import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import SearchBar from '../../search-bar';
import BulkActions from './bulk-actions';
import ColumnManager from './column-manager';
import ColumnManager2 from './column-manager2';
import ResetFilters from './reset-filters';
import FilterAction from './filter-manager/filter-action';
import DownloadActions from './download-actions';
import * as UserUtils from '../../../misc/user-utils';

/**
 * TableActions is a React component that provides
 * a set of actions that can be taken in the context
 * of a Table component. This includes managing columns,
 * resetting table filters, bulk actions, and any other
 * actions that cannot be taken directly from the Table
 * component. This component can be placed anywhere in
 * a page, but is designed to be used with a single
 * Table component.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class TableActions extends React.Component {
  /**
   * Table#searchBar returns a search bar component
   * if an onSearch function was provided in props
   * (props.search.onSearch). If one was not provided,
   * it returns null.
   *
   * @return {XML|null}
   */
  searchBar() {
    const { searchable, onSearch, favoriteSearches, editFavoriteSearches } = this.props.search;
    if (!_.isFunction(onSearch)) return null;
    if (!searchable) return null;

    return (
      <SearchBar
        onSearch={onSearch}
        favoriteSearches={favoriteSearches}
        editFavoriteSearches={editFavoriteSearches}
      />
    );
  }

  /**
   * customActions returns custom action components
   *
   * @return {XML|null}
   */
  customActions(customActions) {
    // if customActions were given, render each action
    // into its own column
    const customActionComponents = !_.isEmpty(customActions) ? (
      customActions.map((a, i) => <span className="action-wrapper" key={i}>{a()}</span>)
    ) : null;
    return customActionComponents;
  }

  /**
   * bulkActions returns bulk action components
   * if customBulk action then return custom action
   * passed through the props, if not
   * return BulkAction component
   *
   * @return {XML|null}
   */
  bulkActions() {
    const bulkActions = this.props.bulkActions;
    const { entityType } = this.props;
    const editAcess = UserUtils.hasAccess(entityType, 'update');
    if ((entityType && !editAcess) || _.isEmpty(bulkActions)) return null;
    if (bulkActions.customBulk) {
      return this.customActions(bulkActions.actions);
    }
    return (
      <span className="actions-list">
        <span className="action-wrapper">
          <BulkActions
            actions={this.props.bulkActions.actions}
            selectedRowKeys={this.props.selectedRowKeys}
          />
        </span>
      </span>
    );
  }
  /**
   * @override
   */
  render() {
    const { customActions } = this.props;
    return (
      <div className="table-actions-wrapper">
        <div className="table-actions">
          <div className="actions">
            {this.bulkActions()}
          </div>
          <div className="search">
            {this.searchBar()}
          </div>
          <div className="actions text-right action-wrapper">
            {this.customActions(customActions)}
            <DownloadActions
              download={this.props.download}
            />
            {
            (this.props.actions.filters.fetchData) ? (
              <span className="action-wrapper">
                <FilterAction
                  actions={this.props.actions.filters}
                  template={this.props.template}
                  entityType={this.props.entityType}
                  showEditableOnly={this.props.showEditableOnly}
                />
              </span>
            ) : ''
          }
            {// <span className="action-wrapper">
            //   <ResetFilters
            //     reset={this.props.actions.resetFilters}
            //     filters={this.props.filters}
            //   />
            // </span>
            }
            <span className="action-wrapper">
              <ColumnManager2
                customFields={this.props.customFields}
                entityType={this.props.entityType}
                fields={{
                  save: this.props.actions.fields.save,
                  remove: this.props.actions.fields.remove,
                  add: this.props.actions.fields.add,
                  list: this.props.template.fields,
                  entityName: this.props.template.entityName
                }}
                columns={{
                  list: this.props.columns,
                  onChange: this.props.actions.columns.saveAll,
                }}
              />
            </span>
          </div>
        </div>
      </div>
    );
  }
}

TableActions.propTypes = {
  // form template
  template: PropTypes.shape({
    entityName: PropTypes.string,
    fields: PropTypes.arrayOf(PropTypes.object)
  }),
  // list of functions that perform default actions
  actions: PropTypes.shape({
    // column related actions
    columns: PropTypes.shape({
      // update a single column
      save: PropTypes.func,
      // update all columns
      saveAll: PropTypes.func
    }),
    // field related actions
    fields: PropTypes.shape({
      // add a new field
      add: PropTypes.func,
      // edit a field
      save: PropTypes.func,
      // remove a field
      remove: PropTypes.func
    }),
    filters: PropTypes.shape({
      fetchData: PropTypes.func
    }),
  }).isRequired,
  // list of functions where each accepts a list
  // of selected row keys
  bulkActions: PropTypes.shape(),
  // list of functions where each returns a component
  // that performs an action
  customActions: PropTypes.arrayOf(PropTypes.func),
  // list of table columns for column manager
  columns: PropTypes.arrayOf(PropTypes.object),
  // list of ids for selected rows
  selectedRowKeys: PropTypes.arrayOf(PropTypes.any),
  showEditableOnly: PropTypes.bool,
  // table filters
  filters: PropTypes.instanceOf(Array),
  /**
   * props to enable search
   */
  search: PropTypes.shape({
    onSearch: PropTypes.func,
    favoriteSearches: PropTypes.arrayOf(PropTypes.string),
    editFavoriteSearches: PropTypes.func,
  }),

  // ability to add custom fields
  customFields: PropTypes.bool.isRequired,
  entityType: PropTypes.string.isRequired,

  // ability to download table dataSource
  download: PropTypes.shape({})
};

TableActions.defaultProps = {
  bulkActions: {},
  customActions: [],
  disabled: false,
  filters: [],
  filterOption: {
    categories: [],
    departments: [],
    locations: []
  },
  search: {
    onSearch: null,
    favoriteSearches: [],
    editFavoriteSearches: _.noop
  }
};

export {
  ColumnManager,
  BulkActions,
  ResetFilters
};

export default TableActions;
