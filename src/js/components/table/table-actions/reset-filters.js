import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

/**
 * ResetFilters is a table action that resets table
 * filters.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ResetFilters extends React.Component {
  /**
   * @override
   * @return {*}
   */
  render() {
    const { reset, filters } = this.props;
    if (!_.isFunction(reset)) return null;
    const disabled = _.isEmpty(filters);
    let className = 'icomoon icomoon-reset-filter clickable';

    return (
      <span
        onClick={reset}
        title="reset filters"
        className={className}
      />
    );
  }
}

ResetFilters.propTypes = {
  reset: PropTypes.func,
  filters: PropTypes.shape()
};

ResetFilters.defaultProps = {
  reset: null,
  filters: {}
};

export default ResetFilters;
