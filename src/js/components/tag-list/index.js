import React, { PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import validate from 'validate.js';

import TagDetails from './tag-details';

const TagList = ({ tags, template, history }) => {
  if (validate.isEmpty(tags)) {
    return (
      <div className="text-center">
        <span className="light-gray">
          <FormattedMessage id="sdk.tags.none" />
        </span>
      </div>
    );
  }

  return (
    <ul>
      {tags.map(t => {
        return (
          <li key={t.serial}>
            <TagDetails tag={t} template={template} history={history[t.id]} />
          </li>
        )
      })}
    </ul>
  );
};

TagList.propTypes = {
  tags: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.string,
  ]),
  template: PropTypes.shape(),
  history: PropTypes.shape(),
};

TagList.defaultProps = {
  tags: [],
  history: {},
};

export default TagList;
