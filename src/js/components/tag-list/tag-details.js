import React, { PropTypes } from 'react';
import validate from 'validate.js';
import { injectIntl, intlShape } from 'react-intl';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import _filter from 'lodash/filter';
import _map from 'lodash/map';

import TelemetryChart from './telemetry-chart';

import { NO_DATA_VALUE } from '../../misc/renderers/values';
import { generateFields, generateTelemetryFields } from '../../misc/device-utils';

const renderTelemetryChart = (tag, history) => {
  const tagTelemetry = _filter(tag.data, ['eventType', 'telemetry']);
  if (tagTelemetry.length === 0) return <div />;
  return _map(history, telemetry => (
    <div className="tag-details-temperature-chart" key={telemetry.type}>
      <TelemetryChart history={telemetry} title={telemetry.telemetryType} />
    </div>
  ));
};

const TagDetails = ({ tag, template, history, intl }) => {
  if (validate.isEmpty(tag)) return NO_DATA_VALUE;
  if (validate.isEmpty(template)) return NO_DATA_VALUE;

  return (
    <Row type="flex" align="middle" justify="start">

      <Col span={12}>
        <h5><i className="fa fa-tag" />&nbsp;{tag.serial}</h5>
        {generateFields(template.fields, tag, 'tags')}
        {generateTelemetryFields(tag)}
      </Col>

      <Col span={12}>
        { renderTelemetryChart(tag, history) }
      </Col>

    </Row>
  );
};

TagDetails.propTypes = {
  tag: PropTypes.shape(),
  template: PropTypes.shape(),
  history: PropTypes.shape(),

  // i18n support
  intl: intlShape,
};

export default injectIntl(TagDetails);
