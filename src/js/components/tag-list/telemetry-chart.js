import React, { PropTypes } from 'react';
import validate from 'validate.js';
import { FormattedMessage } from 'react-intl';
import AreaChart from '../charts/area-chart';
import { areaChartColors } from '../../misc/colors';
/**
 *
 * @param history
 * @param title
 * @returns {*}
 * @constructor
 */
const TemperatureChart = ({ history, title }) => {
  if (!history || (history.series && validate.isEmpty(history.series[0].data))) {
    return (
      <div className="no-telemtry">
        <span className={'light-gray none'}>
          <span style={{ 'text-transform': 'capitalize' }}>{title ? title.toLowerCase() : ''} </span>
          <FormattedMessage id="noTelemetryData" />
        </span>
      </div>
    );
  }

  const { data } = history.series[0];
  const { telemetryType } = history;
  let { units } = history;

  if (telemetryType === 'TEMPERATURE') {
    units = 'temperatureC';
  }

  return (
    <div>
      <div className="text-left chart-title">{title}</div>
      <AreaChart
        name={title}
        data={data}
        xAxisType="timestamp"
        yAxisType={units}
        width={350}
        height={110}
        bgColor={areaChartColors[0]}
      />
    </div>
  );
};

TemperatureChart.propTypes = {
  history: PropTypes.shape(),
  title: PropTypes.string,
};

export default TemperatureChart;
