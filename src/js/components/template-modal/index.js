import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Modal from 'antd/lib/modal';
import Notification from 'antd/lib/notification';

import TemplateBuilder from '../form-builder';

import * as modalActions from '../../redux/actions/modal-actions';
import * as templatesActions from '../../redux/actions/templates-actions';
import * as FormUtils from '../../misc/form-utils';

const initialState = {
  template: {},
  validationErrors: new Map(),
};

/**
 * @deprecated May 15, 2017 Use {@link ColumnManager2}
 */
class TemplateModal extends Component {

  constructor(props) {
    super(props);

    this.state = Object.assign({}, initialState, { template: props.template });

    // bind methods that will
    // be called by the dom
    this.submitTemplate = this.submitTemplate.bind(this);
    this.updateTemplate = this.updateTemplate.bind(this);
    this.handleValidate = this.handleValidate.bind(this);
  }

  /**
   * componentWillReceiveProps() allows TemplateModal to
   * perform state transitions when new props are passed
   * down. It replaces this.state.template with the
   * template passed into props.
   *
   * @override
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    // replace template with incoming template
    this.updateTemplate(nextProps.template);
  }

  submitTemplate() {
    const { save } = this.props;
    const { closeModal } = this.props.modalActions;
    const { template, validationErrors } = this.state;
    const validationErrorCount = validationErrors.size;

    if (validationErrorCount > 0) {
      // there are validation errors
      Notification.error({
        message: 'Please fix errors',
        description: `You have (${validationErrorCount}) error(s) in your form`,
      });
    } else {
      save(template);
      closeModal();
      this.setState(initialState);
    }
  }

  /**
   * handleValdate() updates component state
   * with current validation status
   *
   * @param validationStatus
   */
  handleValidate(validationStatus) {
    const { validationErrors } = this.state;

    const updatedErrors = FormUtils.updateValidationErrors(validationErrors, validationStatus);
    this.setState({ validationErrors: updatedErrors });
  }

  updateTemplate(data) {
    this.setState({ template: data });
  }

  render() {
    const { template } = this.state;
    const { displayNotification } = this.props;
    const { visible } = this.props.modal;
    const { closeModal } = this.props.modalActions;
    const { createField, updateField, removeField } = this.props.templatesActions;

    return (
      <Modal
        title={'Add/Remove Columns'}
        visible={visible}
        onCancel={closeModal}
        footer={false}
      >
        <TemplateBuilder
          template={template}
          onChange={this.updateTemplate}
          displayNotification={displayNotification}
          actions={{
            add: createField,
            edit: updateField,
            remove: removeField,
          }}
        />
      </Modal>
    );
  }
}

TemplateModal.propTypes = {
  template: PropTypes.shape({}),

  displayNotification: PropTypes.func,

  modal: PropTypes.shape({
    visible: PropTypes.bool,
  }),
  modalActions: PropTypes.shape({
    closeModal: PropTypes.func,
  }),

  templatesActions: PropTypes.shape({
    createField: PropTypes.func,
    updateField: PropTypes.func,
    removeField: PropTypes.func,
  }),
};

const mapStateToProps = state => ({
  modal: state.modal.fieldsModal,
});

const mapDispatchToProps = dispatch => ({
  modalActions: bindActionCreators(modalActions, dispatch),
  templatesActions: bindActionCreators(templatesActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TemplateModal);
