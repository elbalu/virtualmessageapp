import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Icon, message } from 'antd';
import _ from 'lodash';

import { copyToClipboard } from '../../misc/helpers';
import warning from '../../../resources/data/token-warning.json';

const FormItem = Form.Item;

/**
 * @name TokenModal
 *
 * @description Modal that asks the user for
 * username/password and, upon successful
 * authentication, presents a token that can
 * be used to authenticate a CMX instance to
 * Asset Management.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class TokenModal extends React.Component {
  /**
   * @static
   *
   * @name showToken
   *
   * @description Shows a modal that displays a
   * token and a button to copy the token to the
   * clipboard.
   *
   * @param {string} token
   */
  static showToken(token) {
    Modal.success({
      title: 'Token String',
      content: (
        <div className="content">
          <p className="break-word">{token}</p>
        </div>
      ),
      maskClosable: true,
      okText: 'Copy',
      onOk: () => {
        copyToClipboard(token);
        return Promise.resolve();
      },
    });
  }

  /**
   * @static
   *
   * @description Shows a modal with a warning
   * regarding generating non-expiry tokens. On
   * confirmation, the token modal is shown.
   *
   * @param {string} token
   */
  static showWarning(token) {
    Modal.confirm({
      title: 'Please Read',
      content: (
        <div className="content">
          <p>{warning.message}</p>
        </div>
      ),
      maskClosable: true,
      okText: 'I understand',
      onOk: () => TokenModal.showToken(token),
    });
  }

  /**
   * @constructor
   *
   * @param {Object} props - React props
   */
  constructor(props) {
    super(props);
    this.getToken = this.getToken.bind(this);
    this.showToken = this.showToken.bind(this);
    this.copyAndClose = this.copyAndClose.bind(this);
  }

  /**
   * @name componentWillReceiveProps
   *
   * @override
   *
   * @description Resets the form values when the
   * modal is hidden
   *
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.visible === false) this.props.form.resetFields();
  }

  /**
   * @name getToken
   *
   * @description Gets username and password from
   * this.props.form, validates the input (both
   * are required) and calls this.props.getToken
   * to perform a request for a token. Upon success,
   * it shows the token to the user.
   */
  getToken() {
    const { getToken, form, hide, cmx, token } = this.props;
    form.validateFields((errors, values) => {
      if (!errors) {
        getToken(values.username, values.password, (cmx && cmx.id) ? cmx.id : '')
          .then((token) => {
            hide();
            TokenModal.showWarning(token);
          })
          .catch(() => {
            message.error('invalid credentials');
          });
      }
    });
  }

  showToken() {
    const { token, hide } = this.props;
    hide();
    TokenModal.showWarning(token);
  }

  copyAndClose() {
    const { token, cmx, hide } = this.props;
    const tokenContent = (token) ? token : "";

    hide();
    copyToClipboard(tokenContent);
  }
  /**
   * @override
   *
   * @name render
   *
   * @return {XML}
   */
  render() {
    const { token, cmx, title, info, visible, form, hide } = this.props;

    const tokenContent = (token) ? token : "";
    return (
      <Modal
        title="Log In"
        visible={visible}
        onOk={this.getToken}
        okText="Login"
        cancelText="Cancel"
        onCancel={hide}
      >
        <div className="text-center"><p>{info}</p></div>
        <Form autoComplete="off">
          <FormItem label="Username">
            {form.getFieldDecorator('username', { rules: [{ required: true }] })(
              <Input prefix={<Icon type="user" />} id="username" autoComplete="username" />
            )}
          </FormItem>
          <FormItem label="Password">
            {form.getFieldDecorator('password', { rules: [{ required: true }] })(
              <Input prefix={<Icon type="lock" />} id="password" type="password" autoComplete="new-password" />
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

TokenModal.propTypes = {
  // whether or not the modal is visible
  visible: PropTypes.bool,
  // function that accepts username, password
  // and returns a promise that resolves a token
  getToken: PropTypes.func,
  showToken: PropTypes.func,
  // function to hide the modal
  hide: PropTypes.func,
  token: PropTypes.string,
  // instance of cmx to get token for
  cmx: PropTypes.shape({
    id: PropTypes.number
  }),

  // provided by Form.Create()
  form: PropTypes.shape()
};

TokenModal.defaultProps = {
  visible: false,
  getToken: _.noop,
  showToken: _.noop,
  token: null,
  hide: _.noop,
  cmx: null
};

export default Form.create()(TokenModal);
