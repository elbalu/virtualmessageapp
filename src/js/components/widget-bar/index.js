import React, { Component, PropTypes } from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';

/**
 * wraps children in Col components which
 * are placed inside a Row.
 *
 * @param children
 * @returns {XML}
 */
class WidgetBar extends Component {
  /**
   * Generates a widget by wrapping the
   * given child component in a Col with
   * preset props
   *
   * @param child
   * @param key
   * @returns {XML}
   */
  static generateWidget(child, key = 0) {
    return (
      <Col span={child.props.span || 6} key={key} className="widget">
        {child}
      </Col>
    );
  }

  /**
   * Generates widgets using
   * WidgetBar#generateWidget
   *
   * @returns {*}
   */
  static generateWidgets(children) {
    // process widgets as array
    // or as single widget
    if (children instanceof Array) {
      return children.map(WidgetBar.generateWidget);
    }

    return WidgetBar.generateWidget(children);
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { children } = this.props;

    return (
      <Row gutter={10} type="flex" justify="start" className="widget-bar">
        {WidgetBar.generateWidgets(children)}
      </Row>
    );
  }
}

WidgetBar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default WidgetBar;
