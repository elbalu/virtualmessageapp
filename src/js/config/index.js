import _ from 'lodash';
import defaultConfig from './default.json';

const version = process.env.VERSION;
const env = process.env.NODE_ENV;
const locale = process.env.LOCALE;
export const baseUrls = require(`./env/baseUrls.json`);
export const localeConfig = require(`./locale/${locale}.json`);
export const envConfig = require(`./env/env.json`);

/**
 * config is a Javascript module that is designed to mimic
 * the behavior of the node-config node module. That module
 * cannot be used in browser environments due to dependencies
 * on node-only modules (like fs and path). This module
 * merges a default config file with an environment specific config
 * file. It appends a locale strings file. This module requires
 * webpack to provide the envConfig and localeConfig objects.
 * The webpack config file must use the resolve.alias properties
 * to provide config, sdk-env-config, and sdk-locale-config. See
 * webpack.config.js for an example.
 *
 * @example
 *  import config from 'config';
 *  const sdkManagerUrl = config.get('api.sdkManager');
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
const config = Object.assign(
  defaultConfig,
  baseUrls,
  envConfig,
  localeConfig,
  {
    version,
    env,
    locale
  }
);

/**
 * get works the same way as node-config
 * get() function. for compatibility
 *
 * @param {string} path
 */
const get = path => _.get(config, path);
export default { get };
