import React, { Component, PropTypes } from 'react';

import { setRoute } from '../misc/url-persist-utils';

// TODO add documentation comments to About
class About extends Component {
  componentWillMount() {
    setRoute(this.props.location.pathname);
  }
  render() {
    return (
      <div>About</div>
    );
  }
}

About.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
};

export default About;
