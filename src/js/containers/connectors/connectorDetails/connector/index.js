import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';

import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Tabs from 'antd/lib/tabs';

import * as mapsActions from '../../../../redux/actions/maps-actions';

const Tab = Tabs.TabPane;

/**
 * Connector details
 *
 * @author Balu Loganathan
 */
class ConnectorDetails extends Component {

  render() {
    const { connector } = this.props;

    if (!connector) return null;

    return (
      <Tabs defaultActiveKey={'overview'}>
        <Tab tab="Overview" key="overview">
          <Row className="connector-detail-content" type="flex" align="top">
            <Col span={12}>
              Details
            </Col>
          </Row>
        </Tab>

      </Tabs>
    );
  }
}

ConnectorDetails.propTypes = {
  connector: PropTypes.shape({}),
  telemetry: PropTypes.shape({}),
  templates: PropTypes.shape({}),
  actions: PropTypes.shape({}),
};

const mapDispatchToProps = dispatch => ({
  actions: {
    ...bindActionCreators(mapsActions, dispatch),
  },
});

const mapStateToProps = state => ({
  ...state.mapsReducer,
  ...state.areasReducer,
  connectors: state.connectors.connectors,
  templates: {
    allFields: state.templates.connectorDetail,
    connectors: state.templates.connectors,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(ConnectorDetails));
