import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';

import Modal from 'antd/lib/modal';
import ConnectorDetails from './connector';

const initialState = {
  modalKey: 1
};

/**
 * Connector details
 * This class creates a modal popup to show the connector details
 *
 * @author Balu Loganathan, Benjamin Newcomer
 */
class ConnectorDetailsModal extends Component {

  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * handler to hide
   * the modal
   */
  hideModal() {
    const { hide } = this.props.modalActions;
    hide();
  }

  /**
   * on modal close increment the
   * key counter so that the modal gets
   * re rendrered everytime it opens
   */
  modalClose() {
    this.setState({ modalKey: (this.state.modalKey + 1) });
  }

  renderConnectorDetails() {
    const { deviceDetailsModal, connectorDetails, page } = this.props;
    const { date } = deviceDetailsModal;
    const { connector } = connectorDetails;

    return <ConnectorDetails connector={connector} page={page} date={date} />;
  }

  render() {
    const { visible } = this.props;
    const { connector } = this.props.connectorDetails;
    if (!connector) return null;

    return (
      <Modal
        title={connector.name || connector.serial}
        key={this.state.modalKey}
        className="connector-details"
        visible={visible}
        width={1075}
        height={1000}
        onCancel={() => this.hideModal()}
        footer={false}
        afterClose={() => this.modalClose()}
      >
        { this.renderConnectorDetails() }

      </Modal>
    );
  }
}

ConnectorDetailsModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  page: PropTypes.string,
  connectorDetails: PropTypes.shape({
    connector: PropTypes.shape({}),
  }),
  detailsModal: PropTypes.shape({}),
  modalActions: PropTypes.shape({
    hide: PropTypes.func.isRequired,
  }),

};

const mapStateToProps = state => ({
  ...state.modal,
  ...state.connectors,
});

export default connect(mapStateToProps, null)(injectIntl(ConnectorDetailsModal));
