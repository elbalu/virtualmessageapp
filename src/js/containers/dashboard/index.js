import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';

/**
 * DashbaordWidgetList is a React component that displays a list of
 * Dashboard widget objects that call an add() function when clicked.
 * This list is used when building rules to add condition
 * and action widgets.
 *
 * @class
 * @requires react
 * @requires antd
 * @author Balu Loganthan <baepalap@cisco.com>
 */

class Dashboard extends Component {

  render() {
    return (
      <div>
        <h1>Dashboard</h1>
      </div>
    );
  }
}

Dashboard.propTypes = {};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Dashboard));
