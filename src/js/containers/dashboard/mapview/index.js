import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import update from 'immutability-helper';
import { Row, Col } from 'antd';
import _ from 'lodash';

import config from '../../../config';
import HistoryManager from '../../../misc/history-manager';
import MapManager from '../../../components/map2/map-manager';
import LocationSelector from '../../../components/map2/location-menu/selector';
import FilterAction from '../../../components/table/table-actions/filter-manager/filter-action';
import LocationCounts from '../../../components/map2/location-counts';
import DeviceDetailsModal from '../../../containers/devices/deviceDetails'

import * as mapViewActions from '../../../redux/actions/mapview-actions';
import * as areasActions from '../../../redux/actions/areas-actions';
import * as modalActions from '../../../redux/actions/modal-actions';
import * as deviceActions from '../../../redux/actions/device-actions';
import Location from '../../../misc/map/location';

const globalIntervalTimer = config.get('globalIntervalTimer');

const initialState = {
  deviceDetailsModal: {
    visible: false,
  },
  deviceDetails: []

};

/**
 * MapView is a React component
 *
 * @class
 * @requires react
 * @requires antd
 * @author Balu Loganthan <baepalap@cisco.com>
 */

class MapView extends Component {

  constructor(props) {
    super(props);
    this.state = initialState;
    this.historyManager = new HistoryManager(props.location);
    this.setDefaultLocation = this.setDefaultLocation.bind(this);
    this.setLocation = this.setLocation.bind(this);
    this.selectDevice = this.selectDevice.bind(this);
    // fetch all locations
    props.actions.getAllAreas(this.setDefaultLocation);
    this.getDevices(props.selectedLocation);

    this.toggleDeviceDetailsModalVisibility = this.toggleDeviceDetailsModalVisibility.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    var _this = this;
    if(this.props.selectedLocation == null && nextProps.selectedLocation == null) {
      return;
    }

    //only update if props have changed or on first load
    if(this.props.selectedLocation == null || nextProps.selectedLocation == null || this.props.selectedLocation.data.id !== nextProps.selectedLocation.data.id){
      this.getDevices(nextProps.selectedLocation);
    }


  }

  getDevices(selectedLoc) {
    var _this = this;
    if(selectedLoc == null) {
      //Top level, nothing selected
      this.intervalFunction = this.props.actions.getDeviceCount.bind(this);
      //this.intervalFunction();
    } else if (selectedLoc.data.level == 'CAMPUS') {
      this.intervalFunction = this.props.actions.getDeviceCountCampus.bind(this);;
    } else if (selectedLoc.data.level == 'BUILDING') {
      this.intervalFunction = this.props.actions.getDeviceCountBuilding.bind(this);
    } else if (selectedLoc.data.level == 'FLOOR') {
      this.intervalFunction = (floorId) => {
        _this.props.actions.getClientsByFloor.bind(_this)(floorId);
        _this.props.actions.getRoguesByFloor.bind(_this)(floorId);
        _this.props.actions.getRogueAPsByFloor.bind(_this)(floorId);
        _this.props.actions.getTagsByFloor.bind(_this)(floorId);
      };
      //this.intervalFunction(selectedLoc.data.id);
    } else {
      this.intervalFunction = this.props.actions.getDeviceCount.bind(this);
    }
    // set/clear timer to reload sdk if we stay on this page
    if(this.runningInterval) {
      clearInterval(this.runningInterval);
    }
    if(selectedLoc == null) {
      _this.intervalFunction();
    } else {
      _this.intervalFunction(selectedLoc.data.id);
    }
    this.runningInterval = setInterval(() => {
      if (_this.historyManager.getPath().indexOf('mapview') > -1) {
        if(_this.props.selectedLocation == null) {
          _this.intervalFunction();
        } else {
          _this.intervalFunction(_this.props.selectedLocation.data.id);
        }
        //props.actions.getAllDevices();
      }
    }, globalIntervalTimer);
  }

  /**
   * @namesetDefaultLocation
   *
   * @description Sets the selectedArea
   * in the store using the location variable in the
   * browser url (if one has been set).
   */
  setDefaultLocation() {
    const { getAreaById, selectArea } = this.props.actions;
    const defaultLocationId = _.get(this.historyManager.getState(), 'mapViewLocation');

    if (!_.isNil(defaultLocationId)) getAreaById(defaultLocationId);
    else selectArea(null);
  }
  /**
   * MapView#setLocation sets the given location as the
   * selected location by storing the location id in
   * the browser query string and setting the location
   * as the selected location in the store (using
   * getAreaById)
   *
   * @param {string} locationId - Location id
   */
  setLocation(locationId) {
    const { getAreaById } = this.props.actions;
    // store location id in browser query
    const state = this.historyManager.getState();
    const nextState = update(state, { mapViewLocation: { $set: locationId } });
    this.historyManager.setState(nextState);
    // request location details
    getAreaById(locationId);
  }

  /**
   * @name toggleDeviceDetailsModalVisibility
   *
   * @description Toggle the visibility of
   * the device details modal
   *
   * @param {Object} cmx - Clicked cmx object
   */
  toggleDeviceDetailsModalVisibility() {
    const visible = !this.state.deviceDetailsModal.visible;

    this.setState({ deviceDetailsModal: { visible } });
  }

  /**
   * @name selectDevice
   *
   * @description Selects device(s) by
   * opening the details modal with
   * the device feature geoJSON
   *
   * @param {{ deviceArray:Array }}
   */
  selectDevice(deviceArray) {
    this.setState({
      deviceDetails: deviceArray
    });
    this.toggleDeviceDetailsModalVisibility();
  }

  render() {
    const { locations, selectedLocation, activeFloorClients, activeFloorRogues, activeFloorRogueAPs, activeFloorTags, activeCounts } = this.props;

    const filterActions = (
        <span className="action-wrapper">
          <FilterAction
              actions={this.props.actions.filters}
              showEditableOnly={this.props.showEditableOnly}
          />
        </span>
    );

    return (
      <div className="mapview">
          {/* header */}
          <div className="header">
            <h3>Detect and Locate</h3>
            <Row type="flex" gutter={20}>
              <Col span={8}>
                {/* location selector */}
                <LocationSelector
                    centered
                    locations={locations}
                    selectedLocation={selectedLocation}
                    onSelect={this.setLocation}
                />
              </Col>
              <Col span={8}>
                {/* active counts */}
                <LocationCounts
                    selectedLocation={selectedLocation}
                    activeFloorClients={activeFloorClients}
                    activeFloorRogues={activeFloorRogues}
                    activeFloorRogueAPs={activeFloorRogueAPs}
                    activeFloorTags={activeFloorTags}
                    activeCounts={activeCounts}
                />
              </Col>
              <Col span={8} style={{textAlign:"right"}}>
                {filterActions}
              </Col>
            </Row>
          </div>

        <div className="content">
          <MapManager
              toggleLayers
              activeFloorClients={activeFloorClients}
              activeFloorRogues={activeFloorRogues}
              activeFloorRogueAPs={activeFloorRogueAPs}
              activeFloorTags={activeFloorTags}
              selectDevice={this.selectDevice}
              selectedLocation={selectedLocation}
              locations={locations}
              selectLocation={this.setLocation}
          />
        </div>
        <DeviceDetailsModal
            title="Device Details"
            hide={this.toggleDeviceDetailsModalVisibility}
            visible={this.state.deviceDetailsModal.visible}
            deviceDetails={this.state.deviceDetails}
        />
      </div>
    );
  }
}

MapView.propTypes = {
  // browser location
  location: PropTypes.shape(),
  // tree of locations
  locations: PropTypes.arrayOf(PropTypes.instanceOf(Location)),
  // currently selected location
  selectedLocation: PropTypes.instanceOf(Location),
  deviceDetailsModal: PropTypes.shape({
    visibility: PropTypes.bool
  }),
  // list of devices
  devices: PropTypes.arrayOf(PropTypes.object),
  //list of floor devices
  activeFloorClients: PropTypes.object, //geoJSON object
  //list of floor devices
  activeFloorRogues: PropTypes.object, //geoJSON object
  //list of floor devices
  activeFloorRogueAPs: PropTypes.object, //geoJSON object
  //list of floor devices
  activeFloorTags: PropTypes.object, //geoJSON object
  //object containing device counts for selected location and its children
  activeCounts: PropTypes.object,
  // currently selected category
  currentCategory: PropTypes.shape(),
  actions: PropTypes.shape({
    // get all locations (campus -> floor)
    getAllAreas: PropTypes.func,
    // get devices
    getAllClients: PropTypes.func,
    //get devices by floor
    getClientsByFloor: PropTypes.func,
    getRoguesByFloor: PropTypes.func,
    getRogueAPsByFloor: PropTypes.func,
    getTagsByFloor: PropTypes.func,
    // get details for one area (includings zones)
    getAreaById: PropTypes.func,
    // select a location
    selectArea: PropTypes.func,
    // select a category
    setCurrentCategory: PropTypes.func,
    closeModal: PropTypes.func,
    openDetailsModal: PropTypes.func
  }),
};
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...mapViewActions,
    ...areasActions,
    ...modalActions,
    ...deviceActions,
    setCurrentCategory: mapViewActions.setCurrentCategory
  }, dispatch),
});
const mapStateToProps = state => ({
  ...state.mapviewReducer,
  ...state.areas,
  ...state.modal,
  ...state.devices
});
export default connect(mapStateToProps, mapDispatchToProps)(MapView);
