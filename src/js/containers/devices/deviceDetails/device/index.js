import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {injectIntl, intlShape} from 'react-intl';

import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Tabs from 'antd/lib/tabs';

import History from '../sections/history';
import Table from '../../../../components/table';

import * as alertsActions from '../../../../redux/actions/alerts-actions';

import * as clientTableTemplate from '../../../../../resources/schemas/clients.json';
import * as rogueClientTableTemplate from '../../../../../resources/schemas/rogueClients.json';
import * as rogueAPTableTemplate from '../../../../../resources/schemas/rogueAPs.json';
import * as tagTableTemplate from '../../../../../resources/schemas/tags.json';

const Tab = Tabs.TabPane;
const initialState = {
    slidesToShow: 1,
};

const clientTemplate = {
    macAddress: "MAC Address",
    associated: "Status",
    ipAddress: "IP Address", //TODO
    coordinates: "Co-ordinates",
    computeType: "Compute Type",
    lastSeen: "Last Seen",
    manufacturer: "Manufacturer",
    connectedAP: "Connected AP",
    detectingControllers: "Detecting Controllers",
    ssid: "SSID",
    maxRSSI: "Max RSSI",
    username: "Username",
    band: "Band",
    bytesSent: "Bytes Sent",
    bytesReceived: "Bytes Received"
};

const rogueAPTemplate = {
    macAddress: "MAC Address",
    coordinates: "Co-ordinates",
    computeType: "Compute Type",
    lastSeen: "Last Seen",
    classification: "Classification",
    containmentLevel: "Containment Level",
    state: "State",
    manufacturer: "Manufacturer",
    totalClients: "Total Clients",
    ssid: "SSID",
    numOfDetectingAPs: "Number of Detecting APs",
    maxRSSI: "Max RSSI",
    username: "Username",
    band: "Band",
    bytesSent: "Bytes Sent",
    bytesReceived: "Bytes Received"
};

const rogueClientTemplate = {
    macAddress: "MAC Address",
    coordinates: "Co-ordinates",
    computeType: "Compute Type", //TODO
    lastSeen: "Last Seen", //TODO
    containmentLevel: "Containment Level",
    state: "State",
    manufacturer: "Manufacturer",
    numOfDetectingAPs: "Number of Detecting APs",
    bssid: "BSSID"
};

const tagTemplate = {
    macAddress: "MAC Address",
    coordinates: "Co-ordinates",
    computeType: "Compute Type", //TODO
    lastSeen: "Last Seen", //TODO
    manufacturer: "Manufacturer",
    detectingApMAC: "Detecting AP MAC",
    maxRSSI: "Max RSSI",
    networkStatus: "Network Status",
    remainingBattery: "Remaining Battery",
    batteryAge: "Battery Age",
    numOfDetectingAPs: "Number of Detecting APs",
    vendorData: "Vendor Data"
};

/**
 * Asset details or Asset 360 gives a preview
 * of the Asset
 *
 * @author Balu Loganathan
 */
class DeviceDetails extends Component {

    /**
     * constructor. bind methods that will be called
     * by the DOM.
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    slidesToShow() {
        const {page} = this.props;
        if (page === 'ALERTS') {
            return 'history';
        }
        return 'overview';
    }

    generateSingularDeviceContent(deviceObj) {
        //get template
        var template = {};
        switch(deviceObj.properties.deviceType){
            case "CLIENT": template = clientTemplate; break;
            case "ROGUE_CLIENT": template = rogueClientTemplate; break;
            case "ROGUE_AP": template = rogueAPTemplate; break;
            case "TAG": template = tagTemplate; break;
        }

        var detailsArray = [];
        for(var property in template) {
            var value = deviceObj.properties[property];

            detailsArray.push(<div data-id={property} className="ant-row-flex ant-row-flex-start attribute-row">
                <div className="ant-col-12 label">{ template[property] }</div>
                <div className="ant-col-12 value">{ value }</div>
            </div>)
        }

        return (
            <Col span={12}>
                <div className="general">
                    <div><h5><i className="fa fa-info-circle"></i>&nbsp;
                        <span>General</span></h5>
                        { detailsArray }
                    </div>
                </div>
            </Col>
        )
    }

    generateDevicesContent(deviceArray) {

        var formattedData = [];
        for(var i = 0; i < deviceArray.length; i++){
            formattedData.push(deviceArray[i].properties);
        }

        var template = {};
        if(formattedData.length > 0){
            //get template
            switch(formattedData[0].deviceType){
                case "CLIENT": template = clientTableTemplate; break;
                case "ROGUE_CLIENT": template = rogueClientTableTemplate; break;
                case "ROGUE_AP": template = rogueAPTableTemplate; break;
                case "TAG": template = tagTableTemplate; break;
            }
        }
        
        return (
            <Col span={24}>
                <div className="general">
                    <div>
                        <Table
                            dataSource={formattedData}
                            count={0}
                            template={template}
                            rowKey={"macAddress"}
                            hasActions={false}
                            hasEditAccess={false}
                            entityType={formattedData[0].deviceType}
                            fieldActions={{
                                add: null,
                                save: null,
                                remove: null
                            }}

                        />
                    </div>
                </div>
            </Col>
        )
    }

    render() {
        const {devices} = this.props;
        var deviceContent = null;
        var historyContent = null;

        if (devices.length == 0) return null;
        else if (devices.length == 1) {
            deviceContent = this.generateSingularDeviceContent(devices[0]);
        } else {
            deviceContent = this.generateDevicesContent(devices);
        }

        return (
            <Tabs defaultActiveKey={"overview"}>
                <Tab tab="Overview" key="overview">
                    <Row className="device-detail-content" type="flex" align="middle">
                        { deviceContent }
                    </Row>
                </Tab>

                <Tab tab="History" key="history">
                    <div className="device-history">
                    </div>
                </Tab>
            </Tabs>
        );
    }
}

DeviceDetails.propTypes = {
    device: PropTypes.shape({}),
    templates: PropTypes.shape({}),
    actions: PropTypes.shape({}),
    // i18n support
    intl: intlShape.isRequired,
};

const mapDispatchToProps = dispatch => ({
    actions: {
        ...bindActionCreators(alertsActions, dispatch),
    },
});

const mapStateToProps = state => ({
    ...state.alerts,
    ...state.history,
    templates: {
        devices: state.templates.devices,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(DeviceDetails));