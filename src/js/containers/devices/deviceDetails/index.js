import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import _ from 'lodash';


import Modal from 'antd/lib/modal';
import DeviceDetails from './device';


import * as areasActions from '../../../redux/actions/areas-actions';
import * as mapActions from '../../../redux/actions/maps-actions';
import * as modalActions from '../../../redux/actions/modal-actions';

const initialState = {
    modalKey: 1
};

/**
 * Device Details
 * This class creates a modal popup to show the device details
 *
 * @author Joshua Mayer
 */
class DeviceDetailsModal extends Component {

    constructor(props) {
        super(props);
        this.state = initialState;
    }

    /**
     * handler to hide
     * the modal
     */
    hideModal() {
        const { hide } = this.props;
        hide();
    }

    /**
     * on modal close increment the
     * key counter so that the modal gets
     * re rendrered everytime it opens
     */
    modalClose() {
        this.setState({ modalKey: (this.state.modalKey + 1) });
    }

    renderDeviceDetails() {
        var { deviceDetails } = this.props;
        return <DeviceDetails devices={deviceDetails} />;
    }

    render() {
        const { visible } = this.props;


        return (
            <Modal
                title={"Device Details"}
                key={this.state.modalKey}
                className="device-details"
                visible={visible}
                width={1075}
                height={1000}
                onCancel={() => this.hideModal()}
                footer={false}
                afterClose={() => this.modalClose()}
            >
                { this.renderDeviceDetails() }

            </Modal>
        );
    }
}


DeviceDetailsModal.propTypes = {
    // whether or not the modal is visible
    visible: PropTypes.bool,
    hide: PropTypes.func,
};

DeviceDetailsModal.defaultProps = {
    visible: false,
    hide: _.noop,
};

export default (injectIntl(DeviceDetailsModal));