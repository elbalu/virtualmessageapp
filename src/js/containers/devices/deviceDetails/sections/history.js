import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { DatePicker, Tabs, Row, Col, Spin } from 'antd';


const initialState = {
    date: new Date(),
    activeKey: 1
};
/**
 * Alert and Location History
 * for sdk
 *
 * @author Balu Loganathan
 */
class History extends Component {

    /**
     * @override
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = initialState;

    }

    componentWillMount() {
    }

    componentWillReceiveProps(nextProps) {

    }

    onChange(activeKey) {
        this.setState({ activeKey });
    }


    render() {

        return (
            <div>

                TODO
            </div>
        );
    }
}

History.propTypes = {

};

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(History))