import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * License View Page
 *
 * @author Balu Loganathan
 */

class License extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>License</h1>
      </div>
    );
  }
}

License.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

License.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.license;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(License));
