import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';

import Button from 'antd/lib/button';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Input from 'antd/lib/input';
import Form from 'antd/lib/form';

import * as navigationActions from '../../redux/actions/navigation-actions';
import * as loginActions from '../../redux/actions/login-actions';
import * as rolesActions from '../../redux/actions/roles-actions';

const FormItem = Form.Item;

// TODO add documentation comments to Login
class Login extends Component {

  handleSubmit(e) {
    e.preventDefault();
    const { loginAuth } = this.props;
    this.props.form.validateFields((errors, values) => {
      if (errors) {
        return;
      }
      loginAuth({
        ...values,
      });
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { intl, error } = this.props;

    const usernamePlaceholder = intl.formatMessage({ id: 'users.username' });
    const passwordPlaceholder = intl.formatMessage({ id: 'users.password' });
    const loginFailureMessage = (error) ? <FormattedMessage id="login.failure" /> : null;

    return (
      <div className="login-page">
        <Row>
          <Col span={6} className="left">
            <div className="sdk-icon-cisco" />
          </Col>
          <Col span={18} className="right">
            <div className="login-form">
              <div className="login-title"><FormattedMessage id="login.welcome" /></div>

              <Form>

                <div className="form-row">
                  <span className="fa fa-user" />
                  <FormItem label="">
                    {getFieldDecorator('username', { rules: [{ required: true }] })(
                      <Input id={'username'} placeholder={usernamePlaceholder} />
                    )}
                  </FormItem>
                </div>

                <div className="form-row">
                  <span className="fa fa-lock" />
                  <FormItem label="">
                    {getFieldDecorator('password', { rules: [{ required: true }] })(
                      <Input id={'password'} type="password" placeholder={passwordPlaceholder} />
                    )}
                  </FormItem>
                </div>

                <div className="login-message">
                  {loginFailureMessage}
                </div>

                <div>
                  <Button htmlType="submit" onClick={e => this.handleSubmit(e)}>
                    <FormattedMessage id="login.title" />
                  </Button>
                </div>

              </Form>

            </div>
          </Col>

        </Row>

      </div>
    );
  }
}

Login.propTypes = {
  loginAuth: PropTypes.func.isRequired,
  form: PropTypes.shape(),
  getFieldDecorator: PropTypes.shape(),
  error: PropTypes.shape(),

  // i18n support
  intl: intlShape.isRequired,
};

const mapStateToProps = state => ({
  ...state.login,
  ...state.roles,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    ...loginActions,
    ...rolesActions,
    ...navigationActions,
  }, dispatch),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(injectIntl(Login)));
