import React, { PropTypes } from 'react';
import Select, { Option } from 'antd/lib/select';

// TODO add documentation comments to RoleSelect
const RoleSelect = ({ selected, roles, actions }) => (
  <div className="role-selector">
    <Select
      size="large"
      defaultValue={`${selected.id || 'Super Admin'}`}
      style={{ width: 200 }}
      onChange={val => actions.select(roles.find(role => role.id === parseFloat(val)))}>
      { roles.map(({ id, title }, index) => <Option key={index} value={`${id}`}>{title}</Option>) }
    </Select>
  </div>
);

RoleSelect.propTypes = {
  roles: PropTypes.instanceOf(Array),
  actions: PropTypes.shape({}),
  selected: PropTypes.shape(),
};

export default RoleSelect;
