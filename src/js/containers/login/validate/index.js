import React, { Component, PropTypes } from 'react';
import { Spin } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';


import * as loginActions from '../../../redux/actions/login-actions';

class Validate extends Component {

  componentDidMount() {
    const { location, validateRedirect } = this.props;
    validateRedirect(location.query.token);
  }

  render() {
    const env = process.env.NODE_ENV;
    return (
      <div className="validate">
        <Spin tip="Validating..." size="large" />
      </div>
    );
  }
}

Validate.propTypes = {
  validateRedirect: PropTypes.func.isRequired,
  location: PropTypes.shape(),
};

const mapStateToProps = state => ({
  ...state.login,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    ...loginActions,
  }, dispatch),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Validate));
