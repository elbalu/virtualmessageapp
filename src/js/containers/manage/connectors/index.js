import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import validate from 'validate.js';
import _ from 'lodash';

import req from '../../../misc/agent';
import url from '../../../misc/url';

import EntityModal from '../../../components/entity-modal';
import TokenModal from '../../../components/token-modal';
import Table from '../../../components/table';
import WidgetBar from '../../../components/widget-bar';
import EntityWidget from '../../../components/entity-widget';

import { hasAccess } from '../../../misc/user-utils';

import ConnectorDetails from '../../connectors/connectorDetails';

import * as wlcActions from '../../../redux/actions/wlc-actions';
import * as connectorActions from '../../../redux/actions/connector-actions';
import * as modalActions from '../../../redux/actions/modal-actions';
import * as notificationActions from '../../../redux/actions/notification-actions';
import * as templatesActions from '../../../redux/actions/templates-actions';
import * as tableActions from '../../../redux/actions/table-actions';

import * as template from '../../../../resources/schemas/connectors.json';

const initialState = {
  tokenModal: {
    visible: false,
    token: '',
    cmx: null
  },
  entityModal: {
    visible: false,
    entity: {},
  },
};

/**
 * TODO
 */
class Connectors extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
    this.toggleEntityModalVisibility = this.toggleEntityModalVisibility.bind(this);
    this.toggleTokenModalVisibility = this.toggleTokenModalVisibility.bind(this);
  }

  /**
   * TODO
   */
  componentWillMount() {
    const { getAllConnectors, getFilteredList } = this.props.actions;
    const { getAllWlcs } = this.props.wlcActions;
    const { filterSelect, clearParams } = this.props.tableActions;
    const { getTemplate } = this.props.templatesActions;

    // clear any existing params
    clearParams();
    //getTemplate('CONNECTORS');

    getAllWlcs();
    getAllConnectors();
    getFilteredList();
  }
  /**
   * handler for connector details click
   * opens connector detail view
   */
  viewConnectorsDetails(id) {
    const { openDeviceDetailsModal } = this.props.modalActions;
    openDeviceDetailsModal('connectors', id);
  }

  /**
   * @name getActions
   *
   * @description Gets any custom actions
   * Generates an icon
   * that opens the token modal on click
   *
   * @return {function(*=): XML}
   */
  getActions() {
    const hasTokenAccess = true;
    if (!hasTokenAccess) return [];
    const token = cmx => (
      (<i
        key="get-token-action"
        className="clickable fa fa-key"
        onClick={() => this.toggleTokenModalVisibility(cmx)}
      />)
    );
    return [token];
  }

  /**
   * @name getToken
   *
   * @description Performs a request to
   * authenticate the user. Returns the
   * request Promise.
   *
   * @param {string} username
   *
   * @param {string} password
   *
   * @param {number} cmxId - Id of the cmx instance
   * to get the token for
   */
  getToken(username, password, cmxId) {
      return new Promise((resolve, reject) => {
          req.post(url.tokenAuth + '/connector/' + cmxId)
          // set cmx id and username/password
              .send({ username, password })
              // get response
              .then((response) => {
                  const token = _.get(response, 'body.token');
                  resolve(token);
              })
              .catch(reject);
      });
  }
  /**
   * @name toggleTokenModalVisibility
   *
   * @description Toggle the visibility of
   * the token modal
   *
   * @param {Object} cmx - Clicked cmx object
   */
  toggleTokenModalVisibility(cmx) {
      const visible = !this.state.tokenModal.visible;
      const nextCMX = visible ? cmx : null;
      const token = (nextCMX && nextCMX.jwttoken) ? nextCMX.jwttoken : '';

      this.setState({ tokenModal: { visible, token, cmx: nextCMX } });
  }
  /**
   * TODO
   * @param nextEntity
   */
  toggleEntityModalVisibility(nextEntity) {
      const { entityModal } = this.state;
      const visible = !entityModal.visible;
      const entity = (validate.isObject(nextEntity)) ? nextEntity : entityModal.entity;

      this.setState({ entityModal: { entity, visible } });
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    const { connectors, collections, filteredConnectors, intl } = this.props;
    const { removeConnector, addConnector, editConnector, getFilteredList } = this.props.actions;
    const { openFieldsModal, openDetailsModal, closeModal } = this.props.modalActions;
    const { deviceDetailsModal } = this.props.modal;
    const { display } = this.props.notificationActions;
    const { entityModal } = this.state;
    const { createField, updateField, removeField } = this.props.templatesActions;
    const { filterSelect, setParams, addFields } = this.props.tableActions;
    const { filters, params } = this.props.table;

    const hasEditAccess = hasAccess('connector', 'update');
    const hasDeleteAccess = hasAccess('connector', 'delete');
    const connectorsTitle = intl.formatMessage({ id: 'connector.title' });
    const entityType = "connector";
    const itemKey = "id";

      return (
          <div>
            <WidgetBar>
              <EntityWidget
                  title={connectorsTitle}
                  icon="fa fa-link"
                  add={() => this.toggleEntityModalVisibility({})}
                  entities={connectors}
                  entityType={entityType}
              />
           </WidgetBar>
          <Table
              dataSource={connectors}
              collections={collections}
              count={0}
              template={template}
              rowKey={itemKey}
              edit={(hasEditAccess) ? this.toggleEntityModalVisibility : null}
              remove={(hasDeleteAccess) ? removeConnector : null}
              hasActions={(hasEditAccess || hasDeleteAccess)}
              details={id => this.viewConnectorsDetails(id)}
              hasEditAccess={hasEditAccess}
              entityType={entityType}
              fieldActions={{
                  add: createField,
                  save: updateField,
                  remove: removeField
              }}
              filterSelect={filterSelect}
              setParams={setParams}
              params={params}
              addFields={addFields}
              filters={filters}
              fetchData={getFilteredList}
              actions={this.getActions()}
          />

          <EntityModal
            entity={entityModal.entity}
            visible={entityModal.visible}
            title={connectorsTitle}
            template={template}
            displayNotification={display}
            actions={{
              add: addConnector,
              edit: editConnector,
              remove: removeConnector,
              hide: () => this.toggleEntityModalVisibility(),
            }}
          />
          <ConnectorDetails
              visible={deviceDetailsModal.visible}
              modalActions={{
                  hide: closeModal,
              }}
              page="CONNECTORS"
              connector={deviceDetailsModal.data}
              date={deviceDetailsModal.date}
          />
          <TokenModal
              title="Connector Token"
              info="Please authenticate to view connector token"
              cmx={this.state.tokenModal.cmx}
              token={this.state.tokenModal.token}
              getToken={this.getToken}
              hide={this.toggleTokenModalVisibility}
              visible={this.state.tokenModal.visible}
          />
          {/*<TemplateModal template={template} />*/}
        </div>
      );
  }

}

Connectors.propTypes = {
  // DEFINED BY REDUX

  // connector state
  template: PropTypes.shape(),
  connectors: PropTypes.instanceOf(Array),
  location: PropTypes.shape({ pathname: PropTypes.string }),

  // connector actions
  actions: PropTypes.shape({
      getAllConnectors: PropTypes.func,
      addConnector: PropTypes.func,
      editConnector: PropTypes.func,
      removeConnector: PropTypes.func,
  }),

  // modal actions
  modalActions: PropTypes.shape({
      openFieldsModal: PropTypes.func,
      openDetailsModal: PropTypes.func.isRequired,
      closeModal: PropTypes.func.isRequired,
  }),
  // modal state
  modal: PropTypes.shape({
      detailsModal: PropTypes.object.isRequired,
  }),

  notificationActions: PropTypes.shape({
      display: PropTypes.func,
  }),

  // field actions
  templatesActions: PropTypes.shape({
      createField: PropTypes.func,
      updateField: PropTypes.func,
      removeField: PropTypes.func,
      getTemplate: PropTypes.func,
  }),

  // i18n support
  intl: intlShape.isRequired,
};

const mapStatetoProps = state => {
  return ({
    ...state.connectors,
    modal: state.modal,
    table: state.table,
    collections: {
      CONTROLLERS: state.wlcs.wlcs,
      CONNECTORS: state.connectors.connectors
    }
  })
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(connectorActions, dispatch),
    wlcActions: bindActionCreators(wlcActions, dispatch),
    modalActions: bindActionCreators(modalActions, dispatch),
    notificationActions: bindActionCreators(notificationActions, dispatch),
    templatesActions: bindActionCreators(templatesActions, dispatch),
    tableActions: bindActionCreators(tableActions, dispatch),
});

export default connect(mapStatetoProps, mapDispatchToProps)(injectIntl(Connectors));
