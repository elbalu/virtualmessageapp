import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * Devices View Page
 *
 * @author Balu Loganathan
 */

class Devices extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>Devices</h1>
      </div>
    );
  }
}

Devices.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

Devices.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.devices;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Devices));
