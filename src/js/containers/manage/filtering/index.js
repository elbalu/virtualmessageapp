import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * Filtering View Page
 *
 * @author Balu Loganathan
 */

class Filtering extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>Filtering</h1>
      </div>
    );
  }
}

Filtering.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

Filtering.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.filtering;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Filtering));
