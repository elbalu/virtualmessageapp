/**
 * maps container
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import { setRoute } from '../../../misc/url-persist-utils';

import Map from '../../../components/map';

import { find, isEmpty } from '../../../misc/helpers';

import * as mapsActions from '../../../redux/actions/maps-actions';
import * as areasActions from '../../../redux/actions/areas-actions';

class Maps extends Component {
  componentWillMount() {
    const { fetchAreas, fetchFeatures } = this.props.actions;

    setRoute(this.props.location.pathname);
    // fetch all areas
    fetchAreas();
    fetchFeatures();
  }

  componentWillReceiveProps(next) {
    // select 1st campus -> 1st building by default
    const { currentFloor } = this.props;
    const { selectFloor } = this.props.actions;

    if (next.geoData && next.geoData.length) {
      const floors = next.geoData[0].relationshipData.children[0].relationshipData.children;

      if (isEmpty(currentFloor)) {
        selectFloor(floors[0]);
      }
    }
  }

  prepareDataForCascade() {
    const { geoData } = this.props;

    const parseLevel = level =>
      level.map((l) => {
        const obj = {
          value: l.id,
          label: l.name,
        };

        if (l.children && l.children.length > 0 && l.level.toLowerCase() !== 'building') {
          obj.children = parseLevel(l.children);
        }

        return obj;
      });

    return parseLevel(geoData);
  }

  locationOnChange(e) {
    if (e && e.length > 1) {
      const { geoData } = this.props;
      const { setFloors, selectFloor } = this.props.actions;

      const floors = e.reduce((acc, id) =>
        find(acc, lvl => lvl.id === id).children
      , geoData);

      setFloors(floors);
      selectFloor(floors[0]);
    }
  }
  render() {
    const { geoData, currentFloor } = this.props;

    return (
      <div className="map-page">
        <div className="map-page-content">
          <div className="map-wrapper">
            <Map id="main-map" mode="floor" selectedFloor={currentFloor} geoData={geoData} />
          </div>
        </div>
      </div>
    );
  }
}

Maps.propTypes = {
  currentFloor: PropTypes.shape(),
  geoData: PropTypes.instanceOf(Array),
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),

  actions: PropTypes.shape({
    fetchFeatures: PropTypes.func.isRequired,
    fetchAreas: PropTypes.func.isRequired,
    selectFloor: PropTypes.func.isRequired,
    setFloors: PropTypes.func.isRequired,
  }),
};

const mapDispatchToProps = dispatch => ({
  actions: {
    ...bindActionCreators(mapsActions, dispatch),
    ...bindActionCreators(areasActions, dispatch),
  },
});

const mapStateToProps = state => ({
  ...state.mapsReducer,
  ...state.areasReducer,
});

export default connect(mapStateToProps, mapDispatchToProps)(Maps);
