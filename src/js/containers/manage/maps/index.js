import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Alert } from 'antd';

import { bindActionCreators } from 'redux';

import WidgetBar from '../../../components/widget-bar';
import EntityWidget from '../../../components/entity-widget';

import * as mapsActions from '../../../redux/actions/maps-actions';
import * as areasActions from '../../../redux/actions/areas-actions';
import * as notificationActions from '../../../redux/actions/notification-actions';

import LocationList from './location/list';

const initialState = {
  response: '',
  status: '',
  type: '',
};

/**
 * MapView is a page component that displays information about
 * maps stored in Asset Management. Users can drill down through
 * locations and view floor maps.
 *
 * @class
 * @author Balu Loganathan <baepelap@cisco.com>
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Maps extends React.Component {
  /**
   * @constructor
   */
  constructor() {
    super();
    this.state = initialState;
  }

  /**
   *
   * @return {XML}
   */
  notifyStatus() {
    const { response, type } = this.state;
    return (
      <Alert
        message={response}
        type={type}
        showIcon
      />
    );
  }

  render() {
    const { display } = this.props.notificationActions;
    const { geoData } = this.props;
    const { fetchAreas } = this.props.actions;
    const { query } = this.props.location;

    return (
      <div className="map-page">
        <WidgetBar>
          <EntityWidget
            title="Campus"
            entities={geoData}
            add={() => {}}
            entityType="maps"
            reloadEntities={fetchAreas}
            icon="fa fa-globe"
            displayNotification={display}
          />
        </WidgetBar>

        <LocationList entities={geoData} />
      </div>
    );
  }
}

Maps.propTypes = {
  geoData: PropTypes.instanceOf(Array),

  actions: PropTypes.shape({
    fetchAreas: PropTypes.func.isRequired,
  }),

  notificationActions: PropTypes.shape({
    display: PropTypes.func,
  }),

  // url query params
  location: PropTypes.shape({
    query: PropTypes.object
  })
};

const mapDispatchToProps = dispatch => ({
  actions: {
    ...bindActionCreators(mapsActions, dispatch),
    ...bindActionCreators(areasActions, dispatch),
  },
  notificationActions: bindActionCreators(notificationActions, dispatch),
});

const mapStateToProps = state => ({
  ...state.mapsReducer,
  ...state.areasReducer,
});

export default connect(mapStateToProps, mapDispatchToProps)(Maps);
