import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import { find, map } from '../../../misc/helpers';

import * as mapActions from '../../../redux/actions/maps-actions';

class LayerPropPanel extends Component {

  getLayer() {
    const { activeLayer, layers } = this.props;
    return find(layers, l => l.id === activeLayer);
  }

  setValue(key, val) {
    const { activeLayer, layers } = this.props;
    const { updateLayer } = this.props.actions;

    const layer = this.getLayer();

    updateLayer({
      ...layer,
      [key]: val,
    });
  }

  render() {
    const { activeLayer, layers } = this.props;

    const layer = this.getLayer();

    if (layer) {
      const latlngs = map(layer.latlng, l => String(l)).join('\n');
      return (
        <div>
          <div><FormattedMessage id="maps.id" values={{ id: layer.id }} /></div>
          <div><FormattedMessage id="latlng" values={{ latlng: latlngs }} /></div>
          <div><FormattedMessage id="maps.type" values={{ type: layer.type }} /></div>

          <FormattedMessage id="maps.featureName" />
          <input type="text" value={layer.name} onChange={e => this.setValue('name', e.target.value)} />
        </div>
      );
    }

    return (<span>Select or create feature</span>);
  }
}

LayerPropPanel.propTypes = {
  // TODO add proptypes to LayerPropPanel
};

const mapStateToProps = state => state.mapsReducer;
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...mapActions,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(LayerPropPanel);
