/**
 import _ from 'lodash';
 * ruleswf container
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import _ from 'lodash';
import Collapse from 'antd/lib/collapse';

import { Row, Col, Input, Button } from 'antd';
import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete';

import { hasAccess } from '../../../../misc/user-utils';
import * as mapsActions from '../../../../redux/actions/maps-actions';
import Map from '../../../../components/map';

const Panel = Collapse.Panel;

/**
 * Recursive Location item component.
 * Creates a location item for each child.
 */
class LocationItem extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = { address: '', edit: false };
    this.onChange = address => this.setState({ address });
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    const { location } = this.props;
    const { details } = location;
    const { address } = details;
    if (address) {
      this.setState({ address });
    }
  }
  /**
   * Handle submit on address change
   * @param event, location
   */
  handleFormSubmit(event, location) {
    console.log("ADD ADDRESS - MAPS")
    const { addAddress } = this.props.actions;
    event.preventDefault();
    const { address } = this.state;
    geocodeByAddress(address, (err, { lat, lng }) => {
      if (err) {
        console.error('Oh no!', err);
      }
      location.details.address = address;
      location.details.latitude = lat;
      location.details.longitude = lng;
      addAddress(location.id, location);
      this.toggleEdit();
    });
  }

  /**
   * Toggle edit or read view for address panel
   */
  toggleEdit() {
    this.setState({ edit: !this.state.edit });
  }

  /**
   * Render location list for child objects
   * @param childLocation
   */
  renderLocationList(childLocation) {
    if (!childLocation) {
      return (<div />);
    } else if (childLocation.length === 0) {
      return (<div>No children.</div>);
    }
    return childLocation.map((location, ind) =>
      <Row key={ind} className="ruleswf-list">
        <LocationWrapper location={location} />
      </Row>
    );
  }

  renderIcon(location) {
    const { level } = location;
    if (level) {
      switch (_.lowerCase(level)) {
        case 'campus':
          return 'fa fa-globe';
        case 'building':
          return 'sdk-icon-building';
        default:
          return 'floor';
      }
    }
    if (level === 'FLOOR') {
      return 'sdk-icon-floor';
    }
    return '';
  }

  render() {
    const { location } = this.props;
    const hasEditAccess = hasAccess('maps', 'add');
    if (!location) {
      return (
        <div>Loading ...</div>
      );
    }

    let contentPanel = null;
    const { level, details } = location;
    const { edit } = this.state;
    const customPanelStyle = {
      borderRadius: 4,
      marginBottom: 24,
      border: 0,
    };

    const editButton = (hasEditAccess) ?
      <Button type="default" onClick={e => this.toggleEdit(e)}>edit</Button> : '';

    if (hasEditAccess && level === 'BUILDING' && (!details.address || edit)) {
      const address = (details.address) ? details.address : 'Address';
      contentPanel = (
        <div className="edit-panel">
          <form onSubmit={e => this.handleFormSubmit(e, location)}>
            <PlacesAutocomplete
              hideLabel
              value={this.state.address}
              onChange={this.onChange}
            >
              <Input
                style={{ width: 300 }}
                placeholder={address}
              />
            </PlacesAutocomplete>
            <Button type="primary" htmlType="submit" className="login-form-button">Submit</Button>
            <Button type="default" onClick={e => this.toggleEdit(e)}>Clear</Button>
          </form>
        </div>
      );
    } else if (level === 'BUILDING' && details.address) {
      contentPanel = (
        <div>
          <span className="content-str">{details.address}</span>
          { editButton }
        </div>
      );
    } else if (level === 'FLOOR') {
      contentPanel = (
        <Map
          id={`map-${location.id}`}
          mode="floor"
          nav="manage"
          selectedFloor={location}
        />
      )
    }
    return (
      <Collapse accordion bordered={false} defaultActiveKey={['0']} className="location-map-item" style={customPanelStyle}>
        <Panel header={location.name} key={location.id}>
          <Col>
            <i className={this.renderIcon(location)} />
            <span>
              {location.name}
            </span>
            {contentPanel}
            {this.renderLocationList(location.relationshipData.children)}
          </Col>
        </Panel>
      </Collapse>
    );
  }
}

LocationItem.propTypes = {
  location: PropTypes.shape({}),
  actions: PropTypes.shape({
    addAddress: PropTypes.func.isRequired,
  }),
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(mapsActions, dispatch),
});

const mapStateToProps = state => ({
  ...state.mapsReducer,
});

const LocationWrapper = connect(mapStateToProps, mapDispatchToProps)(injectIntl(LocationItem));
export default LocationWrapper;