import React, { Component, PropTypes } from 'react';

import LocationItem from './item';

/**
 * LocationList is a list of locations (campus, building,
 * floor). Users can drill down through the locations
 * hierarchy and view/edit floor maps.
 *
 * @class
 * @author Balu Loganathan <baepelap@cisco.com>
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class LocationList extends Component {
  /**
   * render location list based on geodata
   * @override
   * @return {XML}
   */
  render() {
    const { entities } = this.props;

    if (!entities) return (<div>Loading...</div>);
    if (entities.length === 0) return (<div>No campuses</div>);

    return (
      <div className="location-map">
        <div className="location-map-list">
          {entities.map((location, ind) => (
            <LocationItem key={ind} location={location} />
          ))}
        </div>
      </div>
    );
  }
}

LocationList.propTypes = {
  entities: PropTypes.instanceOf(Array).isRequired,
};

export default LocationList;
