import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import update from 'immutability-helper';
import _ from 'lodash';

import WidgetBar from '../../../components/widget-bar';
import EntityWidget from '../../../components/entity-widget';
import MapManager from '../../../components/map2/map-manager';
import HistoryManager from '../../../misc/history-manager';
import * as mapsActions from '../../../redux/actions/maps-actions';
import * as poiActions from '../../../redux/actions/poi-actions';
import * as pathActions from '../../../redux/actions/path-actions';
import * as areasActions from '../../../redux/actions/areas-actions';
import * as notificationActions from '../../../redux/actions/notification-actions';

/**
 * MapView is a page component that displays information about
 * maps stored in Asset Management. Users can drill down through
 * locations and view floor maps.
 *
 * @class
 * @author Balu Loganathan <baepelap@cisco.com>
 */
class Maps extends React.Component {
  /**
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.historyManager = new HistoryManager(props.location);
    this.setLocation = this.setLocation.bind(this);
    this.setDefaultLocation = this.setDefaultLocation.bind(this);
    this.selectPoi = this.selectPoi.bind(this);
    // fetch initial data
    this.setDefaultLocation();
  }

  /**
   * MapView#setDefaultLocation sets the selectedArea
   * in the store using the location variable in the
   * browser url (if one has been set).
   */
  setDefaultLocation() {
    const { getAreaById, selectArea } = this.props.areasActions;
    const stateLocationId = _.get(this.historyManager.getState(), 'location');
    if (!_.isNil(stateLocationId)) getAreaById(stateLocationId);
    else selectArea(null);
  }

  componentWillMount() {
    const stateLocationId = _.get(this.historyManager.getState(), 'location');
    this.props.areasActions.getAllAreas();
    this.props.poiActions.getPOIByFloor(stateLocationId);
    this.props.pathActions.getPathByFloor(stateLocationId);
  }
  /**
   * MapView#setLocation sets the given location as the
   * selected location by storing the location id in
   * the browser query string and setting the location
   * as the selected location in the store (using
   * getAreaById)
   *
   * @param {string} locationId - Location id
   */
  setLocation(locationId) {
    const { getAreaById } = this.props.areasActions;

    // store location id in local storage
    const state = this.historyManager.getState();
    const nextState = update(state, { location: { $set: locationId } });
    this.historyManager.setState(nextState);

    // request location details
    getAreaById(locationId);
  }

  selectPoi(poi) {
    const { setSelectedPOI } = this.props.poiActions;
    setSelectedPOI(poi);
  }
  /**
   * @override
   * @return {XML}
   */
  render() {
    const { display } = this.props.notificationActions;
    const { getAllAreas, saveArea, createArea, removeArea, selectArea, selectZone } = this.props.areasActions;
    const { locations, selectedLocation, selectedZone } = this.props.areas;
    if (!locations) {
      return (
        <div>Loading...</div>
      );
    }
    const { poi, selectedPOI } = this.props.poi;
    const { path, selectedPath, activePair } = this.props.path;
    const isPOIs = !!poi.features;
    const allTags = isPOIs ? poi.features.map(f => f.properties.keyWords) : [];
    const tags = _.union(...allTags);
    return (
      <div className="map-page">
        <WidgetBar>
          <EntityWidget
            title="Campus"
            entities={locations}
            entityType="maps"
            reloadEntities={getAllAreas}
            icon="fa fa-globe"
            displayNotification={display}
          />
        </WidgetBar>

        <div className="map-manager-wrapper">
          <MapManager
            menu
            toggleLayers
            locations={locations}
            selectedLocation={selectedLocation}
            selectLocation={this.setLocation}
            poi={poi}
            path={path}
            activePair={activePair}
            selectedPath={selectedPath}
            poiActions={this.props.poiActions}
            pathActions={this.props.pathActions}
            selectedPOI={selectedPOI}
            onSelectPoi={this.selectPoi}
            tags={tags}
          />
        </div>

      </div>
    );
  }
}

Maps.propTypes = {
  areas: PropTypes.shape({
    // locations
    locations: PropTypes.instanceOf(Array),
    // selected location
    selectedLocation: PropTypes.shape(),
    selectedZone: PropTypes.shape()
  }),

  // location related action creators
  areasActions: PropTypes.shape({
    // fetches location hierarchy
    // 3 levels deep (campus, bldg, flr)
    getAllAreas: PropTypes.func,
    // fetches a single location + children
    getAreaById: PropTypes.func,
    // save an updated location
    saveArea: PropTypes.func,
    // create a new location
    createArea: PropTypes.func,
    // remove a location
    removeArea: PropTypes.func,
    // update selected location
    selectArea: PropTypes.func,
    // update selected zone
    selectZone: PropTypes.func
  }),
  poiActions: PropTypes.shape({
    getPOIByFloor: PropTypes.func
  }),
  // display a notification (feedback)
  notificationActions: PropTypes.shape({
    display: PropTypes.func,
  }),

  // react-router browser location
  location: PropTypes.shape()
};

const mapDispatchToProps = dispatch => ({
  mapsActions: bindActionCreators(mapsActions, dispatch),
  areasActions: bindActionCreators(areasActions, dispatch),
  notificationActions: bindActionCreators(notificationActions, dispatch),
  poiActions: bindActionCreators(poiActions, dispatch),
  pathActions: bindActionCreators(pathActions, dispatch)
});

const mapStateToProps = state => {
  return ({
    maps: state.maps,
    areas: state.areas,
    poi: state.poi,
    path: state.path
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Maps);
