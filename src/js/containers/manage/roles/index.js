import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * Roles View Page
 *
 * @author Balu Loganathan
 */

class Roles extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>Roles</h1>
      </div>
    );
  }
}

Roles.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

Roles.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.roles;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Roles));
