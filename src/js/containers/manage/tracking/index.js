import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * Tracking View Page
 *
 * @author Balu Loganathan
 */

class Tracking extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>Tracking</h1>
      </div>
    );
  }
}

Tracking.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

Tracking.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.tracking;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Tracking));
