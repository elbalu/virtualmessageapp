import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * Users View Page
 *
 * @author Balu Loganathan
 */

class Users extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>Users</h1>
      </div>
    );
  }
}

Users.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

Users.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.users;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Users));
