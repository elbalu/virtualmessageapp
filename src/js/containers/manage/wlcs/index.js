import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import _ from 'lodash';
import req from '../../../misc/agent';
import url from '../../../misc/url';
import validate from 'validate.js';
import { Modal } from 'antd';

import EntityModal from '../../../components/entity-modal';
import TokenModal from '../../../components/token-modal';
import TemplateModal from '../../../components/template-modal';
import Table from '../../../components/table';
import WidgetBar from '../../../components/widget-bar';
import EntityWidget from '../../../components/entity-widget';

import { setRoute } from '../../../misc/url-persist-utils';
import { hasAccess } from '../../../misc/user-utils';
import { copyToClipboard } from '../../../misc/helpers';
import WlcDetails from '../../wlcs/wlcDetails';

import * as wlcActions from '../../../redux/actions/wlc-actions';
import * as connectorActions from '../../../redux/actions/connector-actions';
import * as modalActions from '../../../redux/actions/modal-actions';
import * as notificationActions from '../../../redux/actions/notification-actions';
import * as templatesActions from '../../../redux/actions/templates-actions';
import * as tableActions from '../../../redux/actions/table-actions';

import * as template from '../../../../resources/schemas/wlcs.json';
import * as templateWlc from '../../../../resources/schemas/wlc.json';

const initialState = {
    tokenModal: {
        visible: false,
        cmx: null
    },
    entityModal: {
        visible: false,
        entity: {},
    }
};

/**
 * TODO
 */
class Wlcs extends Component {
    /**
     * @override
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = initialState;
        this.toggleEntityModalVisibility = this.toggleEntityModalVisibility.bind(this);
        this.toggleTokenModalVisibility = this.toggleTokenModalVisibility.bind(this);
    }

    /**
     * TODO
     */
    componentWillMount() {
      const { getAllWlcs, getFilteredList } = this.props.actions;
      const { getAllConnectors } = this.props.connectorActions;
      const { filterSelect, clearParams } = this.props.tableActions;
      const { getTemplate } = this.props.templatesActions;

      // clear any existing params
      clearParams();
      //getTemplate('WLCS');

      getAllWlcs();
      getAllConnectors();
      getFilteredList();
    }

    /**
     * handler for controller details click
     * opens wlcs detail view
     */
  viewWlcsDetails(id) {
    const { openDeviceDetailsModal } = this.props.modalActions;
    openDeviceDetailsModal('wlcs', id);
  }

    /**
     * @name getWidgetActions
     *
     * @description Gets any custom actions
     * Generates an icon
     * that opens the token modal on click
     *
     * @return {function(*=): XML}
     */
    getWidgetActions() {
        const hasTokenAccess = true;
        if (!hasTokenAccess) return [];
        const token = cmx => (
          <i
            key="get-token-action"
            className="fa fa-key"
            title="get wlc token"
            label="Token"
            attachClass="wlc-token"
            onClick={() => this.toggleTokenModalVisibility(cmx)}
          >
          </i>
        );
        return [token];
    }
    /**
     * @name getActions
     *
     * @description Gets any custom actions
     * Generates an icon
     * that opens the token modal on click
     *
     * @return {function(*=): XML}
     */
    getActions() {
        const hasTokenAccess = true;
        if (!hasTokenAccess) return [];
        const token = cmx => (
            (<i
                key="get-token-action"
                className="clickable fa fa-key"
                title="get wlc token"
                onClick={() => this.toggleTokenModalVisibility(cmx)}
            />)
        );
        return [token];
    }

    /**
     * @name getToken
     *
     * @description Performs a request to
     * authenticate the user. Returns the
     * request Promise.
     *
     * @param {string} username
     *
     * @param {string} password
     *
     * @param {number} cmxId - Id of the cmx instance
     * to get the token for
     */
    getToken(username, password, cmxId) {
        return new Promise((resolve, reject) => {
            req.post(url.tokenAuth + (cmxId ? '/controller/' + cmxId : '/controller'))
            // set cmx id and username/password
                .send({ username, password })
                // get response
                .then((response) => {
                    const token = _.get(response, 'body.token');
                    resolve(token);
                })
                .catch(reject);
        });
    }
    /**
     * @name toggleTokenModalVisibility
     *
     * @description Toggle the visibility of
     * the token modal
     *
     * @param {Object} cmx - Clicked cmx object
     */
    toggleTokenModalVisibility(cmx) {
        const visible = !this.state.tokenModal.visible;
        const nextCMX = visible ? cmx : null;
        this.setState({ tokenModal: { visible, cmx: nextCMX } });
    }

    /**
     * TODO
     * @param nextEntity
     */
    toggleEntityModalVisibility(nextEntity) {
        const { entityModal } = this.state;
        const visible = !entityModal.visible;
        const entity = (validate.isObject(nextEntity)) ? nextEntity : entityModal.entity;

        this.setState({ entityModal: { entity, visible } });
    }

    /**
     * @override
     * @returns {XML}
     */
    render() {
        const { wlcs, collections, filteredWlcs, intl } = this.props;
        const { removeWlc, addWlc, editWlc, getFilteredList } = this.props.actions;
        const { openFieldsModal, openDetailsModal, closeModal } = this.props.modalActions;
        const { deviceDetailsModal } = this.props.modal;
        const { display } = this.props.notificationActions;
        const { entityModal } = this.state;
        const { createField, updateField, removeField } = this.props.templatesActions;
        const { filterSelect, setParams, addFields } = this.props.tableActions;
        const { filters, params } = this.props.table;

        const hasEditAccess = hasAccess('wlc', 'update');
        const hasDeleteAccess = hasAccess('wlc', 'delete');
        const wlcsTitle = intl.formatMessage({ id: 'wlc.title' });
        const entityType = "wlc";
        const itemKey = "id";

        return (
            <div>
              <WidgetBar>
                <EntityWidget
                    title={wlcsTitle}
                    icon="fa fa-wifi"
                    add={() => this.toggleEntityModalVisibility({})}
                    entities={wlcs}
                    entityType={entityType}
                    showAccessToken={true}
                    customActions={this.getWidgetActions()}
                />
             </WidgetBar>
            <Table
                dataSource={wlcs}
                collections={collections}
                count={0}
                template={template}
                rowKey={itemKey}
                edit={(hasEditAccess) ? this.toggleEntityModalVisibility : null}
                remove={(hasDeleteAccess) ? removeWlc : null}
                hasActions={(hasEditAccess || hasDeleteAccess)}
                details={id => this.viewWlcsDetails(id)}
                hasEditAccess={hasEditAccess}
                entityType={entityType}
                fieldActions={{
                    add: createField,
                    save: updateField,
                    remove: removeField
                }}
                filterSelect={filterSelect}
                setParams={setParams}
                params={params}
                addFields={addFields}
                filters={filters}
                fetchData={getFilteredList}
                actions={this.getActions()}
            />

            <EntityModal
              entity={entityModal.entity}
              visible={entityModal.visible}
              title={wlcsTitle}
              template={templateWlc}
              displayNotification={display}
              actions={{
                add: addWlc,
                edit: editWlc,
                remove: removeWlc,
                hide: () => this.toggleEntityModalVisibility(),
              }}
            />
            <WlcDetails
                visible={deviceDetailsModal.visible}
                modalActions={{ hide: closeModal }}
                page="WLCS"
                connector={deviceDetailsModal.data}
                date={deviceDetailsModal.date}
            />
            <TokenModal
                title="Wireless Controller Token"
                info="Please authenticate to view wireless controller token"
                cmx={this.state.tokenModal.cmx}
                getToken={this.getToken}
                hide={this.toggleTokenModalVisibility}
                visible={this.state.tokenModal.visible}
            />
            {/*<TemplateModal template={template} />*/}
          </div>
        );
    }

}

Wlcs.propTypes = {
    // DEFINED BY REDUX

    // wlc state
    template: PropTypes.shape(),
    wlcs: PropTypes.instanceOf(Array),
    location: PropTypes.shape({ pathname: PropTypes.string }),

    // wlc actions
    actions: PropTypes.shape({
        getAllWlcs: PropTypes.func,
        addWlc: PropTypes.func,
        editWlc: PropTypes.func,
        removeWlc: PropTypes.func,
    }),

    // modal state
    modal: PropTypes.shape({
      detailsModal: PropTypes.object.isRequired,
    }),
// modal actions
    modalActions: PropTypes.shape({
      openFieldsModal: PropTypes.func,
      openDetailsModal: PropTypes.func.isRequired,
      closeModal: PropTypes.func.isRequired,
    }),

    notificationActions: PropTypes.shape({
        display: PropTypes.func,
    }),

    // field actions
    templatesActions: PropTypes.shape({
        createField: PropTypes.func,
        updateField: PropTypes.func,
        removeField: PropTypes.func,
        getTemplate: PropTypes.func,
    }),

    // i18n support
    intl: intlShape.isRequired,
};

const mapStatetoProps = state => {
  return ({
    ...state.wlcs,
    modal: state.modal,
    table: state.table,
    collections: {
      CONTROLLERS: state.wlcs.wlcs,
      CONNECTORS: state.connectors.connectors
    }
  });
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(wlcActions, dispatch),
    connectorActions: bindActionCreators(connectorActions, dispatch),
    modalActions: bindActionCreators(modalActions, dispatch),
    notificationActions: bindActionCreators(notificationActions, dispatch),
    templatesActions: bindActionCreators(templatesActions, dispatch),
    tableActions: bindActionCreators(tableActions, dispatch),
});

export default connect(mapStatetoProps, mapDispatchToProps)(injectIntl(Wlcs));
