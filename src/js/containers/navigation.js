import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hashHistory } from 'react-router';

import NavBar from '../components/navigation/nav-bar';
import Header from '../components/header/index';

import { isEmpty, prepareDataForNavigator } from '../misc/helpers';
import { getNavigation } from '../misc/nav-utils';
import { getUser, setAuthHeader } from '../misc/user-utils';
import { fetchInitialData } from '../redux/store';

import * as loginActions from '../redux/actions/login-actions';
import * as navigationActions from '../redux/actions/navigation-actions';

/**
 * Navigation is a React Component that manages
 * a navigation sidebar and page content. This component
 * generates a list of navigation links based on the currently
 * logged in user and wraps and displays the content
 * of the page.
 *
 * @class
 */
class Navigation extends Component {
  /**
   * Navigation#componentWillMount sets default
   * request headers and generates a navigation
   * list based on the currently logged in user.
   * If there is no user logged in, the browser
   * is redirected to the homepage (which will
   * display login)
   *
   * @override
   */
  componentWillMount() {
    const { setNavigation, currentUser, setUser } = this.props;

    // set request auth header defaults
    setAuthHeader();

    fetchInitialData();

    // set currentUser
    if (isEmpty(currentUser)) {
      const user = getUser();

      if (user) {
        setUser(user);
        // console.log("navigation > compWM ", user);
        // sets navigation on page refresh
        const navigation = prepareDataForNavigator(getNavigation(), user.resources);
        setNavigation(navigation);
      } else {
        hashHistory.push('/');
      }
    }
  }

  /**
   * Navigation#render returns the navigation
   * sidebar component.
   *
   * @override
   * @return {XML}
   */
  render() {
    const { children, location } = this.props;
    const { pathname } = location;

    return (
      <div className="app-wrapper">

        <NavBar currentPath={pathname} />

        <div className="app-body">
          <Header />
          <div className="app-content">
            { children }
          </div>
        </div>


      </div>
    );
  }
}

Navigation.propTypes = {
  children: PropTypes.shape(),
  location: PropTypes.shape(),
  currentUser: PropTypes.shape(),
  setNavigation: PropTypes.func,
  setUser: PropTypes.func
};

const mapStateToProps = state => state.login;

// dispatch should not be exposed to components
const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    ...loginActions,
    ...navigationActions,
  }, dispatch),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
