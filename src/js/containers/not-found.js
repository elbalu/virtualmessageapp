import React from 'react';
import { FormattedMessage } from 'react-intl';

const NotFound = () => <div><FormattedMessage id="notFound" /></div>;

export default NotFound;
