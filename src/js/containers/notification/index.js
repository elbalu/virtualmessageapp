import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * Notification View Page
 *
 * @author Balu Loganathan
 */

class Notification extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>Notification</h1>
      </div>
    );
  }
}

Notification.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

Notification.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.notification;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Notification));
