import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';


const initialState = {};

/**
 * Troubleshooting View Page
 *
 * @author Balu Loganathan
 */

class Troubleshooting extends Component {
  /**
   * @override
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * @override
   * @returns {XML}
   */
  render() {
    return (
      <div>
        <h1>Trouble Shooting</h1>
      </div>
    );
  }
}

Troubleshooting.contextTypes = {
  intl: React.PropTypes.object.isRequired,
};

Troubleshooting.propTypes = {};

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => state.troubleshooting;

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Troubleshooting));
