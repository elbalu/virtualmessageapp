import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';

import Modal from 'antd/lib/modal';
import WlcDetails from './wlc';

const initialState = {
  modalKey: 1
};

/**
 * Controller details
 * This class creates a modal popup to show the wlc details
 *
 * @author Deepika Kommuru, Benjamin Newcomer
 */
class WlcDetailsModal extends Component {

  constructor(props) {
    super(props);
    this.state = initialState;
  }

  /**
   * handler to hide
   * the modal
   */
  hideModal() {
    const { hide } = this.props.modalActions;
    hide();
  }

  /**
   * on modal close increment the
   * key counter so that the modal gets
   * re rendrered everytime it opens
   */
  modalClose() {
    this.setState({ modalKey: (this.state.modalKey + 1) });
  }

  renderWlcDetails() {
    const { deviceDetailsModal, wlcDetails, page } = this.props;
    const { date } = deviceDetailsModal;
    const { wlc } = wlcDetails;

    return <WlcDetails wlc={wlc} page={page} date={date} />;
  }

  render() {
    const { visible, wlcDetails } = this.props;
    const { wlc } = wlcDetails;
    if (!wlc) return null;


    return (
      <Modal
        title={wlc.name || wlc.serial}
        key={this.state.modalKey}
        className="wlc-details"
        visible={visible}
        width={1075}
        height={1000}
        onCancel={() => this.hideModal()}
        footer={false}
        afterClose={() => this.modalClose()}
      >
        { this.renderWlcDetails() }

      </Modal>
    );
  }
}

WlcDetailsModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  page: PropTypes.string,
  wlcDetails: PropTypes.shape({
    wlc: PropTypes.shape({}),
  }),
  detailsModal: PropTypes.shape({}),
  actions: PropTypes.shape({
    mapActions: PropTypes.shape({})
  }),
  modalActions: PropTypes.shape({
    hide: PropTypes.func.isRequired,
  }),

};

const mapStateToProps = state => ({
  ...state.modal,
  ...state.wlcs,
});

export default connect(mapStateToProps, null)(injectIntl(WlcDetailsModal));
