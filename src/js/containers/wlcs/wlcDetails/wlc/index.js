import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';

import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Tabs from 'antd/lib/tabs';


import * as mapsActions from '../../../../redux/actions/maps-actions';

const Tab = Tabs.TabPane;

/**
 * Controller details
 *
 * @author Deepika Kommuru
 */
class WlcDetails extends Component {

  render() {
    const { wlc } = this.props;
    if (!wlc) return null;

    return (
      <Tabs defaultActiveKey={'overview'}>
        <Tab tab="Overview" key="overview">
          <Row className="wlc-detail-content" type="flex" align="top">
            <Col span={12}>
              Details
            </Col>
          </Row>
        </Tab>

      </Tabs>
    );
  }
}

WlcDetails.propTypes = {
  wlc: PropTypes.shape({}),
  telemetry: PropTypes.shape({}),
  templates: PropTypes.shape({}),
  actions: PropTypes.shape({}),
};

const mapDispatchToProps = dispatch => ({
  actions: {
    ...bindActionCreators(mapsActions, dispatch),
  },
});

const mapStateToProps = state => ({
  ...state.mapsReducer,
  wlcs: state.wlcs.wlcs,
  templates: {
    allFields: state.templates.wlcDetail,
    wlcs: state.templates.wlcs,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(WlcDetails));
