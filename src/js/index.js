import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import moment from 'moment';

import config from './config';
import '../less/main.less';

import Dashboard from './containers/dashboard';
import Troubleshooting from './containers/troubleshooting';

import Devices from './containers/manage/devices';
import Ingestor from './containers/manage/wlcs';
import Connectors from './containers/manage/connectors';
import Maps from './containers/manage/maps2';
import Tracking from './containers/manage/tracking';
import Filtering from './containers/manage/filtering';
import Users from './containers/manage/users';
import Roles from './containers/manage/roles';
import NotFound from './containers/not-found';
import Validate from './containers/login/validate';

import Notification from './containers/notification';
import License from './containers/license';
import Navigation from './containers/navigation';

import { initLocaleData } from './misc/i18n-utils';
import UserSnapWidget from './misc/usersnap-widget';
import store from './redux/store';

// initialize locale data for react-intl
const currentLocale = process.env.LOCALE;
initLocaleData(currentLocale);
const messages = config.get('messages');

// configure moment to use utc
moment().utc();

// initialize usersnap widget in certain
// environments because the widget requires
// the UI to be served at a public URL (or
// with special access for usersnap servers)
const env = config.get('env');
const feedbackEnvironments = config.get('feedbackEnvironments');
if (feedbackEnvironments.includes(env)) global.window.onload = UserSnapWidget.init;

/**
 * Application root test
 */
render(
  <Provider store={store}>
    <IntlProvider locale={currentLocale} messages={messages}>
      <LocaleProvider locale={enUS}>
        <Router history={hashHistory}>
          <Route path="/" component={Validate} />
          <Route component={Navigation}>
            <Route path="dashboard" component={Dashboard} />
            <Route path="troubleshooting" component={Troubleshooting} />
            <Route path="manage/ingestor" component={Ingestor} />
            <Route path="manage/connectors" component={Connectors} />
            <Route path="manage/maps" component={Maps} />
            <Route path="manage/cutoff" component={Filtering} />
            <Route path="manage/tracking" component={Tracking} />
            <Route path="manage/filtering" component={Filtering} />
            <Route path="manage/ciscocloudapps" component={Devices} />
            <Route path="manage/prime" component={Devices} />
            <Route path="manage/dnacenter" component={Devices} />
            <Route path="manage/noncisco" component={Devices} />
            <Route path="manage/users" component={Users} />
            <Route path="manage/roles" component={Roles} />
            <Route path="notification" component={Notification} />
            <Route path="license" component={License} />
            <Route path="*" component={NotFound} />
          </Route>
        </Router>
      </LocaleProvider>
    </IntlProvider>
  </Provider>,
  global.document.getElementById('loc-app')
);
