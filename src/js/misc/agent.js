import defaults from 'superagent-defaults';

// Create a defaults context
const superagent = defaults();

export default superagent;
