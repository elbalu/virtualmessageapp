export function calculateEnclosingPolygon(points) {
  // Given an unsorted set of points, find out a polygon
  // that encloses them without any reflex angle.
  // Once found the boundary vertices, instead of just
  // connecting them, we'll apply an external
  //  offset, with arcs on the vertices for better aesthetics

  const pv = {
    pointsPerCircle: 32, // Number of points for drawing the arcs
    offsetPercentage: 5, // Offset as a percentage of the diagonal of the enclosing box
    minOffset: 0.001 // Minimum offset: 0.1 mile aprox.
  };

  if (!points || points.length <= 1) {
    // No points: this should not happen
    console.log('The points array was not provided or has less than two points, cannot calculate an enclosing polygon!');
    return null;
  }

  if (points.length === 1) {
    // Special case: return a circle
    get;
  }

  // Get the enclosing box
  function getEnclosingBox(points) {
    const box = {
      min: {
        lon: 999,
        lat: 999
      },
      max: {
        lon: -999,
        lat: -999
      }
    };
    for (const i in points) {
      const p = points[i];
      if (p.lon < box.min.lon) box.min.lon = p.lon;
      if (p.lon > box.max.lon) box.max.lon = p.lon;
      if (p.lat < box.min.lat) box.min.lat = p.lat;
      if (p.lat > box.max.lat) box.max.lat = p.lat;
    }
    return box;
  }

  const strictIndexes = getStrictEnclosingPolygon(points);

  if (!strictIndexes) {
    return null;
  }
  // Testing: check result

  for (let i = 0; i < strictIndexes.length; i++) {
    const p = points[strictIndexes[i]];
  }

  // We now have a closed counterclockwise polygon: create the offset
  const box = getEnclosingBox(points);
  let offset = pv.offsetPercentage / 100 * getDistance(box.min, box.max);
  if (offset < pv.minOffset) {
      offset = pv.minOffset;
  }

  const op = getOffset(points, strictIndexes, offset);

  return op;

  // Get the rightmost point
  function getRightmostPointIndex(points) {
    const best = {
      lon: -999,
      index: -1
    };
    for (const i in points) {
      if (points[i].lon > best.lon) {
        best.lon = points[i].lon;
        best.index = i;
      }
    }
    return best.index;
  }

  // Get the array of points that make the strict enclosing polygon
  function getStrictEnclosingPolygon(points) {
    // We start with the rightmost point
    const resultIndexes = [];
    resultIndexes[0] = getRightmostPointIndex(points);
    let closed = false;
    while (!closed) {
      const best = {
        index: -1,
        angle: Math.PI * 2
      };
      // Shorten current point
      const cp = points[resultIndexes[resultIndexes.length - 1]];
      // Previous point
      let pp;

      if (resultIndexes.length == 1) {
        // First point: previous is vertical under it
        pp = {
            lon: cp.lon,
            lat: cp.lat - 1
        };
      } else {
        // General case
        pp = points[resultIndexes[resultIndexes.length - 2]];
      }
      for (const i in points) {
        // For each point, get the one with the smallest angle
        if (i != resultIndexes[resultIndexes.length - 1]) {
          const angle = getAngle3Points(pp, cp, points[i]);
          // We look for the smallest angle
          if (angle < best.angle) {
            // Best point until now
            best.angle = angle;
            best.index = i;
          }
        }
      }

      if (best.index == -1) {
        return null;
        throw new Error('No best point found: this should not happen!');
      } else {
        resultIndexes.push(best.index);
      }
      // Check if we have closed the polygon
      if (best.index == resultIndexes[0]) {
        closed = true;
      }
    }
    return resultIndexes;
  }

  // Get the offset around the strict enclosing polygon
  function getOffset(points, resultIndexes, offset) {
    // Set the array of strict enclosing polygon
    const sps = [];
    for (var i in resultIndexes) {
      sps.push(points[resultIndexes[i]]);
    }
    const offsetPolygon = [];
    for (var i = 0; i < sps.length - 1; i++) {
      // For each point (but the last one), create:
      //  - The arc from the previous segment
      //  - The offset of the segment to the next point
      addArcAndSegment(offsetPolygon, sps, i, offset);
    }
    return offsetPolygon;
  }

  // Get the offset around the strict enclosing polygon
  function addArcAndSegment(offsetPolygon, sps, i, offset) {
      // Last point of the previous segment
    let li = i - 1;
    if (i == 0) {
      // First point: get the previous to the last one
      li = sps.length - 2;
    }
    // Get the end point of the previous offset segment
    const lop = getOffsetPoint(sps[li], sps[i], sps[i], offset);
    // Only in the first point, store that point
    if (i == 0) {
      offsetPolygon.push(lop);
    }
    // Get the start point of the current offset segment i -> i+1
    const cop = getOffsetPoint(sps[i], sps[i + 1], sps[i], offset);
    addArc(offsetPolygon, lop, cop, sps[i], offset);
    // Add the current segment's end point
    offsetPolygon.push(getOffsetPoint(sps[i], sps[i + 1], sps[i + 1], offset));
  }

  // Calculate an offset point at the right of p, given a vector p0->p1 and the distance offset
  function getOffsetPoint(p0, p1, p, offset) {
    const module = getDistance(p0, p1);
    // Calculate the point: normal to p0->p1 at offset distance
    const out = {
      lat: p.lat - (p1.lon - p0.lon) / module * offset,
      lon: p.lon + (p1.lat - p0.lat) / module * offset
    };
    return out;
}

// Add an arc from lop to cop and center p
  function addArc(offsetPolygon, lop, cop, p, offset) {
    let startAngle = getAngle2Points(p, lop);
    const endAngle = getAngle2Points(p, cop);
    if (startAngle > endAngle) {
      startAngle -= Math.PI * 2;
    }
    const steps = 1 + Math.floor(pv.pointsPerCircle / (2 * Math.PI) * (endAngle - startAngle));
    const delta = (endAngle - startAngle) / steps;
    for (let angle = startAngle + delta; angle < endAngle - delta / 2; angle += delta) {
      const op = {
        lon: p.lon + Math.cos(angle) * offset,
        lat: p.lat + Math.sin(angle) * offset
      };
      offsetPolygon.push(op);
    }
    };

  // Calculate the angle between p0 -> p1 and 0,0 -> 0,10
  function getAngle2Points(p0, p1) {
    const module = getDistance(p0, p1);
    const cos = (p1.lon - p0.lon) / module;
    const sin = (p1.lat - p0.lat) / module;
    let angle = 0;
    if (Math.abs(cos) > Math.abs(sin)) {
      angle = Math.acos(cos);
      if (sin < 0) {
          angle = -angle;
      }
    } else {
      angle = Math.asin(sin);
      if (cos < 0) {
          angle = Math.PI - angle;
      }
    }
    // Make it always positive
    if (angle < 0) {
      angle += 2 * Math.PI;
    }
    return angle;
  };


  // Calculate the angle between p0 -> p1 and p1 -> p2
  function getAngle3Points(p0, p1, p2) {
    const a0 = getAngle2Points(p0, p1);
    const a1 = getAngle2Points(p1, p2);
    let angle = a1 - a0;
    // Make it always positive
    if (angle < 0) {
      angle += 2 * Math.PI;
    }
    return angle;
  };

  // Calculate the distance between 2 points
  function getDistance(p0, p1) {
    return Math.sqrt((p1.lon - p0.lon) * (p1.lon - p0.lon) + (p1.lat - p0.lat) * (p1.lat - p0.lat));
  }
}
