export const mapCollectionentryIdToName = (collection, dataList, propName) => {
  return dataList.map(d => collection.find(c => c[propName] === d[propName]).name);
};