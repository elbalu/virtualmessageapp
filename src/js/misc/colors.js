export const ciscoBlue = '#049FD9';
export const ciscoBlack = '#222';
export const ciscoBlue1 = '#77cdee';

export const gray = 'gray';
export const darkGray = '#333238';
export const darkGray1 = '#57595B';
export const darkGray2 = '#d0cdcd';
export const darkGray3 = '#6f6b6b';
export const lightGray = '#f5f7fa';
export const lightGray2 = '#b8c2cb';
export const lightGray3 = '#b7b7b7';
export const lightGray4 = '#848484';

export const white = '#FFFFFF';
export const select = '#4ec0f1';
export const danger = '#CF1F2F';

export const green = '#80c362';
export const greenYellow = '#d7e569';
export const yellow = '#F3F754';
export const orange = '#f6a735';
export const darkOrange = '#eb8e30';
export const purple = '#4f23f1';
export const darkGreen = '#eb8e30';
export const red = '#fe5549';
export const darkBlue = '#041093';
export const gold = 'gold';
export const mapBackground = '#c9e4e9';
export const background = '#f4f6f9';
export const highlighterRed = '#d87671';

export const alert = orange;
export const warning = red;
export const normal = green;
export const defaultColor = '#e1e5e9';

/**
 * @name zoneColors
 *
 * @description The list of colors that can
 * be assigned to a Zone Location.
 *
 * @type {string[]}
 */
export const zoneColors = [
  '#0088fe',
  '#00c49f',
  '#29878b',
  '#30497b',
  '#5bbaeb',
  '#6660aa',
  '#7f8c8d',
  '#8b756e',
  '#94be49',
  '#9999c8',
  '#9b59b6',
  '#bdbee7',
  '#c0392b',
  '#e74c3c',
  '#f1c40f',
  '#f6942b',
  '#f7ed60',
  '#fcb564',
  '#ff8042',
  '#ffbb28',
  lightGray3
];


/**
 * @name pieChartColors
 *
 * @description The list of colors used
 * when creating pie charts.
 *
 * @type {string[]}
 */
export const pieChartColors = [
  '#5bbaeb',
  '#008acf',
  '#b0cf78',
  '#94be49',
  '#fcb564',
  '#f6942b',
  '#bdbee7',
  '#9999c8',
  '#6660aa',
  '#4eaeb8',
  '#30497b'
];

/**
 * @name categoryColors
 *
 * @description The list of colors used
 * when instantiating the category selector
 * component.
 *
 * @type {string[]}
 */
export const categoryColors = [
  '#0088FE',
  '#00C49F',
  '#FFBB28',
  '#FF8042',
  '#c0392b',
  '#9b59b6',
  '#34495e',
  '#e74c3c',
  '#f1c40f',
  '#7f8c8d',
  '#e74c3c'
];

/**
 * @name areaChartColors
 *
 * @description The list of colors
 * used in telemetry charts.
 *
 * @type {string[]}
 */
export const areaChartColors = [
  '#0088FE',
  '#00C49F',
  '#FFBB28',
  '#FF8042'
];

/**
 * @name alertWidgetColors
 *
 * @description The list of colors
 * used in the Alert Widget feature.
 *
 * @type {string[]}
 */
export const alertWidgetColors = [
  '#8b756e',
  '#85cddd',
  '#f8bd80',
  '#29878b',
  '#f7ed60'
];
