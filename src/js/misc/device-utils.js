import { FormattedMessage } from 'react-intl';
import React from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import _ from 'lodash';

import Renderer from './renderers';
import Battery from './models/battery-model';

/**
 * This method generates one field with label and value
 *
 * @param key
 * @param label
 * @param value
 * @returns one Row
 */
export const generateField = (key, label, value) => (
  <Row className="attribute-row" data-id={key} key={key} type="flex" justify="start">
    <Col span={12} className="label">{label} </Col>
    <Col span={12} className="value">{value} </Col>
  </Row>
);


/**
 * This method generates fields for each entityName (ASSETS, CATEGORIES, etc)
 * For sdk subtype fields like cat,dep,tag split the actual key out from long key
 * (ex:category.name will be name from category dataset)
 *
 * @param {Object[]} schema
 * @param {Object} entity/data set
 * @param {string} entityName
 * @returns {Object[]} fields Array of Display Rows
 */
export const generateFields = (schema, entity, entityName) => {
  const fields = Object.assign([], schema);
  const detailsFields = [];

  const fieldsIsDefined = !_.isUndefined(fields);
  const fieldsIsArray = _.isArray(fields);
  if (!fieldsIsDefined || !fieldsIsArray) return null;


  // remove tags from general fields as tags are displayed seperately
  _.remove(fields, { key: 'tags' });

  fields.forEach((field) => {
    if (!field.hidden) {
      const key = (entityName) ? (field.key).split('.').pop() : field.key;

      if (key in entity) {
        const value = Renderer.render(field)(entity[key]);
        detailsFields.push(generateField(key, field.label, value));
      }
    }
  });

  return detailsFields;
};

/**
 * This method generates set of sdk general fields
 * containing categories,department
 * @param data
 * @param {Object} template
 * @returns rows of fields
 */
export const createGeneralFields = (data, template) => {
  if (!data || !template) return null;

  const deviceFields = generateFields(template.fields, data);

  return (
    <div className="general">
      <div key="sdk-general">
        <h5><i className="fa fa-info-circle" />&nbsp;{<FormattedMessage id="general" />}</h5>
        { deviceFields }
      </div>
    </div>
  );
};

/**
 * This method generates set of tag
 * telemetry fields
 *
 * @param {Object} tag data
 * @returns {Array} rows of fields
 */
export const generateTelemetryFields = (tag) => {
  const { data } = tag;
  let batteryIcon = null;
  let temperature = null;
  let humidity = null;

  if (!data) return null;

  // find tag.data array on tag.data where eventType == battery
  const batteryState = _.find(data, ['type', 'battery']);
  if (batteryState) {
    const percent = _.get(batteryState, 'data.percentRemaining');
    const days = _.get(batteryState, 'data.daysRemaining');
    batteryIcon = (new Battery(percent, days)).render();
  }
  const batteryField = (batteryState) ? generateField('tagBattery', 'Battery', batteryIcon) : null;

  const tempState = _.find(data, ['type', 'TEMPERATURE']);
  if (tempState) temperature = `${tempState.data.measurement} ${tempState.data.units}`;
  const tempField = (tempState) ? generateField('tagTemperature', 'Temperature', temperature) : null;

  const humidState = _.find(data, ['type', 'HUMIDITY']);
  if (humidState) humidity = `${humidState.data.measurement} ${humidState.data.units}`;
  const humidityField = (humidState) ? generateField('tagHumidity', 'Humidity', humidity) : null;


  return (
    <div>
      {batteryField}
      {tempField}
      {humidityField}
    </div>
  );
};
