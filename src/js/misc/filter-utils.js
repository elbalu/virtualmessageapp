import _ from 'lodash';

export function generateSelectorFields(data) {
  return (!_.isEmpty(data)) ?  data.map((c) => {return {key: c.id, label:c.detailedName || c.name, value: _.toString(c.id)};}) : [];
}

export function convertOptionsToSelectors(data) {
  return (!_.isEmpty(data)) ?  data.map((c) => {return {id: c, name:c};}) : [];
}


export function generateTreeValue(data) {
  return (!_.isEmpty(data)) ?  data.map((c) => {return {label:c.name, value: _.toString(c.id)};}) : [];
}

export function generateStructuredFields(data) {
  return (!_.isEmpty(data)) ?  data.map((c) => {return {id: c.value, name:c.label};}) : [];
}

export function getBasicFilter(template) {
  const basicFilter = _.filter(template, (item) => {
    return item.key === 'category' || item.key === 'department' || item.key === 'location' || item.key === 'staticLocation' ;
  });
  return basicFilter;
}

export function getAdvanceFilter(template, showEditableOnly) {
  const advanceFilter = _.filter(template, (item) => {
    return (!item.hidden && (item.key !== 'x' && item.key !== 'y') && (!showEditableOnly || item.editable));
  });
  return advanceFilter;
}

export function getSelectedData(fields) {
  const selectedData =  generateSelectorFields(_.get(fields[0],'values'));
  return selectedData;
}

export function generateFieldsFilter(template, type, values) {
  const typeTemp = _.find(template, ['key', type]);
  const data = {
    key: _.get(typeTemp, 'key'),
    type: 'ENUM',
    label: _.get(typeTemp,'label'),
    operator: 'selection',
    values,
  }
  return data;
}

export function generateFieldsFilterFor(label, key, values) {
  const data = {
    key,
    type: 'ENUM',
    label,
    operator: 'selection',
    values,
  }
  return data;
}

export function createStructuredFilter() {
  const fields = {
    fields: [{
      key: 'category',
      type: 'ENUM',
      label: 'Category',
      operator: 'selection',
      values: []
    }, {
      key: 'department',
      type: 'ENUM',
      label: 'Department',
      operator: 'selection',
      values: []

    }, {
      key: 'location',
      type: 'ENUM',
      label: 'Location',
      operator: 'selection',
      values: []
    }]
  }
}
