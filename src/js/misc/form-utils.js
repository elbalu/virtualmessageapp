import validate from 'validate.js';
import moment from 'moment';
import _ from 'lodash';

import Fields from '../components/form-generator/fields';
import TemplateRenderer from '../misc/renderers/template-renderer';

const FGTypeV1ToV2 = {
  TEXT: 'text',
  NUMBER: 'numeric',
  CHECKBOX: 'checkbox-group',
  SELECT: 'select',
  RADIO: 'radio-group',
  CASCADER: 'tree',
  DATE: 'date',
  TEL: 'text',
  PARAGRAPH: 'textarea',
  TREE_SELECT: 'tree',
  SWITCH: 'switch'
};

/**
 * convertFGTypeV1ToV2 accepts a Form Generator
 * field and returns the field in a format that
 * is compatible with Form Generator v2.
 *
 * @param field
 * @return {*}
 */
export const convertFGFieldV1ToV2 = (field )=> {
  const v2Field = _.clone(field);

  // convert field type
  const v1Type = _.get(field, 'type', 'TEXT');
  let v2Type = FGTypeV1ToV2[v1Type];

  // special case for checkbox with 2 options
  // (should be switch)
  if (v1Type === 'CHECKBOX' && _.size(field.options) === 2) {
    v2Type = FGTypeV1ToV2.SWITCH;
  }
  _.set(v2Field, 'type', v2Type);

  // convert field collection and new collection
  // fields (collectionKey/collectionLabel)
  const v1Collection = _.get(field, 'collection');
  if (!_.isEmpty(v1Collection)) {
    const v2Collection = _.lowerCase(v1Collection);
    const collectionKey = 'id';
    const collectionLabel = 'name';
    _.set(v2Field, 'collection', v2Collection);
    _.set(v2Field, 'collectionKey', collectionKey);
    _.set(v2Field, 'collectionLabel', collectionLabel);
  }

  const v1Options = _.get(field, 'options');
  const v2Options = v1Options || [];
  _.set(v2Field, 'options', v2Options);

  return v2Field;
};

/**
 * @name switchLabelToValue
 *
 * @description Converts a switch field
 * label (string) to value (boolean).
 *
 * @param {boolean} value
 *
 * @param {Field} field
 *
 * @return {boolean}
 */
export const switchLabelToValue = (value, field) => {
  const trueOption = _.get(field, 'options[0]', '');
  return (value === trueOption)
};

/**
 * @name switchValueToLabel
 *
 * @description Converts a switch field
 * value (boolean) to label (string)
 *
 * @param {string} value
 *
 * @param {Field} field
 *
 * @return {string}
 */
export const switchValueToLabel = (value, field) => {
  const trueOption = _.get(field, 'options[0]', '');
  const falseOption = _.get(field, 'options[1]', '');

  if (value) return trueOption;
  return falseOption;
};

/**
 * fieldLabelToKey accepts a label and a template
 * and returns the key for the field that has
 * the given label. Returns undefined if there
 * is no field with the given label.
 *
 * @param {string} label
 * @param {{ fields:Array }} template
 */
export const fieldLabelToKey = (label, template) => {
  const fields = _.get(template, 'fields', []);
  const field = _.find(fields, ['label', label]);
  return _.get(field, 'key');
};

/**
 * fieldKeyToLabel accepts a key and a template
 * and returns the label for the fields that
 * has the given key. Returns undefined if there
 * is no fields with the given key.
 *
 * @param {string} key
 * @param {{ fields:Array }} template
 */
export const fieldKeyToLabel = (key, template) => {
  const fields = _.get(template, 'fields', []);
  const field = _.find(fields, ['key', key]);
  return _.get(field, 'label');
};

/**
 * VALIDATE_STATUSES is a collection of constants
 * describing the different validation statuses
 * that a single form field can have
 */
export const VALIDATE_STATUSES = {
  SUCCESS: 'success',
  WARNING: 'warning',
  ERROR: 'error',
  VALIDATING: 'validating',
};

/**
 * generateTreeOptions() parses Asset Tracking MapView data into
 * a format that is compatible with the antd tree-select
 *
 * @param level Array
 */
export const generateTreeOptions = level => (
  level.map((l) => {
    const obj = { value: l.id, label: l.name, key: l.id };
    if (l.relationshipData.children &&
        l.relationshipData.children.length > 0 &&
        l.level.toLowerCase() !== 'floor') {
      obj.children = generateTreeOptions(l.relationshipData.children);
    }
    return obj;
  })
);

/**
 * Generate one select and radiogroup compatible
 * option from the given option
 * input. Option may be an object or a string value
 *
 * @param {Object|string} option A single field option
 * @returns {{ label: string, value: string }}
 */
export const generateOption = (option) => {
  if (!validate.isDefined(option)) return null;

  if (validate.isObject(option)) {
    return {
      label: String(option.detailedName || option.label || option.name || option.serial || option.id || `${option.firstName} ${option.lastName}`),
      value: String(option.value || option.id),
    };
  }

  return { label: String(option), value: String(option) };
};

/**
 * Generate options objects for tag selector
 *
 * @param {Object} tag A single tag object
 * @returns {{ label: string, value: string }}
 */
export const generateTagOption = (tag) => {
  if (typeof tag === 'undefined') return null;
  return {
    label: tag.serial,
    value: tag.id,
    disabled: (tag.sdkId > 0),
  };
};

/**
 * Generate options objects for icon selector
 *
 * @param {Object|string} option A single icon object
 * @returns {{ label: string, value: string, iconCls: string }}
 */
export const generateIconOption = (option) => {
  if (typeof option === 'undefined') return null;

  return {
    label: option.name,
    value: option.id,
    iconCls: `${option.icon} fa`,
  };
};

/**
 * sortTagsByAssigned takes two tags and returns
 * them by preferring tags that are unassigned. If
 * both are assigned or both are unassigned, order
 * is preserved.
 *
 * @param tag1
 * @param tag2
 * @returns {number}
 */
export const sortTagsByAssigned = (tag1, tag2) => {
  const isTag1Assigned = tag1.sdkId > 0;
  const isTag2Assigned = tag2.sdkId > 0;

  if (isTag1Assigned === isTag2Assigned) return 0;
  else if (isTag1Assigned) return 1;
  return -1;
};

/**
 * optionsFromCollection() converts a collection
 * of JSON entities into Ant compatible option
 * objects with format {{ value:string, label:string }}
 *
 * @param collection
 */
export const optionsFromCollection = collection => (
    collection.map(c => {
      return ({
        value: String(c.id),
        label: c.name || c.firstName,
      });
    })
);

/**
 * converts collection of tags into Ant.Design
 * compatible options objects. Filters out
 * assigned tags and sorts list
 *
 * @param {Object[]} tags
 * @returns {Object[]}
 */
export const optionsFromTagsCollection = tags => (
  tags
  .sort(sortTagsByAssigned)
  .map(t => generateTagOption(t))
);

/**
 * enable all options that are currently selected. this allows
 * the selected values to be deselected. This method is meant to
 * be used with options for <select/> elements.
 *
 * @param {{ value:string, label:string, disabled:boolean }[]} options
 * @param {String|String[]} selected
 * @return {{ value:string, label:string, disabled:boolean }[]}
 */
export const enableSelectedOptions = (options, selected) => {
  const selectedArray = (selected instanceof Array) ? selected : [selected];

  return options
  .map((option) => {
    /* eslint-disable eqeqeq */
    // we need a == here instead of === because we want
    // (("3" == 3) === true)
    const optionIsSelected = selectedArray.find(s => s == option.value);
    if (!optionIsSelected) return option;
    return { ...option, disabled: false };
  });
};

/**
 * removes disabled options. It is safe to use
 * enableSelectedOptions before using this to ensure
 * that selected options (if they are disabled) are
 * not removed from the options list.
 *
 * @param {{ value:string, label:string, disabled:boolean }[]} options
 * @return {{ value:string, label:string, disabled:boolean }[]}
 */
export const removeDisabledOptions = (options) => options.filter(option => !option.disabled);

/**
 * collectionEntryFromOption() takes in an id
 * and returns the corresponding entry from the
 * given collection.
 *
 * @param id
 * @param collection
 */
export const collectionEntryFromOption = (id, collection) => (
    collection.find(c => String(c.id) === id)
);

/**
 * validates a single value or array of values
 * against the field config passed in. Returns
 * an object describing the validation status
 * of the field.
 *
 * @param {Object} field Field config object
 * @param {*} value The current value of the field
 * @returns {Object} {
 *    key: string
 *    status: 'success'|'warning'|'error',
 *    [message: string]
 * }
 */
export const validateField = (field, value) => {
  const { required, key, label, disabled } = field;
  const validationType = field.validation;
  const valueIsZero = (field.validation === Fields.ValidationTypes.NUMBER && value === 0);

  // handle case where required field is left empty
  if (required && validate.isEmpty(value) && !valueIsZero && !disabled) {
    return {
      key,
      status: VALIDATE_STATUSES.ERROR,
      message: `${label} cannot be empty`,
    };
  }

  // handle case where field does not have a validation type set
  // or the value is empty (and not required)
  if (validate.isEmpty(validationType) || validate.isEmpty(value)) {
    return {
      key,
      status: VALIDATE_STATUSES.SUCCESS,
    };
  }

  // since we have confirmed that validationType
  // has a value, we can get the associated constraint
  const validator = Fields.ValidationConstraints[validationType];

  // handle case where value is an array
  if (validate.isArray(value)) {
    const result = value.map(v => validate.single(v, validator)).filter(v => v) || [];
    if (result.length > 0) {
      return {
        key,
        status: VALIDATE_STATUSES.ERROR,
        message: `${label} ${value} ${result.join(', ')}`,
      };
    }
  } else if (validationType === Fields.ValidationTypes.NUMBER) {
    // handle case where value is a number with max/min set
    const result = validate.single(+value, validator) || [];
    const hasMax = validate.isDefined(field.max);
    const hasMin = validate.isDefined(field.min);

    // if field.max was given, check if value is greater
    if (hasMax && value > field.max) {
      result.push(`number cannot be greater than ${field.max}`);
    }

    // if field.min was given, check if value is less than
    if (hasMin && value < field.min) {
      result.push(`number cannot be less than ${field.min}`);
    }

    // if a validation check failed,
    // return the error
    if (result.length > 0) {
      return {
        key,
        status: VALIDATE_STATUSES.ERROR,
        message: `${label} ${value} ${result.join(', ')}`,
      };
    }
  } else {
    // handle case where value is a single value
    const result = validate.single(value, validator) || [];
    if (result.length > 0) {
      return {
        key,
        status: VALIDATE_STATUSES.ERROR,
        message: `${label} ${value} ${result.join(', ')}`,
      };
    }
  }

  // default return success
  return {
    key,
    status: VALIDATE_STATUSES.SUCCESS,
  };
};

/**
 * validateEntity takes in an entity and a template and
 * validates the values found in the entity according
 * to the list of fields and validation types found in the template.
 * Returns an array of validationStatuses that represent errors.
 * If all values validate successfully then it will return an
 * empty array.
 *
 * @param entity
 * @param template
 * @returns {Array}
 */
export const validateEntity = (entity = {}, template = TemplateRenderer.blankTemplate()) => {
  const errors = [];

  // validate value for each field in template
  template.fields.forEach((field) => {
    // extract value from entity
    const value = _.get(entity, field.key);

    // validate single field
    const validationStatus = validateField(field, value);

    // keep validationStatuses that represent errors
    if (validationStatus.status === VALIDATE_STATUSES.ERROR) {
      errors.push(validationStatus);
    }
  });

  return errors;
};

(() => {
  // Before using it we must add the parse and format functions
  // Here is a sample implementation using moment.js
  validate.extend(validate.validators.datetime, {
    // The value is guaranteed not to be null or undefined but otherwise it
    // could be anything.
    parse: value => +moment.utc(value),

    // Input is a unix timestamp
    format: (value, options) => {
      const format = options.dateOnly ? 'YYYY-MM-DD' : 'YYYY-MM-DD hh:mm:ss';
      return moment.utc(value).format(format);
    },
  });
})();
