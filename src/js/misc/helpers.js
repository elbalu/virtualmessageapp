import _ from 'lodash';
import validate from 'validate.js';
import React from 'react';
import { message } from 'antd';
import update from 'immutability-helper';

/**
 * @name copyToClipboard
 *
 * @description Adds the given text to
 * an input element, selects the element,
 * and then copies the element value to
 * the clipboard.
 *
 * @param {string} text - Text to copy to
 * the clipboard
 */
export const copyToClipboard = (text) => {
  const document = global.document;
  const canCopy = document.queryCommandSupported('copy');

  if (canCopy) {
    // create input element
    const tokenInput = document.createElement('input');
    // set input value to text
    tokenInput.value = text;
    // append input to document body
    document.body.appendChild(tokenInput);
    // select the input dom node
    tokenInput.select();
    // copy the value of the selected node
    // to the clipboard
    document.execCommand('copy');
    // remove the input from the dom
    document.body.removeChild(tokenInput);
    // user feedback
    message.info('Token was copied to your clipboard');
  }
};

/**
 * explode converts a string that is in the format
 * of a comma separated list into an array of values.
 * Values are trimmed (whitespace before and after removed)
 * and blank values (empty strings) are removed.
 *
 * @param {string} str
 * @return {string[]}
 */
export const explode = str => _.compact(str.split(',').map(s => s.trim()));

/**
 * partially applies arguments to
 * the provided function and returns a new
 * function
 *
 * @example
 *  const add = (a, b) => a + b;
 *  const add1 = partial(add, 1);
 *  add1(2);
 *  >> 3
 *
 * @param outerArgs
 * @returns {result}
 */
export const partial = (...outerArgs) => {
  const fn = outerArgs.shift();
  return function result(...innerArgs) {
    return fn.apply(this, outerArgs.concat(innerArgs));
  };
};
/* inverse partial */
export const partialRight = (...outerArgs) => {
  const fn = outerArgs.shift();
  return function result(...innerArgs) {
    return fn.apply(this, innerArgs.concat(outerArgs));
  };
};

/**
 * maps the provided function to each
 * element of the provided iterable
 *
 * @param array
 * @param fn accepts (array[i], i, array)
 * @returns {Array}
 */
export const map = (array, fn) => {
  if (!validate.isArray(array)) return null;
  return array.map(fn);
};

/**
 * Reduce array into single value, optionally using initialValue
 * @returns any
 */
export const reduce = (array, fn, initialValue) => {
  if (!validate.isArray(array)) return null;
  return array.reduce(fn, initialValue);
};

/**
 * filters array, leaving only items,
 * that returned true being passed to provided function
 *
 * @param array
 * @param fn accepts (array[i], i, array)
 * @returns {Array}
 */
export const filter = (array, fn) => {
  if (!validate.isArray(array)) return null;
  return array.reduce(fn);
};

/**
 * finds first element in the provided array
 * where the provided function evaluates
 * to true.
 *
 * @param array
 * @param fn accepts (array[i], i, array)
 * @returns {*}
 */
export const find = (array, fn) => {
  if (!validate.isArray(array)) return null;
  return array.find(fn);
};

// convenience wrapper around find()
export const findById = (array, id) => (
  find(array, item => item.id === id)
);

/**
 * finds the index of the first element
 * in the provided array where the
 * provided function evaluates to true.
 *
 * @param array
 * @param fn accepts (array[i], i, array)
 * @returns {*}
 */
export const findIndex = (array, fn) => {
  if (!validate.isArray(array)) return null;
  return array.findIndex(fn);
};

/**
 * Capitalize first char of string
 *
 * @param str string to capitalize
 * @returns string capitalized
 */
export const capitalize = _.capitalize;

/**
 * Method to check if object has properties
 *
 * @returns true if object has no props
 */
export const isEmpty = validate.isEmpty;

/**
 * Method that formats input string into snake_case
 */
export const snakify = _.snakeCase;
export const snakeCase = _.snakeCase;

/**
 * TODO add comments
 * @param navData
 * @param resources
 */
export const prepareDataForNavigator = (navData, resources) => {
  const parseLevel = level =>
    level.map((l) => {
      const obj = l;
      if (!obj.parent && !obj.defaultShow) {
        obj.hidden = !resources || !(resources.includes(`${obj.type}:list`));
      }

      if (l.children && l.children.length > 0) {
        obj.children = parseLevel(l.children);
      }

      return obj;
    });

  return parseLevel(navData);
};

/**
 * traverses through a tree object recursively
 * and finds the node obj of the provided key/value
 *
 * @param items {array}
 * @param attrs {"key":"value"}
 * @returns {obj}
 */
export const findDeep = (items, attrs) => {
  function match(value) {
    for (const key in attrs) {
      if (!_.isUndefined(value)) {
        if (attrs[key] !== value[key]) {
          return false;
        }
      }
    };
    return true;
  }
  function traverse(value) {
    let result;
    _.forEach(value, (val) => {
      if (match(val)) {
        result = val;
        return false;
      }
      if (_.isObject(val) || _.isArray(val)) {
        result = traverse(val);
      }
      if (result) {
        return false;
      }
      return true;
    });
    return result;
  }
  return traverse(items);
};

/**
 * Returns true if the given pattern matches
 * any values in the given object
 *
 * @param {Object} obj
 * @param {string} pattern
 * @returns {boolean}
 */
export const objectValueMatchesPattern = (obj, pattern) => {
  return (
    _.values(obj).reduce(
      (acc, cur) => {
        if (validate.isFunction(cur.includes)) return cur.includes(pattern);
        return cur === pattern;
      },
    )
  );
};

/**
 * Sorts two values that have been
 * generated by a renderer. This means that
 * the values are either HTML or strings
 *
 * @param {XML|string} a
 * @param {XML|string} b
 * @return {number}
 */
export const compareRenderedValues = (a, b) => {
  const aIsReact = React.isValidElement(a);
  const bIsReact = React.isValidElement(b);

  // handle None values separately because
  // those are HTML not strings
  if (aIsReact || bIsReact) {
    if (aIsReact && bIsReact) return 0;
    if (aIsReact) return 1;
    return -1;
  }

  return a.localeCompare(b);
};

export const checkIfObject = _.isObject;

/**
 * convert from Fahrenheit to Celsius
 * @param {Number} f
 * @return {Number}
 */
export const toCelsius = f => (
  (5 / 9) * (f - 32)
);

/**
 * convert from Celsius to Fahrenheit
 * @param {Number} c
 * @return {Number}
 */
export const toFahrenheit = c => (
   (c * (9 / 5)) + 32
);

/**
 * convert from Seconds to Minutes
 * @param {Number} s
 * @return {Number}
 */
export const secondsToMiutes = s => (
  Math.floor(s / 60)
);

/**
 * convert from Seconds to Hours
 * @param {Number} s
 * @return {Number}
 */
export const secondsToHours = s => (
   Math.floor(s / 3600)
);

/**
 * convert from Seconds to Days
 * @param {Number} s
 * @return {Number}
 */
export const secondsToDays = s => (
   Math.floor(s / (24 * 3600))
);

/**
 * convert from Seconds to Weeks
 * @param {Number} s
 * @return {Number}
 */
export const secondsToWeeks = s => (
   Math.floor(s / (7 * 24 * 3600))
);

/**
 * convert from Minutes to Seconds
 * @param {Number} m
 * @return {Number}
 */
export const minutesToSeconds = m => (
   Math.floor(m * 60)
);

/**
 * convert from Hours to Seconds
 * @param {Number} h
 * @return {Number}
 */
export const hoursToSeconds = h => (
   Math.floor(h * 3600)
);

/**
 * convert from Days to Seconds
 * @param {Number} d
 * @return {Number}
 */
export const daysToSeconds = d => (
   Math.floor(d * 24 * 3600)
);

/**
 * convert from Weeks to Seconds
 * @param {Number} w
 * @return {Number}
 */
export const weeksToSeconds = w => (
   Math.floor((w * 7) * 24 * 3600)
);

/**
 * Round the number to two decimal points
 * @param {Number} num
 * @return {Number}
 */
export const roundToTwo = num => (
   Math.round(num * 100) / 100
);

/**
 * Convert given time to seconds
 * @param {Number} value
 * @param {String} units
 * @return {Number} seconds
 */
export const timeToSeconds = (val, units) => {
  switch (units) {
    case 'seconds' :
      return val;
    case 'minutes' :
      return minutesToSeconds(val);
    case 'hours' :
      return hoursToSeconds(val);
    case 'days' :
      return daysToSeconds(val);
    case 'weeks' :
      return weeksToSeconds(val);
    default:
      return val;
  }
};

/**
 * Convert given time in seconds to required units
 * @param {Number} value
 * @param {String} units
 * @return {Number} seconds
 */
export const timeToUnits = (val, units) => {
  switch (units) {
    case 'seconds' :
      return val;
    case 'minutes' :
      return secondsToMiutes(val);
    case 'hours' :
      return secondsToHours(val);
    case 'days' :
      return secondsToDays(val);
    case 'weeks' :
      return secondsToWeeks(val);
    default:
      return val;
  }
};

/**
 * Convert oject to query String
 * @param {ParamsObject} Object
 * @return {String} query string
 */
export const toQueryString = paramsObject =>
  Object
    .keys(paramsObject)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(paramsObject[key])}`)
    .join('&');

export const firstLetterUpper = string => string.charAt(0).toUpperCase() + string.slice(1);

/**
 * @name updateByPath
 *
 * @description Mirrors the behavior
 * of the immutability helper $set
 * command except this function accepts a
 * string path like lodash _.set rather than
 * the immutability-helper mongodb-style
 * object path. This function DOES NOT
 * mutate the target object.
 *
 * @param {Object} target - Object to update
 *
 * @param {string} path - Path at which to set
 * the new value.
 *
 * @param {*} value - New value
 *
 * @return {*} - Updated object
 */
export const updateByPath = (target, path, value) => {
  // const command = {};
  // _.set(command, `${path}.$set`, value);
  // return update(target, command);
};
