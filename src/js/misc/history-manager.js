import { hashHistory } from 'react-router';
import _ from 'lodash';
import update from 'immutability-helper';

const localStorageKey = 'sdk-history-manager-state';

/**
 * @name HistoryManager
 *
 * @description Wrapper around browser location
 * and react-router hashHistory. This class provides methods
 * for updating all parts of the browser location: path,
 * query, state (local storage).
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class HistoryManager {
  /**
   * @constructor
   *
   * @param {Object} location - Browser location
   *
   * @param {Object} localStorage - Browser Local Storage
   */
  constructor(location, localStorage = global.localStorage) {
    this.location = location;
    this.localStorage = localStorage;
  }

  /**
   * @name getPath
   *
   * @description Gets the browser path
   *
   * @return {string} - path
   */
  getPath() {
    return _.get(this.location, 'pathname');
  }

  /**
   * HistoryManager#getQuery returns an object
   * that contains all query parameters.
   *
   * @return {Object}
   */
  getQuery() {
    return _.get(this.location, 'query');
  }

  /**
   * HistoryManager#setQuery accepts an object
   * with query params and updates the current location
   * query params with those passed in.
   *
   * @param {Object} nextQuery
   */
  setQuery(nextQuery) {
    // update query object with new query params
    let query = this.getQuery();
    query = _.merge(query, nextQuery);

    // update location object with new query object
    const location = update(this.location, { query: { $set: query } });

    // push new history instance
    hashHistory.push(location);
  }

  /**
   * HistoryManager#getState returns the location state. Because
   * Asset Management uses hashHistory, the react-router state
   * API is not available. So this implementation uses localStorage.
   *
   * @return {Object}
   */
  getState() {
    // return local storage state or an empty object
    return JSON.parse(this.localStorage.getItem(localStorageKey)) || {};
  }

  /**
   * HistoryManager#setState updates the location state
   * with the nextState object passed in.
   *
   * @param {Object} nextState
   */
  setState(nextState) {
    // merge the old state with the new state
    let state = this.getState();
    state = _.merge(state, nextState);

    // save the new state in local storage
    this.localStorage.setItem(localStorageKey, JSON.stringify(state));
  }
}

export default HistoryManager;

