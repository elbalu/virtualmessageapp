import { addLocaleData } from 'react-intl';

export const initLocaleData = locale => addLocaleData([...require(`react-intl/locale-data/${locale}`)]);
