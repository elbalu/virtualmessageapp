import React from 'react';
import Modal from 'antd/lib/modal';
import config from '../config';

import req from './agent';

const messages = config.get('messages');

// TODO add documentation comments to ImportUtils
export const uploadMessages = {
  SUCCESS: {
    key: 'import-success',
    title: 'Assets Imported Successfully',
    content: 'success',
  },
  MAPSUCCESS: {
    key: 'map-success',
    title: 'MapView Imported Successfully',
    content: 'success',
  },
  MAPERROR: {
    key: 'map-error',
    title: 'Not able to import maps',
    content: 'error',
  },
};

export const errorNotification = error =>
Modal.error(Object.assign(error, { okText: messages.ok }));

export const successNotification = success =>
Modal.success(Object.assign(success, { okText: messages.ok }));

export const parseUploadResponse = (res) => {
  const UM = uploadMessages;

  if (!res) return;

  const { message, data } = res;

  switch (message) {
    case UM.MAPSUCCESS.key:
      successNotification({
        title: UM.MAPSUCCESS.title,
        content: UM.MAPSUCCESS.content,
      });
      break;
    case UM.MAPERROR.key:
      successNotification({
        title: UM.MAPERROR.title,
        content: UM.MAPERROR.content,
      });
      break;

    case UM.SUCCESS.key:
      successNotification({
        title: 'Asset(s) Imported Successfully',
        content: (
            <div>
              <p>{`${data.numAssetsCreated} Asset(s) created`}</p>
              <p>{`${data.numCategoriesCreated} Categories created`}</p>
              <p>{`${data.numDepartmentsCreated} Department(s) created`}</p>
              <p>{`${data.numTagsCreated} Tag(s) created`}</p>
              <p>{`${data.numAssetsUpdated} Asset(s) updated`}</p>
            </div>
        ),
      });
      break;

    default:
      break;
  }
};

/**
 * fireClick promgrammatically clicks on the
 * DOM node passed in
 *
 * @param node
 */
const fireClick = (node) => {
  if (document.createEvent) {
    const evt = document.createEvent('MouseEvents');
    evt.initEvent('click', true, false);
    node.dispatchEvent(evt);
  } else if (document.createEventObject) {
    node.fireEvent('onclick');
  } else if (typeof node.onclick === 'function') {
    node.onclick();
  }
};

/**
 * generateFile() sends a request for template file
 * with required headers (see misc/agent.js) and
 * triggers a download when the file is received
 */
export const generateFile = ({ url, name, contentType = 'text/csv' }) => {

  req
  .get(url)
  .end((err, res) => {
    if (err) {
      console.error(err);
      return;
    }

    const metaData = `data:${contentType};charset=utf-8`;
    const escapedData = encodeURIComponent(res.text);
    const csvData = `${metaData},${escapedData}`;

    const link = document.createElement('a');
    link.href = csvData;
    link.setAttribute('download', name);
    fireClick(link);
  });
};
