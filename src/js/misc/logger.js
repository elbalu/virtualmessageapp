/* eslint-disable no-console */

import Notification from 'antd/lib/notification';

import config from '../config';
import store from '../redux/store';

const env = process.env.NODE_ENV;

const notificationsAreEnabled = () => (
  store.getState().settings.notifications
);

/**
 * Logger is a logging utility that wraps
 * console. If notifications are enabled (
 * see notificationsAreEnabled above), Logger
 * will notify users of errors.
 *
 * @author Balu Epalapalli Loganathan <baepalap@cisco.com>
 */
class Logger {

  static log(title, message) {
    if (env !== 'production') {
      console.log(title, message);
    }

    if (notificationsAreEnabled()) {
      Notification.success({
        message: title,
        description: message,
      });
    }
  }

  static error(title, message) {
    if (env !== 'production') {
      console.error(title, message);
    }

    if (notificationsAreEnabled()) {
      Notification.error({
        message: title,
        description: message,
      });
    }
  }
}

export default Logger;
