import _ from 'lodash';

/**
 * [getQueryString - generate query url to fetch data]
 * @param  {Array} data
 * @param  {String} type
 * @param  {String} locationId
 * @return {String}
 */
export function getQueryString(data, type, locationId) {
  if (!data || data.length <= 0 || (locationId === 0 && locationId === '-1')) {
    return `/${type}?location=${locationId}`;
  }
  const childrenIds = _.map(data, b => b.id).join(',');
  if (type === 'locations' && locationId !== '-1') {
    return `/${type}?location=${childrenIds}`;
  }
  return `/${type}?location=${locationId}`;
}

export function getHeterarchyIdString(data) {
  return data.id;
}

/**
 * [getHeterarchyTitleString get a full title with parent name]
 * @param  {Object} heterarchyObj
 * @return {String} Full name with parent name
 */
export function getHeterarchyTitleString(heterarchyObj) {
  const { relationshipData, name } = heterarchyObj;
  const { ancestors } = relationshipData;

  if (ancestors && ancestors.length) {
    return `${ancestors[0]} > ${name}`;
  }
  return name;
}

/**
 * [getCampusPolygons get campus cords based on its
 * building cords to draw polygon]
 * @param  {Array} geoData
 * @return {Array} Array of cords
 */
export function getCampusPolygons(geoData) {
  const polyCords = _.map(geoData.relationshipData.children, (building) => {
    const { details } = building;
    const { latitude, longitude } = details;
    if (latitude) {
      return { lon: longitude, lat: latitude };
    }
    return false;
  });
  return _.compact(polyCords);
}

/**
 * [getbounds get bounds to fit the map within
 * the bounds]
 * @param  {Array} geoData
 * @return {Array}
 */
export function getbounds(geoData) {
  const points = _.filter(
      _.map(geoData, (campus) => {
        const { details } = campus;
        const { latitude, longitude } = details;
        return _.compact([longitude, latitude]);
      }), arr => arr.length > 0);
  return points;
}

/**
 * [getBuildingsOfAllCampus Get list of all
 * the buildings of all the campus]
 * @param  {Array} geoData
 * @return {Array}
 */
export function getBuildingsOfAllCampus(geoData) {
  const heterarchyArr = [];

  _.each(geoData, (campus) => {
    const { relationshipData } = campus;
    const { children } = relationshipData;
    _.each(children, (building) => {
      heterarchyArr.push(building);
    });
  });
  return _.filter(heterarchyArr, arr => typeof arr.details.latitude !== 'undefined');
}

/**
 * getCenterCords - Get center cords based on buildings
 * @param  {Array} geoData
 * @return {Array} [lng, lat]
 */
export function getCenterCords(geoData) {
  const center = _.map(getBuildingsOfAllCampus(geoData), (campus) => {
    const { details } = campus;
    const { latitude, longitude } = details;
    return _.compact([longitude, latitude]);
  });
  return center[0];
}

/**
 * [getLngLat] Given a heterarchy Object
 * return lng, lat array
 * @param  {Object} heterarchyObj
 * @return {Array}
 */

export function getLngLat(heterarchyObj) {
  if (!heterarchyObj) {
    return [];
  }
  const { details } = heterarchyObj;
  const { longitude, latitude } = details;

  if (!longitude) {
    return [];
  }
  return [longitude, latitude];
}

/**
 * [getLngLat] Given a heterarchy Object
 * return lng, lat array
 * @param  {Object} heterarchyObj
 * @return {Array}
 */

export function isCampusHaveBuildingAddress(campus) {
  if (!campus) {
    return false;
  }
  const { children } = campus.relationshipData;
  _.each(children, building => getLngLat(building).length > 0);
  return false;
}

/**
 * [buildingsMissingAddress] returns information object
 * about the number of buildings which miss address
 * @param  {Array} geoData
 * @return {Object}
 */
export function buildingsMissingAddress(geoData) {
  const buildingMissingInfo = {
    count: 0,
    missingCount: 0,
    missingBuildings: [],
    missingBuildingNames: [],
  };
  _.each(geoData, (campus) => {
    const { relationshipData } = campus;
    const { children } = relationshipData;
    _.each(children, (building) => {
      buildingMissingInfo.count += 1;
      if (!building.details.latitude) {
        buildingMissingInfo.missingCount += 1;
        buildingMissingInfo.missingBuildings.push(building);
        buildingMissingInfo.missingBuildingNames.push(building.name);
      }
    });
  });
  return buildingMissingInfo;
}

/**
 * [getHeterarchyIds return array of heterarchy ids]
 * @param  {ARRAY} geoData
 * @param  {Object} currentFeature
 * @return {Array}
 */
export function getHeterarchyIds(geoData, currentFeature) {
  const locationArray = [];

  _.each(geoData, (campus) => {
    if (campus.id === currentFeature.id) {
      locationArray.push(campus.id);
    }
    if (campus.relationshipData.children) {
      _.each(campus.relationshipData.children, (building) => {
        if (building.id === currentFeature.id) {
          locationArray.push(campus.id, building.id);
        }
      });
    }
  });
  return locationArray;
}

/**
 * [checkAllCordsAreEqual return array of heterarchy ids]
 * @param  {ARRAY} cords
 * @return Boolean
 */
export function checkAllCordsAreEqual(cords) {
  if (cords.length <= 0) {
    return false;
  }
  if (cords.length === 1) {
    return true;
  }
  const result = _.map(cords, (o, i) => {
    const oo = o;
    const eq = _.find(cords, (e, ind) => {
      if (i > ind) {
        return _.isEqual(e.lat, o.lat) && _.isEqual(e.lon, o.lon);
      }
    })
    if (eq) {
      oo.isDuplicate = true;
      return oo;
    }
  });
  return cords.length === _.compact(result).length + 1;
}
