const querySting = '';

// check if the type is avaliable in filters
const checkType = (filters, type) => filters.findIndex(f => f.key === type) >= 0;

// check if an array have given length
const hasLength = (arr, size) => arr.length === size;

export const clientsFilterByFloorId = (filters) => {
  const { values = [] } = filters;
  const connectionStatus = checkType(filters, 'connectionStatus');
  /* returns empty query string if connectionStatus
   *  is not avaliable in filters or use selectes both
   *  associated and unassociated options
  */
  if (!connectionStatus || hasLength(values, 2)) return querySting;

  return querySting;
};
