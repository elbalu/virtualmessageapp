import _ from 'lodash';
import { LngLatBounds } from 'mapbox-gl';

import url from '../url';
import { getToken, getTempToken } from '../user-utils';

/**
 * @name FloorMap
 *
 * @description Wrapper around mapbox-gl.Map that
 * provides methods for managing floor maps.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class FloorMapUtil {
  /**
   * @name Map.getImageUrl
   *
   * @description Generates a url from an image object
   * that can be used to request the image.
   *
   * @static
   *
   * @param {Object} [image] - MapView Service image object
   *
   * @return {string} - image url
   */
  static getImageUrl(image) {
    // if an image url is provided, return that
    if (_.isString(image.url)) return image.url;

    // construct a url to request the floormap image
    // using the image filename
    const filename = _.get(image, 'sourceFile');
    const env = process.env.NODE_ENV;
    const queryParams = env === 'test' ? 'tenantId=420' : 'tenantId=15';

    return `${url.floormap}/${filename}?${queryParams}`;
  }

  /**
   * @name boundsToCoordinates
   *
   * @description Returns MapBox coordinates array for use when
   * adding an image to the map. The bounding coordinates that
   * are returned will position the image within the bounds of
   * the map and anchored to the center point of the map bounds.
   *
   * @param {Array[]} b - Map bounds in array form: [[lower lng,
   * lower lat], [upper lng, upper lat]]
   *
   * @param {number} p - Optional padding (applied top, bottom,
   * left, right)
   *
   * @return {Array[]} LngLat coordinates returned in an
   * array with order [top left, top right, bottom right,
   * bottom left]
   */
  static boundsToCoordinates(b, p = 0) {
    return [
      [b[0][0] - p, b[1][1] - p], // top left
      [b[1][0] - p, b[1][1] - p], // top right
      [b[1][0] - p, b[0][1] - p], // bottom right
      [b[0][0] - p, b[0][1] - p] // bottom left
    ];
  }

  /**
   * @constructor
   *
   * @param {Map} map - Mapbox map
   */
  constructor(map) {
    this.mapbox = map;
    this.floor = {};
    this.loadImage = this.loadImage.bind(this);
    this.removeImage = this.removeImage.bind(this);
    this.mapToFloorCoords = this.mapToFloorCoords.bind(this);
    this.floorToMapCoords = this.floorToMapCoords.bind(this);
  }

  /**
   * @name Map#loadImage
   *
   * @description Requests an image file from a server
   * and adds the image to the mapbox map.
   *
   * @param {Location} location - MapView service location
   */
  loadImage(location) {
    const image = location.getImage();
    const imageUrl = FloorMapUtil.getImageUrl(image);


    // The values need to be forced into a frame from 0 to 4.
    // 0,0 to 4,4 would be a square; so there is some logic to adjust the bounds to preserve the
    // map image proportions as well.
    // 0,4 4,4
    // 0,0 4,0
    var xBounds = 0;
    var yBounds = 0;
    var pixelWidth = location.data.details.image.width;
    var pixelHeight = location.data.details.image.height;
    if (pixelWidth > pixelHeight) {
      xBounds = 4;
      yBounds = 4 * pixelHeight / pixelWidth;
    } else {
      yBounds = 4;
      xBounds = 4 * pixelWidth / pixelHeight;

    }

    const southWest = [0, 0];
    const northEast = [xBounds, yBounds];
    const bounds = new LngLatBounds(southWest, northEast);
    const coords = FloorMapUtil.boundsToCoordinates(bounds.toArray());
    // store map bounds and image bounds for
    // project/unproject calculations
    this.bounds = bounds;
    this.dimensions = location.getDimensions();

    // remove in case the image is being reloaded
    this.removeImage(image);

    // set map bounds to center around new image
    this.mapbox.fitBounds(bounds);

    // add floor map image as mapbox source
    this.mapbox.addSource(image.imageName, {
      type: 'image',
      url: imageUrl,
      coordinates: coords
    });
  }

  /**
   * @name Map#removeImage
   *
   * @description Removes an image source from the
   * mapbox map
   *
   * @param {Object} image - MapView Service image object
   */
  removeImage(image) {
    if (this.mapbox.getSource(image.imageName)) {
      this.mapbox.removeSource(image.imageName);
    }
  }

  /**
   * @name project
   *
   * @description Converts a lngLat point into an
   * (x, y) point relative to the current floor map.
   *
   * @param {{ lng: number, lat: number }} lngLat
   *
   * @return {{x: number, y: number}}
   */
  project(lngLat) {
    const bounds = this.bounds;
    const dimensions = this.dimensions;
    if (!bounds || !dimensions) return { x: 0, y: 0 };

    const lng = lngLat.lng || 0;
    const lat = lngLat.lat || 0;
    const mapWidth = bounds.getEast() - bounds.getWest();
    const mapHeight = bounds.getNorth() - bounds.getSouth();
    const floorWidth = dimensions.width;
    const floorHeight = dimensions.length;

    return {
      x: ((lng - bounds.getWest()) / mapWidth) * floorWidth,
      y: (1 - ((lat - bounds.getSouth()) / mapHeight)) * floorHeight
    };
  }

  /**
   * @name unproject
   *
   * @description Converts a point that represents a floor
   * location given in (x, y) with units feet into a lngLat
   * point that can be placed on the map relative to the
   * current floor map image.
   *
   * @param {{ x:number, y:number }} point
   *
   * @return {{lng: number, lat: number}}
   */
  unproject(point) {
    const bounds = this.bounds;
    const dimensions = this.dimensions;

    if (!bounds || !dimensions) return { lng: 0, lat: 0 };

    const x = point.x || 0;
    const y = point.y || 0;
    const mapWidth = bounds.getEast() - bounds.getWest();
    const mapHeight = bounds.getNorth() - bounds.getSouth();
    const floorWidth = dimensions.width;
    const floorHeight = dimensions.length;

    return {
      lng: bounds.getWest() + ((x / floorWidth) * mapWidth),
      lat: bounds.getSouth() + ((1 - (y / floorHeight)) * mapHeight)
    };
  }

  /**
   * @name Map#floorToMapCoords
   *
   * @deprecated Use FloorMapUtil#unproject
   *
   * @description Converts from floor (x,y) to
   * map (lng, lat)
   *
   * @param {number} x - X coordinate in feet
   *
   * @param {number} y - Y coordinate in feet
   *
   * @return {{lng: number, lat: number}}
   */
  floorToMapCoords(x, y) {
    return this.unproject({ x, y });
  }

  /**
   * @name Map#mapToFloorCoords
   *
   * @deprecated Use FloorMapUtil#project
   *
   * @description Converts from map (lng, lat) to
   * floor (x, y)
   *
   * @param {number} lng - Longitude
   *
   * @param {number} lat - Latitude
   *
   * @return {{x: number, y: number}}
   */
  mapToFloorCoords(lng, lat) {
    return this.project({ lng, lat });
  }
}

export default FloorMapUtil;
