

import _ from 'lodash';
import update from 'immutability-helper';

import traverse from '../traverse';
import { lightGray3 } from '../colors';

// options for
// location level
const LEVELS = {
  CAMPUS: 'CAMPUS',
  BUILDING: 'BUILDING',
  FLOOR: 'FLOOR',
  ZONE: 'ZONE',
  DEVICE: 'DEVICE',
  CHOKEPOINT: 'CHOKEPOINT',
  AP: 'AP'
};

const defaultZoneColor = lightGray3;

/**
 * getDefaultData returns a data structure
 * in the format of a MapView Service map object.
 * This function generates a random id for the
 * location prepended with temp_.
 *
 * @return {Object}
 */
const getDefaultData = () => ({
  id: `temp_${Math.random().toString(36)}`,
  name: null,
  level: LEVELS.CAMPUS,
  details: {}
});

/**
 * Location represents a single map entity (campus,
 * building, floor, zone, point)
 *
 * @class
 * @author Benjamin Newcomer
 */
class Location {
  /**
   * @static
   *
   * @name Location.LEVELS
   *
   * @return the LEVELS enum
   */
  static get LEVELS() { return LEVELS; }

  /**
   * @name any
   *
   * @return {Location} - A placeholder
   * location instance representing
   * 'any', or a wildcard.
   */
  static any() {
    return new Location({ id: '*', name: 'Any', level: '*' });
  }

  /**
   * Location.hasLevel returns true if the given
   * location has the given level.
   *
   * @static
   * @param {Location|Object} location
   * @param {string} level
   * @return {boolean}
   */
  static hasLevel(location, level) {
    if (_.isEmpty(location)) return false;
    const isLocation = location instanceof Location;
    if (isLocation) return location.getLevel() === level;
    return new Location(location).getLevel() === level;
  }

  /**
   * Location.isFloor returns true if the
   * passed in location represents a floor.
   *
   * @static
   * @param {*} location
   * @return {boolean}
   */
  static isFloor(location) {
    return Location.hasLevel(location, Location.LEVELS.FLOOR);
  }

  /**
   * Location.allCampus returns true if the
   * passed in location is null.
   *
   * @static
   * @param {Location|Object} location
   * @return {boolean}
   */
  static allCampus(location) {
    return location === null;
  }

  /**
   * Location.isCampus returns true if the
   * passed in location represents a campus.
   *
   * @static
   * @param {Location|Object} location
   * @return {boolean}
   */
  static isCampus(location) {
    return Location.hasLevel(location, Location.LEVELS.CAMPUS);
  }

  /**
   * Location.isBuilding returns true if the
   * passed in location represents a building.
   *
   * @static
   * @param {Location|Object} location
   * @return {boolean}
   */
  static isBuilding(location) {
    const oldBuildingFormat = Location.hasLevel(location, 'SUPERZONE');
    return Location.hasLevel(location, Location.LEVELS.BUILDING) || oldBuildingFormat;
  }

  /**
   * Location.isZone returns true if the
   * passed in location represents a zone.
   *
   * @static
   * @param {Location|Object} location
   * @return {boolean}
   */
  static isZone(location) {
    return Location.hasLevel(location, Location.LEVELS.ZONE);
  }

  /**
   * Location.isAP returns true if the
   * passed in location represents an AP.
   *
   * @static
   * @param {Location|Object} location
   * @return {boolean}
   */
  static isAP(location) {
    return Location.hasLevel(location, Location.LEVELS.AP);
  }

  /**
   * Location.isDevice returns true if the
   * passed in location represents a device.
   *
   * @static
   * @param {Location|Object} location
   * @return {boolean}
   */
  static isDevice(location) {
    return Location.hasLevel(location, Location.LEVELS.DEVICE);
  }

  /**
   * @name Location#isNew
   *
   * @description Returns true if this location
   * has never been saved. Unsaved locations have
   * ids that begin with 'temp_'. See {@link Location}
   *
   * @static
   *
   * @param {*} location - Location-like object
   *
   * @return {boolean}
   */
  static isNew(location) {
    return _.startsWith(_.get(location, 'data.id'), 'temp_');
  }

  /**
   * Location.toLocations converts a tree of
   * location objects into a tree of Location
   * instances.
   *
   * @static
   * @param {Object[]} locationData - MapView Service
   * hierarchy object
   */
  static toLocations(locationData) {
    // return Location instance
    const getValue = (data) => {
      if (data instanceof Location) return data;
      return new Location(data);
    };

    const getChildren = data => _.get(data, 'relationshipData.children', []);
    const setChildren = (loc, children) => { loc.setChildren(children); return loc; };
    return traverse(locationData, getValue, getChildren, setChildren);
  }

  /**
   * Location.find accepts a condition function and uses
   * {@link lodash.find} for comparisons.
   *
   * @static
   *
   * @param {Location[]} locations - Locations tree
   *
   * @param {function} condition - Function that is
   * called with (node:Object) and must return true
   * or false.
   *
   * @return {Location|null}
   */
  static find(locations, condition = _.noop) {
    let result = null;

    // traverse getValue function that stores
    // the location if it passes the condition
    const getValue = (l) => { if (condition(l)) result = l; return l; };
    const getChildren = l => l.getChildren();
    const setChildren = (l, c) => { l.setChildren(c); return l; };

    // traverse locations
    traverse(locations, getValue, getChildren, setChildren);

    return result;
  }

  /**
   * Location.same returns true if both locations
   * passed in have the same id.
   *
   * @param {*} locationA
   * @param {*} locationB
   * @return {boolean}
   */
  static same(locationA, locationB) {
    const idA = _.get(locationA, 'data.id', false);
    const idB = _.get(locationB, 'data.id', false);
    return idA && idB && (idA === idB);
  }
  /**
   * Location.toFloors sets a floor name
   * to a hierarchy floor name
   *
   * @static
   * @param {Object[]} locationData - MapView Service
   * hierarchy object
   */
  static toFloors(locationData) {
    if (Array.isArray(locationData)) {
      return locationData.map((data) => {
        const ancestors = data.relationshipData.ancestors.join(' -> ');
        const name = `${ancestors} -> ${data.name}`;
        return Object.assign({}, data, { name });
      });
    }
    const ancestors = locationData.relationshipData.ancestors.join(' -> ');
    const name = `${ancestors} -> ${locationData.name}`;
    return Object.assign({}, locationData, { name });
  }

  /**
   * Location.toLocationHierarchy converts a
   * location node to format expected by
   * backend
   *
   * @static
   * @param {Object[]} leaf - MapView Service
   * hierarchy object
   */
  static toLocationModal(leaf, appendHierarchy) {
    const leafId = leaf.id;
    let locationLabel = '';
    const ancestors = (leaf.relationshipData.ancestors) ?
    leaf.relationshipData.ancestors : [];
    const ancestorsIds = (leaf.relationshipData.ancestorsIds) ?
    Object.assign([], leaf.relationshipData.ancestorsIds) : [];
    ancestorsIds.push(leafId);

    locationLabel = ancestors.map(o => `${o} -> `);
    locationLabel.push(leaf.name);

    const hierarchy = ['campus', 'building', 'floor'];

    const nodeVal = {
      name: (!appendHierarchy) ? leaf.name : locationLabel.join(''),
      id: leafId,
      campus: [],
      building: [],
      floor: [],
      level: leaf.level
    };

    ancestorsIds.forEach((id, i) => {
      nodeVal[hierarchy[i]] = [id];
    });
    return nodeVal;
  }

  /**
   * @constructor
   * @param {Object|string} geoData - MapView Service
   * map object or level for a new location.
   */
  constructor(geoData = getDefaultData()) {
    if (_.isString(geoData)) {
      this.data = getDefaultData();
      this.data.level = geoData;
    } else {
      this.data = geoData;
    }
  }

  /**
   * Location#id returns the id
   * of this location
   *
   * @deprecated use {@link Location#getId}
   */
  get id() {
    return this.getId();
  }

  /**
   * Location#isNew returns true if
   * this location has never been saved.
   *
   * @return {boolean}
   */
  isNew() {
    return Location.isNew(this);
  }

  /**
   * Location#getId returns the location's id
   *
   * @return {string|undefined}
   */
  getId() {
    return _.get(this.data, 'id');
  }

  /**
   * @name Location#setId
   *
   * @description Sets the id.This
   * method mutates the instance. Use
   * an update* method for immutability.
   *
   * @param {string} id - Location id
   *
   * @return {Location} this
   */
  setId(id) {
    _.set(this.data, 'id', id);
    return this;
  }

  /**
   * Location#getName returns this location's name
   * gets the detailed name if any , else the name
   *
   * @return {string|undefined}
   */
  getName() {
    return _.get(this.data, 'detailedName', this.data.name);
  }

  /**
   * @name Location#setName
   *
   * @description Sets the location name.
   * This method mutates the instance. Use
   * an update* method for immutability.
   *
   * @param {string} name
   *
   * @return {Location} this
   */
  setName(name) {
    _.set(this.data, 'name', name);
    return this;
  }

  /**
   * @name updateName
   *
   * @description Updates the name of
   * this location in an immutable fashion and
   * returns the new Location instance. This is
   * useful when updating a Location in state.
   *
   * @param {string} name - New name
   *
   * @return {Location}
   */
  updateName(name) {
    // ensure that this location has the path to name
    const path = 'data.name';
    if (!_.has(path)) _.set(this, path, '');

    return update(this, {
      data: {
        name: {
          $set: name
        }
      }
    });
  }

  /**
   * Locaiton#getDimensions returns the width, length
   * of a floor in feet
   *
   * @return {{ width:number, height: number}}
   */
  getDimensions() {
    return {
      width: _.get(this.data, 'details.width', 0),
      length: _.get(this.data, 'details.length', 0)
    };
  }

  /**
   * Locations#hasChildren returns true if
   * this location has children.
   *
   * @return {boolean}
   */
  hasChildren() {
    const children = _.get(this.data, 'relationshipData.children', []);
    return !_.isEmpty(children);
  }

  /**
   * Location#getChildren returns a list of
   * locations that are children of this location.
   *
   * @return {Array}
   */
  getChildren() {
    if (!this.hasChildren()) return [];
    const children = _.get(this.data, 'relationshipData.children', []);

    // return children as instances of Location
    return children.map((c) => {
      if (c instanceof Location) return c;
      return new Location(c);
    });
  }

  /**
   * Location#setChildren sets the children of
   * this instance to the children array passed in.
   *
   * @param {Location[]} children - List of child locations
   */
  setChildren(children = []) {
    _.set(this.data, 'relationshipData.children', children);
    return this;
  }

  /**
   * @name getCount
   *
   * @description Returns the number of children
   * with the given type.
   *
   * @param {string} type - See Location.LEVELS
   */
  getCount(type) {
    const children = this.getChildren();

    // get count by searching through children and counting
    // how many have the given type
    if (!_.isEmpty(children)) {
      return _.size(_.filter(children, c => c.getLevel() === (type === 'CHOKEPOINT' ? 'DEVICE' : type)));
    }

    // if there are no children, check for childCounts and return
    // the count for the object with level === type
    // count = { level:string, count:string }
    const counts = _.get(this.data, 'relationshipData.childCounts', {});
    const countForType = _.find(counts, c => c.getLevel() === type);
    const count = _.get(countForType, 'data.count', '0');
    return parseInt(count, 10);
  }

  /**
   * @name setCounts
   *
   * @description Stores an array of child types and
   * counts.
   *
   * @param {Object[]} counts - Array of object where
   * each object has format { level:string, count:number }
   *
   * @return {Location}
   */
  setCounts(counts) {
    _.set(this.data, 'relationshipData.childCounts', counts);
    return this;
  }

  /**
   * Location#findParent searches the locations
   * tree given and returns the parent of the
   * location given. If the given location is not
   * found or is a root node, null is returned.
   *
   * @param {Location[]} locations - Locations tree
   */
  findParent(locations) {
    // condition is met if l has a child that
    // matches @param location
    return Location.find(locations, node => (
      _.find(node.getChildren(), c => Location.same(c, this))
    ));
  }

  /**
   * @name getParentName
   *
   * @description Gets the name of the parent of
   * this location using relationshipData.ancestors
   *
   * @return {string} parent location name
   */
  getParentName() {
    return _.get(this.data, 'relationshipData.ancestors[0]');
  }

  /**
   * Location#findPath returns a list of ids
   * that represents each ancestor of this
   * location. The ids represent the path through
   * the locations hierarchy to this location.
   *
   * @param {Location[]} locations - Locations tree
   */
  findPath(locations) {
    const path = [this];
    let location = this;

    // move up the tree by finding the parent
    // of each location and prepending the
    // array with the parent location id
    while (location.findParent(locations)) {
      location = location.findParent(locations);
      path.unshift(location);
    }

    return path;
  }

  /**
   * @name updateVerticesFromPolygon
   *
   * @description Updates the
   * vertices for this Zone location from a
   * GeoJSON Polygon feature. This is an
   * immutable operation.
   *
   * @param {Object} feature - A GeoJSON polygon
   * feature.
   *
   * @param {function} mapToFloorCoords - Function
   * that converts map (lng, lat) to floor (x, y).
   */
  updateVerticesFromPolygon(feature, mapToFloorCoords) {
    let zone = this;
    const linearRing = _.get(feature, 'geometry.coordinates[0]', []);

    // ensure that this location has the path to details
    if (!_.has(this.data, 'details')) {
      zone = update(zone, {
        data: {
          details: {
            $set: {}
          }
        }
      });
    }

    // update zone with new vertex length info
    zone = update(zone, {
      data: {
        details: {
          vertices: {
            $set: linearRing.length
          }
        }
      }
    });

    // for each vertex, attach a prop to the details
    // object with format { x:number, y:number }
    for (let i = 0; i < linearRing.length; i += 1) {
      const lnglat = linearRing[i];
      const vertexName = `v${i}`;
      const vertex = mapToFloorCoords(lnglat[0], lnglat[1]);

      // update zone with new vertex info
      zone = update(zone, {
        data: {
          details: {
            [vertexName]: {
              $set: vertex
            }
          }
        }
      });
    }

    return zone;
  }

  /**
   * Location#hasZones returns true if
   * this location has at least one child
   * that is a zone.
   *
   * @return {boolean}
   */
  hasZones() {
    if (!this.hasChildren()) return false;
    const children = this.getChildren();
    return _.some(children, ['data.level', 'ZONE']);
  }

  /**
   * @name Location#getZones
   *
   * @description Returns the children
   * of this location that are zones.
   *
   * @param {function|Array} condition - Additional
   * filter condition. See lodash.filter.
   *
   * @return {Array}
   */
  getZones(condition = () => true) {
    if (!this.hasZones()) return [];
    const allZones = _.filter(this.getChildren(), ['data.level', 'ZONE']);
    return _.filter(allZones, condition);
  }

  /**
   * Location#hasAPs returns true if
   * this location has at least one child
   * that is a ap.
   *
   * @return {boolean}
   */
  hasAPs() {
    if (!this.hasChildren()) return false;
    const children = this.getChildren();
    return _.some(children, ['data.level', 'AP']);
  }

  /**
   * @name Location#getAPs
   *
   * @description Returns the children
   * of this location that are aps.
   *
   * @param {function|Array} condition - Additional
   * filter condition. See lodash.filter.
   *
   * @return {Array}
   */
  getAPs(condition = () => true) {
    if (!this.hasAPs()) return [];
    const allAPs = _.filter(this.getChildren(), ['data.level', 'AP']);
    return _.filter(allAPs, condition);
  }

  /**
   * Location#getLevel returns the level of
   * the location, which can be CAMPUS,
   * BUILDING, FLOOR, ZONE, POINT
   *
   * @return {string}
   */
  getLevel() {
    return _.get(this.data, 'userLevelDetails.name', _.get(this.data, 'level', null));
  }

  /**
   * @name Location#getColor
   *
   * @description Returns the color
   * associated with this location.
   *
   * @return {string} - Color hex value
   */
  getColor() {
    return _.get(this.data, 'details.internalData.color', defaultZoneColor);
  }

  /**
   * @name Location#setColor
   *
   * @description Sets the passed in
   * color as the location color.
   * This method mutates the instance. Use
   * an update* method for immutability.
   *
   * @param {string} [color] - CSS color
   * value
   *
   * @return {Location} this
   */
  setColor(color = defaultZoneColor) {
    _.set(this.data, 'details.internalData.color', color);
    return this;
  }

  /**
   * @name updateColor
   *
   * @description Updates the color of
   * this location (supported for zones).
   * This method does not mutate the instance
   * and should be used when storing the
   * Location instance in state.
   *
   * @param {string} color - CSS color
   * value
   *
   * @return {Location} this
   */
  updateColor(color = defaultZoneColor) {
    // ensure that this location has the path to color
    const path = 'data.details.internalData.color';
    if (!_.has(path)) _.set(this, path, '');

    return update(this, {
      data: {
        details: {
          internalData: {
            color: {
              $set: color
            }
          }
        }
      }
    });
  }

  /**
   * FloorLocation#hasImage returns true
   * if this floor has an image
   *
   * @return {boolean}
   */
  hasImage() {
    return !_.isEmpty(this.getImage());
  }

  /**
   * @name hasAddress
   *
   * @return {boolean} - True if this location
   * has an address.
   */
  hasAddress() {
    return !_.isEmpty(this.getAddress());
  }

  /**
   * @name hasCoordinates
   *
   * @description Returns true if this location
   * has some address or addresses that can be shown
   * on a map. For campuses, this means children (buildings)
   * with addresses. For a building, it means the address
   * of that building. This method differs from a
   * regular Location#hasLatLng method by checking the
   * children of a campus.
   *
   * @return {boolean}
   */
  hasLngLat() {
    // check the children of a campus
    if (Location.isCampus(this)) {
      return _.some(this.getChildren(), c => c.hasLngLat());
    }

    // check the lnglat of a building
    if (Location.isBuilding(this)) {
      return !_.isEmpty(this.getLngLat());
    }

    // non-building/campus
    // locations do not
    // have lnglat
    return false;
  }

  /**
   * @name getLngLat
   *
   * @description Gets the lng
   * lat location of this location
   *
   * @return {[number, number]}
   */
  getLngLat() {
    const lng = _.get(this.data, 'details.longitude');
    const lat = _.get(this.data, 'details.latitude');

    if (!_.isNumber(lng)) return null;
    if (!_.isNumber(lat)) return null;

    return [lng, lat];
  }

  /**
   * @name setLngLat
   *
   * @description Sets the lng
   * lat location of this location
   *
   * @param {number} lng
   *
   * @param {number} lat
   */
  setLngLat({ lng, lat }) {
    _.set(this.data, 'details.longitude', lng);
    _.set(this.data, 'details.latitude', lat);
    return this;
  }


  /**
   * @name getAddress
   *
   * @return {string} this location's address
   */
  getAddress() {
    return _.get(this.data, 'details.address');
  }

  /**
   * @name setAddress
   *
   * @description Sets the address of this
   * location.
   *
   * @param {string} address - Address
   *
   * @return {Location} - this
   */
  setAddress(address) {
    _.set(this.data, 'details.address', address);
    return this;
  }

  /**
   * FloorLocation#getImage returns the image object
   * for this floor
   *
   * @return {Object|null}
   */
  getImage() {
    return _.get(this.data, 'details.image');
  }

  /**
   * Location#toJSON returns a JSON
   * representation of this location.
   *
   * @return {Object} MapView Service location
   * object
   */
  toJSON() {
    const data = this.data;
    const children = _.map(this.getChildren(), c => c.toJSON());
    _.set(data, 'relationshipData.children', children);
    return data;
  }

  /**
   * Location#toString returns a string
   * representation of this location by
   * returning its name.
   *
   * @return {string}
   */
  toString() {
    return this.getName();
  }

  /**
   * @name getWarnings
   *
   * @description Gets a list of
   * location specific warnings
   *
   * @return {{ message:string, description:string }[]}
   */
  getWarnings() {
    const warnings = [];

    // building warnings
    if (Location.isBuilding(this)) {
      const floorCount = _.size(this.getChildren());

      if (floorCount === 0) {
        warnings.push({
          message: 'no floors',
          description: 'Upload this building again with floors.'
        });
      }
    }

    // floor specific warnings
    // if (Location.isFloor(this)) {
    //   const zoneCount = _.size(this.getZones());
    //
    //   if (zoneCount === 0) {
    //     warnings.push({
    //       message: 'No zones',
    //       description: 'Use the zone tool to create zones.'
    //     });
    //   }
    //
    //   if (!this.hasImage()) {
    //     warnings.push({
    //       message: 'No floor map',
    //       description: 'Import this floor again with a map'
    //     });
    //   }
    // }

    return warnings;
  }

  /**
   * Location#toTreeData returns a representation of this
   * node that is compatible with the antd.TreeSelect
   * treeData prop. It traverses children down to the
   * floor level (excludes zones and devices)
   *
   * @param {number} depth - Max generations of children
   * to include.
   *
   * @param {boolean} canFetchChildren - True if options
   * may be lazy loaded. isLeaf will be false for
   * floors which will prevent the menu from closing
   * when a floor is selected.
   *
   * @return {{value, label, children: Array}}
   */
  toTreeData(depth = 10, canFetchChildren = true) {
    // special cases for zones and 'any' location
    if (Location.isZone(this) || Location.isDevice(this) || this.getId() === '*') {
      return {
        value: this.getId(),
        label: this.getName(),
        isLeaf: true
      };
    }

    // location doesnt have children,
    // but can fetch them
    if (!this.hasChildren() || depth === 1) {
      return {
        value: this.getId(),
        label: this.getName(),
        isLeaf: !canFetchChildren
      };
    }

    return {
      children: this.getChildren().map(c => c.toTreeData(depth - 1, canFetchChildren)),
      value: this.getId(),
      label: this.getName()
    };
  }

  /**
   * Location.toPolygon returns a Mapbox
   * Feature object with a GeoJSON polygon
   * geometry. Only Zones can be converted to
   *
   * @param {function} floorToMapCoords - Function that
   * accepts a floor coordinate (x, y) and returns
   * a map coordinate (lng, lat)
   *
   * @param {Object} [properties] - Additional properties
   * to set on the returned GeoJSON Feature
   *
   * @return {Object} - GeoJSON Feature with polygon
   * geometry
   */
  toPolygon(floorToMapCoords, properties = {}) {
    if (!Location.isZone(this)) return null;

    // get coordinates from details object:
    // @example { vertices: 3, v0:object, v1:object, v2:object }
    // create one GeoJSON LinearRing with these coordinates
    const details = _.get(this.data, 'details', {});
    const numCoordinates = _.get(details, 'vertices', 0);
    const linearRing = [];
    for (let i = 0; i < numCoordinates; i += 1) {
      const xy = _.get(details, `v${i}`, {});
      const coord = floorToMapCoords(xy.x, xy.y);
      if (coord) linearRing.push([coord.lng, coord.lat]);
    }

    if (_.isEmpty(linearRing)) return null;

    // if the first and last coordinates of the linearRing are
    // not the same, copy the first coordinate to the end of the ring
    if (!(_.first(_.first(linearRing)) === _.last(_.first(linearRing))
      && _.first(_.last(linearRing) === _.last(_.last(linearRing))))) {
      linearRing.push(_.first(linearRing));
    }

    // return Feature with the location
    // id as its id
    return {
      ...properties,
      id: this.getId(),
      name: this.getName(),
      type: 'Feature',
      properties: {
        location: this,
        color: this.getColor()
      },
      geometry: {
        type: 'Polygon',
        coordinates: [linearRing]
      }
    };
  }

  /**
   * Location.isDevice returns true if the
   * passed in location represents a device.
   *
   * @static
   * @param {Location|Object} location
   * @return {boolean}
   */
  static isDevice(location) {
    return Location.hasLevel(location, Location.LEVELS.DEVICE);
  }

  updatePointCoordinate(feature, mapToFloorCoords) {
    let device = this;
    const lang = _.get(feature, 'geometry.coordinates[0]', []);
    const lat = _.get(feature, 'geometry.coordinates[1]', []);

    // ensure that this location has the path to details
    if (!_.has(this.data, 'details')) {
      device = update(device, {
        data: {
          details: {
            $set: {}
          }
        }
      });
    }

    // for each vertex, attach a prop to the details
    // object with format { x:number, y:number }
    const coordinate = mapToFloorCoords(lang, lat);

    // update device with new vertex info
    device = update(device, {
      data: {
        details: {
          x: {
            $set : coordinate.x
          },
          y: {
            $set : coordinate.y
          },
          overrideLocation: {
            $set: true
          }
        }
      }
    });
    return device;
  }

  /**
   * Location#hasDevices returns true if
   * this location has at least one child
   * that is a zone.
   *
   * @return {boolean}
   */
  hasDevices() {
    if (!this.hasChildren()) return false;
    const children = this.getChildren();
    return _.some(children, ['data.level', 'DEVICE']);
  }


  /**
   * @name Location#getDevices
   *
   * @description Returns the children
   * of this location that are devices.
   *
   * @param {function|Array} condition - Additional
   * filter condition. See lodash.filter.
   *
   * @return {Array}
   */
  getDevices(condition = () => true) {
    if (!this.hasDevices()) return [];
    const allDevices = _.filter(this.getChildren(), ['data.level', 'DEVICE']);
    return _.filter(allDevices, condition);
  }

  updateDeviceRange(range = defaultZoneColor) {
    // ensure that this location has the path to color
    const path = 'data.details.range';
    if (!_.has(path)) _.set(this, path, '');

    return update(this, {
      data: {
        details: {
          range: {
            $set: range
          }
        }
      }
    });
  }

  updateDeviceMacAddr(macAddress = defaultZoneColor) {
    // ensure that this location has the path to color
    const path = 'data.details.macAddress';
    if (!_.has(path)) _.set(this, path, '');
    const perimeter = false;
    const deviceType = "CHOKEPOINT";

    return update(this, {
      data: {
        details: {
          macAddress: {
            $set: macAddress
          },
          perimeter: {
            $set: perimeter
          },
          deviceType: {
            $set: deviceType
          }
        }
      }
    });
  }

  updateOverrideLocation(override = true) {
    // ensure that this location has the path to override flag
    const path = 'data.details.overrideLocation';
    if (!_.has(path)) _.set(this, path, '');

    return update(this, {
      data: {
        details: {
          overrideLocation: {
            $set: override
          }
        }
      }
    });
  }
  /**
   * Location.toPoint returns a Mapbox
   * Feature object with a GeoJSON point
   * geometry. Only Exitors can be converted to
   *
   * @param {function} floorToMapCoords - Function that
   * accepts a floor coordinate (x, y) and returns
   * a map coordinate (lng, lat)
   *
   * @param {Object} [properties] - Additional properties
   * to set on the returned GeoJSON Feature
   *
   * @return {Object} - GeoJSON Feature with polygon
   * geometry
   */
  toPoint(floorToMapCoords, properties = {}) {
    if (!Location.isDevice(this)) return null;

    // get coordinates from details object
    const details = _.get(this.data, 'details', {});
    const coord = floorToMapCoords(details.x,details.y);

    if (_.isEmpty(coord)) return null;

    // return Feature with the location
    // id as its id
    return {
      ...properties,
      id: this.getId(),
      name: this.getName(),
      type: 'Feature',
      properties: {
        location: this,
        icon: "bicycle",
        color: this.getColor(),
      },
      geometry: {
        type: 'Point',
        coordinates: [coord.lng, coord.lat]
      }
    };
  }
}
export default Location;
