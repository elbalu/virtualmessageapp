export const styles = {
    'version': 8,
    'name': 'Streets-copy',
    'metadata': {
        'mapbox:autocomposite': true,
        'mapbox:type': 'default',
        'mapbox:origin': 'streets-v10',
        'mapbox:groups': {}
    },
    'center': [2.715517454322935, -1.1368683772161603e-13],
    'zoom': 0.8579808817603862,
    'bearing': 0,
    'pitch': 0,
    'sources': {},
    'sprite': '' + window.location.origin + '/sprites/mapbox-sprites',
    'glyphs': 'mapbox://fonts/elbalu/{fontstack}/{range}.pbf',
    'layers': [{
        'id': 'background',
        'type': 'background',
        'layout': {},
        'paint': {
            'background-color': {
                'base': 1,
                'stops': [
                    [11, 'hsl(35, 32%, 91%)'],
                    [13, 'hsl(35, 12%, 89%)']
                ]
            },
            'background-opacity': 0
        }
    }],
    'created': '2017-10-31T17:19:20.350Z',
    'id': 'cj9fvofrs8f8g2rmpqtac28ud',
    'modified': '2017-10-31T17:19:20.350Z',
    'owner': 'elbalu',
    'visibility': 'private',
    'draft': false
};
