import polylabel from 'polylabel';
import _ from 'lodash';

import { getCampusPolygons } from '../map-utils';
import { calculateEnclosingPolygon } from '../calculate-polygon';

/**
 * @class
 *
 * @name FloorMap
 *
 * @description Wrapper around mapbox-gl.Map that
 * provides methods for managing floor maps.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class WorldMap {
  /**
   * @static
   *
   * @name getGeoJSONCircleCoordinates
   *
   * @description Generates a GeoJSON
   * polygon with many sides that looks
   * like a circle with the given center point
   * and radius in km.
   *
   * @param {{ lng:number, lat:number }} center
   *
   * @param {number} [radius] - Circle radius in kilometers
   *
   * @param {number} [smoothness] - Scalar factor 0 - 1000
   * where higher numbers result in smoother circles.
   *
   * @return {Array}
   */
  static getGeoJSONCircleCoordinates(center, radius = 0.1, smoothness = 64) {
    const linearRing = [];

    // convert radius into latitude and longitude distances
    const distanceX = radius / (111.320 * Math.cos((center.lat * Math.PI) / 180));
    const distanceY = radius / 110.574;

    // create smoothness number of points by converting
    // the polar coordinate (center, radius) into
    // (x, y)
    for (let i = 0; i < smoothness; i += 1) {
      const theta = (i / smoothness) * (2 * Math.PI);
      const x = distanceX * Math.cos(theta);
      const y = distanceY * Math.sin(theta);

      linearRing.push([center.lng + x, center.lat + y]);
    }

    // copy the first point to the end of
    // the linearRing to close it
    linearRing.push(linearRing[0]);

    return [linearRing];
  }

  /**
   * @name getCoordinatesForCampus
   *
   * @description Calculates a GeoJSON polygon
   * that represents a campus either based on
   * a circle around a single building address or
   * a rounded polygon enclosing multiple buildings.
   *
   * @param {Location} location - Campus location
   *
   * @return {{ polygon:Object, label:Object }} -
   * Object with polygon, a GeoJSON Polygon, and label, a
   * GeoJSON Point for placing a text label.
   */
  static getCoordinatesForCampus(location) {
    let polygon = null;
    let label = null;
    const polygonCoords = getCampusPolygons(location.toJSON());

    // If there is only one building in the campus
    if (polygonCoords.length === 1) {
      // calculate a circular polygon
      // around the location of the buildng
      polygon = WorldMap.getGeoJSONCircleCoordinates({
        lng: polygonCoords[0].lon,
        lat: polygonCoords[0].lat
      });

      // the text label will go at the center
      // point (with some offset)
      label = [polygonCoords[0].lon, polygonCoords[0].lat];
    }

    // more than one building in the campus
    if (polygonCoords.length > 1) {
      // coordinates are a
      // rounded polygon enclosing
      // all buildings
      polygon = [_.map(
        calculateEnclosingPolygon(polygonCoords),
        c => [c.lon, c.lat]
      )];

      // set the label coordinates
      // as the pole of inaccessibility
      // of the polygon
      if (polygon[0].length > 0) {
        label = polylabel(polygon);
      }
    }

    return { polygon, label };
  }

  /**
   * @constructor
   *
   * @param {Map} map - Mapbox map
   */
  constructor(map) {
    this.mapbox = map;
  }
}

export default WorldMap;
