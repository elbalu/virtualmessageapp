import MapboxDraw from '@mapbox/mapbox-gl-draw';
import _ from 'lodash';
import polylabel from 'polylabel';
import { Marker } from 'react-mapbox-gl';
import React from 'react';

import { zoneColors } from '../../misc/colors';
import defaultStyles from './mapbox-draw-default-styles.json';
import Location from '../../misc/map/location';

const defaultPosition = 'top-left';

/**
 * ZoneDrawControl is a wrapper class around the MapboxDraw
 * mapbox draw. ZoneDrawControl provides context specific
 * management of the MapboxDraw draw to create, edit,
 * and remove zones.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ZoneDrawControl {
  /**
   * @name getSylesForColor
   *
   * @description Generates a set of
   * Mapbox styles for the given color.
   * These styles are designed to match
   * a zone Feature and handle the color
   * for that zone.
   *
   * @param {string} color - Valid CSS color
   *
   * @return {Array}
   */
  static getStylesForColor(color) {
    const styles = [];

    // line color
    styles.push({
      id: `line-color-${color}`,
      type: 'line',
      filter: ['==', 'user_color', color],
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': color,
        'line-dasharray': [0.2, 2],
        'line-width': 2
      }
    });

    // fill color
    styles.push({
      id: `fill-color-${color}`,
      type: 'fill',
      filter: [
        'all',
        ['==', 'user_color', color],
        ['==', 'active', 'false']
      ],
      layout: {},
      paint: {
        'fill-color': color,
        'fill-opacity': 0.3
      }
    });

    // vertex style
    styles.push({
      id: `gl-draw-polygon-and-line-vertex-${color}`,
      type: 'circle',
      filter: [
        'all',
        ['==', 'meta', 'vertex'],
        ['==', '$type', 'Point'],
        ['==', 'user_color', color]
      ],
      paint: {
        'circle-radius': 3,
        'circle-color': color
      }
    });

    // selected fill color
    styles.push({
      id: `active-fill-color-${color}`,
      type: 'fill',
      filter: [
        'all',
        ['==', 'user_color', color],
        ['==', 'active', 'true']
      ],
      layout: {},
      paint: {
        'fill-color': color,
        'fill-opacity': 0.8
      }
    });

    styles.push(
      {
        'id': `highlight-active-points`,
        'type': 'circle',
        'filter': ['all',
          ['==', '$type', 'Point'],
          ['==', 'meta', 'feature'],
          ['==', 'active', 'true'],
        ],
        'paint': {
          'circle-radius': 10,
          'circle-color': 'grey',
          'circle-opacity': 0.6,
        }
      });

    styles.push(
      {
        'id': `points-are-${color}`,
        'type': 'circle',
        'filter': ['all',
          ['==', '$type', 'Point'],
          ['==', 'meta', 'feature'],
          ['==', 'active', 'false'],
          ['==', 'user_color', color],
        ],
        'paint': {
          'circle-radius': 6,
          'circle-color': color,
          'circle-stroke-width': 2,
          'circle-stroke-color': 'white',
          'circle-stroke-opacity': 0.5
        }
      }
    )

    return styles;
  }

  /**
   * @constructor
   *
   * @param {Map} map - mapbox-gl Map
   *
   * @param {function} [floorToMapCoords] -
   * Function that converts a set of coordinates
   * from (x, y) to (lng, lat) for the given map
   *
   * @param {string} [position] - 'top-left' ,  'top-right' ,
   * 'bottom-left' , or  'bottom-right'
   */
  constructor(map, floorToMapCoords, draw, position = defaultPosition) {
    this.map = map;
    this.floorToMapCoords = floorToMapCoords;
    this.position = position;
    this.listeners = {};
    this.drawEnabled = draw;

    // generate a mapbox style for each zone color.
    // When a zone is draw, the GeoJSON feature added
    // to MapboxDraw will have feature.properties.color
    // set to the color of the zone. This property is
    // referenced in the filter below, prefixed with
    // user_
    let zoneStyles = defaultStyles;
    _.each(zoneColors, color => (
      zoneStyles = zoneStyles.concat(ZoneDrawControl.getStylesForColor(_.toLower(color)))
    ));

    // init a new mapbox draw with
    // styles for each zone color

    this.draw = new MapboxDraw({
      displayControlsDefault: false,
      controls: { polygon: this.drawEnabled }, //, point: this.drawEnabled
      userProperties: true,
      boxSelect: false,
      styles: zoneStyles
    });

  }

  /**
   * @name ZoneDrawControl#unmount
   *
   * @description Removes this draw
   * from the map
   *
   * @return {ZoneDrawControl} this
   */
  unmount() {
    this.map.removeControl(this.draw);
    return this;
  }

  /**
   * ZoneDrawControl#mount mounts the MapboxDraw
   * draw on the map.
   *
   * @return {ZoneDrawControl} this
   */
  mount() {
    this.map.addControl(this.draw, this.position);

    // change the polygon tool title
    const button = global.document.querySelector('.mapbox-gl-draw_polygon');
    if (button) button.title = 'Create a zone';

    //const buttonPoint = global.document.querySelector('.mapbox-gl-draw_point');
    //if (buttonPoint) buttonPoint.title = 'Add chokepoint';

    // disable selecting zones if drawing is disabled
    if (!this.drawEnabled) {
      this.draw.changeMode(this.draw.modes.STATIC);
    }

    return this;
  }

  /**
   * @name drawLabels
   *
   * @description Adds labels to the map
   * for the set of features passed in.
   * Each feature should have a set of
   * coordinates and a name property.
   *
   * @param {Object[]} features - List
   * of GeoJSON features with a name
   * property
   */
  drawLabels(features = []) {
    const map = this.map;
    const labelLayerId = 'label-layer';

    // if label layer already exists, remove it.
    // because the label layer has inline source,
    // mapbox also creates a source for it. However,
    // it does not remove the source as part of removeLayer
    // so we also call removeSource and then add the
    // updated source
    if (map.getLayer(labelLayerId)) {
      map.removeLayer(labelLayerId);
      map.removeSource(labelLayerId);
    }

    // generate label feature for each zone feature
    const featureLabels = _.map(features, (feature) => {
      const type = _.get(feature, 'geometry.type', 'Point');
      const coords = _.get(feature, 'geometry.coordinates', []);
      const name = _.get(feature, 'name') || '';
      const center = (type === 'Point') ? coords : polylabel(coords);

      return {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: center
        },
        properties: { name }
      };
    });

    // create layer with label data
    // as source
    const labelLayer = {
      id: labelLayerId,
      type: 'symbol',
      source: {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: featureLabels
        }
      },
      layout: {
        'text-field': '{name}',
        "text-offset": [0, 0.7],
      }
    };

    map.addLayer(labelLayer);
  }

  /**
   * @name ZoneDrawControl#setZones
   *
   * @description Draws a list of zones on the map
   * using the MapboxDraw draw.
   *
   * @param {Location[]} zones - A list of zone
   *
   * @param {Location} [selectedZone] - The currently
   * selected zone. This zone will be highlighted.
   *
   * @param {function} [floorToMapCoords] - Function that
   * accepts a floor coordinate (x, y) and returns
   * a map coordinate (lng, lat)
   *
   * @return {ZoneDrawControl}
   */
  setZones(zones, selectedZone, floorToMapCoords = this.floorToMapCoords) {
    let features = _.compact(_.map(zones, z => {
      if (Location.isZone(z)){
        return z.toPolygon(floorToMapCoords)
      } else {
        return z.toPoint(floorToMapCoords)
      }
    }));
    // if the selectedZone is not in the zones list, add it to make
    // sure it gets drawn
    const selectedZoneId = selectedZone ? selectedZone.getId() : null;
    const isNewZone = Location.isNew(selectedZone);

    // check if a zone was selected or deselected
    if (selectedZoneId) {
      // add or replace selectedZone in the list of zones
      if (isNewZone) {
        // add selectedZone to zone list. This handles drawing new zones.
        // The new zone exists as selectedZone, but is not present in
        // the zones list
        if (Location.isZone(selectedZone)){
          const selectedFeature = selectedZone.toPolygon(floorToMapCoords);
          features = [...features, selectedFeature];
        } else {
          const selectedFeature = selectedZone.toPoint(floorToMapCoords);
          features = [...features, selectedFeature];
        }
      } else {
        // replace zone in list with selectedZone. This handles
        // editing zone coordinates. The zone exists as selectedZone
        // and is present in the zones list. selectedZone is the instance
        // that is updated onChange so this is the one we want to draw
        // (and we do not want to draw both!)
        features = _.compact(_.map(features, (feature) => {
          if (feature.id !== selectedZoneId) return feature;
          return (Location.isZone(selectedZone)) ? selectedZone.toPolygon(floorToMapCoords) : selectedZone.toPoint(floorToMapCoords);
        }));
      }
    }

    // draw the given zones
    if (_.isEmpty(features)) {
      this.draw.deleteAll();
      this.drawLabels([]);
    }
    else {
      this.draw.set({ features, type: 'FeatureCollection' });
      this.drawLabels(features);
    }

    // select the selected zone by setting the MapboxGLDraw
    // mode and giving the selected zone id (when selecting)
    if (this.drawEnabled) {
      if (_.isNil(selectedZoneId)) this.draw.changeMode(this.draw.modes.SIMPLE_SELECT);
      else {
        (!Location.isZone(selectedZone)) ?
          this.draw.changeMode(this.draw.modes.SIMPLE_SELECT, { featureId: selectedZoneId }) :
          this.draw.changeMode(this.draw.modes.DIRECT_SELECT, { featureId: selectedZoneId });
      }
    }

    return this;
  }

  /**
   * @name enableDraw
   *
   * @description Updates the draw tool's mode
   * to enable or disable drawing and selecting
   * features.
   *
   * @param {boolean} enable
   */
  enableDraw(enable = true) {
    const currentMode = this.draw.getMode();
    const isEnabled = currentMode !== this.draw.modes.STATIC;
    let nextMode = currentMode;

    if (isEnabled && !enable) nextMode = this.draw.modes.STATIC;
    if (!isEnabled && enable) nextMode = this.draw.modes.SIMPLE_SELECT;

    this.draw.changeMode(nextMode);
  }

  /**
   * ZoneDrawControl#onSelect registers an event
   * listener that is called each time a zone is
   * selected or deselected. The listener is called
   * with the id of the zone that was clicked.
   *
   * @param {function} listener
   *
   * @return {string} - Selected zone id
   */
  onSelect(listener) {
    if (_.isFunction(listener)) {
      this.listeners.onSelect = listener;
      this.map.on('draw.selectionchange', (e) => {
        const selectedZoneId = _.get(e, 'features[0]');
        listener(selectedZoneId);
      });
    }
    return this;
  }

  /**
   * @name ZoneDrawControl#onCreate
   *
   * @description Registers a listener
   * that is called whenever a new feature
   * is created.
   *
   * @param {function} listener - Function
   * that accepts a GeoJSON feature
   *
   * @return {ZoneDrawControl} this
   */
  onCreate(listener) {
    if (_.isFunction(listener)) {
      this.listeners.onCreate = listener;
      this.map.on('draw.create', e => (
        listener(_.get(e, 'features[0]'))
      ));
    }
    return this;
  }

  /**
   * @name onChange
   *
   * @description Registers a listener that
   * will be called whenever a feature is moved
   * or its coordinates are changed.
   *
   * @param {function} listener - Function
   * that accepts a GeoJSON feature
   *
   * @return {ZoneDrawControl} this
   */
  onChange(listener) {
    if (_.isFunction(listener)) {
      this.listeners.onChange = listener;
      this.map.on('draw.update', e => (
        listener(_.get(e, 'features[0]'))
      ));
    }
    return this;
  }
}

export default ZoneDrawControl;
