import React from 'react';
import _ from 'lodash';
import { FormattedMessage, FormattedNumber} from 'react-intl';

import { NONE_VALUE } from '../renderers/values';

/**
 * Battery is a model class that represents a single
 * battery value. Battery has properties percent, the
 * percent battery remaining, and days, the days
 * remaining. Battery renders XML that displays both
 * values.
 *
 * NOTE: This class should be used instead of icon-renderer#battery
 * in the future because renderers will eventually be deprecated.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Battery {
  /**
   * @constructor
   * @param {number} percent - Percent battery remaining
   * @param {number} days - Days remaining
   */
  constructor(percent, days) {
    this.data = { percent, days };
    this.none = NONE_VALUE;
  }

  /**
   * Battery#percent returns percent
   * as a percentage.
   *
   * @return {XML}
   */
  percent() {
    const value = this.data.percent;

    if (_.isNil(value)) return null;

    return <FormattedNumber value={value / 100} style="percent" />;
  }

  /**
   * Battery#days returns days
   * remaining as XML.
   *
   * @return {XML}
   */
  days() {
    const value = this.data.days;

    if (_.isNil(value)) return null;

    return <FormattedMessage values={{ value }} id="days" />;
  }

  /**
   * Battery#icon returns an icon
   * that represents the battery life
   * remaining based on percent.
   *
   * @return {XML}
   */
  icon() {
    const value = this.data.percent;

    if (_.isNil(value)) return this.none;

    if (value >= 75) return <i className={'green fa fa-battery-full'} />;
    if (value < 75 && value >= 50) return <i className={'battery fa fa-battery-three-quarters'} />;
    if (value < 50 && value >= 25) return <i className={'battery fa fa-battery-half'} />;
    if (value < 25 && value >= 10) return <i className={'fa fa-battery-quarter'} />;
    return <i className={'fa fa-battery-empty'} />;
  }

  /**
   * Battery.render returns XML that displays
   * battery life as a percentage, as days
   * remaining, and with an icon.
   *
   * @return {XML}
   */
  render() {
    const noPercent = _.isNil(this.data.percent);
    const noDays = _.isNil(this.data.days);

    if (noPercent && noDays) return this.none;

    const icon = this.icon();
    const percent = this.percent();
    const days = this.days();

    return (
      <span>{icon} {percent} {days}</span>
    );
  }
}

export default Battery;
