/**
 * Field is a model that represents a single field. This is slightly
 * different from a Form Generator field because this field has some
 * Asset Management specific properties. This model can be though of
 * as a superset of the Form Generator field that includes properties
 * used by other parts of the application.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Field {
  static blank() {
    return {
      clickableLink: false,
      collection: null,
      defaultSelectAll: false,
      description: '',
      editable: true,
      entityName: '',
      general: true,
      hidden: false,
      isDefault: false,
      key: 'new-field',
      label: 'New Field',
      lastModified: 0,
      max: null,
      min: null,
      multiple: false,
      options: [],
      required: false,
      templateHidden: false,
      templateId: null,
      templateLabel: null,
      templateUnique: false,
      type: 'TEXT',
      validation: 'ANY'
    };
  }
}

export default Field;
