import React from 'react';
import { notification as Notifier, Modal, message as Message } from 'antd';
import _ from 'lodash';

import config from '../../config';

const messages = config.get('messages');

export const NOTIFICATION_DEFAULT_TITLE = 'Error';
export const NOTIFICATION_DEFAULT_MESSAGE = 'Unknown Error';
export const NOTIFICATION_DEFAULT_STATUS = 0;
export const NOTIFICATION_DEFAULT_ERROR_CODE = 0;

/**
 * validationErrorTemplate accepts a validate.js
 * style validationErrors object and returns
 * a react unordered list of error messages.
 *
 * @param {Object} errors
 * @return {XML[]}
 */
const validationErrorTemplate = errors => (
  <ul>
    {_.flatMap(errors, (error) => {
      if (!_.isArray(error)) return <li key={error}>{error}</li>;
      return _.map(error, e => <li key={e}>{e}</li>);
    })}
  </ul>
);

export const NOTIFICATION_LEVELS = {
  INFO: 'info',
  WARN: 'warn',
  ERROR: 'error',
  SUCCESS: 'success',
};

export const NOTIFICATION_TYPES = {
  NOTIFICATION: 'notification',
  MODAL: 'modal',
  MESSAGE: 'message',
};

/**
 * given an http status code, returns
 * a notification type
 *
 * @param {number} status HTTP Status
 * @return {string}
 */
const getTypeFromStatus = (status) => {
  if (status === 409) {
    return NOTIFICATION_TYPES.MODAL;
  }

  return NOTIFICATION_TYPES.MESSAGE;
};

/**
 * shows a notification using
 * the Message component.
 *
 * @param {{ level, title, message }} notification
 */
const showNotification = (notification) => {
  Notifier[notification.level]({
    message: notification.title,
    description: notification.message,
  });
};

/**
 * shows a notification using
 * the Message component.
 *
 * @param {{ level, message }} notification
 */
const showMessage = (notification) => {
  Message[notification.level](notification.message);
};

/**
 * shows a notification using the
 * Modal component.
 *
 * @param {{ level, title, message }} notification
 */
const showModal = (notification) => {
  Modal[notification.level]({
    title: notification.title,
    content: notification.message,
    okText: messages.ok,
    onCancel: () => {},
    onOk: () => {}
  });
};

/**
 * Notification represents a user level notification (error
 * messages, success messages, warnings). This class can be used to
 * parse HTTP response (especially those that use the AMError format). It
 * can be used to notify the user of UI errors, form validation errors,
 * and HTTP successes.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Notification {
  /**
   * returns a notification by parsing an
   * Error instance.
   *
   * @static
   * @param {Error} error
   * @return {Notification}
   */
  static fromError(error) {
    return new Notification({
      title: _.get(error, 'title', NOTIFICATION_DEFAULT_TITLE),
      message: _.get(error, 'message', NOTIFICATION_DEFAULT_MESSAGE),
      level: NOTIFICATION_LEVELS.ERROR,
      type: NOTIFICATION_TYPES.MODAL,
    });
  }

  /**
   * returns a notification by parsing
   * an HTTP Error. If the error was sent
   * using the AMError class, then the AMError
   * data is used to create the notification.
   *
   * @static
   * @param {Object} error - APIError JSON object
   * @return {Notification}
   */
  static fromAPIError(error) {
    const title = _.get(error, 'response.body.title', _.get(error, 'response.statusText', NOTIFICATION_DEFAULT_TITLE));
    const message = _.get(error, 'response.body.message', _.get(error, 'response.text', NOTIFICATION_DEFAULT_MESSAGE));
    const httpStatus = _.get(error, 'response.body.httpStatus', _.get(error, 'response.statusCode', NOTIFICATION_DEFAULT_STATUS));
    const errorCode = _.get(error, 'response.body.errorCode', NOTIFICATION_DEFAULT_ERROR_CODE);
    const isRequestTimeout = message.includes('Request has been terminated');
    const level = NOTIFICATION_LEVELS.ERROR;
    const type = getTypeFromStatus(httpStatus);

    if (isRequestTimeout) {
      return new Notification({
        level,
        type,
        message: 'Server is unresponsive',
      });
    }

    return new Notification({
      title,
      message,
      httpStatus,
      level,
      type,
      errorCode
    });
  }

  /**
   * Notification.fromValidationErrors is a factory method
   * that returns a notification built from a
   * validation errors object.
   *
   * @static
   * @param {string[]} errors - List of error messages
   * @param {string} title - Notification title
   * @return {Notification}
   */
  static fromValidationErrors(errors, title) {
    return new Notification({
      title,
      message: validationErrorTemplate(errors),
      level: NOTIFICATION_LEVELS.ERROR,
      type: NOTIFICATION_TYPES.NOTIFICATION,
    });
  }

  /**
   * @constructor
   * @param {string} title Error title
   * @param {string} message Error message
   * @param {number} httpStatus HTTP Status Code
   * @param {number} errorCode - backend error code
   * @param {string} level Urgency (like log level)
   * @param {string} type Message type
   */
  constructor({
    title = NOTIFICATION_DEFAULT_TITLE,
    message = NOTIFICATION_DEFAULT_MESSAGE,
    httpStatus = NOTIFICATION_DEFAULT_STATUS,
    errorCode = NOTIFICATION_DEFAULT_ERROR_CODE,
    level = NOTIFICATION_LEVELS.ERROR,
    type = NOTIFICATION_TYPES.MESSAGE,
  }) {
    this.data = {
      title,
      message,
      level,
      httpStatus,
      type,
      errorCode
    };

    this.notifiers = {
      notification: showNotification,
      message: showMessage,
      modal: showModal,
    };
    this.levels = NOTIFICATION_LEVELS;
    this.types = NOTIFICATION_TYPES;
  }

  /**
   * title of the notification
   */
  get title() { return this.data.title; }
  set title(value) { this.data.title = value || NOTIFICATION_DEFAULT_TITLE; }

  /**
   * content of the notification
   */
  get message() { return this.data.message; }
  set message(value) { this.data.message = value || NOTIFICATION_DEFAULT_MESSAGE; }

  /**
   * severity of the notification
   */
  get level() { return this.data.level; }
  set level(value) { if (_.values(this.levels).includes(value)) this.data.level = value; }

  /**
   * how the notification should be displayed
   */
  get type() { return this.data.type; }
  set type(value) { if (_.values(this.types).includes(value)) this.data.type = value; }

  /**
   * identifier of the error
   */
  get errorCode() { return this.data.errorCode; }
  set errorCode(value) { this.data.errorCode = value; }

  /**
   * http status code
   */
  get httpStatus() { return this.data.httpStatus; }
  set httpStatus(value) { this.data.httpStatus = value; }


  /**
   * shows the notification on the UI
   * according to the level and type
   */
  show() {
    const notifier = this.notifiers[this.type];
    notifier(this.data);
  }
}

export default Notification;
