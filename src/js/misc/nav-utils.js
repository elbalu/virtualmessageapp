const navigation = [
  {
    title: 'Maps',
    icon: 'fa fa-gear',
    path: 'manage/maps',
    type: 'sdk',
  }
];


export const getNavigation = () => navigation;

export default getNavigation;
