import React from 'react';
import _ from 'lodash';

import { YES, NO } from './values';

/**
 * BooleanRenderer provides methods for
 * rendering boolean values into strings
 * for display on the UI.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class BooleanRenderer {

  /**
   * render() boolean values by converting true/false
   * values into strings
   *
   * @param {string} trueOption
   * @param {string} falseOption
   * @returns {function}
   */
  static render(trueOption = 'true', falseOption = 'false') {
    return (value) => {
      if (value) {
        if (trueOption === 'true') {
          return <span key="Yes">YES</span>;
        }

        return trueOption;
      }

      if (falseOption === 'false') {
        return <span key="No">NO</span>;
      }

      return falseOption;
    };
  }

  /**
   * render boolean string as a checkmark or x
   *
   * @returns {XML}
   */
  static checkmark(field) {
    const options = field.options;

    // display checkmark options or default
    // yes/no text
    const yes = _.get(options, '[0]', YES);
    const no = _.get(options, '[1]', NO);

    return (value) => {
      if (value) return (<span><i title={yes} className="fa fa-check green" />{yes}</span>);
      return (<span><i title={no} className="fa fa-times red" />{no}</span>);
    };
  }

  /**
   * render boolean string as a active or inactive
   *
   * @returns {XML}
   */
  static active(field) {
    const options = field.options;

    // display checkmark options or default
    // yes/no text
    const yes = _.get(options, '[0]', YES);
    const no = _.get(options, '[1]', NO);

    return (value) => {
      if (!_.isEmpty(value) && value === 'Active') {
        return (<span><i title={yes} className="fa fa-check green" />{yes}</span>);
      }
      return (<span><i title={no} className="fa fa-times red" />{no}</span>);
    };
  }
}

export default BooleanRenderer;
