import React from 'react';
import validate from 'validate.js';
import _get from 'lodash/get';
import { Popover, Table } from 'antd';

import { NONE_VALUE_NO_COLOR, MULTIPLE } from './values';

/**
 * ClickableRenderer provides functions that
 * render values as clickable links.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ClickableRenderer {

  /**
   * render() returns a function that renders
   * a value (or searches the passed in entity for string fields)
   * as a clickable link that calls the given callback when
   * pressed.
   *
   * @param {function} callback Function that will be called when clicked
   * @param {string} key Row key to render text (should be field.key if this
   *  renderer is used to render a field)
   */
  static render(callback, key = 'serial') {
    return (value, entity) => {
      let dataSet = entity;
      const id = dataSet.id || 0;
      let callbackId = id;
      let displayVal = _get(dataSet, key) || _get(dataSet, 'serial') || _get(dataSet, 'name') || NONE_VALUE_NO_COLOR;


      // if this field contains array of data.Then display Multiple in the table
      if (validate.isArray(value)) {
        if (value.length > 1) {
          callbackId = value;
          displayVal = (
            <div>
              <span className="multiple">{value.length}</span>
              <span>{MULTIPLE}</span>
            </div>
          );
          return (
            <Popover
              title=""
              placement="bottom"
              overlayClassName="multi-popover"
              content={this.renderMultiple(value, key, callback)}
              trigger="click"
            >
              <div className="detailsLink hyperlink multi-item-row ">
                {displayVal}
              </div>
            </Popover>
          );
        }
        dataSet = value[0];
        callbackId = value[0].id;
        displayVal = this.multiItemRowDisplay(dataSet, key);
      }

      if (!validate.isFunction(callback)) {
        return <div>{ displayVal }</div>;
      }

      return (
        <div className="detailsLink hyperlink" onClick={() => callback(callbackId, entity)}>
          {displayVal}
        </div>
      );
    };
  }

  /**
   * multiItemRowDisplay() returns a function that renders
   * a value to be set for a row with multiple item display
   *
   * @param {object} dataSet Data for the rendered row
   * @param {string} key Row key to render text (should be field.key if this
   *  renderer is used to render a field)
   */
  static multiItemRowDisplay(dataSet, key) {
    let displayVal = _get(dataSet, key) || _get(dataSet, 'serial') || _get(dataSet, 'name') || NONE_VALUE_NO_COLOR;
    if (_get(dataSet, 'serial') && _get(dataSet, 'name')) {
      displayVal = `${_get(dataSet, 'serial')} - ${_get(dataSet, 'name')}`;
    }
    return displayVal;
  }

  /**
   * multiItemRowDisplay() returns a function that renders
   * the content that goes inside the popover for multi
   * item view
   *
   * @param {array} items Array of items
   * @param {string} key Row key to render text (should be field.key if this
   *  renderer is used to render a field)
   * @param {function} callback Function that will be called when clicked
   */
  static renderMultiple(items, key, callback) {
    const columns = [{
      dataIndex: 'name',
      render: (val, rec) => {
        const displayVal = this.multiItemRowDisplay(rec, key);
        const callbackId = rec.id;
        return (
          <div
            key={rec.id}
            className="detailsLink hyperlink"
            onClick={() => callback(callbackId)}
          >
            {displayVal}
          </div>
        );
      }
    }];
    return (
      <Table
        className="table-small"
        columns={columns}
        rowKey={r => r.id}
        dataSource={items}
        pagination={{
          size: 'small',
          pageSize: 5
        }}
      />
    );
  }
}

export default ClickableRenderer;
