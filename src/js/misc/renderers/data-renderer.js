import _ from 'lodash';

import Battery from '../models/battery-model';
import { NONE_VALUE } from './values';

/**
 * DataRenderer provides methods that render
 * tag data into readable formats
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class DataRenderer {

  /**
   * tagData() returns a function that renders
   * tag data into a readable format
   *
   * @param key
   * @return {function(*, *)}
   */
  static tagData(key) {
    return (value, row) => {
      if (row && row.tags.length === 0) return NONE_VALUE;
      const tagData = _.get(row, 'tags[0].data');

      // render battery data
      if (key === 'tagBattery') {
        const event = _.find(tagData, ['type', 'battery']);
        return DataRenderer.battery(event);
      }

      // render temperature data
      if (key === 'tagTemperature') {
        const event = _.find(tagData, ['type', 'TEMPERATURE']);
        if (!event) return NONE_VALUE;
        return event.data.measurement.toFixed(2);
      }

      // render humidity data
      if (key === 'tagHumidity') {
        const event = _.find(tagData, ['type', 'HUMIDITY']);
        if (!event) return NONE_VALUE;
        return event.data.measurement;
      }

      return NONE_VALUE;
    };
  }

  /**
   * DataRenderer.battery takes in a tag event object,
   * searches for percent and days remaining, and returns
   * the result of Battery#render.
   *
   * @param {Object} event
   * @return {XML}
   */
  static battery(event) {
    const percent = _.get(event, 'data.percentRemaining');
    const days = _.get(event, 'data.daysRemaining');
    const battery = new Battery(percent, days);

    return battery.render();
  }
}

export default DataRenderer;
