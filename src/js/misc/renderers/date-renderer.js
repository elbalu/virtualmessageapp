import moment from 'moment';
import React from 'react';
import validate from 'validate.js';

import { NONE_VALUE, DATE_FORMAT, DATETIME_FORMAT } from './values';

/**
 * DateRenderer provides functions that render timestamps
 * and other moment-compatible date/datetime formats into
 * a uniform format for display on the UI.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class DateRenderer {
  /**
   * date() returns a render function
   * that parses a UTC string into a date string
   *
   * @returns {function}
   */
  static date() {
    return (value) => {
      if (validate.isEmpty(value)) return NONE_VALUE;
      return moment(value).format(DATE_FORMAT).toString();
    };
  }

  /**
   * datetime() returns a render
   * function that parses a UTC string into
   * a datetime string
   *
   * @returns {function}
   */
  static datetime() {
    return (value) => {
      if (validate.isEmpty(value)) return NONE_VALUE;
      return moment(value).format(DATETIME_FORMAT).toString();
    };
  }

  /**
   * timeFromNow() returns a render
   * function that parses a UTC string into
   * a time from now string
   * ex: a minute ago, a year ago etc
   *
   * @returns {function}
   */
  static timeFromNow() {
    return (value) => {
      if (validate.isEmpty(value)) return NONE_VALUE;
      const daysDiff = moment().diff(moment(value), 'days');
      if (daysDiff > 1) {
        return (
          <span className="non-active">{moment(value).fromNow()}</span>
        );
      }
      return (
        <span>{moment(value).fromNow()}</span>
      );
    };
  }

  /**
   * timeStayed() returns a render
   * function that parses a unix timestamp into
   * seconds/minutes/hours
   * ex: 5sec, 2min, 1hour
   *
   * @returns {function}
   */
  static timeStayed() {
    return (value) => {
      if (validate.isEmpty(value)) return NONE_VALUE;
      const val = moment.duration(value);
      const data = val._data;
      if (data.minutes) {
        return `${data.minutes} minutes`;
      }
      return `${data.seconds} seconds`;
    };
  }
}

export default DateRenderer;
