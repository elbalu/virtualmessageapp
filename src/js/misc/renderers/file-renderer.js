import React from 'react';

import { NONE_VALUE } from './values';

/**
 * FileRenderer provides methods to render
 * different types of files into a format
 * that can be displayed on the UI.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class FileRenderer {

  /**
   * render file list as a list
   * of images
   *
   * @param path {string} file list base path
   * @returns {function}
   */
  static images(path) {
    return (value) => {
      if (!value) return NONE_VALUE;

      return (
        <div>
          {value.map(({ name }, i) => (
            <img
              width={100}
              role="presentation"
              key={i}
              src={`uploads/${path}/${name}`}
            />
          ))}
        </div>
      );
    };
  }
}

export default FileRenderer;
