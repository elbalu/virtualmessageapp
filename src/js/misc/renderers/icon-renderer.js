import React from 'react';
import { FormattedNumber } from 'react-intl';
import Tooltip from 'antd/lib/tooltip';
import _ from 'lodash';

import { NONE_VALUE } from './values';

// icons representing priority (low, med, high)
const highPriority = <i className={'sdk-icon-minor-warning2 icon-medium red'} />;
const mediumPriority = <i className={'sdk-icon-minor-warning2 icon-medium yellow'} />;
const lowPriority = <i className={'sdk-icon-minor-warning2 icon-medium blue'} />;

/**
 * IconRenderer provides methods that render
 * various types of icons (which are typically stored
 * as objects or class names) into a format that
 * can be displayed on the UI.
 *
 * @author Benjamin Newcomer <bnewcomer@cisco.com>
 */
class IconRenderer {
  /**
   * render() returns a render method
   * that takes an icon object and shows the icon
   * and name.
   *
   * @returns {function}
   */
  static render() {
    return (value) => {
      const icon = value;

      if (!icon) return NONE_VALUE;

      return (
        <span className="icon-selector-option">
          <i className={`fa ${icon.iconCls}`} />
          <span className="label">{icon.name}</span>
        </span>
      );
    };
  }

  /**
   * battery() returns a function that
   * renders a percentage 0-100 as an icon representing
   * full, three quarters, half, one quarter, or no
   * battery life.
   *
   * @deprecated since 04/05/2017
   * @see {@link Battery#render}
   * @return {function(*=)}
   */
  static battery() {
    return (value) => {
      if (!value) return NONE_VALUE;

      let battery = <span />;

      if (value >= 75) battery = <i className={'fa fa-battery-full'} />;
      if (value < 75 && value >= 50) battery = <i className={'fa fa-battery-three-quarters'} />;
      if (value < 50 && value >= 25) battery = <i className={'fa fa-battery-half'} />;
      if (value < 25 && value >= 10) battery = <i className={'fa fa-battery-quarter'} />;
      if (value < 10) battery = <i className={'fa fa-battery-empty'} />;

      return (
        <span>
          <FormattedNumber value={value / 100} style="percent" />
          &nbsp;
          {battery}
        </span>
      );
    };
  }

  /**
   * priority() returns a function that
   * renders a priority as an icon representing
   * the intensity - low, medium, high or (1,2,3)
   * with different colors
   *
   * @param {boolean} [text] - Return only text
   * @return {function(*=)}
   */
  static priority(text = false) {
    return (value) => {
      if (!value) return NONE_VALUE;

      let priority = <span />;
      let message = value;

      // convert (high, medium, low) to icon
      if (_.isString(value)) {
        if (value.toLowerCase() === 'high') {
          priority = highPriority;
        }

        if (value.toLowerCase() === 'medium') {
          priority = mediumPriority;
        }

        if (value.toLowerCase() === 'low') {
          priority = lowPriority;
        }
      }

      // convert (3, 2, 1) to icon
      if (_.isNumber(value)) {
        if (value === 3) {
          priority = highPriority;
          message = 'high';
        }

        if (value === 2) {
          priority = mediumPriority;
          message = 'medium';
        }

        if (value === 1) {
          priority = lowPriority;
          message = 'low';
        }
      }

      if (text) return message;

      return (
        <span>
          <Tooltip placement="top" title={message}>
            {priority} {message}
          </Tooltip>
        </span>
      );
    };
  }
}

export default IconRenderer;
