import validate from 'validate.js';
import _get from 'lodash/get';

import { NONE_VALUE } from './values';

/**
 * IdRenderer provides methods to render
 * miscellaneous objects, arrays, and primitives
 * into strings suitable for display on the UI.
 * Also checks for -1
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class DefaultRenderer {

  /**
   * default column render function works for
   * primitives, objects, arrays of primitives,
   * and arrays of objects
   *
   * @param key {string} field key
   * @returns {function}
   */
  static render(key, view) {
    return (value, row) => {
      let parsedValue = value;

      if (validate.isEmpty(parsedValue)) {
        parsedValue = _get(row, key);

        if (validate.isEmpty(parsedValue)) {
          return (view === 'filter') ? '' : NONE_VALUE;
        }
      }

      if (parsedValue === -1 || parsedValue === '-1') return NONE_VALUE;

      if (validate.isArray(parsedValue)) {
        const displayValues = parsedValue.map(v => DefaultRenderer.render(key)(v));
        return displayValues.join(', ');
      }

      if (validate.isObject(parsedValue)) {
        return parsedValue.serial || parsedValue.name || parsedValue.label || parsedValue.id;
      }

      return String(parsedValue);
    };
  }
}

export default DefaultRenderer;
