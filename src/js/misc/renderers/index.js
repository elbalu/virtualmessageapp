import validate from 'validate.js';
import _includes from 'lodash/includes';
import _has from 'lodash/has';

import BooleanRenderer from './boolean-renderer';
import ClickableRenderer from './clickable-renderer';
import DateRenderer from './date-renderer';
import NumberRenderer from './number-renderer';
import SelectRenderer from './select-renderer';
import DefaultRenderer from './default-renderer';
import FileRenderer from './file-renderer';
import IconRenderer from './icon-renderer';
import IdRenderer from './id-renderer';
import LocationRenderer from './location-renderer';
import DataRenderer from './data-renderer';
import TemplateRenderer from './template-renderer';
import ParagraphRenderer from './paragraph-renderer';
import Battery from '../models/battery-model';
import config from '../../config';

import { NONE_CLASSNAME } from './values';

const fieldTypes = config.get('fieldTypes');


/**
 * Renderer provides utilities for rendering many different
 * data formats and data structures into formats that can be
 * displayed across the UI, especially in tables and forms.
 * Using the same Renderer to format data across the UI will
 * provide consistent user experience and a clean development
 * experience.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Renderer {

  /**
   * renderFactory() returns a single render
   * method depending on the field passed in
   *
   * @param field {Object} Field config object
   * @param callback {function} callback function for clickable fields
   * @returns {function}
   */
  static render(field, callback, view, collections) {
    const key = field.key;
    const type = field.type;
    const collection = (field.collection) ? collections[field.collection] : false;
    const viewDetails = field.clickableLink;

    if (key === 'location') {
      if (field.multiView) {
        return LocationRenderer.multiple(view);
      }
      return LocationRenderer.render();
    }

    if (key === 'hierarchy') {
      return LocationRenderer.hierarchyRender();
    }

    if ((key === 'sdkId' || key === 'status') && field.type !== 'TEXT') {
      if (view === 'filter') {
        return BooleanRenderer.render();
      }
      if (key === 'status') return BooleanRenderer.active(field);
      return BooleanRenderer.checkmark(field);
    }

    if (_includes(['tagLocation', 'tagBattery', 'tagTemperature', 'tagHumidity'], key)) {
      return DataRenderer.tagData(key);
    }

    if (key === 'template') {
      return TemplateRenderer.render();
    }

    if (key === 'lastHeard') {
      return DateRenderer.timeFromNow();
    }

    if (key === 'timeStayed') {
      return DateRenderer.timeStayed();
    }

    if (key === 'icon') {
      return IconRenderer.render();
    }

    if (key === 'displayId') {
      return IdRenderer.render();
    }

    if (viewDetails && validate.isFunction(callback)) {
      return ClickableRenderer.render(callback, key);
    }

    if (key === 'battery') {
      return value => (new Battery(value)).render();
    }

    if (key === 'priority') {
      if (view === 'filter') return IconRenderer.priority(true);
      return IconRenderer.priority();
    }

    if (type === fieldTypes.CHECKBOX) {
      return BooleanRenderer.render(field.options[0], field.options[1]);
    }

    if (type === fieldTypes.FILE) {
      return FileRenderer.images(field.path);
    }

    if (type === fieldTypes.DATETIME) {
      return DateRenderer.datetime();
    }

    if (type === fieldTypes.DATE) {
      return DateRenderer.date();
    }

    if (type === fieldTypes.TEXTAREA) {
      return ParagraphRenderer.render();
    }

    if (type === fieldTypes.NUMBER) {
      return NumberRenderer.render(field, key, view);
    }

    if (type === fieldTypes.SELECT && collection.length > 0) {
      //console.log(field, key, collection, view);
      return SelectRenderer.render(field, key, view, collection);
    }

    return DefaultRenderer.render(key, view);
  }

  /**
   * Returns true if the passed in value
   * is a "none" value, which are representations
   * of falsy values that can be displayed. The
   * "none" values are defined in ./values.js
   *
   * @param {*} value
   * @returns {boolean}
   */
  static isNoneValue(value) {
    // test is value is a span
    const isSpan = _has(value, 'type') &&
      value.type === 'span';

    // test if value (assuming it is a span)
    // has class NONE_CLASSNAME
    const hasNoneClass = _has(value, 'props.className') &&
      value.props.className.includes(NONE_CLASSNAME);

    return (isSpan && hasNoneClass);
  }
}

export default Renderer;
