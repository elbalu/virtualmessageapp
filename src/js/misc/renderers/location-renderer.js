import React from 'react';
import validate from 'validate.js';
import _ from 'lodash';
import { Popover, Table } from 'antd';

import { NONE_VALUE, MULTIPLE } from './values';

/**
 * LocationRenderer provides methods that
 * render locations and location data into a
 * format that can be displayed on the UI.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class LocationRenderer {

  /**
   * render() returns a function that
   * renders a location as a string (the
   * name of the map plus names of ancestors)
   *
   * @return {function(*=)}
   */
  static render() {
    return (value) => {
      if (validate.isEmpty(value)) return NONE_VALUE;
      return (value.hierarchy) ? value.hierarchy.replace(/!/g, ' -> ') : NONE_VALUE;
    };
  }

  /**
   * render() returns a function that
   * renders a location hierarchy as a string (the
   * name of the map plus names of ancestors)
   *
   * @return {function(*=)}
   */
  static hierarchyRender() {
    return (value) => {
      if (validate.isEmpty(value)) return NONE_VALUE;
      return (value) ? value.replace(/!/g, ' -> ') : NONE_VALUE;
    };
  }

  /**
   * isLocation() returns true if the object passed
   * in is a location object and can be rendered using
   * this renderer
   *
   * @param value
   */
  static isLocation(value) {
    return _.has(value, 'hierarchy');
  }

  /**
   * parses the geodata array
   * and prepares a new one to feed
   * the cascader form input
   * campus->building->floor
   *
   * @param {Object[]} geoData - Locations list
   *
   * @param {boolean} [any] - Include 'Any' option
   *
   * @returns {Object[]} cascader options
   */
  static prepareLocationsForCascader(geoData, any = false) {
    const options = _.map(geoData, m => LocationRenderer._prepareLocation(m));

    if (any) {
      options.unshift({
        value: '*',
        label: 'Any',
        isLeaf: true
      });
    }

    return options;
  }

  /**
   * LocationRenderer._prepareLocation is a helper
   * that coverts one location object into a
   * cascader entry.
   *
   * @private
   * @param {{ id:number, name:string, [children]:Array }} location
   * @param {number} maxDepth - maximum number of recursive calls
   * @param {number} currentDepth - current call number
   * @return {{ value:number, label:string, [children]:Array }}
   */
  static _prepareLocation(location, maxDepth = Infinity, currentDepth = 0) {
    // cascader entry
    const entry = {
      value: location.id,
      label: location.name,
    };

    // extract children of location
    const children = _.get(location, 'relationshipData.children', []);

    // prepare children data if this location has children
    // and we have not yet reached the max depth
    if (children.length > 0 && currentDepth < maxDepth) {
      entry.children = children.map(c => (
        LocationRenderer._prepareLocation(c, maxDepth, currentDepth + 1)
      ));
    }

    return entry;
  }

  /**
   * same as prepareDataForCascade
   * but send the data in server
   * readable format
   *
   * @param geoData {array}
   * @returns {array}
   */
  static prepareCascadedData(geoData) {
    const parseLevel = level =>
      level.map((l) => {
        const obj = {
          id: l.id,
          name: l.name,
        };

        if (l.children && l.children.length > 0 && l.level.toLowerCase() !== 'floor') {
          obj.children = parseLevel(l.children);
        }

        return obj;
      });

    return parseLevel(geoData);
  }

  /**
   * multiple() returns a function that
   * renders a location as a string (the
   * name of the map plus names of ancestors)
   * for single location, and returns 'multiple'
   * as string for multiple locations
   *
   * @return {function(*=)}
   */
  static multiple(view) {
    return (value) => {
      if (!(value instanceof Object)) return NONE_VALUE;
      if (value instanceof Object && !Array.isArray(value)) {
        return (value.hierarchy) ? value.hierarchy.replace(/!/g, ' -> ') : NONE_VALUE;
      }
      if (value.length > 1) {
        if (view === 'filter') {
          const displayValues = value.map(v => LocationRenderer.render()(v));
          return displayValues.join(', ');
        }
        const multiValues = _.uniqBy(value, 'floorId');
        return (
          <div>
            <Popover
              title=""
              placement="bottom"
              overlayClassName="multi-popover"
              content={this.renderMultiple(multiValues)}
              trigger="click"
            >
              <div className="detailsLink hyperlink multi-item-row ">
                <span className="multiple">{multiValues.length}</span>
                <span>{MULTIPLE}</span>
              </div>
            </Popover>
          </div>
        );
      }
      if (value.length === 1) {
        const location = value[0];
        return (location.hierarchy) ? location.hierarchy.replace(/!/g, ' -> ') : NONE_VALUE;
      }
      return NONE_VALUE;
    };
  }

  /**
   * multiItemRowDisplay() returns a function that renders
   * the content that goes inside the popover for multi
   * item view
   *
   * @param {array} items Array of items
   * @param {string} key Row key to render text (should be field.key if this
   *  renderer is used to render a field)
   * @param {function} callback Function that will be called when clicked
   */
  static renderMultiple(items) {
    const columns = [{
      dataIndex: 'name',
      render: (val, rec) => LocationRenderer.render()(rec)
    }];
    return (
      <Table
        className="table-small"
        columns={columns}
        rowKey={r => r.id}
        dataSource={items}
        pagination={{
          size: 'small',
          pageSize: 5
        }}
      />
    );
  }
}


export default LocationRenderer;
