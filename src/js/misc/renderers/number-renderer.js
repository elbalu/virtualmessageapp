import React from 'react';
import validate from 'validate.js';
import _get from 'lodash/get';

import { NONE_VALUE } from './values';

/**
 * NumberRenderer provides methods to render
 * miscellaneous objects, arrays, and primitives
 * into strings suitable for display on the UI.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class NumberRenderer {

  /**
   * default column render function works for
   * primitives, objects, arrays of primitives,
   * and arrays of objects
   *
   * @param key {string} field key
   * @returns {function}
   */
  static render(field, key, view) {
    return (value, row) => {
      let parsedValue = value;

      if (validate.isEmpty(parsedValue)) {
        parsedValue = _get(row, key);

        if (validate.isEmpty(parsedValue)) {
          return (view === 'filter') ? '' : NONE_VALUE;
        }
      }

      if (field.precision) {
        return parseFloat(parsedValue).toFixed(parseInt(field.precision, 10));
      }

      return parseFloat(parsedValue);
    };
  }
}

export default NumberRenderer;
