import React from 'react';
import validate from 'validate.js';
import _get from 'lodash/get';
import Tooltip from 'antd/lib/tooltip';

import { NONE_VALUE } from './values';

/**
 * paragraph column render function works for
 * long strings or strings inputted ffrom textarea's
 * truncates the overflowing data and displays as tooltip
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class ParagraphRenderer {

  /**
   * paragraph column render function works for
   * long strings or strings inputted ffrom textarea's
   * truncates the overflowing data and displays as tooltip
   *
   * @param key {string} field key
   * @returns {function}
   */
  static render(key) {
    return (value, row) => {
      let parsedValue = value;

      if (validate.isEmpty(parsedValue)) {
        parsedValue = _get(row, key);

        if (validate.isEmpty(parsedValue)) {
          return NONE_VALUE;
        }
      }

      if (validate.isArray(parsedValue)) {
        const displayValues = parsedValue.map(v => ParagraphRenderer.render(key)(v));
        return displayValues.join(', ');
      }

      if (validate.isObject(parsedValue)) {
        return parsedValue.serial || parsedValue.name || parsedValue.label || parsedValue.id;
      }

      return (
        <Tooltip title={parsedValue}>
          <div className="para-ellipsis">{parsedValue}</div>
        </Tooltip>
      );
    };
  }
}

export default ParagraphRenderer;
