import React from 'react';
import validate from 'validate.js';
import _get from 'lodash/get';
import { Popover, Table } from 'antd';
import { NONE_VALUE, MULTIPLE, CONNECTORS, CONTROLLERS } from './values';

/**
 * SelectRenderer provides methods to render
 * miscellaneous objects, arrays, and primitives
 * into strings suitable for display on the UI.
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class SelectRenderer {

  /**
   * Select column render function works for
   * primitives, objects, arrays of primitives,
   * and arrays of objects
   *
   * @param key {string} field key
   * @returns {function}
   */
  static render(field, key, view, collection) {
    return (value, row) => {
      let parsedValue = '';
      if (validate.isArray(value)) {
        parsedValue = (collection) ? value.map(v => collection.filter(item => item.id === v)[0]) : value;
      } else if (value === '-1') {
        parsedValue = 'NONE';
      } else {
        parsedValue = (collection) ? collection.filter(item => item.id === value) : value;
      }

      if (validate.isEmpty(parsedValue)) {
        parsedValue = _get(row, key);

        if (validate.isEmpty(parsedValue)) {
          return (view === 'filter') ? '' : NONE_VALUE;
        }
      }

      if (validate.isArray(parsedValue)) {
        if (parsedValue.length === 1) {
          parsedValue = parsedValue.map(item => item.serial || item.name || item.label || item.id)[0];
        } else {
          const displayValues = parsedValue.map(item => item.serial || item.name || item.label || item.id);
          return this.multiple(displayValues, field.collection);
        }
      }

      if (validate.isObject(parsedValue)) {
        return parsedValue.serial || parsedValue.name || parsedValue.label || parsedValue.id;
      }

      return String(parsedValue);
    };
  }
  /**
   * multiple() returns a function that
   * renders a location as a string (the
   * name of the map plus names of ancestors)
   * for single location, and returns 'multiple'
   * as string for multiple locations
   *
   * @return {function(*=)}
   */
  static multiple(multiValues, fieldCollection) {
    return (
      <div>
        <Popover
          title=""
          placement="bottom"
          overlayClassName="multi-popover"
          content={this.renderMultiple(multiValues)}
          trigger="click"
        >
          <div className="detailsLink hyperlink multi-item-row ">
            <span>{multiValues[0]}</span>
            <span className="multiple">+{multiValues.length - 1}</span>
          </div>
        </Popover>
      </div>
    );
  }

  /**
   * multiItemRowDisplay() returns a function that renders
   * the content that goes inside the popover for multi
   * item view
   *
   * @param {array} items Array of items
   * @param {string} key Row key to render text (should be field.key if this
   *  renderer is used to render a field)
   * @param {function} callback Function that will be called when clicked
   */
  static renderMultiple(items) {
    const columns = [{
      dataIndex: 'name',
      render: (val, rec) => SelectRenderer.render()(rec)
    }];
    return (
      <Table
        className="table-small"
        columns={columns}
        rowKey={r => r.id}
        dataSource={items}
        pagination={{
          size: 'small',
          pageSize: 5
        }}
      />
    );
  }
}

export default SelectRenderer;
