import validate from 'validate.js';
import _isArray from 'lodash/isArray';
import _get from 'lodash/get';
import _reject from 'lodash/reject';

import { NONE_VALUE } from './values';

/**
 * TemplateRenderer provides functions that render
 * templates into readable formats
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class TemplateRenderer {

  /**
   * render() returns a function
   * that renders a template object by joining all
   * field labels into a comma-separated list
   *
   * @returns {function}
   */
  static render() {
    return (value) => {
      if (validate.isEmpty(value) || validate.isEmpty(value.fields)) return NONE_VALUE;
      return value.fields.map(field => field.label).join(', ');
    };
  }

  /**
   * returns true if the value passed
   * in is a template
   *
   * @param value
   */
  static isTemplate(value) {
    return _isArray(_get(value, 'fields'));
  }

  /**
   * returns a template where the fields have
   * been filtered to only include fields that
   * DO NOT have the type given.
   *
   * @param {Template} template
   * @param {String} type
   * @return {Template}
   */
  static rejectFieldsByType(template, type) {
    if (!TemplateRenderer.isTemplate(template)) return template;
    if (!type) return template;

    // reject fields with the given type
    const newFields = _reject(template.fields, field => field.type === type);
    return { ...template, fields: newFields };
  }

  /**
   * returns a blank template
   *
   * @return {{entityName: null, fields: Array}}
   */
  static blankTemplate() {
    return {
      entityName: null,
      fields: [],
    };
  }
}

export default TemplateRenderer;
