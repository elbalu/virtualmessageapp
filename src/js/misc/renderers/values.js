import React from 'react';
import { FormattedMessage } from 'react-intl';

export const NONE_CLASSNAME = 'none';

/**
 * Value if given value is falsy.
 * Classname none is used to identify
 * this span as a "none" value
 *
 * @type {XML}
 */
export const NONE_VALUE = (
  <span className={`light-gray ${NONE_CLASSNAME}`}><FormattedMessage id="none" /></span>
);

/**
 * Value if given value is falsy
 * Classname none is used to identify
 * this span as a "none" value
 *
 * @type {XML}
 */
export const NONE_VALUE_NO_COLOR = (
  <span className={NONE_CLASSNAME}><FormattedMessage id="none" /></span>
);

/**
 * Value if value given is falsy and field is
 * a data field (eg location, temperature, etc)
 * Classname none is used to identify
 * this span as a "none" value
 *
 * @type {XML}
 */
export const NO_DATA_VALUE = (
  <span className={`light-gray ${NONE_CLASSNAME}`}><FormattedMessage id="noData" /></span>
);

/**
 * Value if value given is falsy and
 * field is a temperature field.
 * Classname none is used to identify
 * this span as a "none" value
 *
 * @type {XML}
 */
export const NO_TEMP_DATA_VALUE = (
  <span className={`light-gray ${NONE_CLASSNAME}`}><FormattedMessage id="noTemperatureData" /></span>
);

/**
 * generic value when there is no
 * telemetry data for the tag.
 * Classname none is used to identify
 * this span as a "none" value
 *
 * @type {XML}
 */
export const NO_TELEMTRY_DATA = (
  <span className={`light-gray ${NONE_CLASSNAME}`}><FormattedMessage id="noTelemetryData" /></span>
);

/**
 * table format for datetime
 *
 * @type {string}
 */
export const DATETIME_FORMAT = 'MMM Do, YYYY hh:mm:ss A';

/**
 * table format for date
 *
 * @type {string}
 */
export const DATE_FORMAT = 'ddd, MMM Do, YYYY';

// string that represents null because
// Ant Design Table filter values
// must be strings
export const NULL_PLACEHOLDER = '_null_';

export const MULTIPLE = <FormattedMessage id="multiple" />;

export const CONTROLLERS = <FormattedMessage id="controllers" />;
export const CONNECTORS = <FormattedMessage id="connectors" />;

export const YES = <FormattedMessage id="yes" />;

export const NO = <FormattedMessage id="no" />;
