import _ from 'lodash';
import React from 'react';

import Fields from '../../components/form-generator/fields';
import Renderer from '../renderers';
import { compareRenderedValues } from '../helpers';

// field types that can be filtered
const FILTERABLE_FIELD_TYPES = [
  Fields.Types.TEXT,
  Fields.Types.NUMBER,
  Fields.Types.CHECKBOX,
  Fields.Types.SELECT,
  Fields.Types.RADIO,
  Fields.Types.DATE,
  Fields.Types.DATETIME,
  Fields.Types.TEL,
  Fields.Types.PARAGRAPH,
  Fields.Types.OPTIONS,
];

// fields that have a field type
// that is not filterable by default
// but that have a renderer and therefore
// can be filtered
const FILTERABLE_FIELD_KEYS = [
  'location',
  'template',
];

/**
 * Returns true if the field type can be filtered.
 * Fields that have renderers and where filtering
 * makes semantic sense (it would not make sense for
 * a unique field for example).
 *
 * @param {Object} field Field type
 * @returns {boolean}
 */
const fieldIsFilterable = (field) => {
  const type = field.type;
  const key = field.key;
  const filterable = (field.filterable === undefined) ? true : field.filterable;

  // if field not filterable then return
  if (!filterable) return false;

  // category template does not need to be filterable
  if (key === 'template') return false;

  // field is filterable if its key is in
  // FILTERABLE_FIELD_KEYS
  if (FILTERABLE_FIELD_KEYS.includes(key)) return true;

  // field is filterable if its type
  // is in FILTERABLE_FIELD_TYPES
  return FILTERABLE_FIELD_TYPES.includes(type);
};

/**
 * Returns a sorted list of filters.
 * Sorting policy is defined here.
 * Filters are typically sorted alphabetically,
 * except None, No Data, or other
 * renderings of falsy values are moved
 * to the end of the list.
 *
 * @param {Object[]} filters Unsorted filters
 * @returns {Object[]} Sorted filters
 */
const sortFilters = filters => filters.sort((a, b) => {
  const aText = _.get(a, 'text', '');
  const bText = _.get(b, 'text', '');
  return compareRenderedValues(aText, bText);
});

/**
 * Returns a function that filters records
 * based on value passed in as defined by
 * Ant Design Table component filtering
 *
 * @param {Object} field
 * @return {function}
 */
const onFilterFactory = field => (
  (filter, record) => {
    // extract row value and filter value to test equality. We add
    // toString() because ant converts filter values to strings before
    // this function is called. That means that values that use
    // react components will be given as "[object Object]"
    const rawValue = _.get(record, field.key);
    const value = Renderer.render(field, null, 'filter')(rawValue).toString();
    const multipleValues = field.multiple || field.type === Fields.Types.OPTIONS;

    // value can be a comma separated list
    if (multipleValues && _.isString(value)) return value.includes(filter);

    // value is a single value
    return value === filter;
  }
);

/**
 * returns a list of filter objects and an
 * onFilter function that are compatible with
 * the Ant Design Table component filtering.
 *
 * @param {object} field
 * @param {Array} dataSource
 * @returns {object}
 */
const filterFactory = (field, dataSource) => {
  const key = field.key;

  // return blank filter data if field
  // type is not filterable
  if (!fieldIsFilterable(field)) {
    return { filters: [], onFilter: null };
  }

  // for each table row, extract the desired property
  const dataValues = dataSource.map(row => _.get(row, key));

  // if the properties are arrays, flatten them
  const flattenedData = _.flatten(dataValues);

  // generate a filter object for each value
  const unsortedFilters = flattenedData.filter(value => (!_.isNil(value)))
  .map(value => ({
    value: Renderer.render(field, null, 'filter')(value),
    text: Renderer.render(field, null, 'filter')(value),
  }));

  // make list unique by the filter's text
  const uniqueFilters = _.uniqBy(unsortedFilters, 'text');

  // sort filters alphabetically
  const filters = sortFilters(uniqueFilters);

  // create onFilter function that performs
  // filtering based on the record passed in
  const onFilter = onFilterFactory(field);

  return { filters, onFilter };
};

export default filterFactory;
