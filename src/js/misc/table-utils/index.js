import React from 'react';
import _ from 'lodash';

import Renderer from '../renderers';
import filterFactory from './filter';
import { compareRenderedValues } from '../helpers';
/**
 * default sorter method for objects
 *
 * @param {Object} field
 * @returns {1|0}-1}
 */
export const sorterFactory = field => (
  (row1, row2) => {
    const a = Renderer.render(field, null, 'filter')(_.get(row1, field.key));
    const b = Renderer.render(field, null, 'filter')(_.get(row2, field.key));
    return compareRenderedValues(a, b);
  }
);

/**
 * compareFields() takes in two templates
 * and returns true if they have the same
 * fields, false otherwise.
 *
 * @param fieldsA
 * @param fieldsB
 */
export const compareFields = (fieldsA, fieldsB) => {
  if (!fieldsA || !fieldsB) return false;
  if (fieldsA.length !== fieldsB.length) return false;

  const keysA = fieldsA.map(f => f.key);
  const keysB = fieldsB.map(f => f.key);

  return (_.intersection(keysA, keysB).length === 0);
};

/**
 * actionColumn() returns a Table compatible column
 * definition with two buttons for editing or deleting
 * a table row. See Table component for more information.
 *
 * @param {function} edit Edits an entity
 * @param {function} remove Removes an entity
 * @param {number} [id] Column identifier
 * @param {{ string:function }} [actions] - additional actions
 * @return {{
 *  id: {number},
 *  key: {string},
 *  title: {string},
 *  show: {boolean},
 *  render: {function}
 * }}
 */
export const actionColumn = ({ edit, remove, id = 0 }, actions) => ({
  id,
  key: 'actions',
  title: 'Actions',
  className: 'table-column-actions',
  width: 160,
  show: true,
  fixed: 'right',
  render: (row) => {
    const actionItems = [];
    const defaultCls = (row.default) ? 'disabled' : '';
    // remove button
    if (remove) {
      actionItems.push(
        <i
          key={String(actionItems.length)}
          title="remove"
          className={`action-icon action-remove clickable fa fa-remove ${defaultCls}`}
          onClick={(e) => {
            e.stopPropagation();
            remove(row);
          }}
        />,
      );
    }
    // edit button
    if (edit) {
      actionItems.push(
        <i
          key={String(actionItems.length)}
          title="edit"
          className={'action-icon action-edit clickable fa fa-pencil'}
          onClick={(e) => {
            e.stopPropagation();
            edit(row);
          }}
        />,
      );
    }

    // additional actions. Each action is a function
    // that accepts row and returns a component that
    // performs an action when clicked
    if (!_.isEmpty(actions)) {
      _.each(actions, action => actionItems.push(action(row)));
    }

    return <div>{actionItems}</div>;
  },
});

/**
 * buildColumn() creates a Table compatible column definition. See
 * Table component for more information.
 *
 * @param {number} id A unique column identifier
 * @param {Object} field Object representing a single
 *  field with fields like key, required, type, etc
 *  @param {string} details
 *  @param {Array} [columnVisibility] Array of column keys and
 *  visibility flag
 * @param {Array} dataSource table data
 * @returns {Object}
 */
export const buildColumn = ({
  id,
  field,
  details,
  columnVisibility,
  dataSource,
  collections,
  filteredValue,
  actionsIntegrated
}) => {
  const key = field.key;
  const className = `table-column-${key}`;
  const title = field.label;
  const dataIndex = key;
  const render = Renderer.render(field, details, null, collections);
  const sortFn = (field.sortable === false) ? null : sorterFactory(field);
  const sorter = (actionsIntegrated && field.sortable !== false) ? true : sortFn;
  const { filters, onFilter } = filterFactory(field, dataSource);
  const colWidth = field.width || 'auto';
  let show = true;

  if (columnVisibility) {
    const savedVisibility = columnVisibility.find(c => c.key === field.key);
    if (savedVisibility) show = savedVisibility.show;
  }

  return {
    id,
    key,
    show,
    render,
    sorter,
    title,
    dataIndex,
    filters,
    onFilter,
    className,
    width: colWidth,
    filteredValue,
  };
};

/**
 * buildColumns() builds column definitions that are compatible
 * with the Table component using the buildColumn() function
 *
 * @param {Object} template Schema/template object
 * @param {function} details Callback to open details modal
 * @param {Array} [columnVisibility] Array of column keys and visibility flag
 * @param {Array} dataSource table data
 * @param {boolean} [showEditableOnly] True to filter out readonly fields
 * @returns {Array}
 */
export const buildColumns = ({
  template,
  details,
  columnVisibility,
  dataSource,
  collections,
  showEditableOnly,
  filteredInfo,
  actionsIntegrated
}) => {
  if (!template) return [];
  const { fields } = template;
  if (!_.isArray(fields)) return [];

  // hidden && (not showEditableOnly or editable)
  const visibleFields = fields
    .filter(f => !f.hidden)
    .filter(f => !showEditableOnly || f.editable);

  // sort fields by the columnVisibility array
  // lower index values represent higher sort values
  let orderedFields = visibleFields;
  if (columnVisibility) {
    orderedFields = orderedFields.sort((a, b) => {
      const aValue = columnVisibility.find(c => c.id === a.id);
      const bValue = columnVisibility.find(c => c.id === b.id);

      if (!aValue && !bValue) return 0;
      if (!aValue) return 1;
      if (!bValue) return -1;

      if (aValue.index > bValue.index) return 1;
      if (aValue.index < bValue.index) return -1;
      return 0;
    });
  }
  // generate a column for each field
  return orderedFields
    .map((field, index) => buildColumn({
      field,
      details,
      columnVisibility,
      dataSource,
      collections,
      id: index + 1,
      filteredValue: filteredInfo[field.key] || null,
      actionsIntegrated
    }));
};