import _values from 'lodash/values';

const lsKey = 'sdk-table';
const LocalStorage = localStorage;

/**
 * getColumnsKey returns a string key that
 * can be used to save and retrieve column data in local
 * storage.
 *
 * @param tableName
 */
const getColumnsKey = tableName => `${lsKey}:${tableName}:columns`;


/**
 * retrieveColumns() returns a list of column
 * keys for columns that should be visible
 *
 * @param tableName
 */
export const retrieveColumns = (tableName) => {
  const columnKey = getColumnsKey(tableName);
  const columnsString = LocalStorage.getItem(columnKey);
  if (!columnsString) return null;
  return _values(JSON.parse(columnsString).columnVisibility);
};

/**
 * saveColumns() saves keys of columns where
 * column.show = true
 *
 * @param tableName
 * @param columns
 */
export const saveColumns = (tableName, columns) => {
  const columnKey = getColumnsKey(tableName);
  // map columns to object with format {[key]: { show:boolean, index: number }}
  const columnVisibility = columns.map((c, index) => ({ key: c.key, show: c.show, index }));
  return LocalStorage.setItem(columnKey, JSON.stringify({ columnVisibility }));
};

/**
 * getPaginationKey returns a string key that
 * can be used to save and retrieve column data in local
 * storage.
 *
 * @param tableName
 */
const getPaginationKey = tableName => `${lsKey}:${tableName}:pagination`;

/**
 * savePaginationConfig() saves keys of columns where
 * column.show = true
 *
 * @param tableName
 * @param {Object} config
 */
export const savePaginationConfig = (tableName, config) => {
  const paginationKey = getPaginationKey(tableName);
  const configString = JSON.stringify({ config });
  LocalStorage.setItem(paginationKey, configString);
};

/**
 * retrieveColumns() returns a list of column
 * keys for columns that should be visible
 *
 * @param tableName
 */
export const retrievePaginationConfig = (tableName) => {
  const paginationKey = getPaginationKey(tableName);
  const paginationConfig = LocalStorage.getItem(paginationKey);
  if (!paginationConfig) return null;
  return JSON.parse(paginationConfig).config;
};
