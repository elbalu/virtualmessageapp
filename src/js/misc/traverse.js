import _ from 'lodash';
import update from 'immutability-helper';

/**
 * defaultGetValue gets the value
 * from a node by returning the whole
 * node.
 */
const defaultGetValue = _.identity;

/**
 * defaultGetChildren gets children
 * from a node by returning
 * node.children
 *
 * @param {Object} node
 */
const defaultGetChildren = node => node.children;

/**
 * defaultSetChildren attaches children
 * to a node at the property 'children'
 *
 * @param {Object} node
 * @param {Array} children
 */
const defaultSetChildren = (node, children) => update(node, {
  children: { $set: children }
});

/**
 * traverse visits each node in a tree and
 * applies a getValue function to extract or transform
 * a node. It uses a children getter and setter to find
 * children on a node (it checks node.children by default).
 * traverse returns the transformed tree.
 *
 * @param {Object|Array} node - tree node or list of nodes
 *
 * @param {function} [getValue] - Function to get the
 * value of a node. Accepts (node:object) as arguments
 * and must return an object or null.
 *
 * @param {function} getChildren - Function to get the
 * children of a node. Accepts (node:object) and arguments
 * and must return an array.
 *
 * @param {function} setChildren - Function to reattach
 * child values to the node value. Accepts (nodeValue:object,
 * childValues:Array) as arguments and returns an object,
 * the node with children attached.
 *
 * @return {Object|null}
 */
export default function traverse(
  node,
  getValue = defaultGetValue,
  getChildren = defaultGetChildren,
  setChildren = defaultSetChildren
) {
  if (_.isNil(node)) return null;
  if (_.isEmpty(node)) return null;

  // process each node in array
  if (_.isArray(node)) {
    return node.map(n =>
      traverse(n, getValue, getChildren, setChildren));
  }

  // get value for single node
  const value = getValue(node);
  const children = getChildren(node);

  if (!_.isEmpty(children)) {
    // traverse children
    const childValues = children.map(c =>
      traverse(c, getValue, getChildren, setChildren));

    // set child values and return
    return setChildren(value, childValues);
  }

  return value;
}
