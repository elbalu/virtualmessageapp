import config from '../config';

const baseUrls = config.get('api');
const envConfig = config.get('envConfig');
const hostname = location.hostname;
const protocol = location.protocol;

const url = (path) => {
  // local development with services running locally
  if (process.env.NODE_ENV === 'development') {
    return envConfig.development[path];
  }
  // local development with services pointing to sandbox, test etc
  if (hostname === config.get('localDomain')) {
    return `${envConfig[process.env.NODE_ENV]}/${baseUrls[path]}`;
  }
  // all other deployments
  return `${protocol}//${hostname}/${baseUrls[path]}`;
};

/**
 * all urls used by the UI
 */
export const urls = {
  roles: `${url('authService')}/scope`,
  policies: `${url('authService')}/role`,
  currentUser: `${url('authService')}/user/current/user`,
  login: `${url('authService')}/application/token`,
  logout: `${url('tenantServices')}`,
  tokenAuth: `${url('tokenAuthenticationService')}/application/token`,
  users: `${url('authService')}/user`,
  userInvite: `${url('authService')}/user/invite`,
  password: `${url('authService')}/user/password`,

  areas: `${url('mapsService')}/basehierarchy`,
  maps: `${url('mapsService')}`,
  mapElement: `${url('mapsService')}/mapElement`,
  mapsFetchParent: `${url('mapsService')}/parent`,
  floormap: `${url('mapsService')}/images/floor`,
  nomap: `${url('ui')}/floormaps/nomap.png`,

  timeline: `${url('timelineService')}`,

  widgets: `${url('widgetService')}`,

  import: {
    maps: `${url('mapsService')}/importMap`,
    //maps: `${url('mapsService')}/import`,
  },

  cmxService: `${url('cmxService')}/locationengine`,
  tenantaccess: `${url('deviceManager')}/tenantaccess`,
  wlcs: `${url('deviceManager')}/config/controller`,
  connectors: `${url('deviceManager')}/config/connector`,
  clients: `${url('apiServices')}/clients`,
  poi: `${url('authService')}/points`,
  path: `${url('authService')}/routing`,
  deviceCounts: `${url('apiServices')}/clients/count`,
};

export default urls;
