import React from 'react';
import Modal from 'antd/lib/modal';
import { hashHistory } from 'react-router';
import req from '../misc/agent';
import url from '../misc/url';
import config from '../config';
import _ from 'lodash';

const messages = config.get('messages');

const userLsKey = 'location-react-user-key#fb14fc';
const userToken = 'location-user-token';
const userTenantId = 'userTenantId';
const userBuildingId = 'user-building-id';
const userFloorId = 'user-floor-id';

const sdkQuery = 'user-sdk-query';

// Set current feature (campus/ building) to
// retain the state in dashboard
const currentFeature = 'user-current-feature';

const storage = window.localStorage;
/*eslint-disable*/
export const getUser = () => {
  const userKey = storage.getItem(userLsKey);
  // console.log("userUtils > userKey: ", userKey);
  return (userKey) ? JSON.parse(userKey) : null;
}

/**
 * format full name for use in the
 * app header (or elsewhere where max
 * length is relevant)
 *
 * @param firstName
 * @param lastName
 * @param maxLength
 * @returns {string}
 */
export const formatName = (firstName, lastName, maxLength = 25) => {
  const combinedName = `${firstName} ${lastName}`;

  if (combinedName.length > maxLength - 3) {
    return `${combinedName.substring(0, maxLength - 3)}...`;
  } else {
    return combinedName;
  }
};

//set user after login
export const login = user => {
  storage.setItem(userLsKey, JSON.stringify(user));
};

export const getToken = () => (storage.getItem(userToken));
export const getTempToken = () => {
  return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im5kdWhhaW1lQGNpc2NvLmNvbSIsInVzZXJJZCI6MSwidGVuYW50aWQiOjEsInR5cGUiOiJ1c2VyX2FjY2Vzc190b2tlbiIsImlieSI6IkNvbW1vbiBTZXJ2aWNlcyIsIml0byI6IkxvY2F0aW9uIiwiaWF0IjoxNTA2NTM3MjIwLCJleHAiOjE1MDc3NDY4MjB9.FY7vS0Qe7wSaUdTB-lcuBvnVXFMjL3c5w6Q0RtFx9o8';
};
export const getTenantId = () => (storage.getItem(userTenantId));
export const setToken = token => storage.setItem(userToken, token);
export const setTenantId = (tenantId) => storage.setItem(userTenantId, tenantId);

export const getAssetsQuery = () => (storage.getItem(sdkQuery));
export const setAssetsQuery = query => storage.setItem(sdkQuery, JSON.stringify(query));

export const getLocalCurrentFeature = () => (JSON.parse(storage.getItem(currentFeature)));
export const setLocalCurrentFeature = feature => storage.setItem(currentFeature, JSON.stringify(feature));

//every page load set token and load initial data
export const setAuthHeader = (token, request) => {
  if (!request) request = req;

  if (!token) {
    token = getToken();
  }
  request.set('Authorization', `JWT ${token}`);
};

export const hasAccess = (type, action) => {
  const user = getUser();
  if (user) {
    const resources = user.resources;
    if (resources) {
      return resources.includes(`${type}:${action}`);
    }
  }
  return false;
};

/**
 * apendFullName appends the property 'name'
 * to the user object based on the properties
 * user.firstName and user.lastName. If a suffix
 * property is given, that property is added to
 * the name in angle brackets.
 *
 * @param {Object} user - Auth Service User object
 * @param {string} [suffix] - User property
 * @return {Object|null} - User with name appended
 */
const appendFullName = (user, suffix) => {
  // generate name from firstName/lastName
  const name = `${user.firstName} ${user.lastName}`;
  const postName = _.get(user, suffix, '') || '';
  const hasSuffixProp = _.has(user, suffix);

  // return user name appended
  if (_.isEmpty(name.trim())) return null;
  if (hasSuffixProp) return { ...user, name: `${name} <${postName}>` };
  return { ...user, name }
};


/**
 * appendFullNameToUsers takes in a users
 * collection and appends the derived property
 * name to each user by combining firstName
 * and lastName.
 *
 * @param {Object[]} users
 * @param {string} [suffix] - optional additional field
 * name to append to the end of users' names (eg 'email')
 * @return {Object[]}
 */
export const appendFullNameToUsers = (users, suffix) => {
  const usersWithName = _.map(users, u => appendFullName(u, suffix));
  const usersWithNoNulls = _.compact(usersWithName);
  return _.uniqBy(usersWithNoNulls, 'name');
};

export const setBuildingId = (buildingId) => (storage.setItem(userBuildingId, buildingId));
export const getBuildingId = () => (storage.getItem(userBuildingId));
export const deleteBuildingId = () => (storage.removeItem(userBuildingId));

export const setLocalFloorId = (floorId) => (storage.setItem(userFloorId, floorId));
export const getLocalFloorId = () => (storage.getItem(userFloorId));
export const deleteLocalFloorId = () => (storage.removeItem(userFloorId));


export const logout = () => {
  storage.setItem(userLsKey, JSON.stringify(null));
  storage.setItem(userToken, JSON.stringify(null));
  storage.setItem(userTenantId, JSON.stringify(null));
  deleteBuildingId();
  deleteLocalFloorId();
  window.location.replace(url.logout);
};


export const showLogoutModal = () => {
  Modal.info({
    title: messages['password.change.title'],
    okText: messages['ok'],
    content: (
      <div>
        <p>{messages['password.change.message']}</p>
      </div>
    ),
    onOk() {
      logout();
    },
  });
};
