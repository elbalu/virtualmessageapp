/**
 * UserSnapWidget is a class that wraps the Usersnap
 * JavaScript API and provides only the functionality
 * required by the UI.
 *
 * @class
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class UserSnapWidget {
  /**
   * UserSnapWidget.init creates a script
   * tag that requests the usersnap widget
   * and appends that tag to the dom body.
   */
  static init() {
    const document = global.document;

    // define usersnap widget
    const widget = document.createElement('script');
    widget.async = true;
    widget.type = 'text/javascript';
    widget.src = '//api.usersnap.com/load/263379f3-fa95-47e5-8717-b768fe54dc35.js';

    // insert widget into the DOM before the first script node
    document.body.insertBefore(widget, document.body.childNodes[1]);
  }

  /**
   * UserSnapWidget.open uses the UserSnap
   * JavaScript API to open the feedback window
   */
  static open() {
    if (!global.UserSnap) global.alert('The UserSnap Widget is not available on local development');
    else global.UserSnap.openReportWindow();
  }
}

export default UserSnapWidget;

