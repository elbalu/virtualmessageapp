import _groupBy from 'lodash/groupBy';
import _forEach from 'lodash/forEach';
import _cloneDeep from 'lodash/cloneDeep';
import moment from 'moment';
import store from '../store';
import req from '../../misc/agent';
import url from '../../misc/url';
import { getToken } from '../../misc/user-utils';
import * as C from '../constants/alerts-constants';
import alertSchema from '../../../resources/schemas/alerts.json';
import alertHistorySchema from '../../../resources/schemas/alert-history.json';
import { getWidgets, getActiveWidgetCount } from './widget-actions';

export const getAllAlerts = () => {
  const state = store.getState();
  const params = state.table.params;
  return dispatch =>
    req
      .post(`${url.alerts}`)
      .send(params)
      .then(({ body }) => {
        dispatch({
          type: C.GET_ALL_ALERTS,
          payload: body,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.GET_ALL_ALERTS,
          payload: err,
          error: true,
        })
      );
};

export const getRecentAlerts = () => {
  const params = {
    pagination: {
      page: 0,
      limit: 10,
    }
  };
  return dispatch =>
    req
      .post(`${url.alerts}`)
      .send(params)
      .then(({ body }) => {
        dispatch({
          type: C.GET_RECENT_ALERTS,
          payload: body,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.GET_RECENT_ALERTS,
          payload: err,
          error: true,
        })
      );
};


export const getAlertsByLocation = query =>
  dispatch =>
    req.get(`${url.alertsCount}${query}&type=chart`)
      .then(({ body }) => {
        dispatch({
          type: C.GET_ALERTS_BY_LOCATION,
          payload: body,
        });
        // also update the alert count on header
        dispatch(getActiveWidgetCount({ page: 'alerts' }));
      }

      )
      .catch(err =>
        dispatch({
          type: C.GET_ALERTS_BY_LOCATION,
          payload: err,
          error: true,
        })
      );

export const getRulesAlertsByLocation = query =>
  dispatch =>
    req.get(`${url.alertsCount}${query}&type=chart`)
      .then(({ body }) =>
        dispatch({
          type: C.GET_RULES_ALERTS_BY_LOCATION,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_RULES_ALERTS_BY_LOCATION,
          payload: err,
          error: true,
        })
      );

export const getPriorityAlertsByLocation = query =>
  dispatch =>
    req.get(`${url.alertsCount}${query}&type=chart`)
      .then(({ body }) =>
        dispatch({
          type: C.GET_PRIORITY_ALERTS_BY_LOCATION,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_PRIORITY_ALERTS_BY_LOCATION,
          payload: err,
          error: true,
        })
      );

export const getCategoryAlertsByLocation = query =>
  dispatch =>
    req.get(`${url.alertsCount}${query}&type=chart`)
      .then(({ body }) =>
        dispatch({
          type: C.GET_CATEGORY_ALERTS_BY_LOCATION,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_CATEGORY_ALERTS_BY_LOCATION,
          payload: err,
          error: true,
        })
      );

export const getDepartmentAlertsByLocation = query =>
  dispatch =>
    req.get(`${url.alertsCount}${query}&type=chart`)
      .then(({ body }) =>
        dispatch({
          type: C.GET_DEPARTMENT_ALERTS_BY_LOCATION,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_DEPARTMENT_ALERTS_BY_LOCATION,
          payload: err,
          error: true,
        })
      );

export const prepareHistoryData = (alerts) => {
  const series = [];
  const categories = [];
  const groups = _groupBy(alerts, 'rule.name');
  _forEach(groups, (value, key) => {
    series.push({
      name: key,
      data: value.map(
        // i => [moment(i.triggeredAt).valueOf(), categories.length]
        i => [i.triggeredAt, categories.length]
      )
    });
    categories.push(key);
  });
  return {
    categories,
    series
  };
};

export const getAlertHistoryForAsset = (params) => {
  const query = {
    sdk: params.sdkId,
    startTime: params.startTime || moment().startOf('day').valueOf(),
    endTime: params.endTime || moment().endOf('day').valueOf(),
  };
  return dispatch =>
    req
      .get(`${url.alerts}`)
      .query(query)
      .then(({ body }) =>
        dispatch({
          type: C.GET_ALERTS_FOR_ASSET,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_ALERTS_FOR_ASSET,
          payload: err,
          error: true,
        })
    );
};

export const removeAlert = id =>
  dispatch =>
    req.delete(`${url.alerts}/${id}`)
      .send({ id })
      .then(() =>
        dispatch({
          type: C.REMOVE_ALERT,
          payload: id,
        })
      )
      .catch(error =>
        dispatch({
          type: C.REMOVE_ALERT,
          payload: error,
          error: true,
        })
      );


export const getAlertComments = (alertId) => {
  return dispatch =>
    req
      .get(`${url.alerts}/comments/${alertId}`)
      .then(({ body }) =>
        dispatch({
          type: C.UPDATE_SELECTED_ROW,
          payload: {
            comments: body,
            id: alertId,
          },
        })
      )
      .catch(err =>
        dispatch({
          type: C.UPDATE_SELECTED_ROW,
          payload: err,
          error: true,
        })
      );
};

export const selectRow = (row) => {
  return (dispatch) => {
    if (row.length === 1) {
      dispatch(getAlertComments(row[0].id));
    }
    dispatch(({
      type: C.SELECT_ROW,
      payload: row,
    }));
  };
};


export const addComments = params =>
  dispatch =>
  req.post(`${url.alerts}/comments`)
      .send(params)
      .then(({ body }) => {
        // if single alert comment
        if (params.alerts.length === 1) {
          dispatch(getAlertComments(params.alerts[0]));
        }
        dispatch({
          type: C.ADD_COMMENTS,
          payload: params,
        });
      })
      .catch(error =>
          dispatch({
            type: C.ADD_COMMENTS,
            payload: error,
            error: true,
          })
      );

export const updateAlert = alerts =>
  dispatch =>
    req.put(`${url.alerts}`)
    .send(alerts)
    .then(({ body }) => {
      // dispatch({
      //   type: C.UPDATE_ALERT,
      //   payload: body,
      // });
      dispatch(selectRow(alerts));
      dispatch(getAllAlerts());

      // TODO refresh the widgets
      dispatch(getWidgets({ page: 'alerts' }));
      dispatch(getActiveWidgetCount({ page: 'alerts' }));
    })
    .catch(error =>
        dispatch({
          type: C.UPDATE_ALERT,
          payload: error,
          error: true,
        })
    );

export const getAlertFields = () =>
  (dispatch) => {
    // creates a new copy every single time
    const copy = _cloneDeep(alertSchema);
    dispatch({
      type: C.SET_ALERT_FIELDS,
      payload: copy,
    });
  };


export const getAlertHistoryFields = () => ({
  type: C.SET_ALERT_HISTORY_FIELDS,
  payload: alertHistorySchema,
});

export const setActiveAlertCount = count => ({
  type: C.ACTIVE_ALERT_COUNT,
  payload: count,
});

export const rowSelection = rows => ({
  type: C.ROW_SELECTION,
  payload: rows,
});

export const closeModal = () => ({
  type: C.CLOSE_EDIT_MODAL,
});

export const openBulkEditModal = visibility => ({
  type: C.OPEN_BULK_EDIT_MODAL,
  payload: visibility,
});


// download is a two step process to include structured serach
// first ui makes a post request to send the filters
// backend saves the filters in cache and sends back a uuid
// then ui does a fetch to get the file
export const download = () => {
  const state = store.getState();
  const params = Object.assign({}, state.table.params);
  delete params.pagination;
  return dispatch =>
    req.post(`${url.alerts}/export`)
      .send(params)
      .then(({ body }) => {
        const exportId = body.id;
        global.window.location.href = `${url.alerts}/export?authtoken=${getToken()}&id=${exportId}`;
      }
      )
      .catch(err =>
        dispatch({
          type: C.GET_FILTER_LIST,
          payload: err,
          error: true,
        })
      );
};
