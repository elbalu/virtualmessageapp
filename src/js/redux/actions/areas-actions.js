import { message } from 'antd';
import _ from 'lodash';

import req from '../../misc/agent';
import url from '../../misc/url';
import { getToken } from '../../misc/user-utils';
import * as C from '../constants/areas-constants';
import Location from '../../misc/map/location';

const JWTToken = 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRJZCI6MSwiaWF0IjoxNTE2MTMyMDQ2fQ.-H_jUERFGE5qMSR0-toRhNgLew0P3_HzLW-PWeBKrXg';

const env = process.env.NODE_ENV;
const tenantId = env === 'test' ? 'tenantId=420' : 'tenantId=15';

/**
 * getAllAreas requests all areas from the backend.
 * Areas are also called maps or locations depending
 * on the context. Dispatches an action to set
 * the new list of locations at store.areas.locations.
 */
export const getAllAreas = callback => dispatch => (
  req.get(`${url.areas}?${tenantId}`)
      .set("Authorization", JWTToken)
      .end((err, res) => {
    if (err) {
      return dispatch({
        type: C.SET_ALL_AREAS,
        payload: err,
        error: true,
      });
    }
    if (_.isFunction(callback)) callback();

    if(res.body.map.length > 0) {
      return dispatch({
        type: C.SET_ALL_AREAS,
        payload: Location.toLocations(res.body.map),
      });
    } else {
      return dispatch({
        type: C.SET_ALL_AREAS,
        payload: [],
      });
    }

  })
);

/**
 * getAllFloors requests all floors from the backend.
 * This returns all the floors from all campuses
 */
export const getAllFloors = callback => dispatch => (
  req.get(`${url.maps}/level/FLOOR?${tenantId}`)
      .set("Authorization", JWTToken)
      .end((err, res) => {
    if (err) {
      return dispatch({
        type: C.SET_ALL_FLOORS,
        payload: err,
        error: true,
      });
    }

    if (_.isFunction(callback)) callback();
    return dispatch({
      type: C.SET_ALL_FLOORS,
      payload: Location.toFloors(res.body.map)
    });
  })
);

/**
 * getAreaById requests details for a single
 * location by id. Dispatches an action to update
 * the location in store.areas.locations and to
 * set the location as selected in
 * store.areas.selectedLocation
 *
 * @param {string} id - MapView Service location id
 */
export const getAreaById = id => (dispatch) => {
  if (id === '*' || id === null) {
    dispatch({ type: C.SELECT_AREA, payload: id });
  } else {
    // generate full url
    const path = `${url.maps}/${id}?${tenantId}`;

    // perform request to save location
    req.get(path)
        .set("Authorization", JWTToken)
        .end((err, res) => {
      // request error
      if (err) {
        return dispatch({
          type: C.SELECT_AREA,
          payload: err,
          error: true
        });
      }

      // create Location from data
      const location = new Location(res.body.map);

      // if the location is a building, get the zone
      // counts for its floors from another API
      if (Location.isBuilding(location)) {
        req.get(`${path}/count?${tenantId}`)
            .set("Authorization", JWTToken)
            .end((error, response) => {
          // request error
          if (error) {
            return dispatch({
              type: C.SELECT_AREA,
              payload: error,
              error: true
            });
          }

          // for each floor in the building, find the corresponding
          // floorCount and replace floor children (empty array) with
          // floorCount children
          const counts = response.body;
          location.setChildren(location.getChildren().map((floor) => {
            const floorWithCount = new Location(_.find(counts, ['id', floor.getId()]));
            return floor.setCounts(floorWithCount.getChildren());
          }));

          return dispatch({ type: C.SELECT_AREA, payload: location });
        });
      }

      // if the location is not a building, dispatch a SELECT_AREA
      // action with the location
      return dispatch({ type: C.SELECT_AREA, payload: location });
    });
  }
};

/**
 * @name fetchChildrenById
 *
 * @description Requests a location with
 * children and then dispatches an action
 * to attach the children to the location
 * in the store.
 *
 * @param {string} id - MapView Service location id
 */
export const fetchChildrenById = id => (dispatch) => {
  // generate full url
  const path = `${url.maps}/${id}?${tenantId}`;

  // perform request to get location
  req.get(path)
      .set("Authorization", JWTToken)
      .end((err, res) => {
    if (err) return dispatch({ type: C.SET_CHILDREN, payload: err, error: true });
    return dispatch({ type: C.SET_CHILDREN, payload: new Location(res.body.map) });
  });
};

/**
 * @name fetchParent
 *
 * @description Requestst the parent location
 * of the location with the given id and creates an
 * action to update the parent location in the
 * store. The parent location should already exist
 * in the store location tree because here it will
 * only be updated, not added.
 *
 * @param {string} id - Child Location id
 */
export const fetchParent = id => (dispatch) => {
  // generate full url to get location details
  const path = `${url.mapsFetchParent}/${id}?${tenantId}`;

  // perform request to get location
  req.get(path)
      .set("Authorization", JWTToken)
      .end((err, res) => {
    if (err) return dispatch({ type: C.UPDATE_AREA, payload: err, error: true });
    return dispatch({ type: C.UPDATE_AREA, payload: new Location(res.body.map) });
  });
};

/**
 * saveArea sends a request to update an area
 * and dispatches an action to update the location
 * in store.areas.locations.
 *
 * @param {Location} location - MapView Service location
 * object or instance of Location
 *
 * @param {function} callback - Function to execute
 * after saving location. Accepts (location:Location)
 * as arguments.
 */
export const saveArea = (location, callback) => (dispatch) => {
  // generate full url and get body from
  // object or Location instance
  const path = `${url.mapElement}/${location.id}?${tenantId}`;
  const body = (location instanceof Location) ? location.toJSON() : location;

  // perform request to save location
  req.put(path)
      .set("Authorization", JWTToken)
      .send(body).end((err) => {
    if (err) return dispatch({ type: C.UPDATE_AREA, payload: err, error: true });

    // user feedback
    message.success(`${body.name} has been saved`);
    if (callback) callback(location);
    return dispatch({ type: C.UPDATE_AREA, payload: location });
  });
};

/**
 * selectArea sets the given location
 * as store.areas.selectedLocation.
 *
 * @param {Location} location - Selected location
 */
export const selectArea = location => ({
  type: C.SELECT_AREA,
  payload: location
});

/**
 * @name selectZone
 *
 * @description Saves the selected
 * zone in the store
 *
 * @param {Location] zone
 */
export const selectZone = zone => ({
  type: C.SELECT_ZONE,
  payload: zone
});

/**
 * createArea sends a request to create a new area
 * and then dispatches a SET_AREA action with the
 * newly created location with an id.
 *
 * @param {Location} location
 *
 * @param {string|null} parentId - Id of parent location.
 * If this location is a campus, pass null for the parentId.
 */
export const createArea = (location, parentId) => (dispatch) => {
  // generate url
  const path = `${url.maps}/${parentId}?${tenantId}`;
  const body = (location instanceof Location) ? location.toJSON() : location;
  _.unset(body, 'id');

  // perform request to create location
  req.post(path)
      .set("Authorization", JWTToken)
      .send([body]).end((err, res) => {
    if (err) return dispatch({ type: C.ADD_AREA, payload: err, error: true });

    // user feedback
    message.success(`${location.getName()} has been created`);
    const id = _.get(res, 'body.id[0]', 0);
    location.setId(id);

    return dispatch({
      type: C.ADD_AREA,
      payload: {
        location,
        parent: parentId
      }
    });
  });
};

/**
 * removeArea sends a request to delete the
 * given location. It then dispatches a
 * REMOVE_AREA action.
 *
 * @param {Location} location - Location to delete
 */
export const removeArea = location => (dispatch) => {
  const path = `${url.maps}/${location.getId()}?${tenantId}`;

  req.delete(path)
      .set("Authorization", JWTToken)
      .end((err) => {
    if (err) return dispatch({ type: C.REMOVE_AREA, payload: err, error: true });

    // user feedback
    message.success(`${location.getName()} has been removed`);
    return dispatch({ type: C.REMOVE_AREA, payload: location });
  });
};

/**
 * @deprecated use getAllAreas()
 */
export const fetchAreas = () => (
  dispatch => (
    req.get(`${url.areas}?${tenantId}`)
      .set("Authorization", JWTToken)
      .end((err, res) => {
        if (err) {
          return dispatch({
            type: C.AREAS_FETCHAREAS,
            payload: err,
            error: true,
          });
        }

        return dispatch({
          type: C.AREAS_FETCHAREAS,
          payload: res.body,
        });
      })
  )
);

function setQueryString(options) {
  let qs = '?';
  if (options.floorid) {
    qs = `${qs}floorid=${options.floorid}`;
  }
  return qs;
}

export const fetchFeatures = options => (
    dispatch => (
    req.get(`${url.sdk}${setQueryString(options)}?${tenantId}`)
      .set("Authorization", JWTToken)
      .end((err, res) => {
        if (err) {
          return dispatch({
            type: C.AREAS_FETCHFEATURES,
            payload: err,
            error: true,
          });
        }

        return dispatch({
          type: C.AREAS_FETCHFEATURES,
          payload: res.body,
        });
      })
  )
);
