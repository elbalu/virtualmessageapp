import _toUpper from 'lodash/toUpper';

import url from '../../misc/url';
import * as C from '../constants/connectors-constants';
import { clearParams } from './table-actions';
import req from '../../misc/agent';
import store from '../store';

const processConnector = (connector) => {
  const processedConnector = connector;
  const associatedControllers = (typeof(connector["associatedControllers"]) !== "undefined")
      ? processedConnector.associatedControllers.map((item) => {
            return item.id;
        })
      : [];

  processedConnector.associatedControllers = associatedControllers;
  return processedConnector;
};

const payloadForEdit = (connector) => {
    const processedConnector = {};

    if(typeof(connector["associatedControllers"]) !== "undefined" ) {
        processedConnector["associatedControllers"] = connector["associatedControllers"];
    }

    if(typeof(connector["name"]) !== "undefined" ) {
        processedConnector["name"] = connector["name"];
    }

    return processedConnector;
};

export const getAllConnectors = () =>
  dispatch =>
      req.get(url.connectors)
      .then(({ body }) =>
        dispatch({
          type: C.GET_ALL_CONNECTORS,
          payload: body,
        })
      ).catch(err =>
        dispatch({
          type: C.GET_ALL_CONNECTORS,
          payload: err,
          error: true,
        })
      );

export const getFilteredList = () => {
  const state = store.getState();
  const params = state.table.params;

  return dispatch =>
    req.post(`${url.connectors}/search`)
      .send(params)
      .then(({ body }) => {
        return dispatch({
          type: C.GET_FILTERED_CONNECTORS,
          payload: body,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.GET_FILTERED_CONNECTORS,
          payload: err,
          error: false,
        })
      );
};

export const addConnector = connector => (
  (dispatch) => {
    // convert connector mac address to uppercase
    const processedConnector = processConnector(connector);

    req.post(url.connectors)
    .send(processedConnector)
    .then(({ body }) => {
      dispatch(getFilteredList());
      dispatch({
        type: C.ADD_CONNECTOR,
        payload: { ...body }, //, id: body.id },
      });
    })
    .catch(error => dispatch({
      type: C.ADD_CONNECTOR,
      payload: error,
      error: true,
    }));
  }
);


export const removeConnector = id =>
  dispatch =>
      req.delete(`${url.connectors}/${id}`)
      .send({ id })
      .then(() => {
        // dispatch(clearParams());
        // dispatch(getFilteredList());
        dispatch({
          type: C.REMOVE_CONNECTOR,
          payload: id,
        });
      }
      )
      .catch((error) =>
        dispatch({
          type: C.REMOVE_CONNECTOR,
          payload: error,
          error: true,
        })
      );

export const editConnector = connector => (
  (dispatch) => {
    const processedConnector = processConnector(connector);
    const editPayload = payloadForEdit(processedConnector);
    req.put(`${url.connectors}/${connector.id}`)
    .send(editPayload)
    .then(() =>
        dispatch({
          type: C.EDIT_CONNECTOR,
          payload: connector,
        })
    )
    .catch((error) =>
        dispatch({
          type: C.EDIT_CONNECTOR,
          payload: error,
          error: true,
        })
    );
  }
);

export const getConnectorDetails = (id, entity) => {
    let startTime = null;
    let endTime = null;
    if (entity) {
      startTime = moment(entity.triggeredAt).startOf('day').utc().format();
      endTime = moment(entity.triggeredAt).endOf('day').utc().format();
    }
    return (dispatch) => {
      dispatch({
        type: C.GET_CONNECTOR_DETAILS,
        payload: { id },
      });
      req.get(`${url.connectors}/${id}`)
        .then(({ body }) => {
          dispatch({
            type: C.GET_CONNECTOR_DETAILS,
            payload: body,
          });
        }
      )
        .catch(err =>
          dispatch({
            type: C.GET_CONNECTOR_DETAILS,
            payload: err,
            error: true,
          })
        );
    };
};