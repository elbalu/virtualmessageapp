
import req from '../../misc/agent';
import url from '../../misc/url';

import * as C from '../constants/device-constants';


export const getAllClients = () =>
    dispatch =>
        req.get(url.clients + '?deviceType=CLIENT')
            .then(({ body }) => {
                    return dispatch({
                        type: C.GET_ALL_CLIENTS,
                        payload: body.results,
                    });
                }
            )
            .catch(err =>
                dispatch({
                    type: C.GET_ALL_CLIENTS,
                    payload: err,
                    error: true,
                })
            );

export const getClientsByFloor = floorID =>
    dispatch =>
        req.get(`${url.clients}?deviceType=CLIENT&floorId=${floorID}&format=geojson`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_CLIENTS_BY_FLOORID,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_CLIENTS_BY_FLOORID,
                    payload: err,
                    error: true,
                })
            );

export const getRoguesByFloor = floorID =>
    dispatch =>
        req.get(`${url.clients}?deviceType=ROGUE_CLIENT&floorId=${floorID}&format=geojson`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_ROGUES_BY_FLOORID,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_ROGUES_BY_FLOORID,
                    payload: err,
                    error: true,
                })
            );


export const getRogueAPsByFloor = floorID =>
    dispatch =>
        req.get(`${url.clients}?deviceType=ROGUE_AP&floorId=${floorID}&format=geojson`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_ROGUE_APS_BY_FLOORID,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_ROGUE_APS_BY_FLOORID,
                    payload: err,
                    error: true,
                })
            );


export const getTagsByFloor = floorID =>
    dispatch =>
        req.get(`${url.clients}?deviceType=TAG&floorId=${floorID}&format=geojson`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_TAGS_BY_FLOORID,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_TAGS_BY_FLOORID,
                    payload: err,
                    error: true,
                })
            );


export const getDeviceCount = () =>
    dispatch =>
        req.get(`${url.deviceCounts}/all`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: err,
                    error: true,
                })
            );

export const getDeviceCountCampus = campusId =>
    dispatch =>
        req.get(`${url.deviceCounts}/campus/${campusId}`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: err,
                    error: true,
                })
            );

export const getDeviceCountBuilding = buildingId =>
    dispatch =>
        req.get(`${url.deviceCounts}/building/${buildingId}`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: err,
                    error: true,
                })
            );

export const getDeviceCountFloor = floorId =>
    dispatch =>
        req.get(`${url.deviceCounts}/floor/${floorId}`)
            .then(({ body }) =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: body,
                })
            )
            .catch(err =>
                dispatch({
                    type: C.GET_DEVICE_COUNTS,
                    payload: err,
                    error: true,
                })
            );