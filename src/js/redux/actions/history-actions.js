import moment from 'moment';
import req from '../../misc/agent';
import url from '../../misc/url';
import * as C from '../constants/history-constants';
import locationSchema from '../../../resources/schemas/location-history.json';

export const getTelemetryHistoryData = (sdkId, type, tagId) => {
  // get start and end time in UTC
  // end time is current time
  // start time is 24hrs before end time
  const startTime = moment().subtract(1, 'day').utc().format();
  const endTime = moment().utc().format();
  return (dispatch) => {
    req.get(`${url.history}/telemetryHistoryByType/${sdkId}/${type}/${startTime}/${endTime}`)
    .then(({ body }) =>
      dispatch({
        type: C.SET_TAG_TELEMETRY_HISTORY,
        payload: body,
        tagId,
        telemetryType: type
      })
    )
    .catch(err =>
      dispatch({
        type: C.SET_TAG_TELEMETRY_HISTORY,
        payload: err,
        error: true,
        tagId,
        telemetryType: type
      })
    );
  };
};

export const getLocationHistoryForAsset = (params) => {
  const query = {
    startTime: params.startTime || moment().startOf('day').utc().format(),
    endTime: params.endTime || moment().endOf('day').utc().format(),
  };
  return dispatch =>
    req
      .get(`${url.timeline}/sdk/${params.sdkId}`)
      .query(query)
      .then(({ body }) =>
        dispatch({
          type: C.GET_ASSET_LOCATION_HISTORY,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_ASSET_LOCATION_HISTORY,
          payload: err,
          error: true,
        })
    );
};

export const getLocationFields = () => ({
  type: C.GET_ASSET_LOCATION_FIELDS,
  payload: locationSchema,
});
