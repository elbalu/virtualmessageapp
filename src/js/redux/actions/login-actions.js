import { hashHistory } from 'react-router';
import { message } from 'antd';

import config from '../../config';
import { login, setToken, setAuthHeader, getToken } from '../../misc/user-utils';
import req from '../../misc/agent';
import { prepareDataForNavigator } from '../../misc/helpers';
import { setNavigation } from '../../redux/actions/navigation-actions';
import { getNavigation } from '../../misc/nav-utils';
import { getRoute } from '../../misc/url-persist-utils';
import { fetchInitialData } from '../store';
import * as C from '../constants/login-constants';
import url from '../../misc/url';
import { RESET } from '../constants/settings-constants';

const { tokenHeader, tenantHeader } = config.get('api');
const env = process.env.NODE_ENV;

/**
 * get the token after successful login
 * Set token in local storage for future reference
 * set token as global default header
 * @param token
 */
export const setUserToken = (token) => {
  setToken(token);
  setAuthHeader(token);
};

export const applyNavigation = (user) => {
  const navigation = getNavigation();
  const newData = prepareDataForNavigator(navigation, user.resources);
  setNavigation(newData);
};
/**
* redirects user to desired navigation
* page after login
* @param user
*/
export const routeUser = user =>
  () => {
    login(user);
    fetchInitialData();
    applyNavigation(user);
    const route = getRoute();

    if (route) hashHistory.push(route);
    else hashHistory.push('dashboard');
  };

/**
* set user to redux state
* @param user
*/
export const setUser = user => ({
  type: C.SET_USER,
  payload: user,
});

/**
 * once auth succesful get current
 * logged in user and then redirect
 * to dashboard and load side nav based on user role
 * @param currentUser
 */
export const loginSuccess = (currentUser) => {
  const userUrl = (env === 'mock') ?
   `${url.currentUser}?username=${currentUser.username}&password=${currentUser.password}` :
   url.currentUser;

  return dispatch =>
    req.get(userUrl)
      .set('Content-Type', 'application/json')
      .then(({ body }) => {

        dispatch(routeUser(body));

        // set user to global state
        dispatch({
          type: C.SET_USER,
          payload: body,
        });
      }
      )
      .catch(error =>
        dispatch({
          type: C.SET_USER,
          payload: error,
          error: true,
        })
      );
};

/**
 * authenticate login user
 * uses username and password
 * @param loginUser
 */
export const loginAuth = loginUser => (
  (dispatch) => {
    const JWTToken = getToken();
    req.post(url.login)
    .send(loginUser)
    .set('Authorization', `JWT ${JWTToken}`)
    .then(({
        body,
    }) => {
      dispatch({
        type: C.CMX_TOKEN,
        payload: body.token,
      });
      dispatch({
        type: C.LOGIN_ERROR,
        payload: null,
      });
    })
    .catch((error) => {
      dispatch({
        type: C.LOGIN_ERROR,
        payload: error,
      });
    });
  }
);

/**
 * on app redirect, validate token
 * @param token
 */
export const validateRedirect = token => (
  (dispatch) => {
    const JWTToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRJZCI6MSwiaWF0IjoxNTE2MTMyMDQ2fQ.-H_jUERFGE5qMSR0-toRhNgLew0P3_HzLW-PWeBKrXg';
    req.get(url.currentUser)
      .set('Authorization', `JWT ${JWTToken}`)
      .end((err, res) => {
        if (err) {
          dispatch({
            type: C.SET_USER,
            payload: err,
            error: true,
          });
        } else {
          setUserToken(res.body.token);
          dispatch(routeUser(res.body));
          // set user to global state
          dispatch({
            type: C.SET_USER,
            payload: res.body,
          });
        }
      });
  }
 );

export const logout = () => (
  (dispatch) => {
    dispatch({ type: C.LOGOUT });
    dispatch({ type: RESET });
  }
);

export const setRoles = roles => ({
  type: C.SET_ROLES,
  payload: roles,
});

export const selectRole = roleId => ({
  type: C.SELECT_ROLE,
  payload: roleId,
});

export const setDefaultCMXToken = () => ({
  type: C.CMX_TOKEN_DEFAULT,
});
