/**
 * maps actions
 */
import message from 'antd/lib/message';

import * as C from '../constants/maps-constants';
import * as A from '../constants/areas-constants';


import { partial } from '../../misc/helpers';
import { getTempToken } from '../../misc/user-utils';
import req from '../../misc/agent';
import url from '../../misc/url';

const JWTToken = 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnRJZCI6MSwiaWF0IjoxNTE2MTMyMDQ2fQ.-H_jUERFGE5qMSR0-toRhNgLew0P3_HzLW-PWeBKrXg';

const env = process.env.NODE_ENV;
const tenantId = env === 'test' ? 'tenantId=420' : 'tenantId=15';

const action = (type, payload) => ({
  type,
  payload,
});

export const createZone = partial(action, C.MAPS_CREATEZONE);
export const selectFloor = partial(action, C.SELECT_CURRENT_FLOOR);
export const setFloors = partial(action, C.SET_MAP_FLOORS);
export const setLayers = partial(action, C.SET_LAYERS);
export const setActiveLayer = partial(action, C.SET_ACTIVE_LAYER);
export const addLayer = partial(action, C.ADD_LAYER);
export const updateLayer = partial(action, C.UPDATE_LAYER);
export const removeLayer = partial(action, C.REMOVE_LAYER);
export const openAssetModal = partial(action, C.OPEN_ASSET_MODAL);
export const closeAssetModal = partial(action, C.CLOSE_ASSET_MODAL);
export const openCustomModal = partial(action, C.OPEN_CUSTOM_MODAL);
export const closeCustomModal = partial(action, C.CLOSE_CUSTOM_MODAL);
export const setLayerType = partial(action, C.SET_CURRENT_LAYER_TYPE);
export const setTimeLineItem = partial(action, C.SET_TIME_LINE_ITEM);

export const getZonesByFloorId = floorId => (
  dispatch => (
    req.get(`${url.maps}/${floorId}?${tenantId}`)
      .set("Authorization", JWTToken)
      .end((err, res) => {
        if (err) {
          return dispatch({
            type: C.GET_ZONES_BY_FLOOR_ID,
            payload: err,
            error: true,
          });
        }

        return dispatch({
          type: C.GET_ZONES_BY_FLOOR_ID,
          payload: res.body,
        });
      })
  )
);

export const saveLayers = (id, data) => (
  () => {
    req.put(`${url.areas}/floor/${id}?${tenantId}`)
      .set("Authorization", JWTToken)
      .send(data)
      .end((err) => {
        if (err) {
          message.error(`Error saving data: ${err.message}`);
        } else {
          message.success('Save successful');
        }
      });
  }
);


export const addAddress = (id, data) => {
  return (dispatch) => {
    req.put(`${url.maps}/${id}?${tenantId}`)
      .set("Authorization", JWTToken)
      .send(data)
      .then(({ body }) => {
        message.success('Save successful!');
        dispatch({
          type: C.ADD_ADDRESS_FOR_CAMPUS_BUILDING,
          payload: body,
        });
      })
      .catch((error) => {
        message.success('Error! please try again');
        dispatch({
          type: C.ADD_ADDRESS_FOR_CAMPUS_BUILDING,
          payload: error,
          error: true,
        });
      });
  };
};

export const clearZones = () => ({
  type: C.CLEAR_ZONES,
  payload: {},
});
