/**
 * mapview actions
 */

import * as C from '../constants/mapview-constants';

import store from '../store';

import { partial } from '../../misc/helpers';

const action = (type, payload) => ({
    type,
    payload,
});
export const addMap = partial(action, C.MAPVIEW_ADDMAP);
export const setCurrentMapItems = partial(action, C.MAPVIEW_SET_CURRENT_MAP_ITEM);
export const setCurrentFloor = partial(action, C.MAPVIEW_SET_CURRENT_FLOOR);
export const setCurrentCategory = partial(action, C.MAPVIEW_SET_CURRENT_CATEGORY);
export const setDefaultHeterarchy = partial(action, C.MAPVIEW_SET_DEFAULT_HETERARCHY);
export const setFloors = partial(action, C.MAPVIEW_SET_FLOORS);

export const setMapMode = partial(action, C.MAPVIEW_SET_MAP_MODE);
export const categoryPerFloor = partial(action, C.MAPVIEW_CATEGORY_PER_FLOOR);
export const sdkPerCategoryPerFloor = partial(action, C.MAPVIEW_ASSETS_PER_CATEGORY_PER_FLOOR);

export const moveToBuilding = building =>
    (dispatch) => {
        const floors = building.children;
        if (floors && floors.length) {
            // set active floors
            store.dispatch(setFloors(floors));
            //set first floor as selected by default
            store.dispatch(setCurrentFloor(floors[0]));
            //switch map into floor mode
            store.dispatch(setMapMode('floor'));
        }
    }