import * as types from '../constants/modal-constants';

// common action creators

export const closeModal = () => ({
  type: types.CLOSE_MODAL,
});

export const openEntityModal = data => ({
  type: types.OPEN_ENTITY_MODAL,
  payload: data,
});

export const openFieldsModal = () => ({
  type: types.OPEN_FIELDS_MODAL,
});

export const openDeviceDetailsModal = (type, id, view = 'SINGLE', dataList = []) => (dispatch) => {
    dispatch({
      type: types.OPEN_DETAILS_MODAL,
      payload: { view, dataList },
    });
};

export const openFilterModal = () => ({
  type: types.OPEN_FILTER_MODAL,
});
