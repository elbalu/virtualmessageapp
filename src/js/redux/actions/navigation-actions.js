import { partial } from '../../misc/helpers';

import * as C from '../constants/navigation-constants';

export const setNavigation = nav => ({
  type: C.SET_NAV,
  payload: nav,
});
