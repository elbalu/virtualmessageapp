import * as types from '../constants/notification-constants';

/**
 * dispatches an action to display the
 * given notification
 *
 * @param {Notification|Error} notification
 */
export const display = notification => ({
  type: types.DISPLAY_NOTIFICATION,
  payload: notification,
});
