
import req from '../../misc/agent';
import url from '../../misc/url';

import * as C from '../constants/path-constants';

const env = process.env.NODE_ENV;
const tenantId = env === 'test' ? 'tenantId=420' : 'tenantId=15';

export const getPathByFloor = floorID =>
  dispatch =>
    req.get(`${url.path}?floorId=${floorID}&${tenantId}`)
      .then(({ body }) =>
        dispatch({
          type: C.GET_PATH_BY_FLOORID,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_PATH_BY_FLOORID,
          payload: err,
          error: true,
        })
      );

export const addPath = path => {
  return (dispatch =>
    req.post(`${url.path}?${tenantId}`)
      .send(path)
      .then(({ body }) => {
        dispatch({
          type: C.ADD_PATH,
          payload: body,
        });
        dispatch({
          type: C.SET_SELECTED_PATH,
          payload: null,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.ADD_PATH,
          payload: err,
          error: true,
        })
      ))
}
export const updatePath = path =>
  dispatch =>
    req.put(`${url.path}/${path.properties.id}?${tenantId}`)
      .send(path)
      .then(({ body }) => {
        dispatch({
          type: C.UPDATE_PATH,
          payload: body,
        })
        dispatch({
          type: C.SET_SELECTED_PATH,
          payload: null,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.UPDATE_PATH,
          payload: err,
          error: true,
        })
      );

export const deletePath = path => ({
  type: C.DELETE_PATH,
  payload: path,
});

export const setSelectedPath = path => ({
  type: C.SET_SELECTED_PATH,
  payload: path,
});

export const setActivePair = path => ({
  type: C.SET_ACTIVE_PAIR,
  payload: path,
});
