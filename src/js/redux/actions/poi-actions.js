
import req from '../../misc/agent';
import url from '../../misc/url';

import * as C from '../constants/poi-constants';

const env = process.env.NODE_ENV;
const tenantId = env === 'test' ? 'tenantId=420' : 'tenantId=15';

export const getPOIByFloor = floorID =>
  dispatch =>
    req.get(`${url.poi}?floorId=${floorID}&${tenantId}`)
      .then(({ body }) =>
        dispatch({
          type: C.GET_POI_BY_FLOORID,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.GET_POI_BY_FLOORID,
          payload: err,
          error: true,
        })
      );

export const addPOI = poi => {
  return (dispatch =>
    req.post(`${url.poi}?${tenantId}`)
      .send(poi)
      .then(({ body }) => {
        dispatch({
          type: C.ADD_POI,
          payload: body,
        });
        dispatch({
          type: C.SET_SELECTED_POI,
          payload: null,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.ADD_POI,
          payload: err,
          error: true,
        })
      ))
}
export const updatePOI = poi =>
  dispatch =>
    req.put(`${url.poi}/${poi.properties.id}?${tenantId}&floorId=${poi.properties.floorId}`)
      .send(poi)
      .then(({ body }) => {
        dispatch({
          type: C.UPDATE_POI,
          payload: body,
        })
        dispatch({
          type: C.SET_SELECTED_POI,
          payload: null,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.UPDATE_POI,
          payload: err,
          error: true,
        })
      );

export const deletePOI = poi =>
  dispatch =>
    req.delete(`${url.poi}/${poi.properties.id}?${tenantId}&floorId=${poi.properties.floorId}`)
      .then(({ body }) =>
        dispatch({
          type: C.DELETE_POI,
          payload: body,
        })
      )
      .catch(err =>
        dispatch({
          type: C.DELETE_POI,
          payload: err,
          error: true,
        })
      );

export const setSelectedPOI = poi => ({
  type: C.SET_SELECTED_POI,
  payload: poi,
});
