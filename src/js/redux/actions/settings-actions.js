import * as C from '../constants/settings-constants';

export const toggleNotifications = value => ({
  type: C.TOGGLE_NOTIFICATIONS,
  payload: value,
});
