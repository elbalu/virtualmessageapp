import req from '../../misc/agent';

import * as C from '../constants/table-constants';

export const filterSelect = filters => ({
  type: C.ADD_FILTER,
  payload: filters,
});

export const clearFilters = filters => ({
  type: C.CLEAR_FILTERS,
  payload: filters,
});

export const setParams = params => ({
  type: C.SET_TABLE_PARAMS,
  payload: params
});

export const addFields = fields =>
(dispatch) => {
  dispatch(filterSelect(fields));
  dispatch({
    type: C.ADD_FIELDS,
    payload: {
      fields
    }
  });
};

export const clearParams = params => ({
  type: C.CLEAR_PARAMS,
  payload: params,
});

export const removeField = field => ({
  type: C.REMOVE_FIELD,
  payload: field,
});

export const resetPaging = () => ({
  type: C.RESET_PAGING,
  payload: {},
});

export const addSearch = str => ({
  type: C.ADD_SEARCH,
  payload: str
});
