import _toUpper from 'lodash/toUpper';

import url from '../../misc/url';
import * as C from '../constants/tags-constants';
import { clearParams } from './table-actions';
import req from '../../misc/agent';
import store from '../store';

const processTag = (tag) => {
  const processedTag = tag;
  processedTag.serial = _toUpper(tag.serial);

  return processedTag;
};

export const getAllTags = () =>
  dispatch =>
      req.get(url.tags)
      .then(({ body }) =>
        dispatch({
          type: C.GET_ALL_TAGS,
          payload: body,
        })
      ).catch(err =>
        dispatch({
          type: C.GET_ALL_TAGS,
          payload: err,
          error: true,
        })
      );

export const getFilteredList = () => {
  const state = store.getState();
  const params = state.table.params;
  return dispatch =>
    req.post(`${url.tags}/search`)
      .send(params)
      .then(({ body }) => {
        return dispatch({
          type: C.GET_FILTERED_TAGS,
          payload: body,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.GET_FILTERED_TAGS,
          payload: err,
          error: true,
        })
      );
};

export const addTag = tag => (
  (dispatch) => {
    // convert tag mac address to uppercase
    const processedTag = processTag(tag);

    req.post(url.tags)
    .send(processedTag)
    .then(({ body }) => {
      dispatch(getFilteredList());
      dispatch({
        type: C.ADD_TAG,
        payload: { ...processedTag, id: body.id },
      });
    })
    .catch(error => dispatch({
      type: C.ADD_TAG,
      payload: error,
      error: true,
    }));
  }
);


export const removeTag = id =>
  dispatch =>
      req.delete(`${url.tags}/${id}`)
      .send({ id })
      .then(() => {
        dispatch(clearParams());
        dispatch(getFilteredList());
        dispatch({
          type: C.REMOVE_TAG,
          payload: id,
        });
      }
      )
      .catch((error) =>
        dispatch({
          type: C.REMOVE_TAG,
          payload: error,
          error: true,
        })
      );

export const editTag = tag => (
  (dispatch) => {
    const processedTag = processTag(tag);

    req.put(`${url.tags}/${tag.id}`)
    .send(processedTag)
    .then(() =>
        dispatch({
          type: C.EDIT_TAG,
          payload: processedTag,
        })
    )
    .catch((error) =>
        dispatch({
          type: C.EDIT_TAG,
          payload: error,
          error: true,
        })
    );
  }
);
