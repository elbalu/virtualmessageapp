import _camelCase from 'lodash/camelCase';
import _get from 'lodash/get';
import _ from 'lodash';

import config from '../../config';
import req from '../../misc/agent';
import url from '../../misc/url';
import Fields from '../../components/form-generator/fields';
import * as types from '../constants/templates-constants';
import * as wlcActions from './wlc-actions';
import * as connectorActions from './connector-actions';

const entityNames = config.get('entityNames');

/**
 * returns a template label generated using entitynames or
 * category name and field label
 *
 * @param {Object} field
 * @param {Object} category
 * @return {string}
 */
const generateFieldTemplateLabel = (field, category) => {
  const entityName = _.get(field, 'entityName', 'ASSETS');
  const label = _.get(field, 'label', 'New Field');
  const categoryName = _.get(category, 'name', '');

  const prefix = _get(entityNames, `[${_camelCase(entityName)}].label`);

  if (prefix) return `${prefix} ${label}`;
  return `${categoryName} ${label}`;
};

/**
 * returns a field key
 *
 * @param {Object} field
 * @param {Object} category
 * @return {string}
 */
const generateFieldKey = (field, category) => {
  const entityName = _.get(field, 'entityName');
  const isCustomCategoryField = entityName === entityNames.customCategory.key;

  if (!isCustomCategoryField) return _camelCase(field.label);
  return _camelCase(`${category.name} ${field.label}`);
};

/**
 * generates derived field properties and returns
 * the updated field
 *
 * @param {Object} field
 * @param {Object} category
 * @return {Object}
 */
export const formatNewField = (field, category) => {
  const formattedField = field;

  formattedField.key = generateFieldKey(field, category);
  formattedField.templateLabel = generateFieldTemplateLabel(field, category);
  formattedField.general = true;
  formattedField.editable = true;

  if (field.type === Fields.Types.CHECKBOX) {
    formattedField.options = ['Yes', 'No'];
  }

  return formattedField;
};

/**
 * Updates derived field properties that should be
 * updated
 *
 * @param {Object} field
 * @param {Object} category
 * @return {Object}
 */
export const formatUpdatedField = (field, category) => {
  const formattedField = field;

  // regenerate field template label
  // generate field template label
  formattedField.templateLabel = generateFieldTemplateLabel(field, category);

  return formattedField;
};

/**
 * Returns an action creator that retrieves a template based
 * on entityName and dispatches an action to save the
 * template in the redux store.
 *
 * @param {'ASSETS'|'CATEGORIES'|'DEPARTMENTS'|'TAGS'} entityName
 * @return {function(*)}
 */
export const getTemplate = entityName => (
  (dispatch) => {
    req.get(`${url.template}/${entityName}`)
    .then(({ body }) => {
      dispatch({
        type: types.GET_TEMPLATE,
        payload: body,
      });
    })
    .catch((error) => {
      dispatch({
        type: types.GET_TEMPLATE,
        payload: error,
        error: true,
      });
    });
  }
);

/**
 * Returns an action creator that retrieves the sdk detail
 * template and dispatches an action to save the
 * template in the redux store.
 *
 * @param {Object} [category] If a category is provided, custom fields
 * will be provided for that category
 * @return {function(*)}
 */
export const getDetailTemplate = (category = null) => (
  (dispatch) => {
    const categoryId = (category) ? category.id : 0;

    req.get(`${url.detailTemplate}/${categoryId}`)
    .then(({ body }) => {
      dispatch({
        type: types.GET_TEMPLATE,
        payload: body,
      });
    })
    .catch(error => (
        dispatch({
          type: types.GET_TEMPLATE,
          payload: error,
          error: true,
        })
    ));
  }
);

/**
 * @param {String} type If a type is provided, reloads data of
 * that type from its respective actions
 */
export const reloadData = type => (
  (dispatch) => {
    if (type === 'ASSETS') {
      dispatch(sdkActions.getFilteredList());
    } else if (type === 'CATEGORIES') {
      dispatch(categoryActions.getAllCategories());
    } else if (type === 'DEPARTMENTS') {
      dispatch(departmentActions.getAllDepartments());
    } else if (type === 'TAGS') {
      dispatch(tagActions.getAllTags());
    } else if (type === 'WLCS') {
      dispatch(wlcActions.getAllWlcs());
    } else if (type === 'CONNECTORS') {
        dispatch(connectorActions.getAllConnectors());
    }
  });

/**
 * Returns an action creator that creates a new field
 * and then dispatches an action to save the field
 * to the proper template in the redux store.
 *
 * @param {Object} field
 * @param {Object} [category] Parent category if the field
 *  is a category custom field
 * @return {function(*)}
 */
export const createField = (field, category) => (
  (dispatch) => {
    const categoryId = (category) ? category.id : undefined;
    const formattedField = formatNewField(field, category);

    req.post(url.fields)
    .send({ field: formattedField, category: categoryId })
    .then(({ body }) => {
      // update lists with new field id's
      dispatch({
        type: types.CREATE_FIELD,
        payload: { ...formattedField, id: body.id, key: body.id },
      });
    })
    .catch((error) => {
      dispatch({
        type: types.CREATE_FIELD,
        payload: error,
        error: true,
      });
    });
  }
);

/**
 * Returns an action creator that updates an
 * existing field then dispatches an action to
 * update the field in the redux store.
 *
 * @param {Object} field
 * @param {Object} [category] Parent category if the field
 *  is a category custom field
 * @return {function(*)}
 */
export const updateField = (field, category) => (
  (dispatch) => {
    const categoryId = (category) ? category.id : undefined;
    const formattedField = formatUpdatedField(field, category);

    req.put(`${url.fields}/${field.id}`)
    .send({ field: formattedField, category: categoryId })
    .then(() => {
      dispatch({
        type: types.UPDATE_FIELD,
        payload: formattedField,
      });
    })
    .catch((error) => {
      dispatch({
        type: types.UPDATE_FIELD,
        payload: error,
        error: true,
      });
    });
  }
);

/**
 * Returns an action creator that deletes
 * a field then dispatches an action to
 * remove the field from the redux store.
 *
 * @param {Object} field
 * @return {function(*)}
 */
export const removeField = field => (
  (dispatch) => {
    req.delete(`${url.fields}/${field.id}`)
    .then(() => {
      dispatch({
        type: types.DELETE_FIELD,
        payload: field,
      });
    })
    .catch((error) => {
      dispatch({
        type: types.DELETE_FIELD,
        payload: error,
        error: true,
      });
    });
  }
);
