import { message, Modal } from 'antd';

import req from '../../misc/agent';
import url from '../../misc/url';
import * as C from '../constants/users-constants';
import userTemplate from '../../../resources/schemas/users.json';
import inviteUserTemplate from '../../../resources/schemas/inviteUser.json';
import { getUser, showLogoutModal } from '../../misc/user-utils';
import { setUser, unsetUser, logout } from '../actions/login-actions';

export const updatePassword = ({ oldPassword, newPassword }) => (
  dispatch => (
   req.put(url.password)
     .send({ oldPassword, newPassword })
     .then(() => {
       // logout user after change password
       dispatch(showLogoutModal());
       dispatch(logout());
       dispatch(unsetUser());
       // TODO check 401
       dispatch({
         type: C.UPDATE_PASSWORD,
       });
     }).catch(err =>
       dispatch({
         type: C.UPDATE_PASSWORD,
         payload: err,
         error: true,
       })
     )
  )
);

/**
 * Get user fields
 */
export const getUserFields = () => ({
  type: C.GET_USER_FIELDS,
  payload: { userTemplate },
});

export const getInviteUserFields = () => ({
  type: C.GET_INVITE_USER_FIELDS,
  payload: { inviteUserTemplate },
});

/**
 * async get all
 * Users action creator
 */
export const getAllUsers = () => (
  // return a function
  dispatch => (
    req.get(url.users)
        .end((err, res) => {
          if (err) {
            message.error('Error! please try again');
            return dispatch({
              type: C.GET_ALL_USERS,
              payload: err,
              error: true,
            });
          }

          return dispatch({
            type: C.GET_ALL_USERS,
            payload: res.body,
          });
        })
  )
);

export const updateUser = (data) => {
  const user = data;
  if (user.phone && user.phone.includes('-')) {
    user.phone = user.phone.replace(/-/g, '');
  } else if (user.phone === '') {
    user.phone = null;
  }

  // update role
  if (!(data.role instanceof Array)) {
    user.roleId = data.role;
  }
  user.roleId = data.role.id;
  return user;
};

export const inviteUser = (data) => {
  const newUser = data;
  newUser.appName = 'ASSETMANAGEMENT';
  return (dispatch) => {
    req.post(url.userInvite)
      .send(newUser)
      .end((err, { body }) => {
        if (err) {
          Modal.error({
            title: 'Error',
            content: body.message,
          });
        } else {
          Modal.success({
            title: 'Success',
            content: 'Invitation email sent',
          });
          return dispatch({
            type: C.ADD_USER,
            payload: body,
          });
        }
      });
  };
};

export const resendInvite = (data) => {
  const newUser = data;
  newUser.appName = 'ASSETMANAGEMENT';
  return () => {
    req.put(url.userInvite)
      .send(newUser)
      .end((err, { body }) => {
        if (err) {
          Modal.error({
            title: 'Error',
            content: body.message,
          });
        } else {
          Modal.success({
            title: 'Success',
            content: 'Invitation email sent',
          });
        }
      });
  };
};

export const removeUser = id =>
  (dispatch) => {
    req.delete(`${url.users}/${encodeURIComponent(id)}`)
      .end((err) => {
        if (err) {
          return dispatch({
            type: C.REMOVE_USER,
            payload: err,
            error: true,
          });
        }

        return dispatch({
          type: C.REMOVE_USER,
          payload: id,
        });
      });
  };

export const editUser = (user) => {

  return (dispatch) => {
    // check update logged in user info
    const currentUser = getUser();
    if (currentUser.id === user.id) {
      dispatch(setUser(user));
    }
    const newUser = updateUser(user);

    req.put(`${url.users}/${user.email}`)
      .send(newUser)
      .end((err, { body }) => {
        if (err) {
          return dispatch({
            type: C.EDIT_USER,
            payload: err,
            error: true,
          });
        }

        return dispatch({
          type: C.EDIT_USER,
          payload: body,
        });
      });
  };
};

/**
 * updates user field editibility for add/edit user
 */
export const disableEmailField = disable => ({
  type: C.DISABLE_EMAIL_FIELD,
  payload: disable,
});
