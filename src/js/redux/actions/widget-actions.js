import _ from 'lodash';
import Notification from 'antd/lib/notification';
import req from '../../misc/agent';
import url from '../../misc/url';
import * as C from '../constants/widget-constants';
import { getAllAlerts, setActiveAlertCount, getRecentAlerts } from './alerts-actions';
import * as WLC from './wlc-actions';
import * as CONNECTOR from './connector-actions';
import { clearFilters, addFields, resetPaging } from './table-actions';
import forwardAlertsSchema from '../../../resources/schemas/forward-alerts.json';
import widgetTypes from '../../../resources/data/alerts/widgetTypes.json';
import widgetTimeRanges from '../../../resources/data/alerts/widgetTimeRanges.json';
import config from '../../config';
import store from '../store';


const env = process.env.NODE_ENV;

const messages = config.get('messages');


export const getActiveWidgetCount = (pageParams) => {
  const page = [pageParams.page];
  const updatedUrl = (env === 'mock') ? `${url.widgets}/count` : `${url[page]}/count`;
  return dispatch =>
    req
      .get(updatedUrl)
      .query(pageParams)
      .then(({ body }) => {
        // update count of alerts on page header
        dispatch(setActiveAlertCount(body.count));
        dispatch(getRecentAlerts());
      })
      .catch(err =>
        dispatch({
          type: C.GET_WIDGET_COUNT,
          payload: err,
          error: true,
        })
      );
};

export const getWidgetCount = (pageParams, widget) => {
  const filters = widget.data.params;
  const updatedParams = Object.assign({}, ({ fields: filters }), pageParams);
  const page = [pageParams.page];
  const updatedUrl = (env === 'mock') ? `${url.widgets}/count` : `${url[page]}/count`;
  return dispatch =>
    req
      .post(updatedUrl)
      .send(updatedParams)
      .then(({ body }) => {
        // update count of widgets
        dispatch({
          type: C.GET_WIDGET_COUNT,
          payload: body.count,
          id: widget.id,
        });
        if (pageParams.page === 'alerts' && widget.default) {
          // if default widget then update active alert count
          // on page header whenever widgets are refreshed
          dispatch(setActiveAlertCount(body.count));
        }
      })
      .catch(err =>
        dispatch({
          type: C.GET_WIDGET_COUNT,
          payload: err,
          error: true,
        })
      );
};


export const updateWidgets = (widgets, pageParams) =>
  dispatch => widgets.forEach((widget) => {
    dispatch(getWidgetCount(pageParams, widget));
    const updatedWidget = widget;
    // saving widget rawParams for backend use
    updatedWidget.data.rawParams = widget.data.params || {};
    dispatch({
      type: C.UPDATE_WIDGET,
      payload: updatedWidget,
    });
  });


/**
 * sets the alert count for widget ALL
 *
 * @param {allWidgets}
 */
export const setWidgetTypes = (allWidgets) => {
  const widgetTypeFilters = [];
  widgetTypes.forEach((filter) => {
    const widgetType = filter;
    const widgets = allWidgets.filter(widget => widget.data.filterType === filter.type);
    widgetType.count = (filter.type === 'ALL') ? allWidgets.length : widgets.length;
    widgetTypeFilters.push(widgetType);
  });
  return widgetTypeFilters;
};

export const getWidgets = (pageParams) => {
  return dispatch =>
    req
      .get(`${url.widgets}`)
      .query(pageParams)
      .then(({ body }) => {
        // set up widget filters with counts
        dispatch({
          type: C.GET_WIDGET_TYPES,
          payload: setWidgetTypes(body),
        });

        dispatch({
          type: C.GET_ALL_WIDGETS,
          payload: body,
        });

        // Get widget count only for alerts page
        if (pageParams.page !== 'dashboard') {
          return dispatch(updateWidgets(body, pageParams));
        }
      }

      )
      .catch(err =>
        dispatch({
          type: C.GET_ALL_WIDGETS,
          payload: err,
          error: true,
        })
      );
};


export const updateParams = params => ({
  type: C.UPDATE_WIDGET_PARAMS,
  payload: params,
});

export const getFilterdRecords = params =>
  (dispatch) => {
    if (params.page === 'alerts') {
      dispatch(getAllAlerts());
    }
    if (params.page === 'wlcs') {
      dispatch(WLC.getFilteredList());
    }
    if (params.page === 'connectors') {
      dispatch(CONNECTOR.getFilteredList());
    }
  };

export const setPageQuery = query => ({
  type: C.SET_PAGE_QUERY,
  payload: query,
});


export const selectWidget = (widget, page) =>
(dispatch) => {
  if (widget) {
    // clear any page query params
    dispatch(setPageQuery({}));
    // convert server params to table filter format
    // const updatedParams = getTableFilters(widget.data.params);
    const updatedParams = (widget.default) ? [] : widget.data.params;
    // update params in filter breadcrumbs
    dispatch(updateParams(updatedParams));
    // trigger filters for table
    // dispatch(filterSelect(updatedParams));
    dispatch(addFields(updatedParams));
    const pageParams = { page };
    // reset table paging to first page
    dispatch(resetPaging());
    // fetch new table data based on params
    dispatch(getFilterdRecords(Object.assign({}, pageParams, { fields: widget.data.params })));
    // update the selected widget in the store.
    dispatch({
      type: C.SELECT_WIDGET,
      payload: widget,
    });
  }
};


export const selectDefaultWidget = page =>
(dispatch) => {
  if (page) {
    // get default widget for that page
    const state = store.getState();
    const widgets = state.widgets.allWidgets;
    const widget = _.find(widgets, ['default', true]);
    dispatch(selectWidget(widget, page));
  }
};


export const clearBreadcrumbs = () =>
(dispatch) => {
  dispatch(updateParams([]));
  dispatch(clearFilters());
};

export const updateRecords = params =>
  (dispatch) => {
    dispatch(getFilterdRecords(params));
  };

// TODO
export const download = id => {
}

export const updateWidget = (widget, page, isUpdate = false) => {
  const updatedWidget = widget;
  updatedWidget.data.pages = [page];
  return dispatch =>
      req.put(`${url.widgets}`)
          .send(widget)
          .then(({ body }) => {
            const w = body[0];
            // saving widget rawParams for backend use
            updatedWidget.data.rawParams = widget.data.params || {};
            dispatch({
              type: C.UPDATE_WIDGET,
              payload: widget,
            });
            // Get count only for alerts
            if (page !== 'dashboard') {
              dispatch(getWidgetCount({ page }, body[0]));
            }
            if (isUpdate) {
              Notification.success({
                message: messages.success,
                description: `${widget.label} ${messages['widget.add.update']}`,
              });
            }
          })
          .catch(error =>
              dispatch({
                type: C.UPDATE_WIDGET,
                payload: error,
                error: true,
              })
          );
};

export const deleteWidget = (widget, page) => {
  const updatedWidget = widget;
  updatedWidget.entity = page.toUpperCase();
  return dispatch =>
      req.delete(`${url.widgets}`)
          .send(widget)
          .then(() => {
            dispatch({
              type: C.DELETE_WIDGET,
              payload: widget,
            });
          })
          .catch(error =>
              dispatch({
                type: C.DELETE_WIDGET,
                payload: error,
                error: true,
              })
          );
};

// TODO
export const forwardAlertsInEmail = content => dispatch =>
    req.post(`${url.alerts}/forward`)
        .send(content)
        .then(({ body }) => {
            // do nothing or show confirmation saying email sent
        })
        .catch((error) =>
            dispatch({
              type: C.FORWARD_IN_EMAIL,
              payload: error,
              error: true,
            })
        );


export const getForwardWidgetDataFields = () => ({
  type: C.FORWARD_DATA_SCHEMA,
  payload: forwardAlertsSchema,
});
/**
 * alert add widget modal filter selection
 */
export const selectFilterType = type => ({
  type: C.SELECT_FILTER_TYPE,
  payload: type,
});

export const getWidgetTimeRanges = () => ({
  type: C.WIDGET_TIME_RANGES,
  payload: widgetTimeRanges,
});

/** in Dashboard, when the user drag a marker , we need to
* set temp feture to use it in pinned marker panel component **/

export const setTempMarker = feature => ({
  type: C.SET_TEMP_MARKER,
  payload: feature,
});

export const addCustomWidget = (widget, page) => {
  const updatedWidget = widget;
  updatedWidget.data.pages = [page];
  updatedWidget.entity = page;
  return dispatch =>
  req.post(`${url.widgets}`)
      .send(updatedWidget)
      .then(({ body }) => {
        const w = body[0];
        // saving widget rawParams for backend use
        w.data.rawParams = widget.data.params || [];
        dispatch(clearBreadcrumbs());
        dispatch(selectWidget(body[0], page));
        Notification.success({
          message: messages.success,
          description: `${widget.label} ${messages['widget.add.success']}`,
        });
        // Get count only for alerts
        if (page !== 'dashboard') {
          dispatch(getWidgetCount({ page }, body[0]));
        }
        dispatch({
          type: C.ADD_WIDGET,
          payload: w,
        });
      })
      .catch(error =>
          dispatch({
            type: C.ADD_WIDGET,
            payload: error,
            error: true,
          })
      );
};


export const clearWidgets = () =>
  (dispatch) => {
    dispatch({
      type: C.CLEAR_WIDGETS,
      payload: [],
    });
    dispatch(clearFilters());
  };
