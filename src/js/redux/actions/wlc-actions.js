import _toUpper from 'lodash/toUpper';

import url from '../../misc/url';
import * as C from '../constants/wlcs-constants';
import { clearParams } from './table-actions';
import req from '../../misc/agent';
import store from '../store';

const processWlc = (wlc) => {
  const processedWlc = {};
  const wlcKeys = Object.keys(wlc);
  const attachedConnector = (wlc.connectorId && (typeof wlc.connectorId === "string" || typeof wlc.connectorId === "number"))
      ? wlc.connectorId.toString()
      : (wlc.connectorId && typeof wlc.connectorId === "object" && wlc.connectorId.id) ? (wlc.connectorId.id).toString() : "";

  for(let i = 0; i < wlcKeys.length; i++) {
    let keyItem = wlcKeys[i],
      keyArray = keyItem.split("."),
      keyJson = '',
      jsonProp;

    if (typeof wlc[keyItem] === "undefined") continue;

    if (keyArray.length === 1) {
      processedWlc[keyItem] = wlc[keyItem];
      // jsonProp[keyItem] = wlc[keyItem];
      // Object.assign(processedWlc, jsonProp);
    } else {

      if(processedWlc[keyArray[0]]) {
        for (let j = 2; j < keyArray.length; j++) {
          let subKeyItem = keyArray[j];
          keyJson += "{ '" + subKeyItem + "'" + ": ";
        }
        keyJson += "'" + wlc[keyItem] + "'" + Array(keyArray.length -1).join("}");
        processedWlc[keyArray[0]][keyArray[1]] = JSON.parse(JSON.stringify(eval('('+keyJson+')')));
        // jsonProp[keyArray[0]] = JSON.parse(JSON.stringify(eval('('+keyJson+')')));
        // Object.assign(processedWlc, jsonProp);
      } else {
        for (let j = 1; j < keyArray.length; j++) {
          let subKeyItem = keyArray[j];
          keyJson += "{ '" + subKeyItem + "'" + ": ";
        }
        keyJson += "'" + wlc[keyItem] + "'" + Array(keyArray.length).join("}");
        processedWlc[keyArray[0]] = JSON.parse(JSON.stringify(eval('('+keyJson+')')));
      }
    }
  }


    if(attachedConnector != "") {
        processedWlc.connectorId = attachedConnector;
    }

    return processedWlc;
};

const payloadForEdit = (wlc) => {
    const processedWlc = {};

    if(typeof(wlc["connectorId"]) !== "undefined" ) {
        processedWlc["connectorId"] = wlc["connectorId"];
    }

    if(typeof(wlc["name"]) !== "undefined" ) {
        processedWlc["name"] = wlc["name"];
    }

    if(typeof(wlc["snmp"]) !== "undefined" ) {
        processedWlc["snmp"] = wlc["snmp"];
    }

    return processedWlc;
};

export const getTenantToken = () =>
  dispatch =>
    req.get(url.tenantaccess)
    .then(({ body }) =>
      dispatch({
        type: C.GET_TENANT_TOKEN,
        payload: body,
      })
    ).catch(err =>
    dispatch({
      type: C.GET_TENANT_TOKEN,
      payload: err,
      error: true,
    })
  );

export const getAllWlcs = () =>
  dispatch =>
    req.get(url.wlcs)
    .then(({ body }) =>
      dispatch({
        type: C.GET_ALL_WLCS,
        payload: body,
      })
    ).catch(err =>
      dispatch({
        type: C.GET_ALL_WLCS,
        payload: err,
        error: true,
      })
    );

export const getFilteredList = () => {
  const state = store.getState();
  const params = state.table.params;

  return dispatch =>
    req.post(`${url.wlcs}/search`)
      .send(params)
      .then(({ body }) => {
        return dispatch({
          type: C.GET_FILTERED_WLCS,
          payload: body,
        });
      }
      )
      .catch(err =>
        dispatch({
          type: C.GET_FILTERED_WLCS,
          payload: err,
          error: false,
        })
      );
};

export const addWlc = wlc => (
  (dispatch) => {
    // convert wlc mac address to uppercase
    const processedWlc = processWlc(wlc);

    req.post(url.wlcs)
    .send(processedWlc)
    .then(({ body }) => {
      dispatch(getFilteredList());
      dispatch({
        type: C.ADD_WLC,
        payload: { ...body }, //, id: body.id },
      });
    })
    .catch(error => dispatch({
      type: C.ADD_WLC,
      payload: error,
      error: true,
    }));
  }
);


export const removeWlc = id =>
  dispatch =>
      req.delete(`${url.wlcs}/${id}`)
      .send({ id })
      .then(() => {
        // dispatch(clearParams());
        // dispatch(getFilteredList());
        dispatch({
          type: C.REMOVE_WLC,
          payload: id,
        });
      }
      )
      .catch((error) =>
        dispatch({
          type: C.REMOVE_WLC,
          payload: error,
          error: true,
        })
      );

export const editWlc = wlc => (
  (dispatch) => {
    const processedWlc = processWlc(wlc);
    const editPayload = payloadForEdit(processedWlc);
    req.put(`${url.wlcs}/${wlc.id}`)
    .send(editPayload)
    .then(() =>
        dispatch({
          type: C.EDIT_WLC,
          payload: processedWlc,
        })
    )
    .catch((error) =>
        dispatch({
          type: C.EDIT_WLC,
          payload: error,
          error: true,
        })
    );
  }
);

export const getWlcDetails = (id, entity) => {
    let startTime = null;
    let endTime = null;
    if (entity) {
        startTime = moment(entity.triggeredAt).startOf('day').utc().format();
        endTime = moment(entity.triggeredAt).endOf('day').utc().format();
    }
    return (dispatch) => {
      dispatch({
        type: C.GET_WLC_DETAILS,
        payload: { id },
      });
      req.get(`${url.wlcs}/${id}`)
        .then(({ body }) => {
          dispatch({
              type: C.GET_WLC_DETAILS,
              payload: body,
            });
          }
        )
        .catch(err =>
            dispatch({
                type: C.GET_WLC_DETAILS,
                payload: err,
                error: true,
            })
        );
    };
};
