/**
 * areas Constants
 */
export const SET_ALL_AREAS = 'SET ALL AREAS';
export const UPDATE_AREA = 'UPDATE AREA';
export const SELECT_AREA = 'SELECT AREA';
export const ADD_AREA = 'ADD AREA';
export const REMOVE_AREA = 'REMOVE AREA';
export const SELECT_ZONE = 'SELECT ZONE';
export const SET_CHILDREN = 'SET CHILDREN';
export const SET_ALL_FLOORS = 'SET ALL FLOORS';

export const AREAS_FETCHAREAS = 'FETCH AREAS';
export const AREAS_FETCHFEATURES = 'GET AREA FEATURES';
