export const SET_USER = 'SET USER';
export const UNSET_USER = 'UNSET USER';
export const SET_ROLES = 'SET USER ROLES';
export const SELECT_ROLE = 'SELECT USER ROLE';
export const LOGIN_ERROR = 'LOGIN ERROR';
export const LOGOUT = 'LOGOUT';

export const CMX_TOKEN = 'CMX TOKEN';
export const CMX_TOKEN_DEFAULT = 'CMX TOKEN DEFAULT';
