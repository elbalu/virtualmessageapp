import * as types from '../constants/mapview-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
    currentMapItems: [],
    currentFloor: {},
    currentCategory: null,
    floors: [],
    mapMode: 'nomap',
    categoryPerFloor: [],
    sdkPerCategoryPerFloor: [],
};

/**
 * mapview Reducer
 * @param state
 * @param action {object}
 * @returns {*}
 */
const mapviewReducer = (state = initialState, action) => {
    // destructure action
    const { type, payload, error } = action;

    if (error) return state;

    // switch on action type
    switch (type) {
        case RESET:
            return { ...initialState };

        case types.MAPVIEW_ADDMAP:
            return {
                ...state,
                // your code here
            };

        case types.MAPVIEW_SET_CURRENT_MAP_ITEM:
            return {
                ...state,
                currentMapItems: payload,
            };

        case types.MAPVIEW_SET_CURRENT_FLOOR:
            return {
                ...state,
                currentFloor: payload,
            };

        case types.MAPVIEW_SET_DEFAULT_HETERARCHY:
            return {
                ...state,
                defaultHeterarchy: payload,
            };

        case types.MAPVIEW_SET_CURRENT_CATEGORY:
            return {
                ...state,
                currentCategory: payload,
            };

        case types.MAPVIEW_SET_FLOORS:
            return {
                ...state,
                floors: payload,
            };

        case types.MAPVIEW_SET_MAP_MODE:
            return {
                ...state,
                mapMode: payload,
            };

        case types.MAPVIEW_CATEGORY_PER_FLOOR:
            return {
                ...state,
                categoryPerFloor: payload,
            };

        case types.MAPVIEW_ASSETS_PER_CATEGORY_PER_FLOOR:
            return {
                ...state,
                sdkPerCategoryPerFloor: payload,
            };

        case types.ADD_ADDRESS_FOR_CAMPUS_BUILDING:
            return {
                ...state,
                areas: payload,
            };

        default:
            return state;
    }
};

export default mapviewReducer;