export const GET_POI_BY_FLOORID = 'GET_POI_BY_FLOORID';
export const SET_SELECTED_POI = 'SET_SELECTED_POI';
export const ADD_POI = 'ADD_POI';
export const UPDATE_POI = 'UPDATE_POI';
export const DELETE_POI = 'DELETE_POI';
