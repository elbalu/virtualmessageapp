export const GET_TAG_FIELDS = 'GET TAG FIELDS';
export const SET_TAG_FIELDS = 'SET TAG FIELDS';

export const GET_ALL_TAGS = 'GET ALL TAGS';
export const GET_FILTERED_TAGS = 'GET FILTERED TAGS';
export const ADD_TAG = 'ADD TAG';
export const REMOVE_TAG = 'REMOVE TAG';
export const EDIT_TAG = 'EDIT TAG';
