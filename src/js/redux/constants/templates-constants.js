export const GET_TEMPLATE = 'GET_TEMPLATE';
export const CREATE_FIELD = 'CREATE_FIELD';
export const UPDATE_FIELD = 'UPDATE_FIELD';
export const DELETE_FIELD = 'DELETE_FIELD';
