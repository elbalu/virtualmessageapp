import update from 'immutability-helper';
import _ from 'lodash';
import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as types from '../constants/areas-constants';
import { RESET } from '../constants/settings-constants';
import traverse from '../../misc/traverse';
import Location from '../../misc/map/location';

const initialState = {
  geoData: [],
  locations: null,
  featuresData: [],
  selectedLocation: null,
  selectedZone: null,
  floors: []
};

/**
 * updateArea replaces a location (area) in state
 * with an updated area. Also sets the selectedArea.
 *
 * @param {Object} state - Areas reducer state
 *
 * @param {Location} location - Maps Service Location instance
 *
 * @param {Location} location - MapView Service Location instance
 * @return {Object} - next state
 */
const updateLocation = (state, location) => update(state, {
  // update the area in geoData (locations list)
  locations: {
    $apply: locations => traverse(
      locations,
      l => ((l.id === location.getId()) ? location : l),
      l => ((l.id === location.getId() && Location.isFloor(location)) ? location.getChildren() : l.getChildren()),
      (l, children) => l.setChildren(children)
    )
  }
});

/**
 * @updateChildren
 *
 * @description Updates a location's children in the locations tree.
 *
 * @param {Object} state - Areas reducer state
 *
 * @param {Location} location - Maps Service Location instance
 *
 * @return {Object} - next state
 */
const updateChildren = (state, location) => update(state, {
  // update the area in geoData (locations list)
  locations: {
    $apply: locations => traverse(
      locations,
      l => l,
      l => ((l.getId() === location.getId()) ? location.getChildren() : l.getChildren()),
      (l, children) => l.setChildren(children)
    )
  }
});

/**
 * @name selectLocation
 *
 * @description Updates a location in state.locations
 * and sets that location as state.selectedLocation.
 *
 * @param {Object} state - Areas reducer state
 *
 * @param {Location|string} location - Location to select
 *
 * @return {Object} - next state
 */
const selectLocation = (state, location) => {
  let stateWithUpdateLocations = state;

  if(location == null || location.data.name == null) {
    return update(stateWithUpdateLocations, {
      selectedLocation: {
        $set: location
      }
    });
  }

  if (location instanceof Location) {
    stateWithUpdateLocations = updateLocation(state, location);
  }

  return update(stateWithUpdateLocations, {
    selectedLocation: {
      $set: location
    }
  });
};

/**
 * @name selectZone
 *
 * @description Updates a zone in state.locations
 * and sets that zone as state.selectedZone.
 *
 * @param {Object} state - Areas reducer state
 *
 * @param {Location|string} zone - Zone to select
 *
 * @return {Object} - next state
 */
const selectZone = (state, zone) => {
  const currentId = state.selectedZone ? state.selectedZone.getId() : null;
  const nextId = zone ? zone.getId() : null;
  const isDifferentZone = currentId !== nextId;
  const nextZone = isDifferentZone ? _.cloneDeep(zone) : _.clone(zone);

  return update(state, {
    selectedZone: {
      $set: nextZone
    }
  });
};

/**
 * addLocation adds a location to the locations tree
 * at the parent specified by location.getParent().
 *
 * @param {Object} state - Areas reducer state
 *
 * @param {string} parent - Parent id
 *
 * @param {Location[]} newLocations - One or more
 * new Location instances
 *
 * @return {Object} - Next state
 */
const addLocation = (state, parent, ...newLocations) => {
  return update(state, {
    // update the area in geoData (locations list)
    locations: {
      $apply: locations => traverse(
        locations,
        l => l,
        (l) => {
          if (l.getId() !== parent) return l.getChildren();
          return _.compact([...l.getChildren(), ...newLocations]);
        },
        (l, children) => l.setChildren(children)
      )
    }
  });
}

/**
 * removeLocation removes a location in the
 * locations tree by removing the location
 * from the children of its parent given by
 * location.getParent().
 *
 * @param {Object} state - Areas reducer state
 * @param {Location} location - New location
 * @return {Object} - Next state
 */
const removeLocation = (state, location) => update(state, {
  // update the area in geoData (locations list)
  locations: {
    $apply: locations => traverse(
      locations,
      l => ((l.id === location.getId()) ? location : l),
      l => _.filter(l.getChildren(), c => c.getId() !== location.getId()),
      (l, children) => l.setChildren(children)
    )
  },
  selectedLocation: {
    $apply: (l) => {
      if (!l) return l;

      if (l.getId() === location.getId()) return null;

      const children = _.filter(l.getChildren(), c => c.getId() !== location.getId());
      return l.setChildren(children);
    }
  }
});

/**
 * areas Reducer
 * @param state
 * @param action {object}
 * @returns {*}
 */
const areasReducer = (state = initialState, action) => {
  const { type, payload, error } = action;

  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }
  
  // switch on action type
  switch (type) {
    case RESET: return { ...initialState };

    case types.SELECT_AREA: return selectLocation(state, payload);

    case types.SELECT_ZONE: return selectZone(state, payload);

    case types.SET_ALL_AREAS: return update(state, { locations: { $set: payload } });

    case types.UPDATE_AREA: return updateLocation(state, payload);

    case types.SET_CHILDREN: return updateChildren(state, payload);

    case types.ADD_AREA: return addLocation(state, payload.parent, payload.location);

    case types.REMOVE_AREA: return removeLocation(state, payload);

    case types.AREAS_FETCHAREAS: return { ...state, geoData: payload.map };

    case types.AREAS_FETCHFEATURES: return { ...state, featuresData: payload };

    case types.SET_ALL_FLOORS: return update(state, { floors: { $set: payload } });

    default: return state;
  }
};

export default areasReducer;
