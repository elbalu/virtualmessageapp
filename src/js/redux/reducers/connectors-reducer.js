import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/connectors-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  connectors: [],
  filteredConnectors: {
    data: [],
    count: 0
  },
  connectorDetails: {
    connector: {},
  },
};

export default function (state = initialState, { type, payload, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case C.GET_ALL_CONNECTORS:
      return {
        ...state,
        connectors: payload,
      };

    case C.GET_FILTERED_CONNECTORS:
      return {
        ...state,
        filteredConnectors: payload,
      };

    case C.ADD_CONNECTOR:
      return {
        ...state,
        connectors: state.connectors.concat(payload),
      };

    case C.REMOVE_CONNECTOR:
      return {
        ...state,
        connectors: state.connectors.filter(connector => connector.id !== payload),
      };

    case C.EDIT_CONNECTOR:
      return {
        ...state,
        connectors: state.connectors.map((connector) => {
          if (connector.id === payload.id) return payload;
          return connector;
        })
      };

    case C.GET_CONNECTOR_DETAILS:
      return Object.assign({}, state, {
        connectorDetails: {
          connector: payload,
        }
      });

    default:
      return state;
  }
}
