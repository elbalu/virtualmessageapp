import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/device-constants';
import * as A from '../constants/areas-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
    devices: [],
    activeCounts: {},
    activeFloorClients: {}, //geoJSON object
    activeFloorRogues: {}, //geoJSON object
    activeFloorRogueAPs: {}, //geoJSON object
    activeFloorTags: {}, //geoJSON object
    categories: {}, //key category name, value category options

};

const setCategories = (state, payload) => {
    // save user to local storage
    userUtils.login(Object.assign(userUtils.getUser(), user));

    return {
        ...state,
        currentUser: Object.assign({}, state.currentUser, user),
        resources: Object.assign([], state.resources, user.resources),
        error: null,
    };
};

export default function (state = initialState, { type, payload, error }) {
    if (error) {
        const status = _get(payload, 'response.statusCode', 0);
        if (status === 401) logout();
        else return state;
    }
    switch (type) {
        case RESET:
            return { ...initialState };
        case C.GET_ALL_CLIENTS:
            return {
                ...state,
                devices: payload
            };
        case C.GET_CLIENTS_BY_FLOORID:
            return {
                ...state,
                activeFloorClients: payload
            };
        case C.GET_ROGUES_BY_FLOORID:
            return {
                ...state,
                activeFloorRogues: payload
            };
        case C.GET_ROGUE_APS_BY_FLOORID:
            return {
                ...state,
                activeFloorRogueAPs: payload
            };
        case C.GET_TAGS_BY_FLOORID:
            return {
                ...state,
                activeFloorTags: payload
            };
        case C.GET_DEVICE_COUNTS:
            return {
                ...state,
                activeCounts: payload
            };
        case A.SELECT_AREA:
            return {
                ...state,
                activeCounts: {},
                activeFloorClients: {}
            };
        default:
            return state;
    }
}