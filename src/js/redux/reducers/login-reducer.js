import * as C from '../constants/login-constants';
import * as userUtils from '../../misc/user-utils';
import { RESET } from '../constants/settings-constants';

/**
 * sets a user
 *
 * @param {Object} state
 * @param {User} user
 * @return {{currentUser: *, resources: *, error: null}}
 */
const setUser = (state, user) => {
  // save user to local storage
  userUtils.login(Object.assign(userUtils.getUser(), user));

  return {
    ...state,
    currentUser: Object.assign({}, state.currentUser, user),
    resources: Object.assign([], state.resources, user.resources),
    error: null,
  };
};

const initialState = {
  currentUser: {},
  selectedRole: {},
  roles: [],
  error: null,
  resources: [],
  cmxToken: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
};

export default function loginReducer(state = initialState, { type, payload, error }) {
  if (error) return state;

  switch (type) {
    case RESET:
      return { ...initialState };

    case C.SET_USER:
      return setUser(state, payload);

    case C.LOGOUT:
      userUtils.logout();
      return state;

    case C.SET_ROLES:
      return {
        ...state,
        roles: payload,
      };

    case C.SELECT_ROLE:
      return {
        ...state,
        selectedRole: payload,
      };

    case C.LOGIN_ERROR:
      return {
        ...state,
        error: payload,
      };

    case C.CMX_TOKEN:
      return {
        ...state,
        cmxToken: payload,
      };

    case C.CMX_TOKEN_DEFAULT:
      return {
        ...state,
        cmxToken: initialState.cmxToken,
      };

    default:
      return state;
  }
}
