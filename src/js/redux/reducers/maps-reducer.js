import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as types from '../constants/maps-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  floors: [],
  currentFloor: {},
  activeLayer: undefined,
  currentLayerType: null,
  layers: [],
  sdkModal: false,
  customModal: false,
  zonesByFloorId: {}
};

/**
 * maps Reducer
 * @param state
 * @param action {object}
 * @returns {*}
 */
const mapsReducer = (state = initialState, action) => {
  // destructure action
  const { type, payload, error } = action;

  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }


  // switch on action type
  switch (type) {
    case RESET:
      return { ...initialState };

    case types.MAPS_CREATEZONE:
      return {
        ...state,
        // your code here
      };

    case types.SELECT_CURRENT_FLOOR:
      return {
        ...state,
        currentFloor: payload,
      };

    case types.SET_MAP_FLOORS:
      return {
        ...state,
        floors: payload,
      };

    case types.SET_MAP_CATEGORY:
      return {
        ...state,
        floors: payload,
      };

    case types.SET_LAYERS:
      return {
        ...state,
        layers: payload,
      };

    case types.SET_ACTIVE_LAYER:
      return {
        ...state,
        activeLayer: payload,
      };

    case types.ADD_LAYER:
      return {
        ...state,
        layers: state.layers.concat(payload),
      };

    case types.UPDATE_LAYER:
      return {
        ...state,
        layers: state.layers.map((l) => {
          if (l.id === payload.id) {
            return { ...l, ...payload };
          }
          return l;
        }),
      };

    case types.REMOVE_LAYER:
      return {
        ...state,
        layers: state.layers.filter(l => l.id !== payload),
      };

    case types.SET_TIME_LINE_ITEM:
      return {
        ...state,
        timeLineItem: payload,
      };

    case types.OPEN_ASSET_MODAL:
      return {
        ...state,
        sdkModal: true,
      };

    case types.CLOSE_ASSET_MODAL:
      return {
        ...state,
        sdkModal: false,
      };

    case types.OPEN_CUSTOM_MODAL:
      return {
        ...state,
        customModal: true,
      };

    case types.CLOSE_CUSTOM_MODAL:
      return {
        ...state,
        customModal: false,
      };

    case types.SET_CURRENT_LAYER_TYPE:
      return {
        ...state,
        currentLayerType: payload,
      };

    case types.GET_ZONES_BY_FLOOR_ID:
      return {
        ...state,
        zonesByFloorId: payload,
      };

    case types.CLEAR_ZONES:
      return {
        ...state,
        zonesByFloorId: {},
      };

    default:
      return state;
  }
};

export default mapsReducer;
