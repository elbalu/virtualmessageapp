import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as types from '../constants/modal-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  entityModal: {
    visible: true,
    data: {}
  },
  fieldsModal: {
    visible: false,
    data: {}
  },
  deviceDetailsModal: {
    visible: false,
    dataList: []
  },
  filterModal: {
    visible: false,
    data: {}
  }
};

const modalReducer = (state = initialState, action) => {
  const { type, payload, error } = action;

  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case types.OPEN_ENTITY_MODAL:
      return Object.assign({}, state, {
        entityModal: {
          visible: true,
          data: payload,
        },
      });

    case types.OPEN_FIELDS_MODAL:
      return Object.assign({}, state, {
        fieldsModal: {
          visible: true,
          data: payload,
        },
      });

    case types.OPEN_DETAILS_MODAL:
      return Object.assign({}, state, {
        deviceDetailsModal: {
          visible: true,
          dataList: payload.dataList
        },
      });

    case types.OPEN_FILTER_MODAL:
      return Object.assign({}, state, {
        filterModal: {
          visible: true,
          data: payload,
        },
      });

    case types.CLOSE_MODAL:
      return Object.assign({}, initialState);

    default:
      return state;
  }
};

export default modalReducer;
