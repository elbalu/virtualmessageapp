import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/navigation-constants';
import { RESET } from '../constants/settings-constants';

const initialState = [];

// TODO add documentation comments to navigationReducer
export default function navigation(state = initialState, { type, payload, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return [...initialState];

    case C.SET_NAV:
      return payload;

    default:
      return state;
  }
}
