import _get from 'lodash/get';
import _isObject from 'lodash/isObject';

import Notification from '../../misc/models/notification';
import * as C from '../constants/notification-constants';
import { RESET } from '../constants/settings-constants';
import { logout } from '../../misc/user-utils';

const initialState = {};

/**
 * displays information through the
 * UI using the Notification class
 *
 * @param {Error|Notification} data
 * @return {*}
 */
/* eslint-disable no-console */
const display = (data) => {
    if (data instanceof Notification) {
        data.show();
    }

    if (data instanceof Error) {
        const isHTTPError = _isObject(_get(data, 'response'));

        if (isHTTPError) {
            Notification.fromAPIError(data).show();
        }
    }
};

/**
 * Error Reducer handles actions created by error action
 * creators and also handles any action with error === true
 * by storing the errors in the store.
 *
 * @param {Object} state
 * @param {string} type
 * @param {*} payload
 * @param {Error} error
 * @returns {Object} next state
 */
const notificationReducer = (state = initialState, { type, payload, error }) => {
    if (error) {
        const status = _get(payload, 'response.statusCode', 0);
        if (status === 401) logout();
        else display(payload);
    }

    switch (type) {
        case RESET:
            return { ...initialState };
        case C.DISPLAY_NOTIFICATION:
            display(payload);
            return state;
        default:
            return state;
    }
};

export default notificationReducer;