import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/path-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  path: {},
  selectedPath: {},
  activePair: []
};

export default function (state = initialState, { type, payload, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }
  switch (type) {
    case RESET:
      return { ...initialState };
    case C.GET_PATH_BY_FLOORID:
      return {
        ...state,
        path: payload
      };
    case C.ADD_PATH: {
      return {
        ...state,
        path: payload
      };
    }
    case C.UPDATE_PATH: {
      return {
        ...state,
        path: {
          ...state.path,
          features: state.path.features.map((p) => {
            if (p.properties.id === payload.properties.id) return payload;
            return p;
          })
        },
        selectedPath: payload
      };
    }
    case C.DELETE_PATH: {
      return {
        ...state,
        path: {
          ...state.path,
          features: state.path.features.filter(p => p.properties.id !== payload.properties.id),
          pairs: state.path.pairs.filter(p => !p.includes(payload.properties.id))
        },
        selectedPath: null
      };
    }
    case C.SET_SELECTED_PATH: {
      return {
        ...state,
        selectedPath: payload
      };
    }
    case C.SET_ACTIVE_PAIR: {
      return {
        ...state,
        activePair: payload
      };
    }
    default:
      return state;
  }
}
