import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/poi-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  poi: {},
  selectedPOI: null
};

export default function (state = initialState, { type, payload, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }
  switch (type) {
    case RESET:
      return { ...initialState };
    case C.GET_POI_BY_FLOORID:
      return {
        ...state,
        poi: payload
      };
    case C.ADD_POI:
      return {
        ...state,
        poi: {
          ...state.poi,
          features: state.poi.features.concat(payload)
        }
      };
    case C.UPDATE_POI: {
      return {
        ...state,
        poi: {
          ...state.poi,
          features: state.poi.features.map((p) => {
            if (p.properties.id === payload.properties.id) return payload;
            return p;
          })
        },
        selectedPOI: payload
      };
    }
    case C.DELETE_POI: {
      return {
        ...state,
        poi: {
          ...state.poi,
          features: state.poi.features.filter(p => p.properties.id !== payload.id)
        },
        selectedPOI: null
      };
    }
    case C.SET_SELECTED_POI: {
      return {
        ...state,
        selectedPOI: payload
      };
    }
    default:
      return state;
  }
}
