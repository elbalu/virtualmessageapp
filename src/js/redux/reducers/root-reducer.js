import { combineReducers } from 'redux';

import areas from './areas-reducer';
import devices from './device-reducers';
import maps from './maps-reducer';
import mapviewReducer from './mapview-reducer';
import navigation from './navigation-reducer';
import modal from './modal-reducer';
import users from './users-reducers';
import login from './login-reducer';
import wlcs from './wlcs-reducer';
import connectors from './connectors-reducer';
import tags from './tags-reducer';
import templates from './templates-reducer';
import table from './table-reducer';
import widgets from './widget-reducer';
import notifications from './notification-reducer';
import settings from './settings-reducer';
import poi from './poi-reducers';
import path from './path-reducers';

const rootReducer = combineReducers({
  areas,
  devices,
  maps,
  mapviewReducer,
  navigation,
  modal,
  users,
  login,
  wlcs,
  connectors,
  tags,
  templates,
  table,
  widgets,
  notifications,
  settings,
  poi,
  path
});

export default rootReducer;
