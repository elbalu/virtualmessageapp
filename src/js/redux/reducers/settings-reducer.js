import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import update from 'react-addons-update';
import { RESET, TOGGLE_NOTIFICATIONS } from '../constants/settings-constants';

const LocalStorage = window.localStorage;
const notificationKey = 'sdk-settings-notifications-a0e305e6-4b8d-4c27-8c1c-d5b867dfc2c6';

const initialState = {
  notifications: (LocalStorage[notificationKey] === 'true'),
};

/**
 * Settings reducer
 */
const settingsReducer = (state = initialState, action) => {
  const { type, payload, error } = action;

  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case TOGGLE_NOTIFICATIONS:
      LocalStorage[notificationKey] = String(payload);
      return update(state, { notifications: { $set: payload } });

    default:
      return state;
  }
};

export default settingsReducer;
