import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/table-constants';
import { RESET } from '../constants/settings-constants';
import _ from 'lodash';

const initialState = {
  filters: [],
  pageSize: 10,
  params: {
    pagination: {
      page: 0,
      limit: 10
    },
    fields: [],
    search: ''
  }
};

export default function (state = initialState, { type, payload, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case C.ADD_FILTER: {
      return Object.assign({}, state, {
        filters: payload || [],
      });
    }


    case C.REMOVE_FILTER:
      return Object.assign({}, state, {
        filters: state.categories.filter(c => c.id !== payload),
      });

    case C.CLEAR_FILTERS: {
      return Object.assign({}, state, {
        filters: [],
      });
    }

    case C.SET_TABLE_PARAMS: {
      return Object.assign({}, state, {
        params: Object.assign({}, state.params, payload.params),
        pageSize: payload.pageSize,
      });
    }

    case C.ADD_FIELDS: {
      return Object.assign({}, state, {
        params: Object.assign({}, state.params, payload)
      });
    }

    case C.CLEAR_PARAMS: {
      return Object.assign({}, state, {
        params: {
          pagination: {
            page: 0,
            limit: state.pageSize,
          },
          fields: [],
        }
      });
    }

    case C.REMOVE_FIELD: {
      const fields = _.remove(state.params.fields, (f) => {
        return f.key !== payload.key;
      });
      return Object.assign({}, state, {
        params: Object.assign({}, state.params, {
          fields,
        })
      });
    }

    case C.RESET_PAGING: {
      const updateParams = state.params;
      updateParams.pagination.page = 0;
      return Object.assign({}, state, {
        params: updateParams
      });
    }

    case C.ADD_SEARCH: {
      return Object.assign({}, state, {
        params: Object.assign({}, state.params, {
          search: payload
        })
      });
    }


    default:
      return state;
  }
}
