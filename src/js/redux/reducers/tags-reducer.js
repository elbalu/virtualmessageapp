import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/tags-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  tags: [],
  filteredTags: {
    data: [],
    count: 0
  },
};

export default function (state = initialState, { type, payload, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case C.GET_ALL_TAGS:
      return {
        ...state,
        tags: payload,
      };

    case C.GET_FILTERED_TAGS:
      return {
        ...state,
        filteredTags: payload,
      };

    case C.ADD_TAG:
      return {
        ...state,
        tags: state.tags.concat(payload),
      };

    case C.REMOVE_TAG:
      return {
        ...state,
        tags: state.tags.filter(tag => tag.id !== payload),
      };

    case C.EDIT_TAG:
      return {
        ...state,
        tags: state.tags.map((tag) => {
          if (tag.id === payload.id) return payload;
          return tag;
        }),
        filteredTags: {
          data: state.filteredTags.data.map((tag) => {
            if (tag.id === payload.id) {
              return payload;
            }
            return tag;
          }),
          count: state.filteredTags.count
        }
      };

    default:
      return state;
  }
}
