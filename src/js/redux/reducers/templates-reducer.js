import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import _camelCase from 'lodash/camelCase';
import { message } from 'antd';

import config from '../../config';
import { RESET } from '../constants/settings-constants';
import * as types from '../constants/templates-constants';
import Logger from '../../misc/logger';

const entityNames = config.get('entityNames');
const initialState = {};

/**
 * Adds a template to the templates object
 * in state and returns the new state.
 *
 * @param {Object} state Redux reducer estate
 * @param {Object} template Template instance
 * @return {Object} next state
 */
const addTemplate = (state, template) => {
  const templateKey = _camelCase(template.entityName);

  if (!templateKey) {
    Logger.error('Templates Reducer', `Template missing entity name: ${template}`);
    return state;
  }

  // add template to templates object
  return { ...state, [templateKey]: template };
};

/**
 * Adds a field to an existing template
 * and returns the new reducer state.
 *
 * @param {Object} state Redux reducer estate
 * @param {Object} field Field instance
 * @return {Object} next state
 */
const addField = (state, field) => {
  const templateKey = _camelCase(field.entityName);
  const template = state[templateKey];

  // user feedback
  message.success(`${field.label} was created`);

  if (field.entityName === entityNames.customCategory.key) {
    return state;
  }

  // add field to fields array of template
  const nextFields = [...template.fields, field];

  // update template with new fields
  const nextTemplate = { ...template, fields: nextFields };

  // update state with new template
  return { ...state, [templateKey]: nextTemplate };
};

/**
 * Updates a field in an existing template
 * and returns the new reducer state.
 *
 * @param {Object} state Redux reducer estate
 * @param {Object} field Field instance
 * @return {Object} next state
 */
const updateField = (state, field) => {
  const templateKey = _camelCase(field.entityName);
  const template = state[templateKey];

  // user feedback
  message.success(`${field.label} was updated`);

  if (field.entityName === entityNames.customCategory.key) {
    return state;
  }

  // generate new fields array where updated field has been replaced
  // replace template fields array with updated fields
  const nextFields = template.fields.map((f) => {
    if (f.key === field.key) return field;
    return f;
  });

  // update template with new fields
  const nextTemplate = { ...template, fields: nextFields };

  // update state with new template
  return { ...state, [templateKey]: nextTemplate };
};

/**
 * Removes the field from the correct template's
 * fields array and returns the new state.
 *
 * @param {Object} state Redux reducer estate
 * @param {Object} field Field instance
 * @return {Object} next state
 */
const deleteField = (state, field) => {
  const templateKey = _camelCase(field.entityName);
  const template = state[templateKey];

  // user feedback
  message.success(`${field.label} was deleted`);

  if (field.entityName === entityNames.customCategory.key) {
    return state;
  }

  // generate new fields array where deleted
  // field has been removed
  // replace template fields array with updated fields
  const nextFields = template.fields.filter(f => f.key !== field.key);

  // update template with new fields
  const nextTemplate = { ...template, fields: nextFields };

  // update state with new template
  return { ...state, [templateKey]: nextTemplate };
};


/**
 * Returns a new state object based on the action passed in.
 * Specifically performs state updates on actions related to
 * templates and fields.
 *
 * @param {Object} state
 * @param {string} type
 * @param {Object} payload
 * @param {Boolean} error
 * @return {{templates: * }}
 */
const templatesReducer = (state = initialState, { type, payload, error }) => {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case types.GET_TEMPLATE:
      return addTemplate(state, payload);

    case types.CREATE_FIELD:
      return addField(state, payload);

    case types.UPDATE_FIELD:
      return updateField(state, payload);

    case types.DELETE_FIELD:
      return deleteField(state, payload);

    default:
      return state;
  }
};

export default templatesReducer;
