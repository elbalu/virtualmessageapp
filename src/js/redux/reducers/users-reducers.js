import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as types from '../constants/users-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  users: [],
  template: {}
};

/**
 * Users Reducer
 * @param state
 * @param action {object}
 * @returns {*}
 */
const usersReducer = (state = initialState, action) => {
  // destructure action
  const { type, payload, error } = action;

  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  // switch on action type
  switch (type) {
    case RESET:
      return { ...initialState };

    case types.GET_ALL_USERS:
      return {
        ...state,
        users: payload,
      };

    case types.ADD_USER:
      return {
        ...state,
        users: state.users.concat(payload),
      };

    case types.EDIT_USER:
      return {
        ...state,
        users: [...state.users.map((user) => {
          if (user.email === payload.email) {
            return payload;
          }
          return user;
        })],
      };

    case types.REMOVE_USER:
      return {
        ...state,
        users: state.users.filter(u => u.email !== payload),
      };

    case types.GET_USER_FIELDS:
      return {
        ...state,
        template: payload.userTemplate,
      };

    case types.GET_INVITE_USER_FIELDS:
      return {
        ...state,
        inviteUserTemplatemplate: payload.inviteUserTemplate,
      };

    case types.SET_USER_FIELDS:
      return Object.assign({}, state, {
        template: { ...payload },
      });

    case types.DISABLE_EMAIL_FIELD:
      return {
        ...state,
        template: {
          fields: [...state.template.fields.map((field) => {
            if (field.key === 'email') {
              if (payload) Object.assign(field, { disabled: true });
              else Object.assign(field, { disabled: false });
              return field;
            }
            return field;
          })]
        }
      };

    default:
      return state;
  }
};

export default usersReducer;
