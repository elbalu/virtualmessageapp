import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import _map from 'lodash/map';
import * as C from '../constants/widget-constants';
import { RESET } from '../constants/settings-constants';

/**
 * initial state of the alerts reducer
 * @type {{template: {}, alerts: Array}}
 */
const initialState = {
  allWidgets: [],
  widgetTypes: [],
  filteredWidgets: [],
  widgetTimeRanges: [],
  selectedWidget: {},
  forwardWidgetDataSchema: {},
  widgetParams: [],
  pageQuery: {},
  tempMarker: {}
};

/**
 * Alerts reducer
 * @param state
 * @param type
 * @param payload
 * @param error
 * @returns {*}
 */
export default function (state = initialState, { type, payload, id, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case C.WIDGET_TIME_RANGES:
      return Object.assign({}, state, { widgetTimeRanges: payload });

    case C.GET_ALL_WIDGETS:
      return {
        ...state,
        allWidgets: payload,
        filteredWidgets: payload,
      };

    case C.GET_WIDGET_COUNT:
      return {
        ...state,
        allWidgets: state.allWidgets.map((p) => {
          const w = p;
          if (w.id === id) {
            w.data.count = payload;
          }
          return w;
        }),
        filteredWidgets: state.filteredWidgets.map((p) => {
          const w = p;
          if (w.id === id) {
            w.data.count = payload;
          }
          return w;
        }),
      };


    case C.GET_WIDGET_TYPES:
      return {
        ...state,
        widgetTypes: payload,
      };


    case C.SELECT_FILTER_TYPE:
      return Object.assign({}, state, {
        filteredWidgets: (payload === 'ALL') ? state.allWidgets : state.allWidgets.filter(w => w.data.filterType === payload),
      });

    case C.ADD_WIDGET: {
      const widgets = state.allWidgets;
      widgets.splice(1, 0, payload);
      return Object.assign({}, state, {
        allWidgets: widgets,
        filteredWidgets: [...state.filteredWidgets, payload],
      });
    }

    case C.DELETE_WIDGET: {
      return Object.assign({}, state, {
        allWidgets: state.allWidgets.filter(w => w.id !== payload.id),
        filteredWidgets: state.filteredWidgets.filter(w => w.id !== payload.id),
      });
    }

    case C.SELECT_WIDGET:
      return {
        ...state,
        selectedWidget: payload,
      };

    case C.UPDATE_WIDGET: {
      return Object.assign({}, state, {
        allWidgets: state.allWidgets.map((w) => {
          if (w.id !== payload.id) return w;
          return payload;
        }),
        filteredWidgets: state.filteredWidgets.map((w) => {
          if (w.id !== payload.id) return w;
          return payload;
        }),
      });
    }


    case C.FORWARD_DATA_SCHEMA: {
      return Object.assign({}, state, { forwardWidgetDataSchema: payload });
    }

    case C.UPDATE_WIDGET_PARAMS:
      return Object.assign({}, state, {
        widgetParams: payload || {}
      });
    case C.CLEAR_WIDGETS:
      return Object.assign({}, state, {
        allWidgets: payload,
        selectedWidget: {},
        widgetParams: [],
      });

    case C.SET_PAGE_QUERY:
      return Object.assign({}, state, {
        pageQuery: payload,
      });

    case C.SET_TEMP_MARKER:
      return Object.assign({}, state, { tempMarker: payload || {} });

    default:
      return state;
  }
}
