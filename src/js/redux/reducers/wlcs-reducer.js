import _get from 'lodash/get';

import { logout } from '../../misc/user-utils';
import * as C from '../constants/wlcs-constants';
import { RESET } from '../constants/settings-constants';

const initialState = {
  wlcs: [],
  accessToken: '',
  filteredWlcs: {
    data: [],
    count: 0
  },
  wlcDetails: {
    wlc: {},
  },
};

export default function (state = initialState, { type, payload, error }) {
  if (error) {
    const status = _get(payload, 'response.statusCode', 0);
    if (status === 401) logout();
    else return state;
  }

  switch (type) {
    case RESET:
      return { ...initialState };

    case C.GET_TENANT_TOKEN:
      return {
        ...state,
        accessToken: payload,
      };

    case C.GET_ALL_WLCS:
      return {
        ...state,
        wlcs: payload,
      };

    case C.GET_FILTERED_WLCS:
      return {
        ...state,
        filteredWlcs: payload,
      };

    case C.ADD_WLC:
      return {
        ...state,
        wlcs: state.wlcs.concat(payload),
      };

    case C.REMOVE_WLC:
      return {
        ...state,
        wlcs: state.wlcs.filter(wlc => wlc.id !== payload),
      };

    case C.EDIT_WLC:
      return {
        ...state,
        wlcs: state.wlcs.map((wlc) => {
          if (wlc.id === payload.id) return payload;
          return wlc;
        })
      };

    case C.GET_WLC_DETAILS:
      return Object.assign({}, state, {
        wlcDetails: {
          wlc: payload,
        }
      });

    default:
      return state;
  }
}
