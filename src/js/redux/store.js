import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import config from '../config';
import { hasAccess } from '../misc/user-utils';

import rootReducer from './reducers/root-reducer';
//import { getAllTags } from './actions/tag-actions';
import { getAllAreas, getAllFloors } from './actions/areas-actions';
//import { getAllRoles } from './actions/roles-actions';
import { getAllUsers } from './actions/users-actions';
// import { getTemplate, getDetailTemplate } from './actions/templates-actions';

/**
 * create a single redux store. thunk is middleware that
 * enables async action creators. this is necessary for
 * fetching data from the server without blocking the ui.
 * the composeEnhancers method adds support for react dev
 * tools, which also has a chrome extension for debugging.
 * The dev tools are only active when
 * process.env.NODE_ENV = 'development'
 */
/* eslint-disable  no-underscore-dangle */
/* eslint-disable  no-undef */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk)),
);

const fetchScopedAPIs = () => {
  const hasUserAccess = hasAccess('user', 'list');
  if (hasUserAccess) {
  }
};

const fetchAllAPIs = () => {
  store.dispatch(getAllAreas());
  store.dispatch(getAllFloors());
  //store.dispatch(getAllUsers());

  //store.dispatch(getRulesTemplate());
  //store.dispatch(getTemplate(entityNames.sdk.key));
  // store.dispatch(getTemplate(entityNames.categories.key));
  // store.dispatch(getTemplate(entityNames.departments.key));
  // store.dispatch(getTemplate(entityNames.tags.key));
  // store.dispatch(getDetailTemplate());
  // store.dispatch(getAllRules());
};

export const fetchInitialData = () => {
  // fetch initial data
  fetchAllAPIs();
};

export default store;
