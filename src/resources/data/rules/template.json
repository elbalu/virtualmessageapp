{
  "information": [
    {
      "type": "text",
      "label": "Name",
      "key": "name",
      "required": true,
      "placeholder": "My Rule"
    },
    {
      "type": "textarea",
      "label": "Description",
      "key": "description",
      "placeholder": "My Rule checks that battery is above 10% and temperature is above 40 C for all sdk."
    },
    {
      "type": "radio-group",
      "label": "Priority",
      "options": [
        "Low",
        "Medium",
        "High"
      ],
      "key": "priority",
      "required": true,
      "description": "Alerts triggered by the rule will have this priority level"
    },
    {
      "type": "switch",
      "label": "Enabled",
      "key": "enabled"
    },
    {
      "type": "switch",
      "label": "Advanced",
      "key": "advanced",
      "description": "Enabling advanced mode allows you to set the Asset Filter and Trigger Options for each condition block. With advanced mode off, you set these options only once for the entire rule."
    }
  ],
  "email": [
    {
      "type": "text",
      "label": "Subject",
      "key": "subject",
      "required": true,
      "placeholder": "My Rule Was Triggered"
    },
    {
      "type": "textarea",
      "label": "Message",
      "key": "message",
      "placeholder": "Please go and change the batteries of the sdk"
    },
    {
      "type": "select",
      "multiple": true,
      "label": "Users",
      "collection": "users",
      "collectionKey": "id",
      "collectionLabel": "name",
      "key": "users",
      "placeholder": "John Smith, Jane Smith"
    },
    {
      "type": "text",
      "label": "Email Addresses",
      "placeholder": "john@gmail.com, jane@email.com, ...",
      "key": "direct",
      "multiple": true,
      "max": 20,
      "validator": "email",
      "description": "We will send notifications to these additional email addresses"
    },
    {
      "type": "numeric",
      "label": "Send Every",
      "options": [
        "minutes",
        "hours",
        "days",
        "weeks"
      ],
      "key": "frequency",
      "description": "We will not send notifications more often than this frequency",
      "placeholder": "2"
    }
  ],
  "sms": [
    {
      "type": "textarea",
      "label": "Message",
      "key": "message",
      "required": true,
      "placeholder": "Please go and change the batteries of the sdk"
    },
    {
      "type": "select",
      "multiple": true,
      "label": "Users",
      "collection": "users",
      "collectionKey": "id",
      "collectionLabel": "name",
      "key": "users",
      "placeholder": "John Smith, Jane Smith"
    },
    {
      "type": "text",
      "label": "Phone Numbers",
      "placeholder": "+1 408-111-2222, ...",
      "key": "direct",
      "multiple": true,
      "max": 20,
      "validator": "phone",
      "description": "We will send notifications to these additional phone numbers"
    },
    {
      "type": "numeric",
      "label": "Send Every",
      "key": "frequency",
      "options": [
        "minutes",
        "hours",
        "days",
        "weeks"
      ],
      "description": "We will not send notifications more often than this frequency",
      "placeholder": "2"
    }
  ],
  "http": [
    {
      "type": "textarea",
      "label": "Urls",
      "key": "direct",
      "required": true,
      "validator": "url",
      "multiple": true,
      "max": 20,
      "placeholder": "http://api.com, http://api2.com, ...",
      "description": "You can add multiple urls as a comma separated list and we will send a GET request to each one when the rule is triggered"
    },
    {
      "type": "textarea",
      "label": "Message",
      "key": "message",
      "placeholder": "Please go and change the batteries of the sdk"
    },
    {
      "type": "numeric",
      "label": "Send Every",
      "key": "frequency",
      "options": [
        "minutes",
        "hours",
        "days",
        "weeks"
      ],
      "description": "We will not send notifications more often than this frequency",
      "placeholder": "2"
    }
  ],
  "attribute_update": [
    {
      "type": "select",
      "label": "Asset Attribute",
      "key": "path",
      "collection": "fields",
      "collectionKey": "key",
      "collectionLabel": "label",
      "required": true
    },
    {
      "type": "text",
      "label": "Change Value To",
      "key": "value",
      "required": true
    }
  ],
  "attribute": [
    {
      "type": "select",
      "label": "Asset Attribute",
      "key": "condition.path",
      "collection": "fields",
      "collectionKey": "key",
      "collectionLabel": "label",
      "required": true
    },
    {
      "type": "select",
      "label": "Operator",
      "key": "condition.operator",
      "required": true,
      "options": [
        "Above",
        "Between",
        "Below",
        "NotEqual",
        "Equal"
      ]
    },
    {
      "type": "text",
      "label": "Value",
      "key": "condition.value",
      "required": true
    }
  ],
  "battery": [
    {
      "type": "range",
      "label": "Threshold",
      "key": "condition.threshold",
      "min": 0,
      "options": [
        "percentage",
        "days"
      ],
      "required": true
    }
  ],
  "location": [
    {
      "type": "radio-group",
      "label": "Type",
      "key": "condition.type",
      "collection": "options",
      "collectionLabel": "label",
      "collectionKey": "value",
      "description": "This is the type of location change that you want to track"
    }
  ],
  "temperature": [
    {
      "type": "range",
      "label": "Threshold",
      "key": "condition.threshold",
      "options": [
        "celsius",
        "fahrenheit"
      ],
      "required": true
    }
  ],
  "missing": [
    {
      "type": "switch",
      "label": "Notify On Return",
      "key": "condition.notifyOnReappearance",
      "description": "We will send you a notification when the sdk(s) reappear"
    }
  ],
  "filter": [
    {
      "type": "checkbox-group",
      "label": "Filter Assets By",
      "options": [
        "Department",
        "Category"
      ],
      "key": "filter.filterBy",
      "description": "We will only check against sdk in the categories or departments that you choose"
    },
    {
      "type": "select",
      "label": "Departments",
      "collection": "departments",
      "collectionLabel": "name",
      "collectionKey": "id",
      "multiple": true,
      "key": "filter.department",
      "placeholder": "Select Some Departments"
    },
    {
      "type": "select",
      "label": "Categories",
      "collection": "categories",
      "collectionLabel": "detailedName",
      "collectionKey": "id",
      "multiple": true,
      "key": "filter.category",
      "placeholder": "Select Some Categories"
    },
    {
      "type": "select",
      "label": "Apply to Assets",
      "collection": "sdk",
      "collectionLabel": "detailedName",
      "collectionKey": "id",
      "multiple": true,
      "key": "filter.sdk",
      "placeholder": "Select Some Assets"
    }
  ],
  "trigger": [
    {
      "type": "numeric",
      "label": "Trigger After Time",
      "min": 0,
      "key": "period",
      "options": [
        "minutes",
        "hours",
        "days",
        "weeks"
      ],
      "description": "Let the sdk(s) meet the condition for this long before notifying anybody"
    },
    {
      "type": "range",
      "label": "Trigger After Count",
      "key": "count",
      "min": 0,
      "options": [
        "percentage",
        "count"
      ],
      "description": "Only trigger the rule if this many sdk meet the condition"
    }
  ],
  "humidity": [
    {
      "type": "range",
      "label": "Threshold",
      "key": "condition.threshold",
      "options": [
        "%"
      ],
      "required": true
    }
  ],
  "collections": {
    "options": [
      {
        "label": "dwell",
        "value": "INSIDE"
      },
      {
        "label": "entrance",
        "value": "ENTRANCE"
      },
      {
        "label": "exit",
        "value": "EXIT"
      },
      {
        "label": "distance",
        "value": "UPDATE"
      },
      {
        "label": "movement",
        "value": "LOCATION_AREA_CHANGE"
      }
    ]
  }
}
