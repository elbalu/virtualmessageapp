/**
 *
 */
class Component {
  /**
   * @constructor
   * @param {Object} browser Webdriver browser object
   * @param {Object} assert Reference to assert module
   * @param {String} [baseUrl]
   */
  constructor({
      browser,
      assert,
      baseUrl = 'http://localhost:3000',
  }) {
    this.browser = browser;
    this.assert = assert;
    this.baseUrl = baseUrl;

    // shortcuts to select DOM elements
    this.$ = browser.element;
    this.$$ = browser.elements;
  }

  /**
   * Opens the given baseUrl+path
   */
  open(path) {
    const { browser, baseUrl } = this;
    const url = `${baseUrl}${path}`;

    this.url = url;
    browser.newWindow(url);

    return this;
  }

  /**
   * shorcut to browser.pause() with
   * default wait time
   *
   * @param {number} milliseconds
   */
  pause(milliseconds = 1000) {
    this.browser.pause(milliseconds);
    return this;
  }

  /**
   * shortcut to browser logger
   * info method
   *
   * @param {string} message
   */
  log(message = '') {
    this.browser.logger.info(message);
    return this;
  }

  /**
   * shortcut to browser logger
   * error method
   *
   * @param {string} message
   */
  error(message = '') {
    this.browser.logger.error(message);
    return this;
  }

  /**
   * verifies that there are elements
   * that match all passed in selectors
   *
   * @param {String} selectors List of CSS selectors
   */
  verifyElements(...selectors) {
    const { browser, assert } = this;

    // verify that there are elements that
    // match all selectors for login page
    selectors.forEach((selector) => {
      const elementExists = browser.isExisting(selector);
      const doesNotExistMessage = `${selector} does not exist`;
      assert(elementExists, doesNotExistMessage);

      const elementIsVisible = browser.isVisible(selector);
      const invisibleMessage = `${selector} is not visible`;
      assert(elementIsVisible, invisibleMessage);
    });

    return this;
  }
}

module.exports = Component;
