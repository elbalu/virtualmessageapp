/* eslint-disable no-undef */
const Component = require('../components/base.component');

/**
 * Page represents a web browser page. It depends
 * on the Webdriver browser object and is designed
 * to be used with Webdriver functional test suites.
 * Page provides some base methods but is meant to
 * be subclassed for specific pages. This design pattern
 * (using page classes as a wrapper around the browser
 * object) is a recommendation by webdriver.io
 *
 * @author Balu Loganathan <baepalap@cisco.com>
 */
class Page extends Component {}

module.exports = Page;
