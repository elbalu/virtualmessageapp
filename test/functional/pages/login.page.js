const Page = require('./base.page');
const USER = Object.freeze(require('../json/login.json'));

/**
 * LoginPage
 */
class LoginPage extends Page {

  static get SELECTORS() {
    return {
      username: '#username',
      password: '#password',
      loginButton: '.login-form button',
      loginMessage: '.login-message span',
    };
  }

  get usernameInput() { return this.$(LoginPage.SELECTORS.username); }
  get passwordInput() { return this.$(LoginPage.SELECTORS.password); }
  get loginButton() { return this.$(LoginPage.SELECTORS.loginButton); }
  get loginMessage() { return this.$(LoginPage.SELECTORS.loginMessage); }

  /**
   * @override
   */
  open() {
    return super.open('/');
  }

  /**
   * performs login action by entering
   * a username and password and then
   * clicking the login button
   *
   * @param {String} username
   * @param {String} password
   * @return {LoginPage}
   */
  login(username = USER.admin.email, password = USER.admin.password) {
    const { usernameInput, passwordInput, loginButton } = this;

    // verify that form fields and button
    // exist
    this.verifyElements(
      LoginPage.SELECTORS.username,
      LoginPage.SELECTORS.password,
      LoginPage.SELECTORS.loginButton
    );

    // fill form fields
    usernameInput.setValue(username);
    passwordInput.setValue(password);

    // submit login
    loginButton.click();

    return this;
  }
}

module.exports = LoginPage;
