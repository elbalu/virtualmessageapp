const Page = require('./base.page');
const AssetDetailsPage = require('./assetDetails.page');
const assert = require('assert');

const selectors = {
  assetsMapLink: 'a[href="#mapview"]',
  assetsListLink: 'a[href="#listview"]',
  map: '#main-map',
  mapImage: 'img.leaflet-image-layer',
  location: '.location-select',
  locationText: '.location-select .ant-cascader-picker-label',
  locationMenu: '.ant-cascader-menu',
  campuses: '.ant-cascader-menus ul:first-child .ant-cascader-menu-item',
  buildings: '.ant-cascader-menus ul:nth-child(2) .ant-cascader-menu-item',
  categories: '.category-selector-wrapper .category',
  allAssetCount: '.category-selector-wrapper li:first-child .category-count',
  markerIcon: '.leaflet-marker-icon',
  firstMarker: '.leaflet-marker-pane .leaflet-marker-icon:first-child',
};

/**
 * Assets Map view Page
 */
class MapviewPage extends Page {

  /**
   * @constructor
   * @param {Object}
   */
  constructor(config) {
    super(config);
    this.assetDetails = new AssetDetailsPage(config);
  }

  get assetsMapLink() { return this.browser.element(selectors.assetsMapLink); }
  get assetsListLink() { return this.browser.element(selectors.assetsListLink); }
  get location() { return this.browser.element(selectors.location); }
  get campuses() { return this.browser.elements(selectors.campuses); }
  get buildings() { return this.browser.elements(selectors.buildings); }
  get categorySelector() { return this.browser.element(selectors.categorySelector); }
  get allAssetCount() { return this.browser.getText(selectors.allAssetCount); }
  get firstMarker() { return this.browser.element(selectors.firstMarker); }

  /**
   * navigates to mapview page
   * using left nav buttons
   */
  navigateToMapViewPage() {
    // verify and search for assets
    this.verifyElements(selectors.assetsMapLink);
    this.assetsMapLink.click();
  }

  navigateToListViewPage() {
    // verify and search for assets
    this.verifyElements(selectors.assetsListLink);
    this.assetsListLink.click();
  }

  /**
   * check if map has
   * loaded with an image
   */
  hasMapLoaded() {
    this.browser.waitForExist(selectors.map);

    this.browser.waitForExist(selectors.mapImage);

    // TODO is there a better way to wait until image loads?
    this.browser.pause(1000);

    // Also check if image has loaded
    // img tag always loads but the image itself may not load due to incorrect url or missing image
    // The only way to check this is to check the naturalWidth for the image.
    // If it is 0 that means the image hasent loaded.
    const result = this.browser.execute(() => document.querySelectorAll('img.leaflet-image-layer')[0].naturalWidth);

    assert(result.value > 0);
  }

  /**
   * check if updating location updates
   * categories and map
   */
  changeLocation() {
    this.browser.waitForExist(selectors.location);

    // check if one location is selected by default
    assert.notEqual(this.browser.getText(selectors.locationText), '');
    const categories = this.browser.elements(selectors.categories);
    const assetCount = this.allAssetCount;

    // check if markers on the map match the asset count
    // TODO: this does not work as same markers are generated multiple times in dom
    // click on one marker and test asset details
    const markers = this.browser.elements(selectors.markerIcon).value;
    if (markers.length > 0) {
      this.browser.moveToObject(selectors.firstMarker);
      this.browser.leftClick();

      this.assetDetails.verifyAssetDetails('MAP_VIEW');

      // this.firstMarker.click();
      // this.browser.pause(3000);
    }

    // assuming first one is ALL so checking if there is one or more categories
    if (categories > 1) {
    } else {
      // change location
      this.location.click();
      this.browser.waitForExist(selectors.locationMenu);
      // click on second campus if it exists
      if (this.campuses.value.length > 1) {
        const secondBuilding = this.campuses.value[1];
        secondBuilding.click();
        // check if buildings have loaded
        if (this.buildings.value.length > 0) {
          // click on first building if it exists
          const building = this.buildings.value[0];
          building.click();
        }
      }
    }
  }

  /**
   * open asset details popup
   */
  showAssetDetails() {
    this.assetDetails.showAssetDetails();
  }
}

module.exports = MapviewPage;
