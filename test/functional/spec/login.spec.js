/* eslint-disable no-undef */
const assert = require('assert');

const LoginPage = require('../pages/login.page');

let page = null;

/**
 * functional test suite
 */
describe('LoginPage', () => {
  before(() => {
    page = new LoginPage({ browser, assert });
    page.open();
  });

  it('should display error message on unsuccessful login', () => {
    page
    .log('Login: Wrong: Logging in with incorrect info...')
    .login('wrong', 'wrong');

    // assert that there is text
    // in the login message
    const loginMessage = page.loginMessage.getText();
    assert(loginMessage.length > 0);
    page.log('Login: Wrong: Done');
  });

  it('should login', () => {
    page
    .log('Login: Logging in...')
    .login()
    .log('Login: Done');
  });
});
