/* eslint-disable no-undef */
const assert = require('assert');

const MapViewPage = require('../pages/mapview.page');
const LoginPage = require('../pages/login.page');

let page = null;

/**
 * Map view page test suite
 */
describe('MapViewPage', () => {
  before(() => {
    const loginPage = new LoginPage({ browser, assert });
    page = new MapViewPage({ browser, assert });

    page.log('Map View Assets: Login: Logging in...');
    loginPage.open().login();
    page.pause();
    page.log('Map View Assets: Login: Done');
  });

  it('should navigate to map view assets page', () => {
    page.log('Map View Assets: Navigating:  Navigating to map view assets page');
    page.navigateToMapViewPage();
    page.pause();
    page.log('Map View Assets:  Navigating: Navigation done');
  });
  it('should load map', () => {
    page.log('Map View Assets: Load Map: Navigating to map view assets page');
    page.hasMapLoaded();
    page.log('Map View Assets: Load Map: done');
  });
  //
  it('load markers on the map', () => {
    page.log('Map View Assets: Markers: load markers on the map');
    page.changeLocation();
    page.log('Map View Assets: Markers: done');
  });

  it('should navigate to map view -> list view -> map view', () => {
    page.log('Map View Assets: Navigating:  Navigating to map view assets page');
    page.navigateToMapViewPage();
    page.log('Map View Assets: Navigating:  Navigating to list view assets page');
    page.navigateToListViewPage();
    page.log('Map View Assets: Navigating:  Navigating to map view assets page');
    page.navigateToMapViewPage();
    page.log('Map View Assets: Check if map loaded again');
    page.hasMapLoaded();
    page.log('Map View Assets: Markers: load markers on the map');
    page.changeLocation();
    page.pause();
    page.log('Map View Assets:  Navigating: Navigation done');
  });
});
