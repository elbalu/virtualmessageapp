import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

export default function chaiEnzymeSetup() {
  chai.use(chaiEnzyme());
}
