/* eslint-disable no-undef */
/* eslint-disable no-parsdk-reassign */
import { jsdom } from 'jsdom';

/**
 * jsdomSetup creates a mock browser environemnt
 * for testing frontend code.
 *
 * @param {Object} context - global object
 */
export default function jsdomSetup(context) {
  context.document = jsdom('');
  context.window = context.document.defaultView;

  // import localStorage mock script that
  // creates a localStorage object on
  // the global context
  /* eslint-disable global-require*/
  require('mock-local-storage');

  Object.keys(context.document.defaultView).forEach((property) => {
    if (typeof context[property] === 'undefined') {
      context[property] = context.document.defaultView[property];
    }
  });

  context.navigator = {
    userAgent: 'node.js'
  };

  const matchMediaStub = () => ({
    matches: false,
    addListener: () => {},
    removeListener: () => {}
  });

  // stub matchMedia required by enquire.js
  context.matchMedia = context.matchMedia || matchMediaStub;
}
