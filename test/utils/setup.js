import chaiEnzymeSetup from './chai-enzyme-setup';
import jsdomSetup from './jsdom-setup';

chaiEnzymeSetup();
jsdomSetup(global);
