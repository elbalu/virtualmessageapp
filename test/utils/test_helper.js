/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable mocha/no-exclusive-tests */
import React from 'react';
import _$ from 'jquery';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import jsdom from 'jsdom';
import chai, { expect } from 'chai';
//import chaiJquery from 'chai-jquery';
import { mount, shallow } from 'enzyme';


import { IntlProvider, intlShape } from 'react-intl';

import messages from '../../src/js/config/locale/en.json';

// Create the IntlProvider to retrieve context for wrapping around.
const intlProvider = new IntlProvider({ locale: 'en', messages }, {});
const { intl } = intlProvider.getChildContext();

// set up testing environment to run like a browser in the command line.
global.document = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.window = global.document.defaultView;
global.navigator = global.window.navigator;
global.history = global.window.history;
global.location = global.window.location;
global.btoa = global.window.btoa;
const $ = _$(global.window);

// set up local storage to test environment
global.window.localStorage = {
  getItem: key => global.window.localStorage[key],
  setItem: (key, value) => {
    global.window.localStorage[key] = value;
  },
};


const nodeWithIntlProp = node => React.cloneElement(node, { intl });

/**
 * uses TestUtils shallow render
 * helps to render a component one level deep only
 */
export const shallowWithIntl = node => shallow(nodeWithIntlProp(node), { context: { intl } });

/**
 * renders a component multi level deep.
 * useful for parent child components rendering
 * @param  {[type]} node [description]
 * @return {[type]}      [description]
 */
export const mountWithIntl = node => mount(nodeWithIntlProp(node), {
  context: { intl },
  childContextTypes: { intl: intlShape },
  attachTo: global.document.querySelector('#app'),
});

// set up chai jquery
// extension to the chai assertion library that provides a set of jQuery-specific assertions.
//chaiJquery(chai, chai.util, $);

// build render helper that should render a given react class
export const renderComponent = (ComponentClass, props = {}) => {
  const componentInstance = TestUtils.renderIntoDocument(
    <ComponentClass {...props} />
  );

  return $(ReactDOM.findDOMNode(componentInstance));
};

// helper for simulating events
// $.fn.simulate = function(eventName, value) {
//   if (value) {
//     this.val(value);
//   }
//   TestUtils.Simulate[eventName](this[0]);
// };

export { expect };
