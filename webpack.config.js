/* eslint-disable no-console */
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const config = require('./src/js/config/default.json');

// extract environment variables
const env = process.env.NODE_ENV;
const locale = process.env.LOCALE;
const distDir = process.env.DIST_DIR;
const version = process.env.SDK_VERSION;
const mainFilename = process.env.SDK_MAIN_FILENAME;
const vendorFilename = process.env.SDK_VENDOR_FILENAME;
const styleFilename = process.env.SDK_STYLE_FILENAME;
const watch = (process.env.SDK_WATCH === 'true' || false);
const uglifiedEnvironments = config.uglifiedEnvironments;

/**
 * isDependency() determines if a module
 * is a third party dependency
 *
 * @param userRequest
 * @returns {boolean}
 */
const isDependency = ({ userRequest }) => {
  if (typeof userRequest !== 'string') return false;

  return (
    userRequest.indexOf('bower_components') >= 0 ||
    userRequest.indexOf('node_modules') >= 0 ||
    userRequest.indexOf('libs') >= 0
  );
};

/**
 *
 * @return {[*,*,*]}
 */
const getPlugins = () => {
  // default webpack plugins
  const plugins = [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: vendorFilename,
      minChunks: isDependency,
    }),
    new ExtractTextPlugin(styleFilename, {
      allChunks: true,
    }),
    // define environment variables that
    // can be accessed in the frontend
    new webpack.DefinePlugin({
      'process.env': {
        LOCALE: JSON.stringify(locale),
        NODE_ENV: JSON.stringify(env),
        VERSION: JSON.stringify(version)
      },
    })
  ];

  // uglify plugin
  if ([uglifiedEnvironments].includes(env)) {
    console.log('webpack: including uglify plugin');
    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
        },
        mangle: true,
      })
    );
  }

  return plugins;
};

/**
 *
 */
const getDevtool = () => {
  if (env !== 'prod') return 'source-map';
  return null;
};

module.exports = {
  watch,
  plugins: getPlugins(),
  entry: './src/js/index',
  postcss: () => [autoprefixer],
  output: {
    path: path.join(__dirname, distDir),
    filename: mainFilename,
  },
  devtool: getDevtool(),
  // fix for mapbox fs dependency error
  node: { fs: 'empty' },
  module: {
    // fix for mapbox prebuilt js file warning
    noParse: /node_modules\/mapbox-gl\/dist\/mapbox-gl.js/,
    loaders: [
      {
        test: /\.json$/,
        loader: 'json',
      },
      {
        exclude: /node_modules/,
        test: /\.js$/,
        loader: 'babel',
        babelrc: true,
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css?-url'),
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract('style', 'css?-url!postcss!less'),
      },
    ],
  },
};
